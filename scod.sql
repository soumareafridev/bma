-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 03 fév. 2020 à 15:05
-- Version du serveur :  5.7.19
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `scod`
--

-- --------------------------------------------------------

--
-- Structure de la table `companies`
--

DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slogan` varchar(255) DEFAULT NULL,
  `description` text,
  `logo` varchar(255) DEFAULT NULL,
  `short_name` varchar(3) DEFAULT NULL,
  `one_word` varchar(40) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `theme_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_companies_themes1` (`theme_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `companies`
--

INSERT INTO `companies` (`id`, `slogan`, `description`, `logo`, `short_name`, `one_word`, `name`, `theme_id`) VALUES
(11, 'Nous faisons des applications surmesure.', '', NULL, 'DEV', 'Informatique', 'Afridev', 4),
(12, 'Nous faisons des applications surmesure.', NULL, NULL, 'DEV', 'Developpement', 'Afridev', 5),
(13, 'Compte administrateur', 'Compte pour suivre les différentes entreprises inscrites.', NULL, 'afd', 'Super Admin', 'Afridev Group', 1);

-- --------------------------------------------------------

--
-- Structure de la table `customer_types`
--

DROP TABLE IF EXISTS `customer_types`;
CREATE TABLE IF NOT EXISTS `customer_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_customer_types_companies1` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `distributions`
--

DROP TABLE IF EXISTS `distributions`;
CREATE TABLE IF NOT EXISTS `distributions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `poi_customer_id` int(11) DEFAULT NULL,
  `poi_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `distribution_statut_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orders_pois1_idx` (`poi_id`),
  KEY `fk_orders_customer_poi1` (`poi_customer_id`),
  KEY `fk_orders_users1` (`user_id`),
  KEY `fk_orders_companies1_idx` (`company_id`),
  KEY `fk_distributions_distribution_stattus1` (`distribution_statut_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `distributions`
--

INSERT INTO `distributions` (`id`, `amount`, `created`, `modified`, `poi_customer_id`, `poi_id`, `user_id`, `company_id`, `order_id`, `distribution_statut_id`) VALUES
(1, 70665, '2019-12-12 15:23:28', '2020-01-15 12:16:32', NULL, 1, 9, 11, 1, 2),
(3, 141330, '2019-12-16 16:43:21', '2019-12-16 17:22:24', NULL, 1, 9, 11, 2, 3);

-- --------------------------------------------------------

--
-- Structure de la table `distribution_lines`
--

DROP TABLE IF EXISTS `distribution_lines`;
CREATE TABLE IF NOT EXISTS `distribution_lines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taxe` float DEFAULT '18',
  `quantite` int(11) NOT NULL,
  `price_ht` float NOT NULL,
  `amount` float DEFAULT NULL,
  `amount_ht` float DEFAULT NULL,
  `distribution_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `order_line_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_lines_products1` (`product_id`),
  KEY `fk_distribution_lines_distributions1` (`distribution_id`),
  KEY `fk_distibution_lines_order_lines1` (`order_line_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `distribution_lines`
--

INSERT INTO `distribution_lines` (`id`, `taxe`, `quantite`, `price_ht`, `amount`, `amount_ht`, `distribution_id`, `product_id`, `order_line_id`) VALUES
(1, 18, 3, 23555, NULL, 70665, 1, 1, 1),
(3, 18, 2, 23555, NULL, 47110, 3, 1, 2),
(4, 18, 3, 23555, NULL, 70665, 3, 1, 3),
(5, 18, 1, 23555, NULL, 23555, 3, 1, 4);

-- --------------------------------------------------------

--
-- Structure de la table `distribution_statuts`
--

DROP TABLE IF EXISTS `distribution_statuts`;
CREATE TABLE IF NOT EXISTS `distribution_statuts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `distribution_statuts`
--

INSERT INTO `distribution_statuts` (`id`, `name`) VALUES
(1, 'En attente'),
(2, 'En cours'),
(3, 'Distribuée'),
(4, 'Annulée');

-- --------------------------------------------------------

--
-- Structure de la table `fields`
--

DROP TABLE IF EXISTS `fields`;
CREATE TABLE IF NOT EXISTS `fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `choices` text,
  `type_field_id` int(11) DEFAULT NULL,
  `form_id` int(11) DEFAULT NULL,
  `value_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_fields_forms1_idx` (`form_id`),
  KEY `fk_fields_type_fields1_idx` (`type_field_id`),
  KEY `fk_fields_values1_idx` (`value_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `fields`
--

INSERT INTO `fields` (`id`, `name`, `choices`, `type_field_id`, `form_id`, `value_id`) VALUES
(1, 'Nom', NULL, 1, 1, NULL),
(2, 'Prénom', NULL, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `forms`
--

DROP TABLE IF EXISTS `forms`;
CREATE TABLE IF NOT EXISTS `forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` int(1) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_forms_companies1_idx` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `forms`
--

INSERT INTO `forms` (`id`, `name`, `status`, `company_id`) VALUES
(1, 'Formulaire', 1, 11);

-- --------------------------------------------------------

--
-- Structure de la table `frequences`
--

DROP TABLE IF EXISTS `frequences`;
CREATE TABLE IF NOT EXISTS `frequences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `frequences`
--

INSERT INTO `frequences` (`id`, `name`) VALUES
(1, 'Journalière'),
(2, 'Hebdomadaire'),
(3, 'Mensuelle'),
(4, 'Trimestrielle');

-- --------------------------------------------------------

--
-- Structure de la table `gammes`
--

DROP TABLE IF EXISTS `gammes`;
CREATE TABLE IF NOT EXISTS `gammes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gammes_companies1_idx` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `gammes`
--

INSERT INTO `gammes` (`id`, `name`, `company_id`) VALUES
(1, 'TEST', 11);

-- --------------------------------------------------------

--
-- Structure de la table `gestions`
--

DROP TABLE IF EXISTS `gestions`;
CREATE TABLE IF NOT EXISTS `gestions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gestions_surveys1_idx` (`survey_id`),
  KEY `fk_gestions_teams1_idx` (`team_id`),
  KEY `fk_gestions_companies1_idx` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `gestions`
--

INSERT INTO `gestions` (`id`, `survey_id`, `team_id`, `company_id`) VALUES
(1, 1, 1, 11);

-- --------------------------------------------------------

--
-- Structure de la table `groupes`
--

DROP TABLE IF EXISTS `groupes`;
CREATE TABLE IF NOT EXISTS `groupes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_groupes_companies1` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_images_products1` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `images`
--

INSERT INTO `images` (`id`, `name`, `url`, `product_id`) VALUES
(1, 'hkmBRTI1Z6-1.png', '/img/produits/hkmBRTI1Z6-1.png', 1);

-- --------------------------------------------------------

--
-- Structure de la table `infos`
--

DROP TABLE IF EXISTS `infos`;
CREATE TABLE IF NOT EXISTS `infos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) NOT NULL,
  `value` text,
  `poi_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_infos_pois1_idx` (`poi_id`),
  KEY `fk_infos_fields1_idx` (`field_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `infos`
--

INSERT INTO `infos` (`id`, `field_id`, `value`, `poi_id`) VALUES
(3, 1, 'SOUM', 1),
(4, 2, 'SALLOU', 1);

-- --------------------------------------------------------

--
-- Structure de la table `members`
--

DROP TABLE IF EXISTS `members`;
CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_members_users1_idx` (`user_id`),
  KEY `fk_members_teams1_idx` (`team_id`),
  KEY `fk_members_companies1_idx` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `members`
--

INSERT INTO `members` (`id`, `user_id`, `team_id`, `company_id`) VALUES
(2, 7, 1, 11);

-- --------------------------------------------------------

--
-- Structure de la table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float NOT NULL,
  `order_statut_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `poi_customer_id` int(11) DEFAULT NULL,
  `poi_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0 - Commande 1-Distribution',
  `limited` tinyint(1) NOT NULL DEFAULT '0',
  `date_limited` datetime DEFAULT NULL,
  `date_debut` datetime DEFAULT NULL,
  `frequences` text,
  `frequence_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orders_pois1_idx` (`poi_id`),
  KEY `fk_orders_order_statuts1_idx` (`order_statut_id`),
  KEY `fk_orders_customer_poi1` (`poi_customer_id`),
  KEY `fk_orders_users1` (`user_id`),
  KEY `fk_orders_companies1_idx` (`company_id`),
  KEY `fk_orders_frquences1` (`frequence_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `orders`
--

INSERT INTO `orders` (`id`, `amount`, `order_statut_id`, `created`, `modified`, `poi_customer_id`, `poi_id`, `user_id`, `company_id`, `type`, `limited`, `date_limited`, `date_debut`, `frequences`, `frequence_id`) VALUES
(1, 117775, 2, '2019-12-09 16:00:36', '2019-12-09 16:27:54', NULL, 1, 7, 11, 1, 1, '2019-12-15 00:00:00', '2019-12-09 00:00:00', '1;2;3;4;5', 1),
(2, 188440, 2, '2019-12-16 16:15:12', '2019-12-16 16:15:12', NULL, 1, 7, 11, 1, 1, '2019-12-29 00:00:00', '2019-12-15 00:00:00', '1;2;3;4', 1),
(3, 70665, 2, '2020-01-14 16:22:09', '2020-01-15 12:04:17', NULL, 1, 7, 11, 0, 0, NULL, NULL, NULL, NULL),
(4, 70665, 2, '2020-01-14 16:23:49', '2020-01-14 16:23:49', NULL, 1, 7, 11, 1, 1, '2020-09-11 00:00:00', '2020-01-14 00:00:00', '3', 2),
(5, 0, 1, '2020-01-14 16:37:08', '2020-01-14 16:37:08', NULL, 1, 7, 11, 0, 0, NULL, NULL, NULL, NULL),
(6, 47110, 1, '2020-01-14 16:40:18', '2020-01-14 16:40:18', NULL, 1, 7, 11, 0, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `order_lines`
--

DROP TABLE IF EXISTS `order_lines`;
CREATE TABLE IF NOT EXISTS `order_lines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taxe` float DEFAULT '18',
  `quantite` int(11) NOT NULL,
  `quantite_max` int(11) DEFAULT NULL,
  `quantite_livre` int(11) DEFAULT NULL,
  `price_ht` float NOT NULL,
  `amount` float DEFAULT NULL,
  `amount_ht` float DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_lines_orders1_idx` (`order_id`),
  KEY `fk_order_lines_products1` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `order_lines`
--

INSERT INTO `order_lines` (`id`, `taxe`, `quantite`, `quantite_max`, `quantite_livre`, `price_ht`, `amount`, `amount_ht`, `order_id`, `product_id`) VALUES
(1, 18, 5, 20, 0, 23555, NULL, 117775, 1, 1),
(2, 18, 2, 20, -2, 23555, NULL, 47110, 2, 1),
(3, 18, 5, 20, -3, 23555, NULL, 117775, 2, 1),
(4, 18, 1, 20, -1, 23555, NULL, 23555, 2, 1),
(5, 18, 3, 10, 0, 23555, NULL, 70665, 3, 1),
(6, 18, 3, 10, 0, 23555, NULL, 70665, 4, 1),
(7, 18, 2, 10, 0, 23555, NULL, 47110, 6, 1);

-- --------------------------------------------------------

--
-- Structure de la table `order_statuts`
--

DROP TABLE IF EXISTS `order_statuts`;
CREATE TABLE IF NOT EXISTS `order_statuts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `order_statuts`
--

INSERT INTO `order_statuts` (`id`, `name`) VALUES
(1, 'En attente'),
(2, 'Confirmée'),
(3, 'Cloturée'),
(4, 'Annulée'),
(5, 'Suspendue');

-- --------------------------------------------------------

--
-- Structure de la table `pois`
--

DROP TABLE IF EXISTS `pois`;
CREATE TABLE IF NOT EXISTS `pois` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lng` float NOT NULL,
  `lat` float NOT NULL,
  `dns` varchar(255) DEFAULT NULL,
  `phone` varchar(25) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `type_poi_id` int(11) DEFAULT NULL,
  `survey_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `groupe_id` int(11) DEFAULT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pois_type_pois1_idx` (`type_poi_id`),
  KEY `fk_pois_surveys1_idx` (`survey_id`),
  KEY `fk_pois_users1_idx` (`user_id`),
  KEY `fk_pois_goupes1` (`groupe_id`),
  KEY `fk_pois_zones1` (`zone_id`),
  KEY `fk_pois_companies1_idx` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `pois`
--

INSERT INTO `pois` (`id`, `lng`, `lat`, `dns`, `phone`, `photo`, `status`, `type_poi_id`, `survey_id`, `user_id`, `groupe_id`, `zone_id`, `created`, `company_id`) VALUES
(1, -17.4441, 14.7241, 'Bureau Castor', '7790990090', NULL, 1, 1, 1, 7, NULL, 1, '2019-11-25 13:35:09', 11);

-- --------------------------------------------------------

--
-- Structure de la table `poi_customers`
--

DROP TABLE IF EXISTS `poi_customers`;
CREATE TABLE IF NOT EXISTS `poi_customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fn` varchar(255) NOT NULL,
  `ln` varchar(255) NOT NULL,
  `phone` varchar(24) DEFAULT NULL,
  `customer_type_id` int(11) DEFAULT NULL,
  `poi_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_poi_customers_customer_types1` (`customer_type_id`),
  KEY `fk_poi_customers_pois1` (`poi_id`),
  KEY `fk_poi_customers_companies1_idx` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `pack` varchar(255) NOT NULL,
  `poids_net` float NOT NULL,
  `poids_brut` float NOT NULL,
  `description` text,
  `price_ht` float DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1 active 2inactive 3supprimé',
  `stock` int(11) DEFAULT NULL,
  `gamme_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_products_gammes1` (`gamme_id`),
  KEY `fk_products_companies1_idx` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `products`
--

INSERT INTO `products` (`id`, `code`, `name`, `pack`, `poids_net`, `poids_brut`, `description`, `price_ht`, `status`, `stock`, `gamme_id`, `created`, `modified`, `company_id`) VALUES
(1, 'FA556', 'Produit', 'ESSAI', 23, 44, '', 23555, 1, NULL, 1, '2019-11-25 12:50:54', '2019-11-25 12:50:54', 11);

-- --------------------------------------------------------

--
-- Structure de la table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `profiles`
--

INSERT INTO `profiles` (`id`, `name`) VALUES
(1, 'Enqueteur'),
(2, 'Commercial'),
(3, 'Responsable commercial'),
(4, 'Administrateur'),
(5, 'Distributeur'),
(6, 'Super Administrateur');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_roles_users_idx` (`user_id`),
  KEY `fk_roles_profiles1_idx` (`profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `user_id`, `profile_id`) VALUES
(4, 7, 4),
(5, 7, 1),
(6, 7, 2),
(7, 7, 3),
(8, 8, 4),
(9, 9, 5),
(10, 10, 6);

-- --------------------------------------------------------

--
-- Structure de la table `statuts`
--

DROP TABLE IF EXISTS `statuts`;
CREATE TABLE IF NOT EXISTS `statuts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statuts_companies1_idx` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `surveys`
--

DROP TABLE IF EXISTS `surveys`;
CREATE TABLE IF NOT EXISTS `surveys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type_poi_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_surveys_type_pois1_idx` (`type_poi_id`),
  KEY `fk_surveys_forms1_idx` (`form_id`),
  KEY `fk_surveys_companies1_idx` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `surveys`
--

INSERT INTO `surveys` (`id`, `name`, `type_poi_id`, `form_id`, `company_id`) VALUES
(1, 'Test enquete', 1, 1, 11);

-- --------------------------------------------------------

--
-- Structure de la table `teams`
--

DROP TABLE IF EXISTS `teams`;
CREATE TABLE IF NOT EXISTS `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_teams_companies1_idx` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `teams`
--

INSERT INTO `teams` (`id`, `name`, `company_id`) VALUES
(1, 'Informatique', 11);

-- --------------------------------------------------------

--
-- Structure de la table `themes`
--

DROP TABLE IF EXISTS `themes`;
CREATE TABLE IF NOT EXISTS `themes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `css` varchar(255) NOT NULL DEFAULT 'theme-default.css',
  `image` varchar(255) NOT NULL DEFAULT 'theme-default.png',
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `themes`
--

INSERT INTO `themes` (`id`, `css`, `image`, `name`) VALUES
(1, 'theme-default.css', 'theme-default.png', 'Le thème par défaut'),
(2, 'theme-bleurose.css', 'theme-bleurose.png', 'Le thème bleu et rose'),
(3, 'theme-bleusombre.css', 'theme-bleusombre.png', 'Le thème bleu sombre'),
(4, 'theme-pink.css', 'theme-pink.png', 'Le thème rose'),
(5, 'theme-marron.css', 'theme-marron.png', 'Le thème marron');

-- --------------------------------------------------------

--
-- Structure de la table `type_fields`
--

DROP TABLE IF EXISTS `type_fields`;
CREATE TABLE IF NOT EXISTS `type_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `type_fields`
--

INSERT INTO `type_fields` (`id`, `name`, `type`) VALUES
(1, 'varchar', 'text'),
(2, 'int', 'number'),
(3, 'float', 'number'),
(4, 'checkbox', 'checkbox'),
(5, 'radio', 'radio'),
(6, 'text', 'textarea'),
(7, 'date', 'date'),
(8, 'datetime', 'datetime'),
(9, 'liste', 'text');

-- --------------------------------------------------------

--
-- Structure de la table `type_pois`
--

DROP TABLE IF EXISTS `type_pois`;
CREATE TABLE IF NOT EXISTS `type_pois` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_type_pois_companies1_idx` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `type_pois`
--

INSERT INTO `type_pois` (`id`, `name`, `company_id`) VALUES
(1, 'Maison', 11),
(2, 'Grossiste', 11);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fn` varchar(255) NOT NULL,
  `ln` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(24) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users_companies1` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `fn`, `ln`, `email`, `password`, `phone`, `token`, `active`, `company_id`) VALUES
(7, 'Abdalaye', 'SOUMARE', 'test@pdv.com', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', '779008402', 'cc00f2f6bed8b0936a4b27312fe3b21a', 1, 11),
(8, 'Macoumba', 'FALL', 'mfall@afridev-group.com', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', '779008402', '365161aa2526af328093cc4fb1977db0', 1, 12),
(9, 'Abdalaye', 'SENE', 'distributeur@pdv.com', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', '772002020', NULL, 0, 11),
(10, 'Macoumba', 'FALL', 'technique@afridev-group.com', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', '772782829', NULL, 1, 13);

-- --------------------------------------------------------

--
-- Structure de la table `values`
--

DROP TABLE IF EXISTS `values`;
CREATE TABLE IF NOT EXISTS `values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `zones`
--

DROP TABLE IF EXISTS `zones`;
CREATE TABLE IF NOT EXISTS `zones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `zones`
--

INSERT INTO `zones` (`id`, `name`, `code`) VALUES
(1, 'Dakar', 'SN-DK'),
(2, 'Diourbel', 'SN-DB'),
(3, 'Fatick', 'SN-FK'),
(4, 'Kaolack', 'SN-KL'),
(5, 'Kolda', 'SN-KD'),
(6, 'Louga', 'SN-LG'),
(7, 'Matam', 'SN-MT'),
(8, 'Saint-Louis', 'SN-SL'),
(9, 'Tambacounda', 'SN-TC'),
(10, 'Thiès', 'SN-TH'),
(11, 'Ziguinchor', 'SN-ZG'),
(12, 'Kaffrine', 'SN-KA'),
(13, 'Kédougou', 'SN-KE'),
(14, 'Sédhiou', 'SN-SE');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `companies`
--
ALTER TABLE `companies`
  ADD CONSTRAINT `fk_companies_themes1` FOREIGN KEY (`theme_id`) REFERENCES `themes` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `customer_types`
--
ALTER TABLE `customer_types`
  ADD CONSTRAINT `fk_customer_types_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `distributions`
--
ALTER TABLE `distributions`
  ADD CONSTRAINT `fk_distributions_distribution_stattus1` FOREIGN KEY (`distribution_statut_id`) REFERENCES `distribution_statuts` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `distribution_lines`
--
ALTER TABLE `distribution_lines`
  ADD CONSTRAINT `fk_distibution_lines_order_lines1` FOREIGN KEY (`order_line_id`) REFERENCES `order_lines` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_distribution_lines_distributions1` FOREIGN KEY (`distribution_id`) REFERENCES `distributions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `fields`
--
ALTER TABLE `fields`
  ADD CONSTRAINT `fk_fields_forms1` FOREIGN KEY (`form_id`) REFERENCES `forms` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_fields_type_fields1` FOREIGN KEY (`type_field_id`) REFERENCES `type_fields` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_fields_values1` FOREIGN KEY (`value_id`) REFERENCES `values` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `forms`
--
ALTER TABLE `forms`
  ADD CONSTRAINT `fk_forms_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `gammes`
--
ALTER TABLE `gammes`
  ADD CONSTRAINT `fk_gammes_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `gestions`
--
ALTER TABLE `gestions`
  ADD CONSTRAINT `fk_gestions_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_gestions_surveys1` FOREIGN KEY (`survey_id`) REFERENCES `surveys` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_gestions_teams1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `groupes`
--
ALTER TABLE `groupes`
  ADD CONSTRAINT `fk_groupes_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `fk_images_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `infos`
--
ALTER TABLE `infos`
  ADD CONSTRAINT `fk_infos_fields1` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_infos_pois1` FOREIGN KEY (`poi_id`) REFERENCES `pois` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `members`
--
ALTER TABLE `members`
  ADD CONSTRAINT `fk_members_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_members_teams1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_members_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `fk_orders_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_orders_frquences1` FOREIGN KEY (`frequence_id`) REFERENCES `frequences` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_orders_poi_customers1` FOREIGN KEY (`poi_customer_id`) REFERENCES `poi_customers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_orders_pois1` FOREIGN KEY (`poi_id`) REFERENCES `pois` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_orders_statuts1` FOREIGN KEY (`order_statut_id`) REFERENCES `order_statuts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_orders_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `order_lines`
--
ALTER TABLE `order_lines`
  ADD CONSTRAINT `fk_order_lines_orders1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_order_lines_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pois`
--
ALTER TABLE `pois`
  ADD CONSTRAINT `fk_pois_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pois_goupes1` FOREIGN KEY (`groupe_id`) REFERENCES `groupes` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pois_surveys1` FOREIGN KEY (`survey_id`) REFERENCES `surveys` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pois_surveys2` FOREIGN KEY (`survey_id`) REFERENCES `surveys` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pois_type_pois1` FOREIGN KEY (`type_poi_id`) REFERENCES `type_pois` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pois_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pois_zones1` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `poi_customers`
--
ALTER TABLE `poi_customers`
  ADD CONSTRAINT `fk_poi_customers_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_poi_customers_customer_types1` FOREIGN KEY (`customer_type_id`) REFERENCES `customer_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_poi_customers_pois1` FOREIGN KEY (`poi_id`) REFERENCES `pois` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_products_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_products_gammes1` FOREIGN KEY (`gamme_id`) REFERENCES `gammes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `fk_roles_profiles1` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_roles_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `statuts`
--
ALTER TABLE `statuts`
  ADD CONSTRAINT `fk_statuts_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `surveys`
--
ALTER TABLE `surveys`
  ADD CONSTRAINT `fk_surveys_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_surveys_forms1` FOREIGN KEY (`form_id`) REFERENCES `forms` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_surveys_type_pois1` FOREIGN KEY (`type_poi_id`) REFERENCES `type_pois` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `teams`
--
ALTER TABLE `teams`
  ADD CONSTRAINT `fk_teams_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `type_pois`
--
ALTER TABLE `type_pois`
  ADD CONSTRAINT `fk_type_pois_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
