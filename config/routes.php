<?php

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::prefix('admin', function ($routes) {
    $routes->connect('/', ['controller' => 'Dashbord', 'action' => 'index']);

    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('commercial', function ($routes) {
    $routes->connect('/', ['controller' => 'Dashbord', 'action' => 'index']);

    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('responsable', function ($routes) {
    $routes->connect('/', ['controller' => 'Dashbord', 'action' => 'index']);

    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('enqueteur', function ($routes) {
    $routes->connect('/', ['controller' => 'Dashbord', 'action' => 'index']);

    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('super', function ($routes) {
    $routes->connect('/', ['controller' => 'Dashbord', 'action' => 'index']);

    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('auth', function($routes){
    $routes->setExtensions(['json', 'xml']);
    $routes->resources('Users');
    $routes->fallbacks(DashedRoute::class);
});

Router::prefix('api', function($routes){
    $routes->setExtensions(['json', 'xml']);
    $routes->resources('Users');
    $routes->resources('Distributions');
    $routes->fallbacks(DashedRoute::class);
});

Router::scope('/', function (RouteBuilder $routes) {
    // Register scoped middleware for in scopes.
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true
    ]));

    /**
     * Apply a middleware to the current route scope.
     * Requires middleware to be registered via `Application::routes()` with `registerMiddleware()`
     */

    //$routes->applyMiddleware('csrf');

    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    $routes->connect('/', ['controller' => 'Users', 'action' => 'login', 'login']);
    $routes->connect('/inscription', ['controller' => 'Pages', 'action' => 'register']);

    $routes->connect(
        '/cartographie',
        ['controller' => 'Pois', 'action' => 'cartographie']
    );

    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *
     * ```
     * $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);
     * $routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);
     * ```
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});

/**
 * If you need a different set of middleware or none at all,
 * open new scope and define routes there.
 *
 * ```
 * Router::scope('/api', function (RouteBuilder $routes) {
 *     // No $routes->applyMiddleware() here.
 *     // Connect API actions here.
 * });
 * ```
 */
