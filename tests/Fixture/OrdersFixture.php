<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * OrdersFixture
 */
class OrdersFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'amount' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'order_statut_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'poi_customer_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'poi_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'company_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'type' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '0 - Commande 1-Distribution', 'precision' => null],
        'limited' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'date_limited' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'date_debut' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'frequences' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'frequence_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_orders_pois1_idx' => ['type' => 'index', 'columns' => ['poi_id'], 'length' => []],
            'fk_orders_order_statuts1_idx' => ['type' => 'index', 'columns' => ['order_statut_id'], 'length' => []],
            'fk_orders_customer_poi1' => ['type' => 'index', 'columns' => ['poi_customer_id'], 'length' => []],
            'fk_orders_users1' => ['type' => 'index', 'columns' => ['user_id'], 'length' => []],
            'fk_orders_companies1_idx' => ['type' => 'index', 'columns' => ['company_id'], 'length' => []],
            'fk_orders_frquences1' => ['type' => 'index', 'columns' => ['frequence_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_orders_companies1' => ['type' => 'foreign', 'columns' => ['company_id'], 'references' => ['companies', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'fk_orders_frquences1' => ['type' => 'foreign', 'columns' => ['frequence_id'], 'references' => ['frequences', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
            'fk_orders_poi_customers1' => ['type' => 'foreign', 'columns' => ['poi_customer_id'], 'references' => ['poi_customers', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_orders_pois1' => ['type' => 'foreign', 'columns' => ['poi_id'], 'references' => ['pois', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_orders_statuts1' => ['type' => 'foreign', 'columns' => ['order_statut_id'], 'references' => ['order_statuts', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_orders_users1' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'amount' => 1,
                'order_statut_id' => 1,
                'created' => '2019-12-03 11:31:00',
                'modified' => '2019-12-03 11:31:00',
                'poi_customer_id' => 1,
                'poi_id' => 1,
                'user_id' => 1,
                'company_id' => 1,
                'type' => 1,
                'limited' => 1,
                'date_limited' => '2019-12-03 11:31:00',
                'date_debut' => '2019-12-03 11:31:00',
                'frequences' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'frequence_id' => 1
            ],
        ];
        parent::init();
    }
}
