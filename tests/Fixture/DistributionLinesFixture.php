<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DistributionLinesFixture
 */
class DistributionLinesFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'taxe' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => '18', 'comment' => ''],
        'quantite' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'price_ht' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'amount' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'amount_ht' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'distribution_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'order_line_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_order_lines_products1' => ['type' => 'index', 'columns' => ['product_id'], 'length' => []],
            'fk_distribution_lines_distributions1' => ['type' => 'index', 'columns' => ['distribution_id'], 'length' => []],
            'fk_distibution_lines_order_lines1' => ['type' => 'index', 'columns' => ['order_line_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_distibution_lines_order_lines1' => ['type' => 'foreign', 'columns' => ['order_line_id'], 'references' => ['order_lines', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
            'fk_distribution_lines_distributions1' => ['type' => 'foreign', 'columns' => ['distribution_id'], 'references' => ['distributions', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'taxe' => 1,
                'quantite' => 1,
                'price_ht' => 1,
                'amount' => 1,
                'amount_ht' => 1,
                'distribution_id' => 1,
                'product_id' => 1,
                'order_line_id' => 1
            ],
        ];
        parent::init();
    }
}
