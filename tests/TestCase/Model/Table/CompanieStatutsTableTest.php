<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CompanieStatutsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CompanieStatutsTable Test Case
 */
class CompanieStatutsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CompanieStatutsTable
     */
    public $CompanieStatuts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CompanieStatuts',
        'app.Companies'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CompanieStatuts') ? [] : ['className' => CompanieStatutsTable::class];
        $this->CompanieStatuts = TableRegistry::getTableLocator()->get('CompanieStatuts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CompanieStatuts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
