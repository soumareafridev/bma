<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GestionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GestionsTable Test Case
 */
class GestionsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\GestionsTable
     */
    public $Gestions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Gestions',
        'app.Surveys',
        'app.Teams',
        'app.Companies'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Gestions') ? [] : ['className' => GestionsTable::class];
        $this->Gestions = TableRegistry::getTableLocator()->get('Gestions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Gestions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
