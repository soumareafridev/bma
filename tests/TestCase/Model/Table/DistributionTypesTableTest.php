<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DistributionTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DistributionTypesTable Test Case
 */
class DistributionTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DistributionTypesTable
     */
    public $DistributionTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.DistributionTypes',
        'app.Distributions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DistributionTypes') ? [] : ['className' => DistributionTypesTable::class];
        $this->DistributionTypes = TableRegistry::getTableLocator()->get('DistributionTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DistributionTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
