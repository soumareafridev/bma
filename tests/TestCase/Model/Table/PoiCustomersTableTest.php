<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PoiCustomersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PoiCustomersTable Test Case
 */
class PoiCustomersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PoiCustomersTable
     */
    public $PoiCustomers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PoiCustomers',
        'app.CustomerTypes',
        'app.Pois',
        'app.Companies',
        'app.Orders'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PoiCustomers') ? [] : ['className' => PoiCustomersTable::class];
        $this->PoiCustomers = TableRegistry::getTableLocator()->get('PoiCustomers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PoiCustomers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
