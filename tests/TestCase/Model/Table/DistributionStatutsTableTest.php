<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DistributionStatutsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DistributionStatutsTable Test Case
 */
class DistributionStatutsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DistributionStatutsTable
     */
    public $DistributionStatuts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.DistributionStatuts',
        'app.Distributions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DistributionStatuts') ? [] : ['className' => DistributionStatutsTable::class];
        $this->DistributionStatuts = TableRegistry::getTableLocator()->get('DistributionStatuts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DistributionStatuts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
