<?php $this->layout = 'auth';
$this->assign('title', ' Envoi d\'un email d\'activation') ?>

<div class="col-md-12 bg-white text-primary text-center py-1" style="z-index: 1;box-shadow: 0 1px 3px -1px rgba(0,0,0,.5);">
    <h2 class="bold m-0 d-sm-flex align-items-center justify-content-around">
        <img src="<?= $this->Url->image('logo.png') ?>" alt="logo" style="max-height: 50px;margin-bottom: 8px">
        <span>Mot de passe oublié ?</span>
    </h2>

</div>
<div class="col-md-12 bg-white mt-5 p-0 d-sm-flex align-items-center justify-content-center">
    <div class="text-center cont-form" style="min-width: 500px">

        <?= $this->Flash->render() ?>

        <p class="mb-2 text-dark"><strong>Entrez votre adresse email</strong></p>
        <form class="bg_grisdark login" method="post" action="<?= $this->Url->Build(['action' => 'forgotpassword']) ?>">
            <div class="container">
                <div class="form-group bg_grisdark mt-3 mb-4">
                    <label class="text-white text-left text-uppercase">Email</label>
                    <input class="form-control-gris pl-2" type="email" required name="email" placeholder="votre email" />
                    <i class="fas fa-envelope"></i>
                </div>

                <button class="btn-rose bg-rose mb-3" type="submit">
                    Valider
                    <i class="fas fa-arrow-right ml-2"></i>
                </button>
            </div>
        </form>
    </div>
</div>
