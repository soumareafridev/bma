<?php $this->layout = 'auth';
$this->assign('title', ' Envoi d\'un email d\'activation') ?>

<div class="col-md-12 bg-white text-primary text-center py-1" style="z-index: 1;box-shadow: 0 1px 3px -1px rgba(0,0,0,.5);">
    <h2 class="bold m-0 d-sm-flex align-items-center justify-content-around">
        <img src="<?= $this->Url->image('logo.png') ?>" alt="logo" style="max-height: 50px;margin-bottom: 8px">
        <span>Activation de compte</span>
    </h2>

</div>
<div class="col-md-12 p-0 pt-2 d-sm-flex bg-white align-items-center justify-content-center" style="min-height: calc(100vh - (66px));">
    <div class="px-1 text-center">
        <?php if($success) : ?> 
        <div>
            <img src="<?= $this->Url->image('icons/okok.gif') ?>" alt="Enregistrement réussi" style="max-height: 200px;">
        </div>
        <h3>Activation réussie</h3>
        <?php else:  ?>
        <h3>
            <i class="fas fa-times-circle fa-2x text-danger"></i> <br> Désolé, mais 
        </h3>
        <?php endif; ?>
        <p style="width: 75%;margin-left: auto;margin-right: auto">
            <?= $alert ?>
        </p>
        <div>
        <a href="<?= $this->Url->build(["controller" => 'users', 'action' => 'login']) ?>" class="btn mt-4 shadow-sm bg-rose text-white">Se connecter <i class="fas fa-long-arrow-alt-right ml-3" ></i></a>
        </div>
    </div>
</div>