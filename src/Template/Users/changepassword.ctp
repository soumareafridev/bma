<?php $this->layout = 'auth';
$this->assign('title', ' Envoi d\'un email d\'activation') ?>

<div class="col-md-12 bg-white text-primary text-center py-1" style="z-index: 1;box-shadow: 0 1px 3px -1px rgba(0,0,0,.5);">
    <h2 class="bold m-0 d-sm-flex align-items-center justify-content-around">
        <img src="<?= $this->Url->image('logo.png') ?>" alt="logo" style="max-height: 50px;margin-bottom: 8px">
        <span>Réinitialisation mot de passe</span>
    </h2>

</div>
<div class="col-md-12 bg-white mt-5 p-0 d-sm-flex align-items-center justify-content-center">
    <div class="text-center cont-form" style="min-width: 500px">

        <?= $this->Flash->render() ?>
        <div class="alert alert-danger hidden" id="alert"></div>
        <p class="mb-2 text-dark"><strong>Changez votre mot de passe</strong></p>
        <form class="bg_grisdark login" id="formulaire" method="post" action="<?= $this->Url->Build(['action' => 'changepassword',$cle]) ?>">
            <div class="container">
                <div class="form-group bg_grisdark mt-3 mb-4">
                    <label class="text-white text-left text-uppercase">Nouveau mot de passse</label>
                    <input class="form-control-gris pl-2" type="password" id="password" required name="password" placeholder="nouveau mot de passe" />
                    <i class="fas fa-lock"></i>
                </div>
                <div class="form-group bg_grisdark mt-3 mb-4">
                    <label class="text-white text-left text-uppercase">Répetez mot de passse</label>
                    <input class="form-control-gris pl-2" type="password" id="repassword" required  placeholder="repetez mot de passe" />
                    <i class="fas fa-lock"></i>
                </div>

                <button class="btn-rose bg-rose mb-3" type="submit">
                    Valider
                    <i class="fas fa-arrow-right ml-2"></i>
                </button>
            </div>
        </form>
    </div>
</div>
<?php $this->start('script_bottom'); ?>
    <script>
        $(function() {
            $('#formulaire').submit(function() {
                if($('#password').val() !== $('#repassword').val()) {
                    $('#alert').text('Vos deux mots ne sont pas identiques !').removeClass('hidden').fadeIn();
                    return false;
                }

            });
        });
    </script>
<?php $this->end(); ?>
