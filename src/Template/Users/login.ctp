<?php $this->layout = 'auth'; $this->assign('title','Authentification') ?>

<div class="col-md-12 bg-white min-vh-100 p-0 d-sm-flex align-items-center justify-content-center">
    <div class="text-center cont-form" style="min-width: 300px">
        <div class="entete mb-1">
            <img src="<?= $this->Url->image('logo.png') ?>" alt="logo">
        </div>

        <?= $this->Flash->render() ?>

        <p class="mb-2 text-dark"><strong>Se connecter</strong></p>
        <form class="bg_grisdark login" method="post" action="<?= $this->Url->Build(['action' => 'login']) ?>">
            <div class="container">
                <div class="form-group bg_grisdark mt-3 mb-4">
                    <label class="text-white text-left text-uppercase">identifiant</label>
                    <input class="form-control-gris pl-2" type="text" required name="phone" placeholder="Téléphone/Email" />
                    <i class="fas fa-envelope"></i>
                </div>

                <div class="form-group bg_grisdark mb-4">
                    <label class="text-white text-left text-uppercase">mot de passe</label>
                    <input type="password" class="form-control-gris pl-2" name="password" required placeholder="Votre mot de passe" />
                    <i class="fas fa-lock"></i>
                    <?php if (isset($redirect)) : ?>
                        <input type="hidden" name="redirect" value="<?= $redirect ?>">
                    <?php endif; ?>
                </div>

                <button class="btn-rose bg-rose mb-3" type="submit">
                    Se connecter
                    <i class="fas fa-arrow-right ml-2"></i>
                </button>
            </div>
        </form>
        <!-- <hr class="mt-5"> -->
        <div class="m-5 text-center">
            <span class="gris bold">Pas encore de compte ?
                <a href="/inscription" class="text-rose">Inscrivez-vous</a>
            </span><br>
            <a class="gris bold" href="<?= $this->Url->build(['action' => 'forgotpassword']) ?>">Mot de passe oublié ?</a>
            <p class="mt-4"><small class="gris text-md-light">SCOD - v0.0.1</small></p>
        </div>
    </div>
</div>
