<?php $this->layout = "auth"; ?>

<div class="container my-5">
    <?= $this->Flash->render() ?>
    <div class="card">
        <div class="card-body shadow-sm">
            <h3 class="card-title text-primary"><?= $survey->name ?></h3>
            <hr>
            <?php if(isset($survey) && $survey) :  ?>
            <form class="col-md-12" id="form" method="post" action="">
                <div class="row">
                    <?php if ($survey && isset($survey->form) && count($survey->form->fields) > 0) : ?>
                        <div class="col-md-6 col-sm-12 mb-2">
                            <?php foreach ($survey->form->fields as $field) : ?>
                                <div class="form-group">
                                    <label for="info<?= $field->id ?>"> <?= $field->name ?> </label>
                                    <?php if ($field->type_field_id !== 5 && $field->type_field_id !== 4 && $field->type_field_id !== 7 && $field->type_field_id !== 8) : ?>
                                        <input type="<?= $field->type_field->type ?>" ngModel name="info<?= $field->id ?>" class="form-control form-control-sm">
                                    <?php elseif ($field->type_field_id == 5 || $field->type_field_id == 4) : $choices = explode(';', $field->choices) ?>
                                        <select name="info<?= $field->id ?>" ngModel class="form-control form-control-sm">
                                            <option value default disabled>Selectionner votre choix</option>
                                            <?php foreach ($choices as $choice) : ?>
                                                <option value="<?= $choice ?>"><?= $choice ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    <?php elseif ($field->type_field_id == 7 || $field->type_field_id == 8) : ?>
                                        <input type="date" name="info<?= $field->id ?>" id="info<?= $field->id ?>">
                                    <?php endif; ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label>Votre nom complet</label>
                                <input type="text" name="dns" required class="form-control form-control-sm">
                                <input type="hidden" name="type_poi_id" value="<?= $survey->type_poi_id ?>">
                            </div>
                            <div class="form-group">
                                <label>Numéro de téléphone*</label>
                                <input type="number" name="phone" required ngModel class="form-control form-control-sm">
                            </div>

                            <div class="form-group">
                                <label>Coordonnées (Latitude,Longitude)*</label>
                                <div class="input-group">
                                    <input type="number" step="any" name="lat" id="lat" required class="form-control form-control-sm" placeholder="Latitude">
                                    <input type="number" step="any" name="lng" id="lng" required class="form-control form-control-sm" placeholder="Longitude">
                                </div>
                            </div>
                            <div id="map" style="min-height: calc(100vh - (400px))"></div>
                        </div>
                        <div class="col-md-12 p-3">
                            <button class="btn btn-primary btn-block" type="submit">
                                Soumettre mon formulaire
                            </button>
                        </div>
                    <?php else : ?>
                        <div class="col-md-12">
                            <div class="jumbotron text-center">
                                <div class="h5">
                                    Veuillez séléctionner une enquête de votre choix
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </form>
            <?php else:  ?>
                <div class="col-md-12">
                    <div class="jumbotron text-center">
                        <div class="h5">
                            Aucune enquête disponible !
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>

</div>

<?php $this->start('script_bottom'); ?>
<script>
    var map;
    var marker;
    var lattitude = 14.7645042;
    var longitude = -17.366028599999936;

    function initMap2() {
        var map = new google.maps.Map(
            document.getElementById('map'), {
                center: {
                    lat: lattitude,
                    lng: longitude
                },
                zoom: 12,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
        );
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(lattitude, longitude),
            map: map,
            draggable: true
        });
        var geocoder = new google.maps.Geocoder();

        google.maps.event.addListener(marker, 'dragend', function(event) {
            $('#lat').val(event.latLng.lat());
            $('#lng').val(event.latLng.lng());
            reverseGeocode(geocoder, event.latLng);
        });

        var input = document.getElementById('address');
        var options = {
            types: ['(cities)'],
            componentRestrictions: {
                country: 'sn'
            }
        };
        // var autocomplete = new google.maps.places.Autocomplete(
        //         input, options);
        var autocomplete = new google.maps.places.Autocomplete(
            input, {
                placeIdOnly: true
            });
        autocomplete.bindTo('bounds', map);

        // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();

            if (!place.place_id) {
                return;
            }

            geocodeAddress(geocoder, map);
            return false;

        });
    }

    function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
        geocoder.geocode({
            'address': address
        }, function(results, status) {
            if (status === 'OK') {
                var pos = results[0].geometry.location;
                resultsMap.setZoom(14);
                resultsMap.setCenter(pos);
                if (marker)
                    marker.setMap(null);
                marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location,
                    draggable: true
                });
                var components = results[0].address_components
                google.maps.event.addListener(marker, "dragend", function(event) {
                    document.getElementById('lat').value = marker.getPosition().lat();
                    document.getElementById('lng').value = marker.getPosition().lng();
                    reverseGeocode(geocoder, event.latLng);
                });
                document.getElementById('lat').value = marker.getPosition().lat();
                document.getElementById('lng').value = marker.getPosition().lng();

                getVillePays(components);

            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    function reverseGeocode(geocoder, latlng) {
        // debugger;

        geocoder.geocode({
            'latLng': latlng
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var components = results[1].address_components
                getVillePays(components);
                if (results[1]) {
                    setLocation(results[1]);
                }
            } else {
                alert("Geocoder failed due to: " + status);
            }
        });
    }

    function getVillePays(components) {
        for (var i = 0; i < components.length; i++) {
            var addressType = components[i].types[0];

            if (addressType == "administrative_area_level_1") {
                itemVille = components[i].short_name;
                document.getElementById('city').value = itemVille;
            }

            if (addressType == "country") {
                itemCountry = components[i].long_name;
                document.getElementById('country').value = itemCountry;
            }
        }
    }

    function setLocation(address) {
        $('#address').val(address.formatted_address);
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5z3yow8LqTpw3WlqBmWpvYYPl1i3mtLM&libraries=places&callback=initMap2"></script>
<?php $this->end(); ?>
