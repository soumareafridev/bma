<?php $this->layout = 'map'; ?>
    <div class="col-md-12 p-0" style="height: 100%;" id="map"></div>
    <div class="hidden">
    <?php if(sizeof($pois) > 0): for ($i=0; $i <sizeof($pois) ; $i++): ?>
        <span class="list-group-item d-sm-flex align-items-center justify-content-between marker" id="marker<?= $i ?>" data-created="<?= $pois[$i]->created->nice('Africa/Dakar','fr-Fr') ?>" data-pin="<?= $this->Url->image('md-pin.png') ?>"  data-id="<?= $pois[$i]->id ?>" data-dns="<?= $pois[$i]->dns ?>" data-lng="<?= $pois[$i]->lng ?>" data-lat="<?= $pois[$i]->lat ?>" data-img="<?= $pois[$i]->photo !== null ? $this->Url->image('pois/'.$pois[$i]->photo) : null ?>"></span>
    <?php endfor; endif; ?>
    </div>
    <div class="panel-filtre panel-filtre-open" id="panel">
        <input type="hidden" name="icon" id="icon" value="<?= $this->Url->image('md-pin.png') ?>">
        <i class="fas fa-angle-double-left i-action i-close"></i>
        <h5 class="h5 text-primary text-center pt-3 pb-1 mr-3 text-uppercase bold border-bottom-primary"><i class="fas fa-filter"></i> Les filtres</h5>
        <div class="row m-0">
            <div class="col-md-12">
                <h6 class="h6 bold">TOTAL</h6>
                <label class="label-radio-blue" style="font-size: 12px"><input type="checkbox" checked disabled name="pois" id=""><span>Maison <span class="badge bg-light text-primary"><?= count($pois) ?></span></span></label>
                <label class="label-radio-blue" style="font-size: 12px"><input type="checkbox" checked disabled name="pois" id=""><span>Maison <span class="badge bg-light text-primary">0</span></span></label>
                <label class="label-radio-pink" style="font-size: 12px"><input type="checkbox" checked disabled name="pois" id=""><span><i class="fas fa-eye mr-1"></i><span class="badge bg-light" id="filtres" style="color: #e83e8c"><?= count($pois) ?></span></span></label>
            </div>
            <hr class="col-md-11 mt-2 mb-2">
        </div>
        <div class="row m-0 labels">
            <div class="col-md-12">
                <h6 class="h6 bold">Type de POI</h6>
                <label class="label-radio-blue" for="poi1"><input type="checkbox" name="type_poi" checked data-id="1" id="poi1"><span>Maison</span></label>
                <label class="label-radio-blue" for="poi2"><input type="checkbox" name="type_poi" checked data-id="2" id="poi2"><span>Maison</span></label>
            </div>
            <hr class="col-md-11 mt-2 mb-2">
            <div class="col-md-12">
                <h6 class="h6 bold">Les zones</h6>
                <?php foreach($zones as $zone): ?>
                <label class="label-radio-gray" for="zone<?= $zone->id ?>"><input type="checkbox" checked name="zone_id" data-id="<?= $zone->id ?>" id="zone<?= $zone->id ?>"><span><?= $zone->name ?></span></label>
                <?php endforeach; ?>
            </div>
            <hr class="col-md-11 mt-2 mb-2">
            <div class="col-md-12">
                <h6 class="h6 bold">Les goupes</h6>
                <?php if(count($groupes) > 0): foreach($groupes as $groupe): ?>
                <label class="label-radio-pink" for="groupe<?= $groupe->id ?>"><input type="checkbox" checked name="groupe_id" data-id="<?= $groupe->id ?>" id="groupe<?= $groupe->id ?>"><span><?= $groupe->name ?></span></label>
                <?php endforeach; else:?>
                <div class="text-gray-500">Aucun groupe créé</div>
                <?php endif;?>
            </div>
        </div>
    </div>
    <i class="fas fa-angle-double-right i-action i-open hidden"></i>
    <?php $this->start('script_bottom'); ?>
    <script> var pois = <?php echo json_encode($datas); ?>; var zones = <?php echo json_encode($zones); ?>; </script>
    <?= $this->Html->script('carto.js');?>
        <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <?php $this->end(); ?>
