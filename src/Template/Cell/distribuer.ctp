<?= $this->Html->css('select2.min.css', ['block' => 'css']) ?>
<?= $this->Html->css('slick.css', ['block' => 'css2']) ?>
<div class="col-md-12 mt-3">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-start mb-2 mt-2">
        <a href="<?= $this->Url->Build(['action' => 'index']) ?>" class="btn btn-sm btn-primary text-white px-3 text-white mr-2"><i class="fas fa-arrow-left"></i></a>
        <h1 class="h4 text-gray-800">Distribution de la commande N° : <?= $order->id ?></h1>
    </div>
</div>
<form class="col-md-12" id="form" method="post" action="<?= $this->Url->Build(['controller' => 'distributions','action' => 'add', $order->id]) ?>">
    <div class="alert animated hidden" style="position: fixed;z-index: 1000;bottom: 12px;right: 15px;margin: 0;" id="alert"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseList" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseList">
                    <h6 class="m-0 font-weight-bold text-primary">Lignes de commandes de la distribution</h6>
                    <input type="hidden" name="order_id" value="<?= $order->id ?>">
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseList">
                    <div class="table-responsive shadow p-1">
                        <table class="table table-borderless table-hover table-striped table-condensed table-striped" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>SAP</th>
                                    <th>Produit</th>
                                    <th>Pack</th>
                                    <th>PU</th>
                                    <th>Quantité</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody id="tbody">
                                <?php if (count($order->order_lines) > 0) : foreach ($order->order_lines as $line) :
                                        if($line->quantite_max) $qreste = $line->quantite_max - $line->quantite_livre;
                                         else $qreste = $line->quantite;
                                        if($qreste > 0) : ?>
                                        <tr id="line<?= $line->id ?>" class="ligne_valide">
                                            <input type="hidden" name="lignes[]" value="<?= $line->id ?>">
                                            <td><i class="fas fa-check text-success"></i><?= $line->product->code ?></td>
                                            <td><?= $line->product->name ?><input type="hidden" name="product_id<?= $line->id ?>" value="<?= $line->product->id ?>"></td>
                                            <td><?= $line->product->pack ?></td>
                                            <td>
                                                <?= $this->Number->format($line->product->price_ht, ['locate' => 'fr_FR']) ?>
                                                <input type="hidden" name="price_ht<?= $line->id ?>" value="<?= $line->product->price_ht ?>">
                                            </td>
                                            <td>
                                                <select name="q<?= $line->id ?>" class="form-control form-control-sm form-control-plaintext">
                                                    <?php for ($i = 1; $i <= $qreste; $i++) { ?>
                                                        <option value="<?= $i ?>" <?= $i == $line->quantite ? 'selected' : '' ?>><?= $i ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                            <td class="text-center">
                                                <i class="fas fa-times text-danger mr-2 pl-1 pr-2 remove" data-id="<?= $line->id ?>" style="cursor:pointer"></i>
                                            </td>
                                        </tr>
                                        <?php endif; endforeach;
                                    else : ?>
                                    <tr id="line_info">
                                        <td colspan="8" class="text-center">La commande est probalement épuisée</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseForm" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseForm">
                    <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-share-alt mr-1"></i> Finalisez votre distribution</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseForm">
                    <div class="card-body">
                        <div class="form-group animated fadeIn" id="poi">
                            <label for="user_id">Séléctionner l'agent distributeur</label>
                            <select name="user_id" id="user_id" required class="form-control select form-control-sm">
                                <option selected disabled>Séléctionner le distributeur</option>
                                <?php if (count($roles) > 0) : foreach ($roles as $item) : ?>
                                        <option value="<?= $item->user->id ?>"><?= $item->user->fn .' '. $item->user->ln ?></option>
                                <?php endforeach;
                                endif; ?>
                            </select>
                            <hr>
                            <button type='submit' class="btn btn-primary text-white">Attribuer</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<?php $this->start('script_bottom'); ?>
<?= $this->Html->script('vendor/select2.min.js'); ?>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<?= $this->Html->script('vendor/slick.min.js'); ?>
<script>
    $(function() {
        var nb = 0;
        var product;
        var q;
        //var type;
        var nombre_lignes = $('.ligne_valide').length;

        $('select.select').select2();

        $('.remove').click(function() {
            let id = $(this).data('id');
            if(nombre_lignes > 1) {$('#line'+id).remove(); nombre_lignes--;}
            else customalert('Désolé vous ne vous pas supprimé toutes les lignes de commandes pour une distribution !','bg-danger text-white');
        });

        // FONCTION D ENVOI DE LA REQUETE
        $('#form').submit(function() {
            if(nombre_lignes > 0 && (type && type != "")) {
                return confirm('Vous allez passer à la distribution de la commande. \n Continuer ?');
            }
            else if(nombre_lignes == 0) customalert('Vous devez ajouter des lignes de commande avant de soumettre.','alert-danger bg-danger text-white');
            //else if(!type || type == "") customalert('Vous devez choisir le client avant de soumettre.','alert-danger bg-danger text-white');
            return false;
        });


        function imgurl(url) {
            return window.location.host + url;
        }

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        }

        function getById(tableau, id) {
            const item = tableau.find(
                v => {
                    return v.id === id
                }
            )
            return item;
        }

        function customalert(msg, type) {
            if (type == 'bg-success txt-blanc') {
                $("#alert").html('<i class="fas fa-check"></i> ' + msg);
            } else {
                $("#alert").html('<i class="fas fa-times"></i> ' + msg);
            }

            $("#alert").removeClass('hidden lightSpeedOut').addClass('lightSpeedIn ' + type);
            setTimeout(
                () => {
                    $("#alert").removeClass('lightSpeedIn ' + type).addClass('lightSpeedOut hidden');
                }, 6000
            )
        }
    });
</script>
<?php $this->end(); ?>
