<div class="col-md-12 mt-3">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 mt-2 customer__header bg-primary text-white">
        <h1 class="h4 text-gray-800">Liste des Commandes</h1>
        <!-- Topbar Search -->
        <a href="<?= $this->Url->build(['controller'=>'Orders','action'=>'add']) ?>" class="btn-sm btn-primary text-white">Créer une commande<i class="fas fa-shopping-cart ml-2"></i></a>
    </div>
    <!-- DataTales Example -->
    <div class="table-responsive shadow p-4">
        <table class="table table-borderless table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>N°</th>
                <th>Montant</th>
                <th>Client</th>
                <th>Etat</th>
                <th>Date Création</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>N°</th>
                <th>Montant</th>
                <th>Client</th>
                <th>Etat</th>
                <th>Date Création</th>
                <th>Actions</th>
            </tr>
            </tfoot>
            <tbody>
                <?php foreach ($orders as $order) :?>
                    <tr>
                        <td><?= $order->id; ?></td>
                        <td><?= $this->Number->format($order->amount,['locate' => 'fr_FR']); ?></td>
                        <td><?= $order->poi_id !== null ? $order->pois->dns : $order->poi_customer->fn ." ". $order->poi_customer->ln ?></td>
                        <td><?= $order->order_statut !== null ? $order->order_statut->name : "<small><i>Non défini</i></small>" ?></td>
                        <td><?= $order->created->nice('Africa/Dakar','fr-Fr') ?></td>
                        <td>
                            <a href="<?= $this->Url->Build(['controller' => 'Orders', 'action' => 'view', $order->id]) ?>" class="bg-white shadow-sm btn-sm mx-1">
                                <i class="fas fa-eye text-primary"></i>
                            </a>
                            <a href="<?= $this->Url->Build(['controller' => 'Orders', 'action' => 'edit', $order->id]) ?>" class="bg-white shadow-sm btn-sm mx-1">
                                <i class="fas fa-edit text-success"></i>
                            </a>
                            <!-- <a title="distribuer la commande" href="<?= $this->Url->Build(['controller' => 'Orders', 'action' => 'distribuer', $order->id]) ?>" class="bg-white shadow-sm btn-sm mx-1">
                                <i class="fas fa-share-alt text-warning"></i>
                            </a> -->
                            <?= $this->Form->postLink(__('<i class="fas fa-trash-alt text-danger"></i>'), ['controller' => 'Orders', 'action' => 'delete', $order->id], ['escape' => false, 'class' => 'bg-white shadow-sm btn-sm mx-1', 'confirm' => __('Voulez vous supprimer la commande n° :  {0}?', $order->id)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?php $this->start('script_bottom'); ?>
<!-- Page le*vel plugins -->
    <?= $this->Element('Components/exportScript') ?>
<?php $this->end(); ?>
