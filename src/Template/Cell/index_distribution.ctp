<div class="col-md-12 mt-3">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 mt-2 customer__header bg-primary text-white">
        <h3 class="text-white h4 mb-0 bolder" style="line-height: .7">Liste des Distributions
            <?php if ($this->fetch('subtitle') != '') { ?> <small class="ml-2 badge badge-info"><?= $this->fetch('subtitle') ?></small> <?php } ?>
        </h3>
    </div>
    <!-- DataTales Example -->
    <div class="table-responsive shadow p-4">
        <table class="table table-borderless table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>N°</th>
                    <th>Montant</th>
                    <th>Client</th>
                    <th>Commande</th>
                    <th>Statut</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>N°</th>
                    <th>Montant</th>
                    <th>Client</th>
                    <th>Commande</th>
                    <th>Statut</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
            <tbody>
                <?php foreach ($distributions as $distribution) : ?>
                    <tr>
                        <td><?= $distribution->id; ?></td>
                        <td><?= $this->Number->format($distribution->amount, ['locate' => 'fr_FR']); ?></td>
                        <td><?= $distribution->poi_id !== null ? $distribution->pois->dns : $distribution->poi_customer->fn . " " . $distribution->poi_customer->ln ?></td>
                        <td>
                            <a href="<?= $this->Url->Build(['controller' => 'Orders', 'action' => 'view', $distribution->order_id]) ?>" class="btn-success shadow-sm btn-sm mx-1">
                                <i class="fas fa-shopping-cart text-white"></i> N° <?= $distribution->order_id ?>
                            </a>
                        </td>
                        <td>
                            <?= $distribution->distribution_statut->name; ?>
                        </td>
                        <td>
                            <a href="<?= $this->Url->Build(['controller' => 'Distributions', 'action' => 'view', $distribution->id]) ?>" class="bg-white shadow-sm btn-sm mx-1">
                                <i class="fas fa-eye text-primary"></i>
                            </a>
                            <a href="<?= $this->Url->Build(['controller' => 'Distributions', 'action' => 'edit', $distribution->id]) ?>" class="bg-white shadow-sm btn-sm mx-1">
                                <i class="fas fa-edit text-success"></i>
                            </a>
                            <?= $distribution->distribution_statut_id == 1 ? $this->Form->postLink(__('<i class="fas fa-trash-alt text-danger"></i>'), ['controller' => 'Distributions', 'action' => 'delete', $distribution->id], ['escape' => false, 'class' => 'bg-white shadow-sm btn-sm mx-1', 'confirm' => __('Voulez vous supprimer la distribution n° :  {0}?', $distribution->id)])  : '' ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?php $this->start('script_bottom'); ?>
<!-- Page le*vel plugins -->
<?= $this->Element('Components/exportScript') ?>
<?php $this->end(); ?>
