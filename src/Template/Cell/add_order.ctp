<?= $this->Html->css('select2.min.css', ['block' => 'css']) ?>
<?= $this->Html->css('slick.css', ['block' => 'css2']) ?>
<div class="col-md-12 mt-3">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-start mb-2 mt-2">
        <a href="<?= $this->Url->Build(['action' => 'index']) ?>" class="btn btn-sm btn-primary text-white px-3 text-white mr-2"><i class="fas fa-arrow-left"></i></a>
        <h1 class="h4 text-gray-800">Création de commande</h1>
    </div>
</div>
<form class="col-md-12" id="form" method="post" action="<?= $this->Url->Build(['action' => 'add']) ?>">
    <input type="hidden" name="lignes" id="lines" value="">
    <div class="alert animated hidden" style="position: fixed;z-index: 1000;bottom: 12px;right: 15px;margin: 0;" id="alert"></div>
    <div class="row">
        <div class="col-md-12">
            <!-- Collapsable Card Example -->
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseModif" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseModif">
                    <h4 class="m-0 font-weight-bold text-primary"><i class="fas fa-shopping-cart mr-4"></i> Remplir le panier</h4>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseModif">
                    <div class="card-body row">
                        <div class="col-md-12 mb-3">
                            <div class="card shadow-sm">
                                <div class="card-header bg-primary text-white">Informations de la commande</div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 alert alert-box alert-warning text-center">
                                            <i class="fas fa-info-circle mr-2"></i> Les champs avec <b>(*)</b> sont obligatoire une fois activé.
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="type">Type de distribution *</label>
                                                <input type="hidden" name="order[limited]" id="limited" value="0">
                                                <select name="order[type]" id="type" class="form-control form-control-sm">
                                                    <option value="0">Unique</option>
                                                    <option value="1">Manuel</option>
                                                    <option value="2">Automatique</option>
                                                </select>
                                            </div>
                                            <div class="form-group" id="automatique">
                                                <label for="date_limited">Date limite de la distribution (* automatique)</label>
                                                <div class="input-group">
                                                    <input type="date" name="date_limited" id="date_limited" style="flex-grow: 3 !important" disabled class="form-control base auto form-control-sm mb-3">
                                                    <!-- <input type="number" disabled name="fin_heure" min="0" value="00" max="23" class="form-control form-control-sm base mb-3" placeholder="heure">
                                                    <input type="number" name="fin_minute" min="0" max="59" value="00" disabled class="form-control form-control-sm base mb-3" placeholder="minute"> -->
                                                </div>
                                                <div class="input-group">
                                                    <select name="order[frequence_id]" id="frequence_id" disabled class="form-control base auto form-control-sm">
                                                        <option value default>La fréquence</option>
                                                        <?php if (count($frequences) > 0) : foreach ($frequences as $freq) :  ?>
                                                                <option value="<?= $freq->id ?>"><?= $freq->name ?></option>
                                                        <?php endforeach;
                                                        endif;  ?>
                                                    </select>
                                                    <div class="input-group-append">
                                                        <label class="input-group-text small form-control-sm" style="font-size: 14px"><small>fréquence</small></label>
                                                    </div>
                                                </div>
                                                <label for="frequence1" class="mt-2 mb-0 small">Choisir le jour</label>
                                                <select class="form-control form-control-sm select mb-3" required disabled name="order[frequences]" id="frequence1" required>
                                                    <option value="1">Lundi</option>
                                                    <option value="2">Mardi</option>
                                                    <option value="3">Mercredi</option>
                                                    <option value="4">Jeudi</option>
                                                    <option value="5">Vendredi</option>
                                                    <option value="6">Samedi</option>
                                                    <option value="7">Dimanche</option>
                                                </select>
                                                <label for="frequence2" class="mt-2 mb-0 small">Choisir les jours</label>
                                                <select class="form-control form-control-sm select mb-3" required disabled multiple name="jours[]" id="frequence2" required>
                                                    <option value="1">Lundi</option>
                                                    <option value="2">Mardi</option>
                                                    <option value="3">Mercredi</option>
                                                    <option value="4">Jeudi</option>
                                                    <option value="5">Vendredi</option>
                                                    <option value="6">Samedi</option>
                                                    <option value="7">Dimanche</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group" id="manuel">
                                                <label for="debut" class="mt-2">Date de démarrage *</label>
                                                <div class="input-group">
                                                    <input type="date" id="debut" disabled required name="date_debut" style="flex-grow: 3 !important" class="form-control base form-control-sm mb-3">
                                                    <!-- <input type="number" name="debut_heure" min="0" value="00" max="23" class="form-control base form-control-sm mb-3" disabled placeholder="heure">
                                                    <input type="number" name="debut_minute" min="0" value="00" max="59" class="form-control base form-control-sm mb-3" disabled placeholder="minute"> -->
                                                </div>
                                                <label for="limited">Limite total par ligne de commande</label>
                                                <input type="number" disabled min="1" value="20" id="g_limite" class="form-control base form-control-sm mb-3" placeholder="Limite en quantité ligne de commande">
                                            </div>
                                            <div class="card">
                                                <div class="card-header py-2"><i class="fas fa-info-circle mr-2"></i> Suggestions</div>
                                                <div class="card-body py-1 p-1">
                                                    <div class="jumbotron text-justify small px-1 py-3 mb-1">
                                                        <blockquote>
                                                            Cette section vous permet de poser les bases de votre commande. <br>Veuillez bien prendre le soin de remplir minitieusement chaque champs avant de continuer.
                                                        </blockquote>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="name">Nom du produit</label>
                                <select name="product_id" id="produits" class="form-control select form-control-sm">
                                    <option value="" selected>sélectionner le produit</option>
                                    <?php foreach ($products as $product) : ?>
                                        <option value="<?= $product->id ?>"><?= $product->name ?> -- <?= $product->gamme->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-7 mb-4" id="images">
                            <div class="jumbotron text-center" id="infoproduit">
                                sélectionner un produit
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description">Produit séléctionné</label>
                                <div class="list-group card shadow-sm list-pois p-1 col-md-12">
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Nom Produit</span>
                                        <span class="bolder spaninfo" id="p-name"></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Prix HT</span>
                                        <span class="bolder spaninfo" id="p-price"></span>
                                    </li>
                                    <!-- <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Pack</span>
                                        <span class="bolder spaninfo" id="p-pack"></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Poids net / Poids brut (KG)</span>
                                        <span class="bolder spaninfo" id="p-poids"></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Statut du Produit</span>
                                        <span class="text-success">Active</span>
                                    </li> -->
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Quantité</span>
                                        <span><input type="number" id="q" placeholder="Saisir la quantité"></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-end">
                                        <span><button type="button" class="btn bg-primary btn-primary text-white" data-url="<?= $this->Url->Build(["controller" => "OrderLines", 'action' => 'add']) ?>" disabled id="add">Ajouter</button></span>
                                    </li>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseList" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseList">
                    <h6 class="m-0 font-weight-bold text-primary">Votre panier - Lignes de commandes</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseList">
                    <div class="table-responsive shadow p-1">
                        <table class="table table-borderless table-hover table-striped table-condensed table-striped" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>SAP</th>
                                    <th>Produit</th>
                                    <th>Limite</th>
                                    <th>PU x Q</th>
                                    <th>Total</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th colspan="4" class="text-center">TOTAL TTC</th>
                                    <th colspan="3" class="text-center" id="total">0</th>
                                </tr>
                            </tfoot>
                            <tbody id="tbody">
                                <tr id="line_info">
                                    <td colspan="8" class="text-center">Aucun produit dans le panier</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseForm" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseForm">
                    <h6 class="m-0 font-weight-bold text-primary">Finalisez votre commande</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseForm">
                    <div class="card-body row">
                        <div class="form-group col-md-12">
                            <label for="order_status">Etat de la commande</label>
                            <?= $this->Form->select('order_statut_id', $orderStatuts, ['class' => 'form-control form-control-sm', 'required', 'id' => 'order_status']) ?>
                        </div>
                        <div class="form-group col-12">
                            <label for="compte" style="display: block">Type de client</label>
                            <label for="c_1" class="mx-4"><input type="radio" class="mr-2" id="c_1" name="compte" checked value="1" />Personne morale</label>
                            <label for="c_2" class="mx-4">
                                <input type="radio" class="mr-2" id="c_2" name="compte" value="2" />Personne physique
                            </label>
                        </div>

                        <div class="form-group animated fadeIn col-12" id="poi">
                            <label for="poi_id">Séléctionner un POI</label>
                            <select name="poi_id[]" id="poi_id" multiple required class="form-control select form-control-sm">
                                <option selected disabled>Séléctionner le POIs</option>
                                <?php if (count($pois) > 0) : foreach ($pois as $item) : ?>
                                        <option value="<?= $item->id ?>"><?= $item->name ?></option>
                                <?php endforeach;
                                endif; ?>
                            </select>
                        </div>
                        <div class="form-group animated col-12 fadeOut" id="contact">
                            <label for="poi_customer_id">Séléctionner un contact</label>
                            <select name="poi_customer_id" multiple class="form-control select form-control-sm" id="poi_customer_id" disabled required class="form-control form-control-sm">
                                <option selected disabled>Séléctionner un contact</option>
                                <?php if (count($poiCustomers) > 0) : foreach ($poiCustomers as $contact) : ?>
                                        <option value="<?= $contact->id ?>"><?= "$contact->fn $contact->ln" ?></option>
                                <?php endforeach;
                                endif; ?>
                            </select>
                        </div>

                        <div class="form-group col-12 text-right">
                            <input type="hidden" name="user_id" value="<?= $_user->id ?>">
                            <button type='submit' class="btn btn-primary text-white">Enregistrer</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<?php $this->start('script_bottom'); ?>
<?= $this->Html->script('vendor/select2.min.js'); ?>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<?= $this->Html->script('vendor/slick.min.js'); ?>
<script>
    $(function() {
        var products = <?= json_encode($product_format_json) ?>;
        var product_id = 0;
        var nb = 0;
        var lignes = [];
        var product;
        var q;
        var order;
        var type;
        var total = 0;

        $('select.select').select2();

        /*==========================================================
         *
         **  FUNCTIONS DU PARAMETRAGE DE LA COMMANDE
         *
         *=========================================================*/
         var limite = Number($('#g_limite').val());


         $("#g_limite").keyup(function() {
            limite = Number($('#g_limite').val());
         });

        $('#type').change(function() {
            let val = $('#type').val();
            if (val > 0) {
                if(val == 2) requireFieldsAuto();
                else requireFieldsAuto(false);
                changeInputDisabled('#automatique input.base, #automatique select.base, #manuel input.base', false);
                $('#limited').val('1');
            } else {
                changeInputDisabled('#automatique input.base, #automatique select.base, #manuel input.base', true);
                $('#limited').val('0');
            }

        });

        $('#frequence_id').change(function() {
            let value = Number($(this).val());
            if (value > 0) {
                if (value > 1) {
                    changeInputDisabled("select#frequence2", true);
                    changeInputDisabled("select#frequence1", false);
                }
                else {
                    changeInputDisabled("select#frequence2", false);
                    changeInputDisabled("select#frequence1", true);
                }
            }
            else {
                changeInputDisabled("select#frequence2", true);
                changeInputDisabled("select#frequence1", true);
            }
        });


        function changeInputDisabled(elementInputs, statut) {
            $(`${elementInputs}`).prop('disabled', statut);
        }
        function requireFieldsAuto(required = true) {
            if(required) {
                $('.auto').prop('required', true);
            }
            else $('.auto').prop('required', false);
        }
        /*==========================================================
         *
         **  FIN FUNCTIONS DU PARAMETRAGE DE LA COMMANDE
         *
         *=========================================================*/



        /*==========================================================
         *
         **  FUNCTIONS DU PANIER
         *
         *=========================================================*/

        $('#produits').change(function() {
            product_id = Number($(this).val());
            addBtnActive();

            if (product_id > 0) {
                product = products.find(value => {
                    return value.id === product_id;
                });
                bindValue(product);
                if (product.images.length == 0) $('#images').append(noImages());
                else {
                    $('#images').append(getImages(product));
                    // $("#images").fadeIn('slow', function() {
                    //     $('#images').slick({
                    //         arrows: false,
                    //         infinite: true,
                    //         autoplay: true,
                    //         speed: 300,
                    //         slidesToShow: 3,
                    //         slidesToScroll: 3,
                    //         responsive: [{
                    //                 breakpoint: 1424,
                    //                 settings: {
                    //                     slidesToShow: 2,
                    //                     slidesToScroll: 2,
                    //                     infinite: true
                    //                 }
                    //             },
                    //             {
                    //                 breakpoint: 1024,
                    //                 settings: {
                    //                     slidesToShow: 1,
                    //                     slidesToScroll: 1,
                    //                     infinite: true
                    //                 }
                    //             }
                    //         ]
                    //     });
                    // })
                }
            } else {
                bindValue();
                $('#images').append(noImages('Séléctionner un produit'));
            }

        });

        $('#q').on('change keyup', function() {
            q = Number($(this).val());
            addBtnActive();
        });

        //Ajout ligne commande
        $('#add').click(function() {
            $(this).text("Ajout en cours...").attr("disabled", "disabled");
            setTimeout(
                () => {
                    customalert('Ligne de commande ajoutée avec succès', 'bg-success text-white');
                    $('#tbody').append(buildOrderLine(product));
                    $('#add').text("Ajouter").prop("disabled", false);
                    q = null;
                    $('[name=product_id]').val('').change();
                    $('#q').val('').change();
                    bindValue();
                    $('#images').append(noImages('Séléctionner un produit'));
                    //Ajout de la suppression
                    $('#tbody').fadeIn('fast', function() {
                        $('.isup').click(function() {
                            lignes = [];
                            let id = $(this).data('id');
                            total = 0;
                            $(this).parent().parent().fadeOut('slow').remove();
                            $.each($('.isup'), function(i, item) {
                                var temp_id = $(this).data('id');
                                var total_item = Number($('#total' + temp_id).data("amount"));
                                total += total_item;
                                lignes.push($(this).data('id'));

                            });
                            if (lignes.length == 0) {
                                $("#total").text('0');
                                if ($('#line_info').hasClass('hidden')) $('#line_info').removeClass('hidden');
                            }
                            $("#total").text(numberWithCommas(total));
                        });
                    });
                }, 1000
            )
        });

        //Choisir le type de client
        $('input[name=compte]').change(function() {
            type = null;
            let val = Number($(this).val());
            if (val === 1) {
                if ($('#contact').hasClass("fadeIn")) {
                    $('#contact').removeClass("fadeIn").addClass('fadeOut hidden');
                    $('#poi_customer_id').attr('disabled', 'disabled');
                }
                if ($('#poi').hasClass("fadeOut")) {
                    $('#poi').removeClass("fadeOut hidden").addClass('fadeIn');
                    $('#poi_id').removeAttr('disabled');
                }
            } else {
                if ($('#poi').hasClass("fadeIn")) {
                    $('#poi').removeClass("fadeIn").addClass('fadeOut hidden');
                    $('#poi_id').attr('disabled', 'disabled');
                }
                if ($('#contact').hasClass("fadeOut")) {
                    $('#contact').removeClass("fadeOut hidden").addClass('fadeIn');
                    $('#poi_customer_id').removeAttr('disabled', 'disabled');
                }
            }
        });

        $('#poi select,#contact select').change(function() {
            type = $(this).val();
        });

        // FONCTION D ENVOI DE LA REQUETE
        $('#form').submit(function() {
            $('#lines').val(JSON.stringify(lignes));
            if (lignes.length > 0 && (type && type != "")) {
                for(var ligne of lignes) {
                    let value = $('#q'+ligne).val();
                    let max = $('#qmax'+ligne).val();
                    if(max && max !== '') {
                        if(Number(value) > Number(max)) {
                            customalert("La quantité de la ligne de commande ne peut pas être supérieure à la quantité maxiamle.", 'alert-danger bg-danger text-white');
                            return false;
                        }
                    }
                }
                return confirm('Vous allez passer à la création de la commande. \n Continuer ?');
            } else if (lignes.length == 0) customalert('Vous devez ajouter des lignes de commande avant de soumettre.', 'alert-danger bg-danger text-white');
            else if (!type || type == "") customalert('Vous devez choisir le client avant de soumettre.', 'alert-danger bg-danger text-white');
            return false;
        });

        function imgurl(url) {
            return window.location.host + url;
        }

        function getImages(product) {
            var c = "";
            if ($('#images').hasClass('slick-initialized')) {
                $('#images').slick('unslick');
            }
            $('#images').html(' ');
            var images = product.images;
            $.each(images, function(i, image) {
                //c += '<div class="img text-center animated fadeIn"><img style="max-height: 200px;" src="' + imgurl(image.url) + '" alt="' + product.name + '"></div>';
                c += '<a href="' + image.url + '" data-fancybox="' + product.name + '" data-caption="' + image.name + '"><img src="' + image.url + '" alt="' + product.name + '" class="img-icon shadow-sm" style="margin-right: 5px;"></a>';
            });

            return c;
        }

        function bindValue(produit = null) {
            if (produit) {
                $("#p-name").text(produit.name);
                $("#p-price").text(numberWithCommas(produit.price_ht));
                // $("#p-poids").text(produit.poids_net + " / " + product.poids_brut);
                // $("#p-pack").text(produit.pack);
            } else {
                $.each($("span.spaninfo"), function(i, item) {
                    $(item).text('');
                });
            }
        }

        function noImages(message = "Aucune image pour ce produit") {
            if ($('#images').hasClass('slick-initialized')) {
                $('#images').slick('unslick');
            }
            $('#images').html(' ');
            return '<div class="jumbotron animated fadeIn text-center" id="infoproduit">' +
                message + '</div>';
        }

        function addBtnActive() {
            if (q > 0 && product_id > 0) {
                $('#add').prop('disabled', false);
            } else $('#add').prop('disabled', true);
        }

        function buildOrderLine(product) {
            nb++;
            let ligne = {
                price_ht: product.price_ht,
                quantite: q,
                amount_ht: (product.price_ht * q),
                product_id: product_id
            };
            lignes.push(nb);
            total += Number(ligne.amount_ht);
            $("#total").text(numberWithCommas(total));
            if (lignes.length > 0) {
                if (!$('#line_info').hasClass('hidden')) $('#line_info').addClass('hidden');
            }
            return '<tr class="col-lg-4 col-md-6 animated fadeIn ligne'+nb+'">' +
                '<input type="hidden" name="price_ht' + nb + '" value="' + ligne.price_ht + '" />' +
                '<input type="hidden" id="q' + nb + '" name="q' + nb + '" value="' + q + '" />' +
                '<input type="hidden" name="product_id' + nb + '" value="' + ligne.product_id + '" />' +
                '<input type="hidden" name="amount_ht' + nb + '" value="' + ligne.amount_ht + '" />' +
                '<td>' + product.code + '</td>' +
                '<td>' + product.name + '</td>' +
                '<td><input class="form-control form-control-sm" id="qmax' + nb + '" name="qmax' + nb + '" type="number" min="1" value="' + limite + '"</td>' +
                '<td>' + numberWithCommas(product.price_ht) + ' x ' + numberWithCommas(q) + '</td>' +
                '<td id="total' + nb + '" data-amount="' + ligne.amount_ht + '">' + numberWithCommas(ligne.amount_ht) + '</td>' +
                '<td class="text-center">' +
                '<i class="fas fa-times text-danger mr-2 pl-1 pr-2 isup" id="isup' + nb + '" data-id="' + nb + '" style="cursor:pointer"></i>' +
                '</td>' +
                '</tr>';
        }

        /*==========================================================
         *
            **  FIN FUNCTIONS DU PANIER
         *
         *=========================================================*/

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        }

        function getById(tableau, id) {
            const item = tableau.find(
                v => {
                    return v.id === id
                }
            )
            return item;
        }

        function customalert(msg, type) {
            if (type == 'bg-success txt-blanc') {
                $("#alert").html('<i class="fas fa-check"></i> ' + msg);
            } else {
                $("#alert").html('<i class="fas fa-times"></i> ' + msg);
            }

            $("#alert").removeClass('hidden lightSpeedOut').addClass('lightSpeedIn ' + type);
            setTimeout(
                () => {
                    $("#alert").removeClass('lightSpeedIn ' + type).addClass('lightSpeedOut hidden');
                }, 6000
            )
        }

    });
</script>
<?php $this->end(); ?>
