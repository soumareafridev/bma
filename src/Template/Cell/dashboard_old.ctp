<?php $this->layout = 'accueil';?>
    <div class="col-md-7 col-lg-8 pt-5" style="min-height: calc(100vh - (37px));overflow-y: auto">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h3 class="text-primary h1 mb-0 bolder" style="line-height: .7">Dashboard
                <br> <small class="h6 text-black-50 m-0" style="font-size:14px">
                <?= $this->Vue->getInfoSite($_user->company_id,'slogan'); ?>
            </small>
            </h3>
        </div>

    <!-- Content Row -->
    <div class="row">
        <?php if(count($type_pois) > 0) : ?>
            <?= $this->Element('Components/dashboard') ?>
        <?php else : ?>
            <div class="jumbotron text-center col-md-11 p-3">
                <h3>Vous n'avez encore aucun enregistrement</h3>
                <?php if($_user->profile_id == 4) { ?>
                <a href="<?= $this->Url->build(["controller" => 'parametres', 'action' => 'bases']) ?>" class="btn mt-4 shadow-sm bg-rose text-white">Commencer par ici <i class="fas fa-long-arrow-alt-right ml-3" ></i></a>
                <?php } ?>
            </div>
        <?php endif; ?>
    </div>

        <!-- Content Row -->
        <div class="row">
            <h4 class="col-md-12 h4">
                Les 5 dernier(s) POIs enregistré(s)
            </h4>
            <div class="list-group col-md-12 list-pois">
                <?php if(sizeof($pois) > 0): for ($i=0; $i < sizeof($pois) ; $i++): ?>
                <li class="list-group-item d-sm-flex align-items-center justify-content-between marker" id="marker<?= $i ?>" data-created="<?= $pois[$i]->created->nice('Africa/Dakar','fr-Fr') ?>" data-pin="<?= $this->Url->image('md-pin.png') ?>"  data-id="<?= $pois[$i]->id ?>" data-dns="<?= $pois[$i]->dns ?>" data-lng="<?= $pois[$i]->lng ?>" data-lat="<?= $pois[$i]->lat ?>" data-img="<?= $pois[$i]->photo !== null ? $this->Url->image('pois/'.$pois[$i]->photo) : null ?>">
                    <p class="eye"><i class="fas fa-eye icon-circle-blue" style="cursor:pointer"></i></p>
                    <div class="info">
                        <h3 class="h5 bold mb-0" style="font-size: 14px;"><?= $pois[$i]->dns; ?></h3>
                        <small>Distributeur: <?= $pois[$i]->infos[0]->value; ?> </small>
                    </div>
                    <div class="info-sup">
                        <span>
                            <span class="marqueur-blue"><?= $pois[$i]->type_pois->name; ?> </span> <br>
                            <small>Enregistré le <?= $pois[$i]->created->nice('Africa/Dakar','fr-Fr') ?></small>
                        </span>
                        <!-- <?php if($pois[$i]->photo): ?>
                            <a href="<?= $this->Url->image('pois/'.$pois[$i]->photo) ?>" data-fancybox="<?=h($pois[$i]->dns) ?>" data-caption="<?=h($pois[$i]->dns) ?>" style="display:inline-block">
                                <img src="<?= $this->Url->image('pois/'.$pois[$i]->photo) ?>" class="ml-1" style="max-width: 35px;border-radius: 4px;box-shadow: 0 2px 3px rgba(0,0,0,.4);cursor:zoom-in" alt="Photo enregistrée de la POI">
                            </a>
                        <?php else: ?>
                            <img src="<?= $this->Url->image('empty.png') ?>" class="ml-1" style="max-width: 35px;border-radius: 4px;box-shadow: 0 1px 2px rgba(0,0,0,.1);" alt="Photo enregistrée de la POI">
                        <?php endif; ?> -->
                    </div>
                    <a href="<?= $this->Url->Build(['controller' => 'Pois','action' => 'view', $pois[$i]->id]) ?>"><i class="fas fa-arrow-right"></i></a>
                </li>
                <?php endfor; else: ?>
                    <div class="jumbotron text-center">
                        <h1 class="h2 text-gray-700">
                            Aucun enregistrement pour le moment.
                        </h1>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="col-md-5 col-lg-4 hidden-sm bg-primary text-white text-center" style="min-height: calc(100vh - (37px));min-height: calc(100vh - (37px));overflow:hidden" id="map"></div></div>

<?php $this->start('script_bottom'); ?>
    <?= $this->Html->script('main.js');?>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<?php $this->end(); ?>
