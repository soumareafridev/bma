<?= $this->Html->css('select2.min.css', ['block' => 'css']) ?>
<?= $this->Html->css('slick.css', ['block' => 'css2']) ?>
<div class="col-md-12 mt-3">
    <div class="d-sm-flex align-items-center justify-content-start mb-2 mt-2">
        <h1 class="h4 text-gray-800">
            <a href="javascript:history.back()" class="btn btn-sm btn-primary text-white px-3 text-white mr-2"><i class="fas fa-arrow-left"></i></a>
            Consulter distribution N° : <?= $distribution->id ?>
        </h1>
    </div>
</div>
<div class="col-md-12">
    <div class="row">
        <div class="col-md-12">
            <!-- Collapsable Card Example -->
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseModif" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseModif">
                    <h4 class="m-0 font-weight-bold text-primary"><i class="fas fa-share-alt mr-4"></i> Information de la distribution et du client</h4>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseModif">
                    <div class="card-body row">
                        <div class="col-md-6">
                            <div class="list-group list-pois p-1">
                                <li class="list-group-item li-detail-item shadow-sm">
                                    <span class="text-gray-600">Dernière mise à jour</span><br>
                                    <span class="bolder text-primary">
                                        <?= $distribution->modified ? $distribution->modified->nice('Africa/Dakar','fr-Fr') : "non disponible" ?>
                                    </span>
                                </li>
                                <li class="list-group-item li-detail-item shadow-sm">
                                    <span class="text-gray-600">Attribuée à</span><br>
                                    <span class="bolder text-primary">
                                        <?= $distribution->user->fn . ' ' .$distribution->user->ln ?> <small class="badge badge-primary"><?= $distribution->user->phone ?></small>
                                    </span>
                                </li>
                                <li class="list-group-item li-detail-item shadow-sm">
                                    <span class="text-gray-600">Montant HT</span><br>
                                    <span class="bolder text-primary spaninfo"><?= $this->Number->format($distribution->amount, ['locate' => 'fr_FR', 'after' => ' FCFA']); ?></span>
                                </li>
                                <li class="list-group-item li-detail-item shadow-sm">
                                    <span class="text-gray-600">Etat</span><br>
                                    <span class="bolder text-primary spaninfo"><?= $distribution->distribution_statut !== null ? $distribution->distribution_statut->name : "<small><i>Non défini</i></small>" ?></span>
                                </li>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="list-group list-pois p-1">
                                <li class="list-group-item li-detail-item shadow-sm">
                                    <span class="text-gray-600">Type de compte client</span><br>
                                    <span class="bolder spaninfo text-primary"><?= $distribution->poi_id !== null ? 'Personne morale' : 'Personne physique' ?></span>
                                </li>
                                <li class="list-group-item li-detail-item shadow-sm">
                                    <span class="text-gray-600">Nom du client</span><br>
                                    <span class="bolder text-primary spaninfo"><?= $distribution->poi_id !== null ? $distribution->pois->dns : $distribution->poi_customer->fn . ' ' . $distribution->poi_customer->fn ?></span>
                                </li>
                                <li class="list-group-item li-detail-item shadow-sm">
                                    <span class="text-gray-600">Numéro de téléphone</span><br>
                                    <span class="bolder text-primary spaninfo"><?= $distribution->poi_id !== null ? $distribution->pois->phone : $distribution->poi_customer->phone ?></span>
                                </li>
                                <?php if ($distribution->poi_customer_id !== null) : ?>
                                    <li class="list-group-item li-detail-item shadow-sm">
                                        <span class="text-gray-600">Entreprise</span><br>
                                        <span class="bolder text-primary spaninfo"><?= $distribution->poi_customer->pois->dns ?></span>
                                    </li>
                                <?php endif; ?>
                                <li class="list-group-item li-detail-item shadow-sm">
                                    <span class="text-gray-600">Type de point d'interêt</span><br>
                                    <span class="bolder text-primary spaninfo"><?= $distribution->poi_id !== null ? $distribution->pois->type_pois->name : $distribution->poi_customer->pois->type_pois->name ?></span>
                                </li>
                                <li class="list-group-item li-detail-item shadow-sm">
                                    <span class="text-gray-600">Compte créé depuis</span><br>
                                    <span class="bolder text-primary spaninfo"><?= $distribution->poi_id !== null ? $distribution->pois->created->nice('Africa/Dakar','fr-Fr') : $distribution->poi_customer->pois->created->nice('Africa/Dakar','fr-Fr') ?></span>
                                </li>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseList" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseList">
                    <h6 class="m-0 font-weight-bold text-primary">Détails de la distribution</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseList">
                    <div class="card-body">
                        <div class="table-responsive shadow">
                            <table class="table table-borderless table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Produit</th>
                                        <th>Gamme</th>
                                        <th>Prix HT</th>
                                        <th>Qauntité</th>
                                        <th>Total HT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;
                                    foreach ($distribution->distribution_lines as $line) : ?>
                                        <tr>
                                            <td><?= $i ?></td>
                                            <td><?= $line->product->name ?></td>
                                            <td><?= $line->product->gamme->name ?></td>
                                            <td><?= $this->Number->format($line->price_ht, ['locale' => 'fr_FR', 'after' => ' FCFA']) ?></td>
                                            <td><?= $line->quantite ?></td>
                                            <td><?= $this->Number->format(($line->price_ht * $line->quantite), ['locale' => 'fr_FR', 'after' => ' FCFA']) ?></td>
                                        </tr>
                                    <?php $i++;
                                    endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
