
<?php $this->start('script'); ?>
    <script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>
<?php $this->end(); ?>
<?= $this->Html->css('slick.css',['block' => 'css']) ?>
<!-- Page Heading -->
<div class="col-md-12">
    <div class="d-sm-flex align-items-center justify-content-start mb-2 mt-2">
    <h1 class="h4 text-gray-800">
        <a href="javascript:history.back()" class="btn btn-sm btn-primary text-white px-3 text-white mr-2"><i class="fas fa-arrow-left"></i></a>
            Modification Produit - <small class="h6">N° <?= $product->id ?></small>
        </h1>
    </div>
</div>

<form class="col-md-12" method="post" enctype="multipart/form-data" action="<?= $this->Url->Build(['action' => 'edit', $product->id]) ?>">
    <div class="row">
        <div class="col-md-12">
            <!-- Collapsable Card Example -->
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseModif" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseModif">
                    <h6 class="m-0 font-weight-bold text-primary">Remplir les informations du produit</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseModif">
                    <div class="card-body row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Nom du produit</label>
                                <input type="text" id="name" name="product[name]" required value="<?= $product->name ?>" class="form-control form-control-sm" >
                            </div>
                            <div class="form-group">
                                <label for="code">Code du produit</label>
                                <input type="text" id="code" name="product[code]" required value="<?= $product->code ?>" class="form-control form-control-sm" >
                            </div>
                            <div class="form-group">
                                <label for="pack">Pack du produit</label>
                                <input type="text" id="pack" name="product[pack]" required value="<?= $product->pack ?>" class="form-control form-control-sm" >
                            </div>
                            <div class="form-group">
                                <label for="price_ht">Prix TTC du produit</label>
                                <input type="number" step="any" id="price_ht" name="product[price_ht]" required value="<?= $product->price_ht ?>" class="form-control form-control-sm" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="poids_net">Poids net du produit (KG)</label>
                                <input type="number" step="any" id="poids_net" name="product[poids_net]" required value="<?= $product->poids_net ?>" class="form-control form-control-sm" >
                            </div>
                            <div class="form-group">
                                <label for="poids_brut">Poids brut du produit (KG)</label>
                                <input type="number" step="any" id="poids_brut" name="product[poids_brut]" required value="<?= $product->poids_brut ?>" class="form-control form-control-sm" >
                            </div>
                            <div class="form-group">
                                <label for="gamme_id">Gamme du produit</label>
                                <?= $this->Form->select('product[gamme_id]',$gammes, ['class' => 'form-control','default' => $product->gamme_id, 'required']) ?>
                            </div>
                            <div class="form-group">
                                <label for="status">Statut par défaut</label>
                                <?= $this->Form->select('product[status]',[1 => 'Actif',2 => 'Inactif'], ['class' => 'form-control','default' => $product->status]) ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description">Description du produit</label>
                                <textarea name="product[description]" id="description" class="form-control form-control-sm"  rows="10">
                                    <?= $product->description ?>
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseList" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseList">
                    <h6 class="m-0 font-weight-bold text-primary">Ajouter des photos - taille 2M</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseList">
                    <div class="card-body">
                    <?php if(count($product->images) > 0) : ?>
                        <?php $n= 0; foreach($product->images as $image): ?>
                        <div class="icon-block">
                            <img src="<?= $this->Url->image($image->url) ?>" alt="<?= $product->name ?>" class="img-icon shadow-sm">
                            <?= $n == 0 ? '<form style="display:none;"></form>' : "" ?>
                            <?= $this->Form->postLink(__('<i class="fas fa-times fa-2x"></i>'), ['controller' => 'Images', 'action' => 'delete', $image->id], ['escape' => false, 'confirm' => __('Voulez vous supprimer l\'image suivante :  {0}?', $image->name)]) ?>
                        </div>
                    <?php $n++; endforeach;  endif; ?>
                        <div class="form-group mt-4">
                            <label for="images[1]">Image 1</label>
                            <input type="file" name="images[1]" id="images[1]" class="form-control form-control-sm" >
                        </div>
                        <div class="form-group">
                            <label for="images[2]">Image 2</label>
                            <input type="file" name="images[2]" id="images[2]" class="form-control form-control-sm" >
                        </div>
                        <div class="form-group">
                            <label for="images[3]">Image 3</label>
                            <input type="file" name="images[3]" id="images[3]" class="form-control form-control-sm" >
                        </div>
                        <div class="form-group">
                            <label for="images[4]">Image 4</label>
                            <input type="file" name="images[4]" id="images[4]" class="form-control form-control-sm" >
                        </div>
                        <div class="form-group mt-4 text-right">
                            <a href="<?= $this->Url->Build(['controller' => 'Products', 'action' => 'view', $product->id]) ?>" class="btn btn-info ml-2">
                                    <i class="fas fa-eye mr-2"></i> Voir
                            </a>
                            <button type="submit" class="btn btn-success"><i class="fas fa-cloud mr-2"></i>Mettre à jour</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php $this->start('script_bottom'); ?>
    <script>
        CKEDITOR.replace('description', {
            // language: 'fr',
            //uiColor: '#2C80BE'
        });
        $(document).ready(function() {
            var description = $('#description').val();
            //Bind value description
            $('#description').change(function() {
                description = $(this).val();
            });

        });
    </script>
<?php $this->end(); ?>
