<?php $this->start('script'); ?>
    <script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>
<?php $this->end(); ?>
<!-- Page Heading -->
<div class="col-md-12">
    <div class="d-sm-flex align-items-center justify-content-start mb-2 mt-2">
        <h1 class="h4 text-gray-800">
        <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-sm btn-primary text-white px-3 text-white mr-2"><i class="fas fa-arrow-left"></i></a>
            Ajout - Nouveau produit
        </h1>
    </div>
</div>

<form class="col-md-12" method="post" enctype="multipart/form-data" action="<?= $this->Url->Build(['action' => 'add']) ?>">
    <div class="row">
        <div class="col-md-12">
            <!-- Collapsable Card Example -->
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseModif" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseModif">
                    <h6 class="m-0 font-weight-bold text-primary">Remplir les informations du produit</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseModif">
                    <div class="card-body row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Nom du produit</label>
                                <input type="text" id="name" name="product[name]" required class="form-control form-control-sm" >
                            </div>
                            <div class="form-group">
                                <label for="code">Code du produit</label>
                                <input type="text" id="code" name="product[code]" required class="form-control form-control-sm" >
                            </div>
                            <div class="form-group">
                                <label for="pack">Pack du produit</label>
                                <input type="text" id="pack" name="product[pack]"  class="form-control form-control-sm" >
                            </div>
                            <div class="form-group">
                                <label for="price_ht">Prix TTC du produit</label>
                                <input type="number" step="any" id="price_ht" name="product[price_ht]" required class="form-control form-control-sm" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="poids_net">Poids net du produit (KG)</label>
                                <input type="number" step="any" id="poids_net" name="product[poids_net]" required class="form-control form-control-sm" >
                            </div>
                            <div class="form-group">
                                <label for="poids_brut">Poids brut du produit (KG)</label>
                                <input type="number" step="any" id="poids_brut" name="product[poids_brut]" required class="form-control form-control-sm" >
                            </div>
                            <div class="form-group">
                                <label for="gamme_id">Gamme du produit</label>
                                <?= $this->Form->select('product[gamme_id]',$gammes, ['class' => 'form-control','empty' => 'Choisissez la Gamme du produit', 'required']) ?>
                            </div>
                            <div class="form-group">
                                <label for="status">Statut par défaut</label>
                                <?= $this->Form->select('product[status]',[1 => 'Actif',2 => 'Inactif'], ['class' => 'form-control']) ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description">Description du produit</label>
                                <textarea name="product[description]" id="description" class="form-control form-control-sm"  rows="10"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseList" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseList">
                    <h6 class="m-0 font-weight-bold text-primary">Ajouter des photos - taille 2M</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseList">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="images[1]">Image principale</label>
                            <input type="file" name="images[1]" id="images[1]" class="form-control form-control-sm" >
                        </div>
                        <div class="form-group">
                            <label for="images[2]">Image 2</label>
                            <input type="file" name="images[2]" id="images[2]" class="form-control form-control-sm" >
                        </div>
                        <div class="form-group">
                            <label for="images[3]">Image 3</label>
                            <input type="file" name="images[3]" id="images[3]" class="form-control form-control-sm" >
                        </div>
                        <div class="form-group">
                            <label for="images[4]">Image 4</label>
                            <input type="file" name="images[4]" id="images[4]" class="form-control form-control-sm" >
                        </div>
                        <div class="form-group mt-4 text-right">
                            <button type="submit" class="btn btn-primary text-white"><i class="fas fa-plus-circle mr-2"></i>Enregistrer</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php $this->start('script_bottom'); ?>
    <script>
        CKEDITOR.replace('description', {
            // language: 'fr',
            //uiColor: '#2C80BE'
        });
        $(document).ready(function() {
            var description = $('#description').val();
            //Bind value description
            $('#description').change(function() {
                description = $(this).val();
            });

        });
    </script>
<?php $this->end(); ?>
