<div class="col-lg-12 mt-1">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 mt-2 customer__header bg-primary text-white">
        <h1 class="h4 text-gray-800"><?= $this->fetch('title') ?></h1>
    </div>
    <!-- DataTales Example -->
    <div class="table-responsive shadow p-4">
        <?= $this->fetch('table') ?>
    </div>
</div>
<?php $this->start('script_bottom'); ?>
    <?= $this->Element('Components/exportScript') ?>
<?php $this->end(); ?>
