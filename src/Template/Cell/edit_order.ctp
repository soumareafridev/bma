<?= $this->Html->css('select2.min.css', ['block' => 'css']) ?>
<?= $this->Html->css('slick.css', ['block' => 'css2']) ?>
<div class="col-md-12 mt-3">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-start mb-2 mt-2">
        <a href="<?= $this->Url->Build(['action' =>'index']) ?>" class="btn btn-sm btn-primary text-white px-3 text-white mr-2"><i class="fas fa-arrow-left"></i></a>
        <h1 class="h4 text-gray-800">Modification de la commande N° : <?= $order->id ?></h1>
    </div>
</div>
<form class="col-md-12" style="" id="form" method="post" action="<?= $this->Url->Build(['action' =>'edit',$order->id]) ?>">
    <input type="hidden" name="lignes" id="lines" value="">
    <div class="alert animated hidden" style="position: fixed;z-index: 1000;bottom: 12px;right: 15px;margin: 0;" id="alert"></div>
    <div class="row">
        <div class="col-md-12">
            <!-- Collapsable Card Example -->
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseModif0" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseModif0">
                    <h4 class="m-0 font-weight-bold text-primary"><i class="fas fa-user-check mr-4"></i> Informations de la commande</h4>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseModif0">
                    <div class="card-body row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="list-group card shadow-sm list-pois p-1 col-md-12">
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Type de compte</span>
                                        <span class="bolder spaninfo"><?= $order->poi_id !== null ? 'Personne morale' : 'Personne physique' ?></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Nom compte</span>
                                        <span class="bolder spaninfo"><?= $order->poi_id !== null ? $order->pois->dns : $order->poi_customer->fn . ' ' . $order->poi_customer->fn ?></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Montant HT</span>
                                        <span class="bolder spaninfo"><?= $this->Number->format($order->amount, ['locate' => 'fr_FR', 'after' => ' FCFA']); ?></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Etat</span>
                                        <span class="bolder spaninfo"><?= $order->order_statut !== null ? $order->order_statut->name : "<small><i>Non défini</i></small>" ?></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Type de commande</span>
                                        <span class="bolder">
                                            <?= $order->type > 0 ? ($order->type == 1 ? 'Manuelle' : 'Automatique') : 'Unique' ?>
                                        </span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Date de début</span>
                                        <span class="bolder spaninfo"><?= $order->date_debut !== null ? $order->date_debut->nice('Africa/Dakar','fr-Fr') : $order->poi_customer->fn . ' ' . $order->poi_customer->fn ?></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Date de fin</span>
                                        <span class="bolder spaninfo"><?= $order->date_limited !== null ? $order->date_limited->nice('Africa/Dakar','fr-Fr') : 'Non - définie' ?></span>
                                    </li>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Collapsable Card Example -->
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseModif" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseModif">
                    <h4 class="m-0 font-weight-bold text-primary"><i class="fas fa-shopping-cart mr-4"></i> Ajouter dans le panier</h4>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse" id="collapseModif">
                    <div class="card-body row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="name">Nom du produit</label>
                                <select name="product_id" id="produits" class="form-control form-control-sm" >
                                    <option value="" selected>sélectionner le produit</option>
                                    <?php foreach ($products as $product) : ?>
                                        <option value="<?= $product->id ?>"><?= $product->name ?> -- <?= $product->gamme->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-7 mb-4 text-center" id="images">
                            <div class="jumbotron text-center" id="infoproduit">
                                sélectionner un produit
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description">Produit séléctionné</label>
                                <div class="list-group card shadow-sm list-pois p-1 col-md-12">
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Nom Produit</span>
                                        <span class="bolder spaninfo" id="p-name"></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Prix HT</span>
                                        <span class="bolder spaninfo" id="p-price"></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Pack</span>
                                        <span class="bolder spaninfo" id="p-pack"></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Poids net / Poids brut (KG)</span>
                                        <span class="bolder spaninfo" id="p-poids"></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Statut du Produit</span>
                                        <span class="text-success">Active</span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Quantité</span>
                                        <span><input type="number" id="q" placeholder="Saisir la quantité"></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-end">
                                        <span><button type="button" class="btn btn-primary text-white" data-url="<?= $this->Url->Build(["controller" => "OrderLines", 'action' => 'add']) ?>" disabled id="add">Ajouter</button></span>
                                    </li>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseList" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseList">
                    <h6 class="m-0 font-weight-bold text-primary">Votre panier - Lignes de commandes</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseList">
                    <form action="" class="hidden"></form>
                    <div class="table-responsive shadow p-1">
                        <table class="table table-borderless table-hover table-striped table-condensed table-striped" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>SAP</th>
                                    <th>Produit</th>
                                    <th>Pack</th>
                                    <th>PU x Q</th>
                                    <th>Total</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th colspan="4" class="text-center">TOTAL TTC</th>
                                    <th colspan="3" class="text-center" data-total="<?= $order->amount ?>" id="total"><?= $this->Number->format($order->amount, ['locate' => 'fr_FR']) ?></th>
                                </tr>
                            </tfoot>
                            <tbody id="tbody">
                            <?php if (count($order->order_lines) > 0) : foreach ($order->order_lines as $line) : ?>
                                <tr>
                                    <td><i class="fas fa-check text-success"></i><?= $line->product->code ?></td>
                                    <td><?= $line->product->name ?></td>
                                    <td><?= $line->product->pack ?></td>
                                    <td><?= $line->quantite. ' x '. $this->Number->format($line->product->price_ht, ['locate' => 'fr_FR']) ?></td>
                                    <td><?= $this->Number->format($line->amount_ht, ['locate' => 'fr_FR']) ?></td>
                                    <td class="text-center">
                                        <?= $this->Form->postLink(__('<i class="fas fa-times text-danger mr-2 pl-1 pr-2" style="cursor:pointer"></i>'), ['controller' => 'OrderLines', 'action' => 'delete', $line->id], ['escape' => false, 'confirm' => __('Voulez vous supprimer la ligne N° :  {0}?', $line->id)]) ?>
                                    </td>
                                </tr>
                                <?php endforeach;
                                else : ?>
                                <tr id="line_info">
                                    <td colspan="8" class="text-center">Aucun produit dans le panier</td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseForm" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseForm">
                    <h6 class="m-0 font-weight-bold text-primary">Finalisez votre commande</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseForm">
                    <div class="card-body row">
                    <div class="form-group col-md-12">
                            <label for="order_status">Etat de la commande</label>
                            <?= $this->Form->select('order_statut_id',$orderStatuts,['class' => 'form-control form-control-sm','required','id' => 'order_status', 'default' => $order->order_statut_id]) ?>
                        </div>
                        <div class="form-group col-12">
                            <label for="compte" style="display: block">Type de client</label>
                            <label for="c_1" class="mx-4"><input type="radio" class="mr-2" id="c_1" name="compte" checked value="1" />Personne morale</label>
                            <label for="c_2" class="mx-4">
                                <input type="radio" class="mr-2" id="c_2" name="compte" value="2" />Personne physique
                            </label>
                        </div>

                        <div class="form-group animated fadeIn col-12" <?= $order->poi_id == null ? 'fadeOut': 'fadeIn' ?> id="poi">
                            <label for="poi_id">Séléctionner un POI</label>
                            <select name="poi_id" id="poi_id" required class="form-control form-control-sm" >
                                <option>Séléctionner le POIs</option>
                                <?php if (count($pois) > 0) : foreach ($pois as $item) : ?>
                                        <option value="<?= $item->id ?>" <?= $item->id == $order->poi_id ? 'selected' : '' ?>><?= $item->name ?></option>
                                <?php endforeach;
                                endif; ?>
                            </select>
                        </div>
                        <div class="form-group animated col-12 <?= $order->poi_customer_id == null ? 'fadeOut': 'fadeIn' ?>" id="contact">
                            <label for="poi_customer_id">Séléctionner un contact</label>
                            <select name="poi_customer_id" class="form-control form-control-sm"  id="poi_customer_id" disabled required class="form-control form-control-sm" >
                                <option>Séléctionner un contact</option>
                                <?php if (count($poiCustomers) > 0) : foreach ($poiCustomers as $contact) : ?>
                                        <option value="<?= $contact->id ?>" <?= $contact->id == $order->poi_customer_id ? 'selected' : '' ?>><?= "$contact->fn $contact->ln" ?></option>
                                <?php endforeach;
                                endif; ?>
                            </select>
                        </div>

                        <div class="form-group col-12 text-right">
                            <button type='submit' class="btn btn-primary text-white">Enregistrer</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<?php $this->start('script_bottom'); ?>
<?= $this->Html->script('vendor/select2.min.js'); ?>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<?= $this->Html->script('vendor/slick.min.js'); ?>
<script>
    $(function() {
        var products = <?= json_encode($product_format_json) ?>;
        var product_id = 0;
        var nb = 0;
        var lignes = [];
        var product;
        var q;
        var order;
        var type = 1;
        var total = Number($("#total").data('total'));
        $('select.form-control').select2();

        function addBtnActive() {
            if (q > 0 && product_id > 0) {
                $('#add').prop('disabled', false);
            } else $('#add').prop('disabled', true);
        }

        $('#produits').change(function() {
            product_id = Number($(this).val());
            addBtnActive();

            if (product_id > 0) {
                product = products.find(value => {
                    return value.id === product_id;
                });
                bindValue(product);
                if (product.images.length == 0) $('#images').append(noImages());
                else {
                    $('#images').append(getImages(product));
                }
            } else {
                bindValue();
                $('#images').append(noImages('Séléctionner un produit'));
            }

        });

        $('#q').on('change keyup', function() {
            q = Number($(this).val());
            addBtnActive();
        });

        //Ajout ligne commande
        $('#add').click(function() {
            $(this).text("Ajout en cours...").attr("disabled", "disabled");
            setTimeout(
                () => {
                    customalert('Ligne de commande ajoutée avec succès', 'bg-success text-white');
                    $('#tbody').append(buildOrderLine(product));
                    $('#add').text("Ajouter").prop("disabled", false);
                    q = null;
                    $('[name=product_id]').val('').change();
                    $('#q').val('').change();
                    bindValue();
                    if($('#no-line').html() !== undefined) $('#no-line').remove();
                    $('#images').append(noImages('Séléctionner un produit'));
                    //Ajout de la suppression
                    $('#tbody').fadeIn('fast', function() {
                        $('.isup').click(function() {
                            lignes = [];
                            let id = $(this).data('id');
                            total = Number($("#total").data('total'));
                            $(this).parent().parent().fadeOut('slow').remove();
                            $.each($('.isup'), function(i, item) {
                                var temp_id = $(this).data('id');
                                var total_item = Number($('#total'+temp_id).data("amount"));
                                total += total_item;
                                lignes.push($(this).data('id'));

                            });
                            if(lignes.length == 0) {
                                $("#total").text('0');
                                if($('#line_info').hasClass('hidden')) $('#line_info').removeClass('hidden');
                            }
                            $("#total").text(numberWithCommas(total));
                        });
                    });
                }, 1000
            )
        });

        //Choisir le type de client
        $('input[name=compte]').change(function() {
            let val = Number($(this).val());
            if (val === 1) {
                if ($('#contact').hasClass("fadeIn")) {
                    $('#contact').removeClass("fadeIn").addClass('fadeOut hidden');
                    $('#poi_customer_id').attr('disabled', 'disabled');
                }
                if ($('#poi').hasClass("fadeOut")) {
                    $('#poi').removeClass("fadeOut hidden").addClass('fadeIn');
                    $('#poi_id').removeAttr('disabled');
                }
            } else {
                if ($('#poi').hasClass("fadeIn")) {
                    $('#poi').removeClass("fadeIn").addClass('fadeOut hidden');
                    $('#poi_id').attr('disabled', 'disabled');
                }
                if ($('#contact').hasClass("fadeOut")) {
                    $('#contact').removeClass("fadeOut hidden").addClass('fadeIn');
                    $('#poi_customer_id').removeAttr('disabled', 'disabled');
                }
            }
        });

        $('#poi select,#contact select').change(function() {
            type = $(this).val();
        });

        // FONCTION D ENVOI DE LA REQUETE
        $('#form').submit(function() {
            $('#lines').val(JSON.stringify(lignes));
            if (type && type != "") {
                return confirm('Vous allez passer à la modification de la commande. \n Continuer ?');
            } else if (lignes.length == 0) customalert('Vous devez ajouter des lignes de commande avant de soumettre.', 'alert-danger bg-danger text-white');
            else if (!type || type == "") customalert('Vous devez choisir le client avant de soumettre.', 'alert-danger bg-danger text-white');
            return false;
        });

        function imgurl(url) {
            return window.location.host + url;
        }

        function getImages(product) {
            var c = "";
            if ($('#images').hasClass('slick-initialized')) {
                $('#images').slick('unslick');
            }
            $('#images').html(' ');
            var images = product.images;
            $.each(images, function(i, image) {
                //c += '<div class="img text-center animated fadeIn"><img style="max-height: 200px;" src="' + imgurl(image.url) + '" alt="' + product.name + '"></div>';
                c += '<a href="' + image.url + '" data-fancybox="' + product.name + '" data-caption="' + image.name + '"><img src="' + image.url + '" alt="' + product.name + '" class="img-icon shadow-sm" style="margin-right: 5px;"></a>';
            });

            return c;
        }

        function bindValue(produit = null) {
            if (produit) {
                $("#p-name").text(produit.name);
                $("#p-price").text(numberWithCommas(produit.price_ht));
                $("#p-poids").text(produit.poids_net + " / " + product.poids_brut);
                $("#p-pack").text(produit.pack);
            } else {
                $.each($("span.spaninfo"), function(i, item) {
                    $(item).text('');
                });
            }
        }

        function noImages(message = "Aucune image pour ce produit") {
            if ($('#images').hasClass('slick-initialized')) {
                $('#images').slick('unslick');
            }
            $('#images').html(' ');
            return '<div class="jumbotron animated fadeIn text-center" id="infoproduit">' +
                message + '</div>';
        }

        function buildOrderLine(product) {
            nb++;
            let ligne = {
                price_ht: product.price_ht,
                quantite: q,
                amount_ht: (product.price_ht * q),
                product_id: product_id
            };
            lignes.push(nb);
            total += Number(ligne.amount_ht);
            $("#total").text(numberWithCommas(total));
            if(lignes.length > 0) {
                    if(!$('#line_info').hasClass('hidden')) $('#line_info').addClass('hidden');
                }
                return '<tr class="col-lg-4 col-md-6 animated fadeIn">'+
                        '<input type="hidden" name="price_ht' + nb + '" value="' + ligne.price_ht + '" />' +
                        '<input type="hidden" name="q' + nb + '" value="' + q + '" />' +
                        '<input type="hidden" name="product_id' + nb + '" value="' + ligne.product_id + '" />' +
                        '<input type="hidden" name="amount_ht' + nb + '" value="' + ligne.amount_ht + '" />' +
                        '<td>' + product.code + '</td>'+
                        '<td>' + product.name + '</td>'+
                        '<td>' + product.pack + '</td>'+
                        '<td>' + numberWithCommas(product.price_ht) + ' x '+ numberWithCommas(q) +'</td>'+
                        '<td id="total' + nb + '" data-amount="'+ligne.amount_ht+'">' + numberWithCommas(ligne.amount_ht) + '</td>'+
                        '<td class="text-center">'+
                            '<i class="fas fa-times text-danger mr-2 pl-1 pr-2 isup" id="isup' + nb + '" data-id="' + nb + '" style="cursor:pointer"></i>'+
                        '</td>'+
                    '</tr>';
        }

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        }

        function getById(tableau, id) {
            const item = tableau.find(
                v => {
                    return v.id === id
                }
            )
            return item;
        }

        function customalert(msg, type) {
            if (type == 'bg-success txt-blanc') {
                $("#alert").html('<i class="fas fa-check"></i> ' + msg);
            } else {
                $("#alert").html('<i class="fas fa-times"></i> ' + msg);
            }

            $("#alert").removeClass('hidden lightSpeedOut').addClass('lightSpeedIn ' + type);
            setTimeout(
                () => {
                    $("#alert").removeClass('lightSpeedIn ' + type).addClass('lightSpeedOut hidden');
                }, 6000
            )
        }
    });
</script>
<?php $this->end(); ?>
