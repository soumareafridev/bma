<?php $this->start('css'); ?>
<?= $this->Html->css('slick.css') ?>
<?php $this->end(); ?>
<!-- Page Heading -->
<div class="col-md-12">
    <div class="d-sm-flex align-items-center justify-content-start mb-2 mt-2">
        <h1 class="h4 text-gray-800">
        <a href="javascript:history.back()" class="btn btn-sm btn-primary text-white px-3 text-white mr-2"><i class="fas fa-arrow-left"></i></a>
            Consultation Produit - <small class="h6">N° <?= $product->id ?></small>
        </h1>
    </div>
</div>

<div class="col-md-12">
    <div class="row">
        <div class="col-md-12">
            <!-- Collapsable Card Example -->
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseModif" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseModif">
                    <h6 class="m-0 font-weight-bold text-primary">Les informations du produit</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseModif">
                    <div class="card-body row">
                        <div class="list-group list-pois col-md-6">
                            <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                <span class="text-gray-600">Nom Produit</span>
                                <span class="bolder"><?= h($product->name) ?></span>
                            </li>
                            <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                <span class="text-gray-600">Prix HT</span>
                                <span class="bolder"><?= $this->Number->format(h($product->price_ht),['locale' => "fr_FR", 'after' => ' FCFA']) ?></span>
                            </li>
                            <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                <span class="text-gray-600">Gamme du produit</span>
                                <span class="bolder"><?= h($product->gamme->name) ?></span>
                            </li>
                            <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                <span class="text-gray-600">Statut du Produit</span>
                                <?= $product->status == 1 ? "<span><span class='badge bg-success text-white'>Actif</span></span>" : "<span><span class='badge bg-danger'>Inactif</span></span>"  ?>
                            </li>
                            <li class="list-group-item li-detail-item">
                                <Label>Description</Label>
                                <div class="description">
                                    <?= $product->description ?>
                                </div>
                            </li>
                        </div>
                        <div class="col-md-6">
                            <?php if(count($product->images) > 0) : ?>
                            <div class="img-slides text-center">
                                <?php foreach($product->images as $image): ?>
                                    <div class="img"><img src="<?= $this->Url->image($image->url) ?>" style="max-height: 280px;" alt="<?= $product->name ?>" class="radius shadow-sm"></div>
                                <?php endforeach; ?>
                            </div>
                            <?php else: ?>
                            <div class="jumbotron text-center">
                                <h3>Pas d'images pour ce produit!</h3>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseList" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseList">
                    <h6 class="m-0 font-weight-bold text-primary">Toutes les images </h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseList">
                    <div class="card-body">
                        <?php if(count($product->images) > 0) : ?>
                        <?php foreach($product->images as $image): ?>
                            <a href="<?= $this->Url->image($image->url) ?>" data-fancybox="<?=h($product->name) ?>" data-caption="<?=h($image->name) ?>"><img src="<?= $this->Url->image($image->url) ?>" alt="<?= $product->name ?>" class="img-icon shadow-sm"></a>
                        <?php endforeach; ?>
                        <?php else : ?>
                        <div class="jumbotron text-center">
                            <h3>Aucune photo disponible</h3>
                        </div>
                        <?php endif; ?>
                        <div class="form-group mt-4 text-right">
                            <a href="<?= $this->Url->Build(['controller' => 'Products', 'action' => 'edit', $product->id]) ?>" class="btn btn-sm btn-info ml-2">
                                <i class="fas fa-edit mr-2"></i> Modifier
                            </a>
                            <?= $this->Form->postLink(__('<i class="fas fa-trash-alt mr-2"></i> Supprimer'), ['controller' => 'Products', 'action' => 'delete', $product->id], ['escape' => false, 'class' => 'btn btn-danger btn-sm mx-1', 'confirm' => __('Voulez vous supprimer le produit suivant :  {0}?', $product->name)]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->start('script_bottom'); ?>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <?= $this->Html->script('vendor/slick.min.js');?>
    <script>
        $(function() {
            $('.img-slides').slick({
                arrows: false,
                infinite: true,
                autoplay: true,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1
            });
        });
    </script>
<?php $this->end(); ?>
