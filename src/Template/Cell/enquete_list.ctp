<div class="col-lg-12 mt-1">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 mt-2 customer__header bg-primary text-white">
        <h1 class="h4 text-gray-800">Liste des Enquêtes</h1>
        <!-- Topbar Search -->
        <a href="<?= $this->Url->build(['controller'=>'surveys','action'=>'add']) ?>" class="btn-sm btn-primary text-white">Ajouter un utilisateur</a>
    </div>
    <!-- DataTales Example -->
    <div class="table-responsive shadow p-4">
        <table class="table table-borderless table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Intitulé</th>
                <th>Type POI</th>
                <th>Formulaire</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Intitulé</th>
                <th>Type POI</th>
                <th>Formulaire</th>
                <th>Actions</th>
            </tr>
            </tfoot>
            <tbody>
                <?php foreach ($surveys as $survey) : ?>
                    <tr>
                        <td><?= $survey->name; ?></td>
                        <td><?= $survey->type_pois->name; ?></td>
                        <td><?= $survey->form->name; ?></td>
                        <td>
                            <a href="<?= $this->Url->Build(['controller' => 'surveys', 'action' => 'edit', $survey->id]) ?>" class="bg-white shadow-sm btn-sm mx-1 text-success">
                                    <i class="fas fa-edit"></i>
                            </a>
                            <a href="<?= $this->Url->Build(['controller' => 'surveys', 'action' => 'consulter', $survey->id]) ?>" class="bg-white shadow-sm btn-sm mx-1 text-primary">
                                    <i class="fas fa-eye"></i>
                            </a>
                            <?= $this->Form->postLink(__('<i class="fas fa-trash-alt"></i>'), ['controller' => 'Surveys', 'action' => 'delete', $survey->id], ['escape' => false, 'class' => 'bg-white shadow-sm btn-sm mx-1 text-danger', 'confirm' => __('Voulez vous supprimer l\'enquête suivante:  {0}?', $survey->name)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <form class="" action="<?= $this->Url->Build(['controller' => 'Surveys','action' => "add"]) ?>" method="post">
            <table class="table table-borderless table-hover table-striped">
                <tr>
                    <td>
                        <input type="text" name="name" class="form-control bg-white" required placeholder="Intitulé">
                    </td>
                    <td>
                        <?= $this->Form->select('type_poi_id', $type_pois,['class' => 'form-control','empty' => 'Type de POIs']); ?>
                    </td>
                    <td>
                        <?= $this->Form->select('form_id', $forms,['class' => 'form-control','empty' => 'Choix du formulaire']); ?>
                    </td>
                    <td>
                        <button type="submit" class="btn btn-primary text-white">Ajouter <i class="fas fa-plus-circle ml-3"></i></button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

<?php $this->start('script_bottom'); ?>
    <?= $this->Element('Components/exportScript') ?>
<?php $this->end(); ?>
