<div class="col-md-12 mt-3">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 mt-2 customer__header bg-primary text-white">
        <h1 class="h4 text-gray-800">Liste des produits</h1>
        <!-- Topbar Search -->
        <a href="<?= $this->Url->build(['controller'=>'Products','action'=>'add']) ?>" class="btn-sm btn-primary text-white">Ajout produit<i class="fas fa-plus-circle ml-2"></i></a>
    </div>
    <!-- DataTales Example -->
    <div class="table-responsive shadow p-4">
        <table class="table table-borderless table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Code</th>
                <th>Nom commercial</th>
                <th>Gamme</th>
                <th>PRIX TTC</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Code</th>
                <th>Nom commercial</th>
                <th>Gamme</th>
                <th>PRIX TTC</th>
                <th>Actions</th>
            </tr>
            </tfoot>
            <tbody>
                <?php foreach ($products as $product) :?>
                <tr>
                        <td><?= $product->code ?></td>
                        <td><?= $product->name; ?></td>
                        <td><?= $product->gamme->name ?></td>
                        <td><?= $this->Number->format($product->price_ht,['locate' => 'fr_FR']); ?></td>
                        <td>
                            <a href="<?= $this->Url->Build(['controller' => 'Products', 'action' => 'edit', $product->id]) ?>" class="bg-white shadow-sm btn-sm mx-1">
                                    <i class="fas fa-edit text-success"></i>
                            </a>
                            <a href="<?= $this->Url->Build(['controller' => 'Products', 'action' => 'view', $product->id]) ?>" class="bg-white shadow-sm btn-sm mx-1">
                                <i class="fas fa-eye text-primary"></i>
                            </a>
                            <?= $this->Form->postLink(__('<i class="fas fa-trash-alt text-danger"></i>'), ['controller' => 'Products', 'action' => 'delete', $product->id], ['escape' => false, 'class' => 'bg-white shadow-sm btn-sm mx-1', 'confirm' => __('Voulez vous supprimer le produit suivant :  {0}?', $product->name)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?php $this->start('script_bottom'); ?>
<!-- Page le*vel plugins -->
    <?= $this->Element('Components/exportScript') ?>
<?php $this->end(); ?>
