<?= $this->Html->css('select2.min.css', ['block' => 'css']) ?>
<?= $this->Html->css('slick.css', ['block' => 'css2']) ?>
<div class="col-md-12 mt-3">
    <div class="d-sm-flex align-items-center justify-content-between mb-2 mt-2">
        <h1 class="h4 text-gray-800 mb-0">
            <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-sm btn-primary text-white px-3 text-white mr-2"><i class="fas fa-arrow-left"></i></a>
            Consulter commande N° : <?= $order->id ?>
        </h1>
        <?php if($order->type == 1 || $order->type == 2) { ?>
            <a href="<?= $this->Url->build(['controller'=>'Orders','action'=>'distribuer', $order->id]) ?>" class="btn-sm btn-primary text-white">Nouvelle distribution<i class="fas fa-share-alt ml-2"></i></a>
        <?php } ?>
    </div>
</div>
<div class="col-md-12">
    <div class="row">
        <div class="col-md-12">
            <!-- Collapsable Card Example -->
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseModif" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseModif">
                    <h4 class="m-0 font-weight-bold text-primary"><i class="fas fa-shopping-cart mr-4"></i> Information de la commande et du client</h4>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseModif">
                    <div class="card-body row">
                        <div class="col-md-6">
                            <div class="list-group list-pois p-1">
                                <li class="list-group-item li-detail-item shadow-sm">
                                    <span class="text-gray-600">Type de distribution</span><br>
                                    <span class="bolder text-primary">
                                        <?= $order->type > 0 ? ($order->type == 1 ? 'Manuelle' : 'Automatique') : 'Unique' ?>
                                    </span>
                                </li>
                                <li class="list-group-item li-detail-item shadow-sm">
                                    <span class="text-gray-600">Créée le</span><br>
                                    <span class="bolder text-primary">
                                        <?= $order->created->nice('Africa/Dakar','fr-Fr') ?>
                                    </span>
                                </li>
                                <li class="list-group-item li-detail-item shadow-sm">
                                    <span class="text-gray-600">Créée par</span><br>
                                    <span class="bolder text-primary">
                                        <?= $order->user->fn . ' ' .$order->user->ln ?> <small class="badge badge-primary"><?= $order->user->phone ?></small>
                                    </span>
                                </li>
                                <li class="list-group-item li-detail-item shadow-sm">
                                    <span class="text-gray-600">Montant HT</span><br>
                                    <span class="bolder text-primary spaninfo"><?= $this->Number->format($order->amount, ['locate' => 'fr_FR', 'after' => ' FCFA']); ?></span>
                                </li>
                                <li class="list-group-item li-detail-item shadow-sm">
                                    <span class="text-gray-600">Etat</span><br>
                                    <span class="bolder text-primary spaninfo"><?= $order->order_statut !== null ? $order->order_statut->name : "<small><i>Non défini</i></small>" ?></span>
                                </li>
                                <?php if ($order->type == 1 || $order->type == 2) : ?>
                                    <li class="list-group-item li-detail-item shadow-sm">
                                        <span class="text-gray-600">Date de début</span><br>
                                        <span class="bolder text-primary spaninfo"><?= $order->date_debut !== null ? $order->date_debut->i18nFormat('dd-MM-yyyy') : "Non - définie" ?></span>
                                    </li>
                                    <li class="list-group-item li-detail-item shadow-sm">
                                        <span class="text-gray-600">Date de fin</span><br>
                                        <span class="bolder text-primary spaninfo"><?= $order->date_limited !== null ? $order->date_limited->i18nFormat('dd-MM-yyyy')  : 'Non - définie' ?></span>
                                    </li>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="list-group list-pois p-1">
                                <li class="list-group-item li-detail-item shadow-sm">
                                    <span class="text-gray-600">Type de compte client</span><br>
                                    <span class="bolder spaninfo text-primary"><?= $order->poi_id !== null ? 'Personne morale' : 'Personne physique' ?></span>
                                </li>
                                <li class="list-group-item li-detail-item shadow-sm">
                                    <span class="text-gray-600">Nom du client</span><br>
                                    <span class="bolder text-primary spaninfo"><?= $order->poi_id !== null ? $order->pois->dns : $order->poi_customer->fn . ' ' . $order->poi_customer->fn ?></span>
                                </li>
                                <li class="list-group-item li-detail-item shadow-sm">
                                    <span class="text-gray-600">Numéro de téléphone</span><br>
                                    <span class="bolder text-primary spaninfo"><?= $order->poi_id !== null ? $order->pois->phone : $order->poi_customer->phone ?></span>
                                </li>
                                <?php if ($order->poi_customer_id !== null) : ?>
                                    <li class="list-group-item li-detail-item shadow-sm">
                                        <span class="text-gray-600">Entreprise</span><br>
                                        <span class="bolder text-primary spaninfo"><?= $order->poi_customer->pois->dns ?></span>
                                    </li>
                                <?php endif; ?>
                                <li class="list-group-item li-detail-item shadow-sm">
                                    <span class="text-gray-600">Type de point d'interêt</span><br>
                                    <span class="bolder text-primary spaninfo"><?= $order->poi_id !== null ? $order->pois->type_pois->name : $order->poi_customer->pois->type_pois->name ?></span>
                                </li>
                            </div>
                        </div>
                        <?php if (($order->type == 1 || $order->type == 2) && $order->frequence_id) :  ?>
                            <div class="col-md-12">
                                <div class="list-group list-pois p-1">
                                    <li class="list-group-item px-0">
                                        <table class="table table-condensed table-striped" id="dataTable" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th colspan="2" class="text-center bg-primary text-white">Informations livraison</th>
                                                </tr>
                                            </thead>
                                            <tbody class="text-center">
                                                <tr>
                                                    <td>Fréquences</td>
                                                    <td class="alert alert-success bold">
                                                        <i class="fas fa-check mr-2"></i> <?= $order->frequence->name ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <?php $jours = explode(";", $order->frequences);
                                                    $njours = count($jours); ?>
                                                    <td class="bg-info text-white" <?= $njours > 1 ? "rowspan=$njours" : "" ?>>
                                                        <i class="fas fa-calendar-check fa-2x"></i> <br>
                                                        Jour(s) de livraison
                                                    </td>
                                                    <td class="alert alert-info"><?= $_jours[$jours[0]]; ?></td>
                                                </tr>
                                                <?php if ($njours > 1) : for ($i = 1; $i < $njours; $i++) : ?>
                                                        <tr>
                                                            <td class="alert alert-info"><?= $_jours[$jours[$i]] ?></td>
                                                        </tr>
                                                <?php endfor;
                                                endif; ?>
                                            </tbody>
                                        </table>
                                    </li>
                                </div>
                            </div>
                            <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseList" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseList">
                    <h6 class="m-0 font-weight-bold text-primary">Lignes de commande</h6>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseList">
                    <div class="card-body">
                        <div class="table-responsive shadow">
                            <table class="table table-borderless table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Produit</th>
                                        <th>Gamme</th>
                                        <th>Prix HT</th>
                                        <th>Qauntité</th>
                                        <th>Q Livrée</th>
                                        <th>Q Maximale</th>
                                        <th>Total HT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;
                                    foreach ($order->order_lines as $line) : ?>
                                        <tr>
                                            <td><?= $i ?></td>
                                            <td><?= $line->product->name ?></td>
                                            <td><?= $line->product->gamme->name ?></td>
                                            <td><?= $this->Number->format($line->price_ht, ['locale' => 'fr_FR', 'after' => ' FCFA']) ?></td>
                                            <td><?= $line->quantite ?></td>
                                            <td><?= $line->quantite_livre == null || $line->quantite_livre < 0 ? 0 : $line->quantite_livre ?></td>
                                            <td><?= $line->quantite_max == null || $line->quantite_max < 0 ? 0 : $line->quantite_max ?></td>
                                            <td><?= $this->Number->format(($line->price_ht * $line->quantite), ['locale' => 'fr_FR', 'after' => ' FCFA']) ?></td>
                                        </tr>
                                    <?php $i++;
                                    endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($order->type == 1 || $order->type == 2) : ?>
            <div class="col-md-12">
                <div class="card shadow mb-4">
                    <!-- Card Header - Accordion -->
                    <a href="#collapseDistribution" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseDistribution">
                        <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-share-alt mr-1"></i> Distributions de la commande</h6>
                    </a>
                    <!-- Card Content - Collapse -->
                    <div class="collapse show" id="collapseDistribution">
                        <div class="card-body">
                            <?php if(isset($order->distributions) && count($order->distributions) >0) : ?>
                            <table class="table table-borderless table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Montant</th>
                                        <th>Client</th>
                                        <th>Date Création</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>N°</th>
                                        <th>Montant</th>
                                        <th>Client</th>
                                        <th>Date Création</th>
                                        <th>Actions</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php foreach ($order->distributions as $distribution) : ?>
                                        <tr>
                                            <td><?= $distribution->id; ?></td>
                                            <td><?= $this->Number->format($distribution->amount, ['locate' => 'fr_FR']); ?></td>
                                            <td><?= $distribution->poi_id !== null ? $distribution->pois->dns : $distribution->poi_customer->fn . " " . $distribution->poi_customer->ln ?></td>
                                            <td><?= $distribution->created->nice('Africa/Dakar','fr-Fr') ?></td>
                                            <td>
                                                <a href="<?= $this->Url->Build(['controller' => 'Distributions', 'action' => 'view', $distribution->id]) ?>" class="bg-white shadow-sm btn-sm mx-1">
                                                    <i class="fas fa-eye text-primary"></i>
                                                </a>
                                                <?= $this->Form->postLink(__('<i class="fas fa-trash-alt text-danger"></i>'), ['controller' => 'Distributions', 'action' => 'delete', $distribution->id], ['escape' => false, 'class' => 'bg-white shadow-sm btn-sm mx-1', 'confirm' => __('Voulez vous supprimer la distribution n° :  {0}?', $distribution->id)]) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <?php else : ?>
                                <div class="text-center jumbotron">
                                    Aucune distribution n'a été faite pour cette commande !
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
