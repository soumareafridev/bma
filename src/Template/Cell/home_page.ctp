<?php $this->layout = 'accueil'; ?>
    <div class="col-md-7 col-lg-8 pt-5" style="min-height: calc(100vh - (37px));max-height: calc(100vh + (200px));overflow-y: auto">
        <!-- Page Heading -->
        <?= $this->Element('Navs/search-bar') ?>

        <!-- Content Row -->
        <div class="row">
            <div class="list-group col-md-12 list-pois">
                <?php if(sizeof($pois) > 0): for ($i=0; $i <sizeof($pois) ; $i++): ?>
                <li class="list-group-item d-sm-flex align-items-center justify-content-between marker" id="marker<?= $i ?>" data-created="<?= $pois[$i]->created->nice('Africa/Dakar','fr-Fr') ?>" data-pin="<?= $this->Url->image('md-pin.png') ?>"  data-id="<?= $pois[$i]->id ?>" data-dns="<?= $pois[$i]->dns ?>" data-lng="<?= $pois[$i]->lng ?>" data-lat="<?= $pois[$i]->lat ?>" data-img="<?= $pois[$i]->photo !== null ? $this->Url->image('pois/'.$pois[$i]->photo) : null ?>">
                    <p class="eye"><i class="fas fa-eye icon-circle-blue" style="cursor:pointer"></i></p>
                    <div class="info">
                        <h3 class="h5 bold mb-0" style="font-size: 14px;"><?= $pois[$i]->dns; ?></h3>
                        <small>Distributeur: <?= $pois[$i]->infos[0]->value; ?> </small>
                    </div>
                    <div class="info-sup">
                        <span>
                            <span class="marqueur-blue"><?= $pois[$i]->type_pois->name; ?> </span> <br>
                            <small>Enregistré le <?= $pois[$i]->created->nice('Africa/Dakar','fr-Fr') ?></small>
                        </span>
                    </div>
                    <a href="<?= $this->Url->Build(['controller' => 'Pois','action' => 'view', $pois[$i]->id]) ?>"><i class="fas fa-arrow-right"></i></a>
                </li>
                <?php endfor; else: ?>
                    <div class="jumbotron text-center">
                        <h1 class="h2 text-gray-700">
                            Aucun enregistrement pour le moment.
                        </h1>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="col-md-5 col-lg-4 hidden-sm bg-primary text-white text-center" style="max-height: calc(100vh - (200px));min-height: calc(100vh - (37px));overflow:hidden" id="map"></div></div>

<?php $this->start('script_bottom'); ?>
    <?= $this->Html->script('main.js');?>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<?php $this->end(); ?>
