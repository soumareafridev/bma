
<div class="col-lg-12 mt-1">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 mt-2 customer__header bg-primary text-white">
        <h1 class="h4 text-gray-800">Gestion Enquêtes</h1>
    </div>
    <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapseList" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseList">
            <h6 class="m-0 font-weight-bold text-primary">Liaison entre Equipe et Enquête</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse show" id="collapseList">
            <div class="card-body">
                   <!-- DataTales Example -->
                <div class="table-responsive shadow p-4">
                    <table class="table table-borderless table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Enquête</th>
                                <th>Equipe</th>
                                <th>Formulaire</th>
                                <th>Type POI</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($gestions as $gestion) : $survey = $gestion->survey;$team = $gestion->team;?>
                                <tr>
                                    <td><?= $gestion->id; ?></td>
                                    <td><?= $survey->name; ?></td>
                                    <td><?= $team->name; ?></td>
                                    <td><?= $survey->form->name; ?></td>
                                    <td><?= $survey->type_pois->name; ?></td>
                                    <td>
                                        <?= $this->Form->postLink(__('<i class="fas fa-trash-alt"></i>'), ['controller' => 'Gestions', 'action' => 'delete', $gestion->id], ['escape' => false, 'class' => 'btn-sm btn txt-blanc lighter btn-danger', 'confirm' => __('Voulez vous supprimer l\'élément :  ID: {0}?', $gestion->id)]) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <form class="card-footer" method="post" action="<?= $this->Url->Build(['controller' => 'Gestions','action' => "add"]) ?>">
                <table class="table table-borderless">
                    <tr>
                        <td>
                            <?= $this->Form->select('survey_id', $surveys,['required','class' => 'form-control','required', 'empty' => 'Choisir une enquête']); ?>
                        </td>
                        <td>
                        <?= $this->Form->select('team_id', $teams,['required','class' => 'form-control','required', 'empty' => 'Choisir une équipe']); ?>
                        </td>
                        <td>
                            <button type="submit" class="btn btn-block btn-primary"><i class="fas fa-plus mr-2"></i>Démarrer</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>

<?php $this->start('script_bottom'); ?>
    <?= $this->Element('Components/exportScript') ?>
<?php $this->end(); ?>
