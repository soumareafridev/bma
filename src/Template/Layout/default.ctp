<?php
$cakeDescription = 'SCOD: Enquete POI';
?>
<!DOCTYPE html>
<html lang="fr">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?= $cakeDescription ?></title>

  <!-- Custom fonts for this template-->
  <link href="/js/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" integrity="sha256-ygkqlh3CYSUri3LhQxzdcm0n1EQvH2Y+U5S2idbLtxs=" crossorigin="anonymous" />
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <!-- <link href="css/sb-admin-2.css" rel="stylesheet"> -->
  <?= $_company->theme_id == null ? $this->Html->css('themes/theme-default.css') : $this->Html->css('themes/'.$_company->theme->css) ?>
    <style>
        .table-responsive.p-4 {
            border-radius: 0 0 16px 16px;
        }
        .customer__header {
            border-radius: 16px 16px 0 0;
            padding: 12px 8px;
        }
        .customer__header .text-gray-800 {
            color: #fff !important;
            text-transform: uppercase;
            font-weight: bold;
            margin-bottom: 0;
            font-size: 1.15rem;
        }
        .dt-button {
            border: none;
            border-radius: 8px;
            box-shadow: 0 1px 3px -1px #444;
        }
    </style>

   <!-- Custom styles for this page -->
   <link href="/js/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
   <?= $this->fetch('css') ?>
   <?= $this->fetch('css2') ?>
</head>

<body id="page-top" class="sidebar-toggled">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?= $this->Element('Navs/sidebar') ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content" style="overflow: hidden;">

        <!-- Topbar -->
        <?= $this->Element('Navs/topbar') ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid" style="height: 100%">
            <?= $this->Flash->render() ?>
          <div class="row" style="height: auto">
            <?= $this->fetch('content') ?>
        </div>
        <!-- /.cntainer-fluid-->
      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; SCOD - Groupe Afridev SAS</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Bootstrap core JavaScript-->
  <?= $this->Html->script('vendor/jquery/jquery.min.js');?>
  <?= $this->Html->script('vendor/bootstrap/js/bootstrap.bundle.min.js');?>

  <!-- Core plugin JavaScript-->
  <?= $this->Html->script('vendor/jquery-easing/jquery.easing.min.js');?>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>

  <!-- Custom scripts for all pages-->
  <?= $this->fetch('script'); ?>
  <?= $this->Html->script('sb-admin-2.min.js');?>
  <?= $this->fetch('script_bottom'); ?>
</body>

</html>

