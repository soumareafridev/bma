<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SCOD : <?= $this->fetch('title') ?></title>
      <!-- Custom fonts for this template-->
    <link href="/js/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">

    <!-- Custom styles for this template-->
    <?= $this->Html->css('themes/theme-default.css') ?>

    <?= $this->fetch('css'); ?>

</head>
<body>
    <div class="container-fluid p-0">
        <div class="row m-0">
            <?= $this->fetch('content') ?>
        </div>
    </div>

    <!-- Jquery core JavaScript-->
  <?= $this->Html->script('vendor/jquery/jquery.min.js');?>
  <?= $this->fetch('script_bottom'); ?>
</body>
</html>