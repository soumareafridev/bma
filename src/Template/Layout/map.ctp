<?php
$cakeDescription = 'SCOD: Enquete POI';
?>
<!DOCTYPE html>
<html lang="fr">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?= $cakeDescription ?></title>

  <!-- Custom fonts for this template-->
  <link href="/js/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" integrity="sha256-ygkqlh3CYSUri3LhQxzdcm0n1EQvH2Y+U5S2idbLtxs=" crossorigin="anonymous" />
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <!-- <link href="css/sb-admin-2.css" rel="stylesheet"> -->
  <?= $_company->theme_id == null ? $this->Html->css('themes/theme-default.css') : $this->Html->css('themes/'.$_company->theme->css) ?>

   <!-- Custom styles for this page -->
   <link href="/js/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top" class="sidebar-toggled">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?= $this->Element('Navs/sidebar2') ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Begin Page Content -->
        <div class="container-fluid p-0" style="min-height: calc(100vh - (37px));overflow-y:auto">
            <?= $this->Flash->render() ?>
          <div class="row m-0 p-0" style="height: auto">
            <?= $this->fetch('content') ?>
        </div>
        <!-- /.cntainer-fluid-->
      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; SCOD - Groupe Afridev SAS</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Bootstrap core JavaScript-->
  <?= $this->Html->script('vendor/jquery/jquery.min.js');?>
  <?= $this->Html->script('vendor/bootstrap/js/bootstrap.bundle.min.js');?>

  <!-- Core plugin JavaScript-->
  <?= $this->Html->script('vendor/jquery-easing/jquery.easing.min.js');?>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>

  <!-- Custom scripts for all pages-->
  <?= $this->Html->script('sb-admin-2.min.js');?>
  <?= $this->fetch('script_bottom'); ?>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5z3yow8LqTpw3WlqBmWpvYYPl1i3mtLM&libraries=places&callback=initMap2"></script>

</body>

</html>