
<!-- Page Heading -->
<div class="col-md-12">
    <div class="d-sm-flex align-items-center justify-content-between mb-4 mt-2 customer__header bg-primary text-white">
        <h1 class="h4 text-gray-800">
        <a href="javascript:history.back()" class="btn btn-sm btn-primary text-white px-3 text-white mr-2"><i class="fas fa-arrow-left"></i></a>
            Contacts : Ajout d'un contact
        </h1>
    </div>
</div>

<div class="col-md-12">
    <!-- Collapsable Card Example -->
    <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapseActeur" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseActeur">
            <h6 class="m-0 font-weight-bold text-primary">Les types de contact</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse show" id="collapseActeur">
        <form class="card-body" action="<?= $this->Url->Build(['controller' => 'PoiCustomers', 'action' => 'add']) ?>" method="post">
            <div class="form-group">
                <label for="">Type du contact</label>
                <?= $this->Form->select('customer_type_id', $list_types,['class' => 'form-control','empty' => 'Séléctionner le type','required']); ?>
            </div>
            <div class="form-group">
                <label for="">Prénom</label>
                <input type="text" name="fn" class="form-control form-control-sm"  required placeholder="Prénom">
            </div>
            <div class="form-group">
                <label for="">Nom de famille</label>
                <input type="text" name="ln" class="form-control form-control-sm"  required placeholder="Nom">
            </div>
            <div class="form-group">
                <label for="">Téléphone</label>
                <input type="text" name="phone" class="form-control form-control-sm"  required placeholder="Téléphone">
            </div>
            <div class="form-group">
                <label for="poi_id">POI lié</label>
                <select name="poi_id" id="poi_id" class="form-control form-control-sm" >
                    <option selected>Séléctionner le POIs</option>
                    <?php if(count($list_pois) > 0): foreach($list_pois as $item):?>
                    <option value="<?= $item->id ?>"><?= $item->name ?></option>
                    <?php endforeach; endif; ?>
                </select>
                <!-- <?= $this->Form->select('poi_id', $list_pois,['class' => 'form-control','required','empty' => 'Séléctionner le POIs']); ?> -->
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-sm btn-primary text-white text-white">Ajouter <i class="fas fa-plus-circle ml-3"></i></button>
            </div>
        </form>
        </div>
    </div>
</div>
