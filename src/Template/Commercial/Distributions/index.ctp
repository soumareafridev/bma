<?php $this->layout = 'accueil';?>
    <div class="col-md-12 col-lg-12 pt-5" style="min-height: calc(100vh - (37px));overflow-y: auto">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h3 class="text-primary h1 mb-0 bolder" style="line-height: .7">
                Distributions
            </h3>
        </div>

        <!-- Content Row -->
        <div class="row">
            <div class="col-xl-6 col-md-6 mb-4">
                <a href="<?= $this->Url->Build(['controller' => 'Distributions', 'action' => 'generation', '?' => $this->Vue->hashUrl(2)]) ?>" class="card border-left-warning bg-gradient-primary text-white shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-white text-uppercase mb-1">Génération Automatique</div>
                                <div class="h4 mb-0 font-weight-bold text-white">Distributions</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-cogs fa-2x text-warning"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-xl-6 col-md-6 mb-4">
                <a href="<?= $this->Url->Build(['controller' => 'Distributions', 'action' => 'generation', '?' => $this->Vue->hashUrl(1)]) ?>" class="card border-left-warning bg-gradient-info text-white shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-white text-uppercase mb-1">Génération Manuelle</div>
                                <div class="h4 mb-0 font-weight-bold text-white">Distributions</div>
                            </div>
                            <div class="col-auto">
                                <i class="text-warning fas fa-cog fa-2x"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <hr class="col-pull-1 col-11 separator mb-4 mt-0">
             <?php foreach($type_distributions as $type) : ?>
            <!-- distributions -->
            <div class="col-xl-6 col-md-6 mb-4">
                <a href="<?= $this->Url->Build(['controller' => 'Distributions', 'action' => 'visualiser', '?' => $this->Vue->hashUrl($type->id)]) ?>" class="card border-left-primary shadow-sm h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><?= $type->name ?></div>
                            <div class="h4 mb-0 font-weight-bold text-gray-800"><?= $this->Number->format(count($type->distributions),['locale' => 'fr_FR']) ?></div>
                            </div>
                            <div class="col-auto">
                            <i class="fas fa-share-alt fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
    <?php $this->start('script_bottom'); ?>
        <?= $this->Html->script('main.js');?>
        <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <?php $this->end(); ?>
