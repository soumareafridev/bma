<?php

use Cake\Routing\Router;

    $active = Router::normalize($this->request->getAttribute('here'));
    $id = (int) $this->request->getQuery('i');
 ?>
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="text-primary h2 mb-0 bolder" style="line-height: .7">Cartographie 
        <br> <small class="h6 text-black-50 m-0" style="font-size:14px"><?= $this->Vue->getInfoSite($_user->company_id,'slogan') ?></small>
    </h1>
    <div class="link-active">
        <select name="" id="" class="form-control" onchange="location = this.value;">
            <?php foreach($_typePois as $_type) : $url = $this->Url->Build(['controller' => 'Pois', 'action' => 'pois-enregistres', '?' => $this->Vue->hashUrl($_type->id)]); ?>
                <option value="<?= $url ?>" <?= $id == $_type->id ? 'selected' : '' ?>><?= $_type->name ?></option>
            <?php endforeach;?>
        </select>
    </div>
    <!-- Topbar Search -->
    <!-- <form class="d-none d-sm-inline-block form-inline bg-white">
        <div class="input-group">
            <input type="text" class="form-control bg-light border-0 small bg-white" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
            <div class="input-group-append">
            <button class="btn btn-primary text-white" type="button">
                <i class="fas fa-search fa-sm"></i>
            </button>
            </div>
        </div>
    </form> -->
</div>