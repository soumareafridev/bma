<?php 
  use Cake\Routing\Router;
  $current = Router::normalize($this->request->getAttribute('here'));
  $active = explode("/",$current)[1];
  // dd();
?>
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar" style="position: relative">

<!-- Sidebar - Brand -->
<a class="text-danger bg-white btn-sm shadow-sm text-right px-1" style="position: absolute; top: 4px; right: -18px;z-index: 100" href="<?= $this->Url->Build(['controller' => 'Users', 'action' => 'logout']) ?>" >
  Exit<i class="fas fa-sign-out-alt mr-1 ml-2"></i>
</a>

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand bg-white" href="<?= $this->Url->Build(['controller' => 'Dashboard', 'action' => 'index']) ?>" style="min-height: 120px">
    <?php if ($_company->logo) : ?>
      <img src="<?= $this->Url->image($_company->logo) ?>" style="max-width: 87px">
    <?php else :  ?>
      <b class="h2 bolder text-rose"><?= $this->Vue->getInfoSite($_user->company_id, 'short_name') ?></b> <br>
      <small class="text-primary text-capitalize bolder" style="display: block;max-width: 100px;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;"><?= $this->Vue->getInfoSite($_user->company_id, 'one_word') ?></small>
    <?php endif;  ?>
  </a>


<li class="nav-item p-1 d-sm-flex align-items-center justify-content-center">
    <select style="border:none;font-size: 12px;" class="form-control shadow-sm text-primary dropdown-toggle" onchange="location = this.value;">
      <?php foreach($_user->roles as $_role): $profilId = $_role->profile_id;
        switch($profilId){
          case 4:
            $path = "admin";
            $url=$this->Url->build(['prefix' => 'admin', 'controller' => "dashboard", 'action' => 'index']);
            break;
          case 3:
            $path = "responsable";
            $url=$this->Url->build(['prefix' => 'responsable', 'controller' => "dashboard", 'action' => 'index']);
            break;
          case 2:
            $path = "commercial";
            $url=$this->Url->build(['prefix' => 'commercial', 'controller' => "dashboard", 'action' => 'index']);
            break;
          case 1:
            $path = "enqueteur";
            $url=$this->Url->build(['prefix' => 'enqueteur', 'controller' => "dashboard", 'action' => 'index']);
            break;
        }
        if($profilId != 5) :
      ?>
        <option value="<?= $url ?>" <?= $path == $active ? 'selected': '' ?> ><?= $_role->profile->name ?></option>
        <?php endif; endforeach; ?>
    </select>
  </li>
<!-- Divider -->
<hr class="sidebar-divider my-0">

<?php if(isset($sidebar)) : ?>
  <?php foreach ($sidebar->principals as $key => $header): ?>
  <li class="nav-item <?=  $current == Router::normalize($this->Url->Build(['controller' => $header->controller, 'action' => $header->action])) || ($current == "/" && $key ==0) ? 'active': '' ?>">
  <a class="nav-link" href="<?= $this->Url->Build(['controller' => $header->controller, 'action' => $header->action]) ?>">
    <i class="fas fa-fw <?= $header->fa ?>"></i>
    <span><?= $header->name ?></span></a>
  </li>
  <!-- Divider -->
  <hr class="sidebar-divider my-0">
  <?php endforeach; ?>

  <?php foreach ($sidebar->secondaires as $key => $second) : ?>
      <!-- Nav Item - Utilities Collapse Menu -->
      <?php if (isset($second->items) && count($second->items) > 0) : ?>
        <li class="nav-item <?= in_array(explode('/', $current)[2], $second->refs) ? "active" : '' ?>">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse<?= $key ?>" aria-expanded="true" aria-controls="collapse<?= $key ?>">
          <?php else : ?>
        <li class="nav-item <?= $current == $this->Url->Build(['controller' => $second->controller, 'action' => $second->action]) ? "active" : '' ?>">
          <a class="nav-link collapsed" href="<?= $this->Url->Build(['controller' => $second->controller, 'action' => $second->action]) ?>">
          <?php endif; ?>
          <i class="fas fa-fw <?= $second->fa ?>"></i>
          <span><?= $second->name ?></span>
          </a>
          <?php if (isset($second->items) && count($second->items) > 0) : ?>
            <div id="collapse<?= $key ?>" class="collapse" aria-labelledby="heading<?= $key ?>" data-parent="#accordionSidebar">
              <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header"><?= $second->header ?></h6>
                <?php foreach ($second->items as $item) : if(isset($item->params)): ?>
                  <a class="collapse-item" href="<?= $this->Url->Build(['controller' => $item->controller, 'action' => $item->action, '?' => $item->params]) ?>"><?= $item->name ?></a>
                  <?php else : ?>
                    <a class="collapse-item" href="<?= $this->Url->Build(['controller' => $item->controller, 'action' => $item->action]) ?>"><?= $item->name ?></a>
                  <?php endif; endforeach; ?>
              </div>
            </div>
          <?php endif; ?>
        </li>
      <?php endforeach; ?>
    <?php endif; ?>
<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline mb-5">
  <!-- <button class="rounded-circle border-0" id="sidebarToggle"></button> -->
  <!-- <a href="<?= $this->Url->Build(['controller' => 'Users', 'action' => 'logout']) ?>" class="btn mt-5 btn-outline-danger text-danger btn-sm border-0 bg-white"><i class="fas fa-sign-out-alt mr-1"></i> Deconnexion</a> -->
</div>
</ul>