<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="message small alert alert-danger mt-2 flash text-center mx-auto" style="max-width: 450px;" onclick="this.classList.add('hidden');">
    <i class="fas fa-info-circle fa-pull-left"></i> <?= $message ?>
</div>
