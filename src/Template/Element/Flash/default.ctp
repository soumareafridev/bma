<?php
$class = 'message';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div style="font-size: 13px" class="small <?= h($class) ?>" onclick="this.classList.add('hidden');"><?= $message ?></div>
