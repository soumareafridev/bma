<?php foreach($type_pois as $type) : ?>
<!-- Points de vente -->
<div class="col-xl-6 col-md-6 mb-4">
    <a href="<?= $this->Url->Build(['controller' => 'Pois', 'action' => 'pois-enregistres', '?' => $this->Vue->hashUrl($type->id)]) ?>" class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
        <div class="row no-gutters align-items-center">
            <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><?= $type->name ?></div>
            <div class="h4 mb-0 font-weight-bold text-gray-800"><?= $this->Number->format(count($type->pois),['locale' => 'fr_FR']) ?></div>
            </div>
            <div class="col-auto">
            <i class="fas fa-map-marked fa-2x text-gray-300"></i>
            </div>
        </div>
        </div>
    </a>
</div>
<?php endforeach; ?>
