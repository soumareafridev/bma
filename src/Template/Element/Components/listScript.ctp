    <!-- Page le*vel plugins -->
    <?= $this->Html->script('vendor/datatables/jquery.dataTables.min.js');?>
    <?= $this->Html->script('vendor/datatables/dataTables.bootstrap4.min.js');?>
    <script type="text/javascript">
        $(document).ready(function() {
        $('#dataTable').DataTable( {
            "language": {
                "lengthMenu": "Affiche _MENU_ enregistrement(s) par page",
                "zeroRecords": "Aucune données - désolé",
                "info": "affichage page _PAGE_ sur _PAGES_",
                "infoEmpty": "Pas de données trouvée",
                "infoFiltered": "(filtered from _MAX_ total records)",
                "paginate": {
                    "previous": "Précédent",
                    "next": "Suivant",
                    "first": "Première",
                    "last": "Dernière"
                }
            }
        } );
    } );
    </script>