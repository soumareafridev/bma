<!-- Page le*vel plugins -->
<?= $this->Html->script('vendor/datatables/jquery.dataTables.min.js');?>
<?= $this->Html->script('vendor/datatables/dataTables.bootstrap4.min.js');?>
<script src='//cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js'></script>
<script src='//cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js'></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js'></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js'></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js'></script>
<script src='//cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js'></script>
<script src='//cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js'></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#dataTable').DataTable( {
        "language": {
            "lengthMenu": "Affiche _MENU_ enregistrement(s) par page",
            "zeroRecords": "Aucune données - désolé",
            "info": "affichage page _PAGE_ sur _PAGES_",
            "infoEmpty": "Pas de données trouvée",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "paginate": {
                "previous": "Précédent",
                "next": "Suivant",
                "first": "Première",
                "last": "Dernière"
            }
        },
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print','pageLength'
        ]
    } );
} );
</script>