        <table class="table table-borderless table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Email</th>
                <th>Téléphone</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($members as $member) : $user = $member->user?>
                    <tr>
                        <td><?= $user->fn; ?></td>
                        <td><?= $user->ln; ?></td>
                        <td><?= $user->email; ?></td>
                        <td><?= $user->phone; ?></td>
                        <td>
                            <a href="<?= $this->Url->Build(['controller' => 'Users', 'action' => 'edit', $user->id]) ?>" class="bg-white shadow-sm btn-sm mx-1">
                                <i class="fas fa-edit text-success"></i>
                            </a>
                            <a href="<?= $this->Url->Build(['controller' => 'Users', 'action' => 'consulter', $user->id]) ?>" class="bg-white shadow-sm btn-sm mx-1">
                                <i class="fas fa-eye text-primary"></i>
                            </a>
                            <?= $this->Form->postLink(__('<i class="fas fa-trash-alt text-danger"></i>'), ['controller' => 'Members', 'action' => 'delete', $member->id], ['escape' => false, 'class' => 'bg-white shadow-sm btn-sm mx-1', 'confirm' => __('Voulez vous supprimer l\'utilisateur suivant de cette équipe:  {0}?', $user->fn.' '.$user->ln)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
