<table class="table table-borderless table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Intitulé</th>
                <th>Nombre de membres</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($gestions as $gestion) :?>
                    <tr>
                        <td><?= $gestion->team->name; ?></td>
                        <td><?= count($gestion->team->members) ?></td>
                        <td>
                            <a href="<?= $this->Url->Build(['controller' => 'Teams','action' => "edit", $gestion->team->id]) ?>" class="bg-white shadow-sm btn-sm">
                            <i class="fas fa-cogs text-primary"></i>
                            </a>
                            <?= $this->Form->postLink(__('<i class="fas fa-times text-danger"></i>'), ['controller' => 'Teams', 'action' => 'delete', $gestion->team->id], ['escape' => false,'class' => 'bg-white shadow-sm btn-sm mx-1', 'confirm' => __('Voulez vous supprimer :  {0}?', $gestion->team->name)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
