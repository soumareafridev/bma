<table class="table table-borderless table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>Intitulé</th>
        <th>Type</th>
        <th>Formulaire</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($fields as $field) :?>
            <tr>
                <td><?= $field->name; ?></td>
                <td><?= $field->type_field->name ?></td>
                <td><?= $field->form->name ?></td>
                <td>
                    <a href="<?= $this->Url->Build(['controller' => 'Fields','action' => "edit", $field->id]) ?>" class="bg-white shadow-sm btn-sm mx-1 editStatic" data-type="<?= $field->type_field_id ?>" data-choices="<?= $field->choices ?>" data-nom="<?= $field->name ?>" data-toggle="modal" data-target="#staticModal">
                    <i class="fas fa-edit text-success"></i>
                    </a>
                    <?= $this->Form->postLink(__('<i class="fas fa-times text-danger"></i>'), ['controller' => 'Fields', 'action' => 'delete', $field->id], ['escape' => false,'class' => 'bg-white shadow-sm btn-sm mx-1', 'confirm' => __('Voulez vous supprimer :  {0}?', $field->name)]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
