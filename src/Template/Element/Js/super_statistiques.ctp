<script>
      // Set new default font family and font color to mimic Bootstrap's default styling
        Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
        Chart.defaults.global.defaultFontColor = '#858796';

        function number_format(number, decimals, dec_point, thousands_sep = ' ') {
            // *     example: number_format(1234.56, 2, ',', ' ');
            // *     return: '1 234,56'
            number = (number + '').replace(',', '').replace(' ', '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function(n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
                };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            return s.join(dec);
        }
        var orders = <?= $dataOrders ?>;
        var distributions = <?= $dataDistributions ?>;
        var pois = <?= json_encode($dataPois) ?>;
        var dataUsers = <?= json_encode($dataUsers) ?>;

        // Area Chart Example
        var ctx = document.getElementById("myAreaChart");
        var myLineChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["Jan", "Feb", "Mar", "Avr", "Mai", "Juin", "Juil", "Août", "Sep", "Oct", "Nov", "Déc"],
            datasets: [{
            label: "Commandes",
            lineTension: 0.3,
            backgroundColor: "rgba(78, 115, 223, 0.1)",
            borderColor: "rgba(78, 115, 223, 1)",
            pointRadius: 3,
            pointBackgroundColor: "rgba(78, 115, 223, 1)",
            pointBorderColor: "rgba(78, 115, 223, 1)",
            pointHoverRadius: 3,
            pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
            pointHoverBorderColor: "rgba(78, 115, 223, 1)",
            pointHitRadius: 10,
            pointBorderWidth: 2,
            data: orders,
            },
            {
            label: "Distributions",
            lineTension: 0.3,
            backgroundColor: "rgba(23,166,115, 0.05)",
            borderColor: "rgba(23,166,115, 1)",
            pointRadius: 3,
            pointBackgroundColor: "rgba(23,166,115, 1)",
            pointBorderColor: "rgba(23,166,115, 1)",
            pointHoverRadius: 3,
            pointHoverBackgroundColor: "rgba(23,166,115, 1)",
            pointHoverBorderColor: "rgba(23,166,115, 1)",
            pointHitRadius: 10,
            pointBorderWidth: 2,
            data: distributions,
            },
        ],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
            padding: {
                left: 10,
                right: 15,
                top: 15,
                bottom: 0
            }
            },
            scales: {
            xAxes: [{
                time: {
                unit: 'date'
                },
                gridLines: {
                display: false,
                drawBorder: false
                },
                ticks: {
                maxTicksLimit: 7
                }
            }],
            yAxes: [{
                ticks: {
                maxTicksLimit: 5,
                padding: 10,
                // Include a dollar sign in the ticks
                callback: function(value, index, values) {
                    return number_format(value) + " XOF";
                }
                },
                gridLines: {
                color: "rgb(234, 236, 244)",
                zeroLineColor: "rgb(234, 236, 244)",
                drawBorder: false,
                borderDash: [2],
                zeroLineBorderDash: [2]
                }
            }],
            },
            legend: {
            display: true
            },
            tooltips: {
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#858796",
            titleMarginBottom: 10,
            titleFontColor: '#6e707e',
            titleFontSize: 14,
            borderColor: '#dddfeb',
            borderWidth: 1,
            xPadding: 15,
            yPadding: 15,
            displayColors: false,
            intersect: false,
            mode: 'index',
            caretPadding: 10,
            callbacks: {
                label: function(tooltipItem, chart) {
                var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                return datasetLabel + ': ' + number_format(tooltipItem.yLabel) + ' XOF';
                }
            }
            }
        }
        });


        // Area POIS
        var ctz = document.getElementById("poisChart");
        var poisChart = new Chart(ctz, {
        type: 'line',
        data: {
            labels: ["Jan", "Feb", "Mar", "Avr", "Mai", "Juin", "Juil", "Août", "Sep", "Oct", "Nov", "Déc"],
            datasets: [{
            label: "Points d'interêt (POIS)",
            lineTension: 0.3,
            backgroundColor: "rgba(59,175,247, 0.1)",
            borderColor: "rgba(59,175,247, 1)",
            pointRadius: 3,
            pointBackgroundColor: "rgba(59,175,247, 1)",
            pointBorderColor: "rgba(59,175,247, 1)",
            pointHoverRadius: 3,
            pointHoverBackgroundColor: "rgba(59,175,247, 1)",
            pointHoverBorderColor: "rgba(59,175,247, 1)",
            pointHitRadius: 10,
            pointBorderWidth: 2,
            data: pois,
            }
        ],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
            padding: {
                left: 10,
                right: 15,
                top: 15,
                bottom: 0
            }
            },
            scales: {
            xAxes: [{
                time: {
                unit: 'date'
                },
                gridLines: {
                display: false,
                drawBorder: false
                },
                ticks: {
                maxTicksLimit: 7
                }
            }],
            yAxes: [{
                ticks: {
                maxTicksLimit: 5,
                padding: 10,
                // Include a dollar sign in the ticks
                callback: function(value, index, values) {
                    return number_format(value) + " Poi(s)";
                }
                },
                gridLines: {
                color: "rgb(234, 236, 244)",
                zeroLineColor: "rgb(234, 236, 244)",
                drawBorder: false,
                borderDash: [2],
                zeroLineBorderDash: [2]
                }
            }],
            },
            legend: {
            display: true
            },
            tooltips: {
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#858796",
            titleMarginBottom: 10,
            titleFontColor: '#6e707e',
            titleFontSize: 14,
            borderColor: '#dddfeb',
            borderWidth: 1,
            xPadding: 15,
            yPadding: 15,
            displayColors: false,
            intersect: false,
            mode: 'index',
            caretPadding: 10,
            callbacks: {
                label: function(tooltipItem, chart) {
                var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                return datasetLabel + ': ' + number_format(tooltipItem.yLabel) + ' Poi(s)';
                }
            }
            }
        }
        });

        var cty = document.getElementById("companiesChart");
        var companyChart = new Chart(cty, {
        type: 'line',
        data: {
            labels: ["Jan", "Feb", "Mar", "Avr", "Mai", "Juin", "Juil", "Août", "Sep", "Oct", "Nov", "Déc"],
            datasets: [{
                label: "Utilisateurs",
                lineTension: 0.3,
                backgroundColor: "rgba(189,204,255, 0.1)",
                borderColor: "rgba(189,204,255, 1)",
                pointRadius: 3,
                pointBackgroundColor: "rgba(189,204,255, 1)",
                pointBorderColor: "rgba(189,204,255, 1)",
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(189,204,255, 1)",
                pointHoverBorderColor: "rgba(189,204,255, 1)",
                pointHitRadius: 10,
                pointBorderWidth: 2,
                data: JSON.parse(dataUsers.users),
            },
            {
                label: "Administrateurs",
                lineTension: 0.3,
                backgroundColor: "rgba(81,234,255, 0.1)",
                borderColor: "rgba(81,234,255, 1)",
                pointRadius: 3,
                pointBackgroundColor: "rgba(81,234,255, 1)",
                pointBorderColor: "rgba(81,234,255, 1)",
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(81,234,255, 1)",
                pointHoverBorderColor: "rgba(81,234,255, 1)",
                pointHitRadius: 10,
                pointBorderWidth: 2,
                data: JSON.parse(dataUsers.adminitrateurs),
            },
            {
                label: "Enquêteurs",
                lineTension: 0.3,
                backgroundColor: "rgba(140,186,81, 0.1)",
                borderColor: "rgba(140,186,81, 1)",
                pointRadius: 3,
                pointBackgroundColor: "rgba(140,186,81, 1)",
                pointBorderColor: "rgba(140,186,81, 1)",
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(140,186,81, 1)",
                pointHoverBorderColor: "rgba(140,186,81, 1)",
                pointHitRadius: 10,
                pointBorderWidth: 2,
                data: JSON.parse(dataUsers.enqueteurs),
            },
            {
                label: "Commerciaux",
                lineTension: 0.3,
                backgroundColor: "rgba(255,214,0, 0.1)",
                borderColor: "rgba(255,214,0, 1)",
                pointRadius: 3,
                pointBackgroundColor: "rgba(255,214,0, 1)",
                pointBorderColor: "rgba(255,214,0, 1)",
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(255,214,0, 1)",
                pointHoverBorderColor: "rgba(255,214,0, 1)",
                pointHitRadius: 10,
                pointBorderWidth: 2,
                data: JSON.parse(dataUsers.commerciaux),
            },
            {
                label: "Distributeurs",
                lineTension: 0.3,
                backgroundColor: "rgba(255,107,0, 0.1)",
                borderColor: "rgba(255,107,0, 1)",
                pointRadius: 3,
                pointBackgroundColor: "rgba(255,107,0, 1)",
                pointBorderColor: "rgba(255,107,0, 1)",
                pointHoverRadius: 3,
                pointHoverBackgroundColor: "rgba(255,107,0, 1)",
                pointHoverBorderColor: "rgba(255,107,0, 1)",
                pointHitRadius: 10,
                pointBorderWidth: 2,
                data: JSON.parse(dataUsers.distributeurs),
            },
        ]
        },
        options: {
            maintainAspectRatio: false,
            layout: {
            padding: {
                left: 10,
                right: 15,
                top: 15,
                bottom: 0
            }
            },
            scales: {
            xAxes: [{
                time: {
                unit: 'date'
                },
                gridLines: {
                display: false,
                drawBorder: false
                },
                ticks: {
                maxTicksLimit: 7
                }
            }],
            yAxes: [{
                ticks: {
                maxTicksLimit: 5,
                padding: 10,
                // Include a dollar sign in the ticks
                callback: function(value, index, values) {
                    return number_format(value) + " Agent(s)";
                }
                },
                gridLines: {
                color: "rgb(234, 236, 244)",
                zeroLineColor: "rgb(234, 236, 244)",
                drawBorder: false,
                borderDash: [2],
                zeroLineBorderDash: [2]
                }
            }],
            },
            legend: {
            display: true
            },
            tooltips: {
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#858796",
            titleMarginBottom: 10,
            titleFontColor: '#6e707e',
            titleFontSize: 14,
            borderColor: '#dddfeb',
            borderWidth: 1,
            xPadding: 15,
            yPadding: 15,
            displayColors: false,
            intersect: false,
            mode: 'index',
            caretPadding: 10,
            callbacks: {
                label: function(tooltipItem, chart) {
                var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                return datasetLabel + ': ' + number_format(tooltipItem.yLabel) + ' Agent(s)';
                }
            }
         }
        }
        });

  </script>