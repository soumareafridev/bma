<!-- Page Heading -->
<div class="col-md-12">
    <div class="d-sm-flex align-items-center justify-content-between mb-4 mt-2 customer__header bg-primary text-white">
        <h1 class="h3 text-gray-800">Enquêtes</h1>
    </div>
</div>

<?php for($i=0; $i<sizeof($cards);$i++): $card = $cards[$i]; ?>
    <div class="col-md-12">
        <!-- Collapsable Card Example -->
        <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapse<?= $i ?>" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapse<?= $i ?>">
            <h6 class="m-0 font-weight-bold text-primary"><?= $card['title'] ?></h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse show" id="collapse<?= $i ?>">
            <div class="card-body">
              <table class="table table-borderless table-hover table-striped" class="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                      <th>Intitulé</th>
                      <?php if($i == 0): ?>
                        <th>Type</th>
                        <th>Formulaire</th>
                      <?php endif; ?>
                      <?php if($i == 1): ?>
                        <th>Nombre de membres</th>
                      <?php endif; ?>
                      <?= $i !== 3 ? '<th>Actions</th>': '' ?>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach($card['listes'] as $item): ?>
                        <tr>
                            <td><?= $item->name; ?></td>
                            <?php if($i == 0): ?>
                              <td><?= $item->type_pois->name; ?></td>
                              <td><?= $item->form->name; ?></td>
                            <?php endif; ?>
                            <?php if($i == 1): ?>
                            <td><?= count($item->members); ?></td>
                          <?php endif; ?>
                            <?php if($i !== 3): ?>
                            <td>
                              <a href="<?= $this->Url->Build(['controller' => $card['action']['controller'],'action' => "edit", $item->id]) ?>" class="bg-white shadow-sm btn-sm">
                                <i class="fas fa-cogs text-primary"></i>
                              </a>
                              <a href="<?= $this->Url->Build(['controller' => $card['action']['controller'],'action' => "edit", $item->id]) ?>" data-titre="<?= $card['placeholder'] ?>" data-nom="<?= $item->name ?>" data-toggle="modal" data-target="#<?= $card['modal'] ?>" class="bg-white shadow-sm btn-sm ml-2 <?= $card['btnModal'] ?>">
                                <i class="fas fa-edit text-success"></i>
                              </a>
                                <?= $this->Form->postLink(__('<i class="fas fa-times text-danger"></i>'), ['controller' => $card['action']['controller'], 'action' => 'delete', $item->id], ['escape' => false,'class' => 'bg-white shadow-sm btn-sm ml-2', 'confirm' => __('Voulez vous supprimer :  {0}?', $item->name)]) ?>
                            </td>
                            <?php endif; ?>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
              </table>
            </div>
            <form class="card-footer text-right d-sm-flex align-items-center justify-content-around" action="<?= $this->Url->Build($card['action']) ?>" method="post">
                <input type="text" <?= $i ==3 ? 'disabled': "" ?> name="name" class="btn btn-sm bg-white shadow-sm" required placeholder="<?= $card['placeholder'] ?>">
                <?php if($i == 0):
                    echo $this->Form->select('type_poi_id', $card['type_pois'],['class' => 'btn btn-sm bg-white shadow-sm','empty' => 'Type de POIs']);
                    echo $this->Form->select('form_id', $card['forms'],['class' => 'btn btn-sm bg-white shadow-sm','empty' => 'Choix du formulaire']);
                endif; ?>
                <button type="submit" <?= $i ==3 ? 'disabled': "" ?> class="btn btn-sm btn-outline-primary">Ajouter <i class="fas fa-plus-circle ml-3"></i></button>
            </form>
        </div>
    </div>
    </div>
<?php endfor; ?>

  <!-- Groupe Modal-->
  <div class="modal fade" id="gammeModal" tabindex="-1" role="dialog" aria-labelledby="gammeModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <form method="post" action="" class="modal-content" id="gammeForm">
        <div class="modal-header">
          <h5 class="modal-title" id="gammeModalLabel">Modification de la gamme</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="nameModal" id="labelModal"></label>
                <input type="text" id="nameModal" name="name" required class="form-control">
            </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-sm btn-info" type="submit">Enregistrer</button>
        </div>
      </form>
    </div>
  </div>

  <!-- Groupe and Statut Modal-->
  <div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <form method="post" action="" class="modal-content" id="staticForm">
        <div class="modal-header">
          <h5 class="modal-title" id="staticModalLabel">Modification</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="nameModal" id="labelStaticModal"></label>
                <input type="text" id="nameStaticModal" name="name" required class="form-control">
            </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-sm btn-info" type="submit">Enregistrer</button>
        </div>
      </form>
    </div>
  </div>

<?php $this->start('script_bottom'); ?>
    <script>
        // $("#gammeForm").submit(function() {
        //     $("#gammeModal").modal('hide');
        // });
        // $("#staticForm").submit(function() {
        //     $("#staticModal").modal('hide');
        // });

        $(".editStatic").click(function() {
            $("#nameStaticModal").val($(this).data("nom"));
            $("#staticForm").attr('action', $(this).attr("href"));
        });
    </script>
<?php $this->end(); ?>
