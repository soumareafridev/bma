<?php
$this->extend('/Cell/exports');
$this->assign('title', 'Liste des entreprises');
$this->start('table');
?>
<table class="table table-borderless table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>N°</th>
            <th>Nom</th>
            <th>statut</th>
            <th>Administrateur</th>
            <th>Date ouverture</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $n = 1; foreach ($companies as $company) : ?>
            <tr>
                <td><?= $n; ?></td>
                <td><?= $company->name; ?></td>
                <td><?= $this->Vue->statutCompany()[$company->companie_statut_id]; ?></td>
                <td>
                    <?= $company->users[0]->fn . ' ' . $company->users[0]->ln; ?> <br>
                    <span class="badge badge-primary"><?= $company->users[0]->email; ?></span>
                </td>
                <td><?= $company->created->nice('Africa/Dakar','fr-Fr'); ?></td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Gérer statut
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <?php foreach ($statuts as $statut) :
                                if ($statut->id !== $company->companie_statut_id) : $messageAlert = "Voulez-vous changer le statut de l'entreprise #$company->name au statut : $statut->name ?"; ?>
                                    <?= $this->Form->postLink(__($statut->name), ['controller' => 'Companies', 'action' => 'statut', '?' => ['company_id' => $company->id, 'statut_id' => $statut->id]], ['escape' => false, 'class' => 'dropdown-item', 'confirm' => __($messageAlert)]) ?>
                            <?php endif;
                            endforeach; ?>
                        </div>
                    </div>
                    <a href="<?= $this->Url->build(['controller' => 'Companies', 'action' => 'access', '?' => $this->Vue->hashUrl($company->id)]) ?>" class="btn btn-sm btn-warning my-1">
                        <i class="fas fa-key pl-1"></i> Gestion Accès
                    </a>
                    <a href="<?= $this->Url->build(['controller' => 'Statistiques', 'action' => 'index', '?' => ['entreprise' => $company->id]]) ?>" class="btn btn-sm btn-primary my-1">
                        <i class="fas fa-chart-line pl-1"></i> Statistiques
                    </a>
                </td>
            </tr>
        <?php $n++; endforeach; ?>
    </tbody>
</table>
<?php $this->end(); ?>
