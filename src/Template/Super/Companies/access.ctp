<?php $this->assign('title', 'Configuration accès aux modules'); ?>
<div class="col-md-12">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-start mb-2 mt-2">
        <a href="<?= $this->Url->Build(['action' =>'index']) ?>" class="btn btn-sm btn-primary text-white px-3 text-white mr-2"><i class="fas fa-arrow-left"></i></a>
        <h1 class="h4 text-gray-800 mx-0 my-0">
            <?=  $company->name ?>
        </h1>
    </div>
</div>

<div class="card shadow mb-4 col-md-12">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary d-sm-flex align-items-center justify-content-start">
            Configuration accès aux modules
        </h6>
    </div>
    <div class="card-body">
        <div class="row">
            <form class="col-md-7 list-group" action="" method="post">
                <?php foreach($modules as $key => $module) : ?>
                    <div class="list-group-item list-group-item-success">
                        <label for="acces<?= $key ?>">
                            <input type="checkbox" name="access[]" id="acces<?= $key ?>" <?= in_array($key, $acces) ? 'checked' : '' ?> value="<?= $key?>" class="mx-1 my-1">
                            <?= $module ?>
                        </label>
                    </div>
                <?php endforeach; ?>
                <div class="form-group mt-2">
                    <button type="submit" class="btn btn-block btn-primary"><i class="fas fa-check mr-2"></i> Enregistrer</button>
                </div>
            </form>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <i class="fas fa-building mr-2"></i>
                            Accès aux modules
                    </div>
                    <div class="card-body">
                        <?php if(isset($acces) && count($acces) > 0) : ?>
                            <?php foreach($acces as $module) : ?>
                            <label class="label-radio-gray" style="font-size: 12px">
                                <span>
                                    <?= $modules[$module] ?>
                                    <i class="fas fa-check text-success ml-2"></i>
                                </span>
                            </label>
                        <?php endforeach; else: ?>
                            <div class="jumbotron text-center py-4">Aucun accès !</div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>