<?php $this->layout = 'accueil';
$this->assign('title', 'Statistiques - revenus') ?>
<div class="col-md-12 pt-4">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h3 class="text-primary h2 mb-0 bolder" style="line-height: .7">
            <?= $this->fetch('title'); ?>
        </h3>
    </div>
</div>

<div class="col-md-12 col-sm-12 col-lg-12 text-white text-center" style="min-height: calc(100vh - (37px));overflow:hidden">
    <div class="row">
        <div class="col-md-12">
            <form method="GET" action="">
                <div class="d-sm-flex align-items-center justify-content-between mb-1">
                    <!-- Topbar Search -->
                    <select required name="prix_commande" class="form-control mx-1 my-1 bg-light border-0 small bg-white shadow-sm">
                        <option selected disabled>Prix commande</option>
                        <?php for ($i = 0; $i < 100; $i++) : ?>
                            <option value="<?= $i * 25 ?>" <?= isset($prix_commande) && $prix_commande == $i * 25 ? 'selected' : '' ?>><?= $i * 25 ?> XOF</option>
                        <?php endfor; ?>
                    </select>

                    <select required name="prix_distribution" class="form-control mx-1 my-1 bg-light border-0 small bg-white shadow-sm">
                        <option selected disabled>Prix distribution</option>
                        <?php for ($i = 1; $i < 100; $i++) : ?>
                            <option value="<?= $i * 25 ?>" <?= isset($prix_distribution) && $prix_distribution == $i * 25 ? 'selected' : '' ?>><?= $i * 25 ?> XOF</option>
                        <?php endfor; ?>
                    </select>

                    <select required name="prix_pois" class="form-control mx-1 my-1 bg-light border-0 small bg-white shadow-sm">
                        <option selected disabled>Prix pois</option>
                        <?php for ($i = 1; $i < 10; $i++) : ?>
                            <option value="<?= $i * 500 ?>" <?= isset($prix_pois) && $prix_pois == $i * 500 ? 'selected' : '' ?>><?= $i * 500 ?> XOF</option>
                        <?php endfor; ?>
                    </select>

                </div>
                <div class="d-sm-flex align-items-center justify-content-between mb-1">
                    <!-- Topbar Search -->
                    <select required name="annee" class="form-control mx-1 my-1 bg-light border-0 small bg-white shadow-sm">
                        <option selected disabled>Sélectionner année</option>
                        <?php for ($i = 2019; $i < 2030; $i++) : ?>
                            <option value="<?= $i ?>" <?= isset($dataAnnee) && $dataAnnee == $i ? 'selected' : '' ?>><?= $i ?></option>
                        <?php endfor; ?>
                    </select>

                    <div class="input-group">
                        <?= $this->Form->select('entreprise', $list_companies, ['empty' => 'Séléctionner une entreprise', 'class' => 'form-control bg-light border-0 ml-1 small bg-white shadow-sm']) ?>
                        <div class="input-group-append">
                            <button class="btn btn-primary text-white" type="submit">
                                <i class="fas fa-check fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-12">
            <div class="d-sm-flex align-items-center justify-content-around alert-warning p-2 my-2 shadow-sm">
                <label class="label-radio-gray mb-0" style="font-size: 12px">
                    <input type="checkbox" checked disabled name="pois" id="">
                    <span>
                        Année : <span class="badge badge-light"><?= $dataAnnee ?></span>
                    </span>
                </label>

                <label class="label-radio-gray mb-0" style="font-size: 12px">
                    <input type="checkbox" checked disabled name="pois" id="">
                    <span>
                        Entreprise : <span class="badge badge-light"><?= isset($company) ? $company->name : 'Tout' ?></span>
                    </span>
                </label>
            </div>
        </div>
        <!-- Area Chart -->
        <div class="col-xl-12">
            <div class="card shadow-sm mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Activité Commandes & Distributions</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="myAreaChart"></canvas>
                    </div>
                </div>
            </div>
            <hr>
            <div class="card shadow-sm mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Activité Enregistrement POIS</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="poisChart"></canvas>
                    </div>
                </div>
            </div>
            <hr>
            <div class="card shadow-sm mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Tableau récapitulatif</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <table class="table table-condensed table-striped table-hover table-borderless" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="text-center alert-info">Année</th>
                                <th class="text-center alert-info" colspan="3">Entreprise</th>
                            </tr>
                            <tr>
                                <td class="text-center"><?= $dataAnnee ?></td>
                                <td class="text-center" colspan="3"><?= isset($company) ? $company->name : 'Tout' ?></td>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th class="text-center bg-primary text-white">Mois</th>
                                <th class="text-center alert-info">Commandes</th>
                                <th class="text-center alert-info">Distributions</th>
                                <th class="text-center alert-info">Pois</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <?php for($i = 0; $i < count($mois); $i++): ?>
                                <tr>
                                    <td class="bg-primary text-white"><?= $mois[$i] ?></td>
                                    <td class="text-info"><?= $this->Number->format($dataOrders[$i], ['locale' => 'fr', 'after' => ' XOF']) ?></td>
                                    <td class="text-info"><?= $this->Number->format($dataDistributions[$i], ['locale' => 'fr', 'after' => ' XOF']) ?></td>
                                    <td class="text-info"><?= $this->Number->format($dataPois[$i], ['locale' => 'fr', 'after' => ' XOF']) ?></td>
                                </tr>
                            <?php endfor; ?>
                            <tr>
                                <td>TOTAL</td>
                                <td class="bg-info text-white"><?= $this->Number->format(array_sum($dataOrders), ['locale' => 'fr', 'after' => ' XOF']) ?></td>
                                <td class="bg-info text-white"><?= $this->Number->format(array_sum($dataDistributions), ['locale' => 'fr', 'after' => ' XOF']) ?></td>
                                <td class="bg-info text-white"><?= $this->Number->format(array_sum($dataPois), ['locale' => 'fr', 'after' => ' XOF']) ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->start('script_bottom'); ?>
<!-- Page level plugins -->
<?= $this->Html->script('vendor/chart.js/Chart.min.js'); ?>

<!-- Page level custom scripts -->
<?= $this->Element('Js/super_pricing'); ?>


<?php $this->end(); ?>