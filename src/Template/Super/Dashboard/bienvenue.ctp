<?php $this->layout = 'auth';
$this->assign('title', 'Bienvenue - changer votre thème'); $this->start('css'); ?>
<?php $this->end(); ?>
<div class="col-md-12 bg-white text-primary text-center py-1" style="z-index: 1;box-shadow: 0 1px 3px -1px rgba(0,0,0,.5);">
    <h4 class="bold m-0 d-sm-flex align-items-center justify-content-around text-uppercase">
        <img src="<?= $this->Url->image('logo.png') ?>" alt="logo" style="max-height: 40px;margin-bottom: 8px">
        Votre Thème
    </h4>
</div>
<div class="col-md-12 d-sm-flex align-items-center justify-content-center" style="height: calc(100vh - (132px))">
    <img id="theme_img" style="width: auto; height: 90%;max-width: 95%;box-shadow: 12px -2px 25px 9px rgba(0,0,0,.3);" src="<?= $_company->theme_id == null ? $this->Url->image('themes/theme-default.png') :  $this->Url->image('themes/'. $_company->theme->image) ?>" alt="Theme du site">
</div>
<div class="col-md-12 bg-white text-primary text-center py-3" style="z-index: 1;box-shadow: 0 -1px 3px -1px rgba(0,0,0,.5);">
    <form method="post" action="<?= $this->Url->build(['action' => 'bienvenue']) ?>" class="m-0 d-sm-flex align-items-center justify-content-around">
        <h5 class="bold m-0 d-sm-flex align-items-center justify-content-center">
            <label for="themes" class="my-0 pr-2">Choix de votre Thème</label>
            <div class="select my-0" style="min-width: 300px">
                <select name="theme_id" class="form-control form-control-sm" id="themes">
                    <option value>Selectionner votre thème</option>
                    <?php if(count($themes) > 0) : foreach($themes as $theme) : ?>
                      <option value="<?= $theme->id ?>" <?= $_company->theme_id == $theme->id ? 'selected' : '' ?> data-css="<?= $theme->css ?>" data-url="<?= $this->Url->image('themes/'. $theme->image) ?>"><?= $theme->name ?></option>
                    <?php endforeach; endif; ?>
                </select>
            </div>
        </h5>
        <div class="text-center">
            <button class="btn btn-primary animated pulse infinite" type="submit">Suivant <i class="ml-3 fas fa-arrow-alt-circle-right"></i></button>
        </div>
    </form>
</div>
<?php $this->start('script_bottom'); ?>
    <?= $this->Html->script('vendor/slick.min.js') ?>
    <script>
        $(function() {
            $('#themes').change(function() {
                $('#theme_img').attr('src',$('#themes option:selected').data('url'));
            });
        });
    </script>
<?php $this->end(); ?>