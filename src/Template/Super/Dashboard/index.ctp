<?php $this->layout = 'accueil'; ?>
<div class="col-md-12 pt-5">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h3 class="text-primary h1 mb-0 bolder" style="line-height: .7">Dashboard
            <br> <small class="h6 text-black-50 m-0" style="font-size:14px">
                <?= $this->Vue->getInfoSite($_user->company_id, 'slogan'); ?>
            </small>
        </h3>
    </div>
</div>

<div class="col-md-12 col-sm-12 col-lg-12 text-white text-center" style="min-height: calc(100vh - (37px));overflow:hidden">
    <div class="row">

        <!-- Area Chart -->
        <div class="col-xl-12">
            <div class="card shadow-sm mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Les commandes en chiffre</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Filtrer par année:</div>
                            <a class="dropdown-item" href="?annee=<?= $dataAnnee ?>"><?= $dataAnnee ?></a>
                            <a class="dropdown-item" href="?annee=<?= $dataAnnee - 1 ?>"><?= $dataAnnee - 1 ?></a>
                            <a class="dropdown-item" href="?annee=<?= $dataAnnee - 2 ?>"><?= $dataAnnee - 2 ?></a>
                            <a class="dropdown-item" href="?annee=<?= $dataAnnee - 3 ?>"><?= $dataAnnee - 3 ?></a>
                        </div>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="myAreaChart"></canvas>
                    </div>
                </div>
            </div>
            <hr>
            <div class="card shadow-sm mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Evolutions des ouvertures de compte</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="companiesChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->start('script_bottom'); ?>
  <!-- Page level plugins -->
  <?= $this->Html->script('vendor/chart.js/Chart.min.js'); ?>

  <!-- Page level custom scripts -->
  <?= $this->Element('Js/dashboard-area-super'); ?>


<?php $this->end(); ?>
