<?php $this->layout = 'auth';
$this->assign('title', ' Ouverture de compte') ?>

<div class="col-md-12 bg-white text-primary text-center py-1" style="z-index: 1;box-shadow: 0 1px 3px -1px rgba(0,0,0,.5);">
    <h2 class="bold m-0 d-sm-flex align-items-center justify-content-around">
        <img src="<?= $this->Url->image('logo.png') ?>" alt="logo" style="max-height: 50px;margin-bottom: 8px">
        <span>Ouverture de compte</span>
    </h2>
</div>
<div class="col-md-12">
    <form class="row" method="post" action="/inscription" id="form">
        <div class="col-md-6 p-0 bg-primary text-white d-sm-flex align-items-center justify-content-center" style="min-height: calc(100vh - (66px))">
            <div class="px-1">
                <h3 class="text-white mb-4">Informations personelles</h3>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <input type="text" class="form-control form-control-user" id="exampleFirstName" required name="user[fn]" placeholder="Votre prénom">
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control form-control-user" id="exampleLastName" required name="user[ln]" placeholder="Votre nom de famille">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <input type="email" class="form-control form-control-user" id="email" required name="user[email]" placeholder="Adresse email">
                    </div>
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <input type="tel" required class="form-control form-control-user" id="phone" name="user[phone]" placeholder="Téléphone mobile">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <input type="password" required class="form-control form-control-user" id="password" name="user[password]" placeholder="Mot de passe">
                    </div>
                    <div class="col-sm-6">
                        <input type="password" required class="form-control form-control-user" id="repassword" placeholder="Répeter votre mot de passe">
                    </div>
                </div>
                <div class="form-group text-right">
                    <a href="<?= $this->Url->build(["controller" => 'users', 'action' => 'login']) ?>" class="btn mt-4 shadow-sm bg-primary text-white">J'ai déjà un compte <i class="fas fa-user-check ml-3" ></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-6 p-0 pt-2 d-sm-flex bg-white align-items-center justify-content-center" style="min-height: calc(100vh - (66px));">
            <div class="px-1"> 
                <h3 class="mb-4">Votre organisation</h3>
                <?= $this->Flash->render() ?>
                <div class="message small alert alert-danger mt-2 hidden p-0" id="errors"></div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <input type="text" class="form-control form-control-user" required name="company[name]" placeholder="Nom de votre organisation">
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control form-control-user" name="company[short_name]" maxlength="3" placeholder="Acronyme en 3 lettres">
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" name="company[one_word]" class="form-control form-control-user" maxlength="20" placeholder="Votre activité en un mot, ex: commerce,elevage">
                </div>
                <div class="form-group">
                    <input type="text" name="company[slogan]" class="form-control form-control-user" maxlength="255" placeholder="Courte description de votre activité">
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="btn mt-4 shadow-sm bg-rose btn-lg text-white">Ouvrir mon compte  <i class="fas fa-long-arrow-alt-right ml-3" ></i></button>
                </div>
            </div>
        </div>
    </form>
</div>
<?php $this->start('script_bottom'); ?>
   <script>
       $(function() {
           var regexNumber = /^(7[0768]{1}|002217[0768]{1}|\+2217[0768]{1}|33|0022133)[0-9]{7}$/;
           var regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

           //Vérification du formulaire avant l'envoi
            $("#form").submit(function(e) {
                var messageErreur = "";
                var errors = [];

                if(!regexNumber.test($('#phone').val())) {
                    errors.push("<li class='list-group-item list-group-item-danger'>Veuillez saisir un format de numéro téléphone sénégalais valide</li>");
                    $('#phone').css('border', '1px solid red');
                } else $('#phone').css('border', 'none');

                if(!regexEmail.test($('#email').val())) {
                    $('#email').css('border', '1px solid red');
                    errors.push("<li class='list-group-item list-group-item-danger'>Veuillez saisir un format d'email valide</li>");
                } else $('#email').css('border', 'none');

                if($('#password').val() != $('#repassword').val()) {
                    errors.push("<li class='list-group-item list-group-item-danger'>Les deux mots ne correspondent pas</li>");
                    $('#password,#repassword').css('border', '1px solid red');
                } else $('#password,#repassword').css('border', 'none');

                if(errors.length > 0) {
                    messageErreur = "";
                    e.preventDefault(); //Annulation de l'envoi du formulaire
                    messageErreur += '<ul class="list-group">';
                    for(var i in errors) {
                        messageErreur += errors[i];
                    }
                    messageErreur += '</ul>';
                    $('#errors').html(messageErreur).removeClass('hidden animated fadeOut').addClass('animated fadeIn');

                    setTimeout(
                        () => {
                            $('#errors').addClass('animated fadeOut hidden').html("").removeClass('animated fadeIn');
                        }, 10000
                    )
                }
            });
       });
   </script>
<?php $this->end(); ?>