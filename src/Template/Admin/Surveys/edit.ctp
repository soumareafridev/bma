<!-- Page Heading -->
<div class="col-md-12">
    <div class="d-sm-flex align-items-center justify-content-start mb-2 mt-2">
        <h1 class="h4 text-gray-800">
        <a href="javascript:history.back()" class="btn btn-sm btn-primary text-white px-3 text-white mr-2"><i class="fas fa-arrow-left"></i></a>
            Configuration - <small class="h6"><?= $survey->name ?></small>
        </h1>
    </div>
</div>

    <div class="col-md-12">
        <!-- Collapsable Card Example -->
        <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapseModif" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseModif">
            <h6 class="m-0 font-weight-bold text-primary">Modification de l'enquête</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse show" id="collapseModif">
            <form class="card-body" method="post" action="<?= $this->Url->Build(['controller' => "Surveys",'action' => "edit", $survey->id]) ?>">
              <div class="form-group">
                  <label for="name">Intitulé</label>
                  <input type="text" id="name" value="<?= $survey->name ?>" class="form-control form-control-sm" >
                </div>
              <div class="form-group">
                  <label for="type_poi_id">Type d'enquête</label>
                  <?= $this->Form->select('type_poi_id', $typePois,['id' => 'type_poi_id' ,'class' => 'form-control','default' => $survey->type_poi_id,'required']); ?>
                </div>
              <div class="form-group">
                  <label for="form_id">Séléctionnez un formulaire</label>
                  <?= $this->Form->select('type_poi_id', $forms,['id' => 'form_id' ,'class' => 'form-control','default' => $survey->form_id,'required']); ?>
                </div>
              <div class="form-group">
                  <button type="submit" class="btn btn-primary text-white"><i class="fas fa-edit mr-2"></i>Enregistrer</button>
                </div>
            </form>
        </div>
        </div>
    </div>

    <div class="col-md-12">
        <!-- Collapsable Card Example -->
        <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapseList" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseList">
            <h6 class="m-0 font-weight-bold text-primary">Equipe liée à l'enquête</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse show" id="collapseList">
            <div class="card-body">
                <?= $this->Element('Components/listTeams') ?>
            </div>
        </div>
        </div>
    </div>

    <?php $this->start('script_bottom'); ?>
        <?= $this->Element('Components/exportScript') ?>
    <?php $this->end(); ?>
