<!-- Page Heading -->
<div class="col-md-12">
    <div class="d-sm-flex align-items-center justify-content-start mb-2 mt-2">
        <h1 class="h4 text-gray-800">
        <a href="<?= $this->Url->Build(['controller' => 'Parametres', 'action' => 'enquete']) ?>" class="btn btn-sm btn-primary text-white px-3 text-white mr-2"><i class="fas fa-arrow-left"></i></a>
            Configuration - <small class="h6"><?= $team->name ?></small>
        </h1>
    </div>
</div>

    <div class="col-md-12">
        <!-- Collapsable Card Example -->
        <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapseModif" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseModif">
            <h6 class="m-0 font-weight-bold text-primary">Modification de l'équipe</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse show" id="collapseModif">
            <form class="card-body" method="post" action="<?= $this->Url->Build(['controller' => "Teams",'action' => "edit", $team->id]) ?>">
              <div class="form-group">
                  <label for="name">Nom de l'equipe</label>
                  <input type="text" id="name" value="<?= $team->name ?>" class="form-control form-control-sm" >
                </div>
              <div class="form-group">
                  <button type="submit" class="btn btn-primary text-white"><i class="fas fa-edit mr-2"></i>Enregistrer</button>
                </div>
            </form>
        </div>
        </div>
    </div>

    <div class="col-md-12">
        <!-- Collapsable Card Example -->
        <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapseList" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseList">
            <h6 class="m-0 font-weight-bold text-primary">Membres de l'équipe</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse show" id="collapseList">
            <div class="card-body">
                <?= $this->Element('Components/listMembers') ?>
            </div>
            <form class="card-footer" method="post" action="<?= $this->Url->Build(['controller' => 'Members','action' => "add"]) ?>">
                <table class="table table-borderless table-hover table-striped">
                    <tr>
                        <td>
                            <select name="user_id" class="form-control form-control-sm"  required>
                                <option value default>Séléctionner un utilisateur</option>
                                <?php foreach($no_members as $user): ?>
                                    <option value="<?= $user->id ?>"><?= "$user->fn  $user->ln"?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        <td>
                            <input type="text" class="form-control form-control-sm"  disabled value="<?= $team->name ?>">
                            <input type="hidden" name="team_id" value="<?= $team->id ?>">
                        </td>
                        <td>
                            <button type="submit" class="btn btn-primary text-white"><i class="fas fa-edit mr-2"></i>Enregistrer</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
    </div>

    <?php $this->start('script_bottom'); ?>
        <?= $this->Element('Components/exportScript') ?>
    <?php $this->end(); ?>
