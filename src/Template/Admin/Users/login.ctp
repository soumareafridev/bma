<?php $this->layout = false; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SCOD : Authentification</title>
      <!-- Custom fonts for this template-->
    <link href="/js/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <?= $this->Html->css('sb-admin-2.min.css');?>

</head>
<body>
    <div class="container-fluid p-0">
        <div class="row m-0">
            <div class="col-md-6 bg-image p-0" style="background: linear-gradient(0deg,rgba(78,160,220,.8),rgba(78,160,220,.8)),url(<?= $this->Url->image('login_image.jpg') ?>);background-repeat: no-repeat;background-size:cover;background-position:center"></div>
            <div class="col-md-6 bg-white min-vh-100 p-0 d-sm-flex align-items-center justify-content-center">
                <div class="text-center cont-form" style="min-width: 300px">
                    <div class="entete">
                        <img src="<?= $this->Url->image('logo-login.png') ?>" alt="logo">
                    </div>

                    <?= $this->Flash->render() ?>

                    <p class="mb-3 text-dark"><strong>Se connecter</strong></p>
                    <form class="bg_grisdark login" method="post" action="<?= $this->Url->Build(['action' => 'login']) ?>">
                        <div class="container">
                          <div class="form-group bg_grisdark mt-3 mb-4">
                              <label class="text-white text-left text-uppercase">identifiant</label>
                              <input class="form-control-gris pl-2" type="text" required name="phone" placeholder="Téléphone/Email" />
                              <i class="fas fa-envelope"></i>
                          </div>
                        
                            <div class="form-group bg_grisdark mb-4">
                                <label class="text-white text-left text-uppercase">mot de passe</label>
                                <input type="password" class="form-control-gris pl-2" name="password" required placeholder="Votre mot de passe" />
                                <i class="fas fa-lock"></i>
                                <?php if(isset($redirect)): ?>
                                    <input type="hidden" name="redirect" value="<?= $redirect ?>">
                                <?php endif; ?>
                            </div>
                  
                            <button class="btn-rose bg-rose mb-3" type="submit">
                              Se connecter 
                              <i class="fas fa-arrow-right ml-2"></i>
                            </button>
                        </div>
                    </form>
                    <!-- <hr class="mt-5"> -->
                    <div class="m-5 text-center">
                        <a class="gris bold">Mot de passe oublié ?</a>  <br>
                        <p class="mt-4"><small class="gris text-md-light">SCOD - v0.0.1</small></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>