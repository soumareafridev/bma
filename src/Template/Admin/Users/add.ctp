<div class="card shadow mb-4 col-md-12 mt-5 p-2">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Ajouter un utilisateur</h6>
    </div>
    <div class="card-body">
        <form action="<?= $this->Url->build(['action' => 'add']) ?>" method="post" class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Prénom</label>
                    <input type="text" name="fn" required class="form-control form-control-sm">
                </div>
                <div class="form-group">
                    <label for="">Nom de famille</label>
                    <input type="text" name="ln" required class="form-control form-control-sm">
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" name="email" class="form-control form-control-sm">
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Numéro de téléphone</label>
                    <input type="text" name="phone" required class="form-control form-control-sm">
                </div>
                <div class="form-group">
                    <label for="">Profil de l'utilisateur</label>
                    <?= $this->Form->select('profile',$profiles,['class' => 'form-control form-control-sm','required', 'empty' => 'Selectionnez le type de profil']); ?>
                </div>
                <div class="form-group">
                    <label for="">Mot de passe</label>
                    <input type="password" name="password" class="form-control form-control-sm">
                </div>
            </div>
            <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-primary text-white">Ajouter l'utilisateur</button>
            </div>
        </form>
    </div>
</div>
