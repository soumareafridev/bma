<div class="container mt-5">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 mt-2 customer__header bg-primary text-white">
        <h1 class="h4 text-gray-800">Liste des utilisateurs</h1>
        <!-- Topbar Search -->
        <a href="<?= $this->Url->build(['controller'=>'Users','action'=>'add']) ?>" class="btn-sm btn-primary text-white">Ajouter un utilisateur</a>
    </div>
    <!-- DataTales Example -->
    <div class="table-responsive shadow p-4">
        <table class="table table-borderless table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Email</th>
                <th>Téléphone</th>
                <th>Profil</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Email</th>
            <th>Téléphone</th>
            <th>Profil</th>
            <th>Actions</th>
            </tr>
            </tfoot>
            <tbody>
                <?php foreach ($users as $user) :?>
                    <tr>
                        <td><?= $user->fn; ?></td>
                        <td><?= $user->ln; ?></td>
                        <td><?= $user->email; ?></td>
                        <td><?= $user->phone; ?></td>
                        <td><?= isset($user->roles[0]) ? $user->roles[0]->profile->name: 'non-defini' ?></td>
                        <td>
                            <?= $this->Form->postLink(__('<i class="fas fa-trash-alt"></i>'), ['controller' => 'Users', 'action' => 'delete', $user->id], ['escape' => false, 'class' => 'bg-white shadow-sm btn-sm mx-1 text-danger', 'confirm' => __('Voulez vous supprimer l\'utilisateur suivant:  {0}?', $user->fn.' '.$user->ln)]) ?>
                            <a href="<?= $this->Url->Build(['controller' => 'Users', 'action' => 'edit', $user->id]) ?>" class="bg-white shadow-sm btn-sm mx-1 text-success">
                                <i class="fas fa-edit"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?php $this->start('script_bottom'); ?>
<!-- Page le*vel plugins -->
<script src="/js/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/js/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#dataTable').DataTable( {
        "language": {
            "lengthMenu": "Affiche _MENU_ enregistrement(s) par page",
            "zeroRecords": "Nothing found - sorry",
            "info": "affichage page _PAGE_ of _PAGES_",
            "infoEmpty": "Pas de données trouvée",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }
    } );
} );
</script>
<?php $this->end(); ?>
