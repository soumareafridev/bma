<div class="card shadow mb-4 col-md-12 mt-5 p-2">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary d-sm-flex align-items-center justify-content-start">
        <a href="<?= $this->Url->Build(['action' =>'index']) ?>" class="btn btn-sm btn-primary text-white px-3 text-white mr-2"><i class="fas fa-arrow-left"></i></a>
            Modifier l'utilisateur
        </h6>
        <small><?= "$user->fn $user->ln" ?></small>
    </div>
    <div class="card-body">
        <div class="row">
            <form class="col-md-7" action="<?= $this->Url->build(['action' => 'edit',$user->id]) ?>" method="post">
                <div class="form-group">
                    <label for="">Prénom</label>
                    <input type="text" name="fn" value="<?= $user->fn ?>" required class="form-control form-control-sm" >
                </div>
                <div class="form-group">
                    <label for="">Nom de famille</label>
                    <input type="text" name="ln" value="<?= $user->ln ?>" required class="form-control form-control-sm" >
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" name="email" value="<?= $user->email ?>" class="form-control form-control-sm" >
                </div>
                <div class="form-group">
                    <label for="">Mot de passe</label>
                    <input type="password" name="password" value="<?= $user->password ?>" class="form-control form-control-sm" >
                </div>
                <div class="form-group">
                    <label for="">Numéro de téléphone</label>
                    <input type="text" name="phone" value="<?= $user->phone ?>" required class="form-control form-control-sm" >
                </div>
                <div class="form-group">
                <button type="submit" class="btn btn-block btn-primary"><i class="fas fa-check mr-2"></i> Enregistrer</button>
                </div>
            </form>
            <form class="col-md-5" method="post" action="<?= $this->Url->build(['controller' => 'roles','action' => 'add']) ?>">
                <div class="card">
                    <div class="card-header">
                        <i class="fas fa-user mr-2"></i>
                            Rôles
                    </div>
                    <div class="card-body">
                        <input type="hidden" name="user_id" value="<?= $user->id ?>">
                        <?php if(count($user->roles) > 0) : ?>
                            <form action="" class="hidden"></form>
                            <?php foreach($user->roles as $role) : ?>
                            <label class="label-radio-gray" style="font-size: 12px">
                                <span>
                                    <?= $role->profile->name ?>
                                        <?php echo $this->Form->postLink(__('<i class="fas fa-times text-danger ml-2"></i>'), ['controller' => 'roles', 'action' => 'delete', $role->id], ['escape' => false, 'confirm' => __('Voulez vous supprimer le rôle suivant :  {0}?', $role->profile->name)]) ?>
                                </span>
                            </label>
                        <?php endforeach; endif; ?>
                    </div>
                    <div class="card-footer d-sm-flex align-items-center justify-content-between">
                        <?= $this->Form->select('profile_id', $profiles,['class' => 'form-control', 'empty' => 'Sélectionner un rôle', 'required']); ?>
                        <button type="submit" class="btn btn-sm" id="addRole">Ajouter</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>