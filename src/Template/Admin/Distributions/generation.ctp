<div class="col-md-12 mt-3">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h3 class="text-primary h4 mb-0 bolder" style="line-height: .7">
            Géneration des Distributions <i class="fas fa-cogs"></i>
        </h3>
    </div>

    <form class="d-sm-flex align-items-center justify-content-between mb-4" method="POST" action="<?= $this->Url->build(['controller' => 'Distributions', 'action' => 'generation']); ?>">
        <!-- Topbar Search -->
        <select required name="type" id="type" class="form-control mx-1 my-1 bg-light border-0 small bg-white shadow-sm">
            <option selected disabled>Type de génération</option>
            <option value="1" <?= isset($type) && $type == 1 ? 'selected': '' ?>>Manuelle</option>
            <option value="2" <?= isset($type) && $type == 2 ? 'selected': '' ?>>Automatique</option>
            <option value="<?= isset($type) ? 'selected': '' ?>">Tout</option>
        </select>
        <input type="text" name="date" required onfocus="(this.type='date')" value="<?= isset($date) ? $date->format('Y-m-d') : '' ?>" class="form-control my-1 mx-1 bg-light border-0 small bg-white shadow-sm" id="" placeholder="Date des distributions">
        <!-- <input type="text" onfocus="(this.type='date')" class="form-control my-1 mx-1 bg-light border-0 small bg-white shadow-sm" id="" placeholder="Fin"> -->
        <div class="input-group">
            <!-- <input type="text" class="form-control  mx-1 bg-light border-0 small bg-white shadow-sm" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2"> -->
            <select name="generation" required id="generation" class="form-control bg-light border-0 ml-1 small bg-white shadow-sm">
                <option value="0">Visualiser</option>
                <option value="1">Générer</option>
            </select>
            <div class="input-group-append">
                <button class="btn btn-primary text-white" type="submit">
                    <i class="fas fa-check fa-sm"></i>
                </button>
            </div>
        </div>
    </form>

    <div class="d-sm-flex py-2 px-1 radius-2 bg-gray-300 align-items-center justify-content-between mb-4 shadow-sm">
        <!-- Topbar Search -->
        <h6 class="text-primary h6 mb-0 bolder">
            Ou distributions d'aujourd'hui
        </h6>
        <div>
            <a href="<?= $this->Url->build(['controller' => 'Distributions', 'action' => 'generation', '?' => ['auto' => 2]]); ?>" class="d-xs-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Automatique</a>
            <a href="<?= $this->Url->build(['controller' => 'Distributions', 'action' => 'generation', '?' => ['auto' => 1]]); ?>" class="d-xs-inline-block btn btn-sm btn-info shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Manuelle</a>
        </div>
    </div>

    <?php if(isset($orderDistributions)) : $n = count($orderDistributions); ?>
        <form method="POST" action="<?= $this->Url->build(['controller' => 'Distributions', 'action' => 'generation']); ?>" class="d-sm-flex animated fadeIn py-2 mt-2 px-1 radius-2 <?= $n > 0 ? 'alert-success' : 'alert-danger' ?> align-items-center justify-content-between mb-4 shadow-sm">
            <!-- Topbar Search -->
            <?php if($n > 0) : ?>
                <input type="hidden" name="type" value="<?= $type == false ? 3 : $type ?>">
                <input type="hidden" name="date" value="<?= $date->format('Y-m-d') ?>">
                <input type="hidden" name="generation" value="1">
                <h6 class="text-success h6 mb-0 bolder">
                    <?= $n ?> Génération(s) disponibles pour le <?= $date->i18nFormat('dd-MM-yyyy') ?>
                 </h6>
                 <button type="submit" class="d-xs-inline-block btn btn-sm btn-success shadow-sm"><i class="fas fa-check fa-sm text-white"></i> Générer</button>
            <?php else : ?>
                <h6 class="text-danger h6 mb-0 bolder">
                    Aucune génération disponible pour le <?= $date->i18nFormat('dd-MM-yyyy') ?>
                 </h6>
            <?php endif; ?>
        </form>
    <?php endif; ?>
    <!-- DataTales Example -->
    <div class="table-responsive shadow p-2">
        <table class="table table-borderless table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>N°</th>
                    <th>Montant</th>
                    <th>Client</th>
                    <th>Date</th>
                    <th>Statut</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>N°</th>
                    <th>Montant</th>
                    <th>Client</th>
                    <th>Date</th>
                    <th>Statut</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <tbody>
                <?php foreach ($orders as $order) : ?>
                    <tr>
                        <td><?= $order->id; ?></td>
                        <td><?= $this->Number->format($order->amount, ['locate' => 'fr_FR']); ?></td>
                        <td><?= $order->poi_id !== null ? $order->pois->dns : $order->poi_customer->fn . " " . $order->poi_customer->ln ?></td>
                        <td>
                            <?= $date->nice('Africa/Dakar','fr-Fr') ?>
                        </td>
                        <td>
                            En attente
                        </td>
                        <td>
                            <a title="distribuer la commande" href="<?= $this->Url->Build(['controller' => 'Orders', 'action' => 'distribuer', $order->id]) ?>" class="bg-white shadow-sm btn-sm mx-1">
                                Génération <i class="fas fa-share-alt"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?php $this->start('script_bottom'); ?>
    <!-- Page le*vel plugins -->
    <?= $this->Element('Components/exportScript') ?>
    <script>
        $(function() {
            $('#type').change(function() {
                let val = Number($(this).val());

                if(val == 0) {
                    $('#generation').val('0');
                    $('#generation').prop('disabled', true);
                } else $('#generation').prop('disabled', false);
            });
        });
    </script>
<?php $this->end(); ?>
