<?php
    $this->extend('/Cell/exports');
    $this->assign('title', 'Exports - Liste des Utilisateurs');
    $this->start('table');
?>
<table class="table table-borderless table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>Nom</th>
        <th>Prénom</th>
        <th>Email</th>
        <th>Téléphone</th>
        <th>Profil</th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user) : if(count($user->roles) > 0) $role = $user->roles[0]; else $role = null;?>
            <tr>
                <td><?= $user->fn; ?></td>
                <td><?= $user->ln; ?></td>
                <td><?= $user->email; ?></td>
                <td><?= $user->phone; ?></td>
                <td><?= isset($role) ? $role->profile->name: 'non-defini' ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php $this->end(); ?>
