<?php
    $this->extend('/Cell/exports');
    $this->assign('title', 'Exports - Liste des POIs');
    $this->start('table');
?>
<table class="table table-borderless table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>Type</th>
        <th>DNS</th>
        <th>Téléphone</th>
        <th>Distributeur</th>
        <th>Latittude</th>
        <th>Longitude</th>
        <th>Création</th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($pois as $poi): ?>
            <tr>
                <td><small><?= $poi->type_pois->name; ?></small></td>
                <td><?= $poi->dns; ?></td>
                <td><?= $poi->phone; ?></td>
                <td><?= $poi->infos[0]->value;?></td>
                <td><?= $poi->lat;?></td>
                <td><?= $poi->lng;?></td>
                <td>
                    <small>
                        <?= $poi->created->nice('Africa/Dakar','fr-Fr') ?>
                    </small>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php $this->end(); ?>
