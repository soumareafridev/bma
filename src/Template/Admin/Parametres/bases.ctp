
<!-- Page Heading -->
<div class="col-md-12">
    <div class="d-sm-flex align-items-center justify-content-between mb-4 mt-2 customer__header bg-primary text-white">
        <h1 class="h3 text-gray-800">Paramètres - bases</h1>
    </div>
</div>

<?php for($i=0; $i<sizeof($cards);$i++): $card = $cards[$i];?>
    <div class="<?= $i ==(sizeof($cards)-1) ? "col-md-12" : "col-md-6" ?>">
        <!-- Collapsable Card Example -->
        <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapse<?= $i ?>ok" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapse<?= $i ?>ok">
            <h6 class="m-0 font-weight-bold text-primary"><?= $card['title'] ?></h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse show" id="collapse<?= $i ?>ok">
            <div class="card-body">
              <?php if(count($card['listes']) > 0):
                  foreach($card['listes'] as $item): ?>
              <label class="label-radio-gray" style="font-size: 12px">
                  <span>
                      <?= $item->name ?>
                      <?php if($i != (sizeof($cards)-1)): ?>
                        <a href="<?= $this->Url->Build(['controller' => $card['action']['controller'],'action' => "edit", $item->id]) ?>" data-titre="<?= $card['placeholder'] ?>" data-nom="<?= $item->name ?>" data-jours="<?= isset($item->jours) ? $item->jours : '' ?>" data-toggle="modal" data-target="#<?= $card['modal'] ?>" class="<?= $card['btnModal'] ?>">
                        <i class="fas fa-edit text-success ml-1 px-1"></i></a>
                        <?php echo $this->Form->postLink(__('<i class="fas fa-times text-danger ml-2"></i>'), ['controller' => $card['action']['controller'], 'action' => 'delete', $item->id], ['escape' => false, 'confirm' => __('Voulez vous supprimer :  {0}?', $item->name)]) ?>
                        <?php endif; ?>
                  </span>
              </label>
              <?php endforeach; else: ?>
              <div class="text-center">Aucune données disponibles</div>
              <?php endif; ?>
            </div>
            <?php if($i != (sizeof($cards)-1)): ?>
            <form class="card-footer text-right d-sm-flex align-items-center justify-content-around" action="<?= $this->Url->Build($card['action']) ?>" method="post">
                <input type="text" name="name" class="btn btn-sm bg-white shadow-sm" required placeholder="<?= $card['placeholder'] ?>">

                  <!-- <input type="number" min="1" name="jours" class="btn btn-sm bg-white shadow-sm" required placeholder="Nombre de jours">  -->

                <button type="submit" class="btn btn-sm btn-primary text-white">Ajouter <i class="fas fa-plus-circle ml-2"></i></button>
            </form>
            <?php endif; ?>
        </div>
        </div>
    </div>
<?php endfor; ?>

  <!-- Groupe Modal-->
  <div class="modal fade" id="gammeModal" tabindex="-1" role="dialog" aria-labelledby="gammeModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <form method="post" action="" class="modal-content" id="gammeForm">
        <div class="modal-header">
          <h5 class="modal-title" id="gammeModalLabel">Modification de la fréquence</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="nameModal" id="labelModal"></label>
                <input type="text" id="nameModal" name="name" required class="form-control form-control-sm" >
            </div>
            <div class="form-group">
                <label for="joursModal">Correspondance en jours</label>
                <input type="number" id="joursModal" min="1" name="jours" required class="form-control form-control-sm" >
            </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-sm btn-info" type="submit">Enregistrer</button>
        </div>
      </form>
    </div>
  </div>

  <!-- Groupe and Statut Modal-->
  <div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <form method="post" action="" class="modal-content" id="staticForm">
        <div class="modal-header">
          <h5 class="modal-title" id="staticModalLabel">Modification</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="nameModal" id="labelStaticModal"></label>
                <input type="text" id="nameStaticModal" name="name" required class="form-control form-control-sm" >
            </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-sm btn-info" type="submit">Enregistrer</button>
        </div>
      </form>
    </div>
  </div>

<?php $this->start('script_bottom'); ?>
    <script>
        // $("#gammeForm").submit(function() {
        //     $("#gammeModal").modal('hide');
        // });
        // $("#staticForm").submit(function() {
        //     $("#staticModal").modal('hide');
        // });

        $(".editGamme").click(function() {
            $("#labelModal").text($(this).data("titre"));
            $("#nameModal").val($(this).data("nom"));
            $("#joursModal").val($(this).data("jours"));
            $("#gammeForm").attr('action', $(this).attr("href"));
        });

        $(".editStatic").click(function() {
            $("#labelStaticModal").text($(this).data("titre"));
            $("#nameStaticModal").val($(this).data("nom"));
            $("#staticForm").attr('action', $(this).attr("href"));
        });
    </script>
<?php $this->end(); ?>
