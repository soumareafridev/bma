<?php $this->start('script'); ?>
    <script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>
<?php $this->end(); ?>
<div class="col-md-12">
    <div class="d-sm-flex align-items-center justify-content-between mb-4 mt-2 customer__header bg-primary text-white">
        <h1 class="h3 text-gray-800">Paramètres - Générales</h1>
    </div>
</div>
<div class="col-md-12">
    <div class="card shadow mb-4">
      <div class="card-header py-2">
        <h6 class="m-0 font-weight-bold text-primary d-sm-flex align-items-center justify-content-start">
          <!-- <a href="<?= $this->Url->Build(['action' => 'index']) ?>" class="btn btn-sm btn-primary text-white px-3 text-white mr-2"><i class="fas fa-arrow-left"></i></a> -->
          Paramètres générales du site
        </h6>
      </div>
      <div class="card-body">
        <form class="row"  action="<?= $this->Url->build(['controller' => 'Companies', 'action' => 'edit', $_company->id]) ?>" method="post" enctype="multipart/form-data">
          <div class="col-md-7">
            <div class="form-group">
              <label for="name">Nom de l'entreprise</label>
              <input type="text" name="name" id="name" value="<?= $_company->name ?>" required class="form-control form-control-sm">
            </div>
            <div class="form-group">
              <label for="logo">Choisir une image de logo <span class="badge pull-right">max : 1MB</span></label>
              <input type="file" name="image" accept="image/*" id="logo" class="form-control form-control-sm form-control-file">
            </div>
            <div class="form-group">
              <label for="logo">Choisir votre thème </label>
              <select name="theme_id" class="form-control form-control-sm" id="themes">
                <option value>Selectionner votre thème</option>
                <?php if(count($themes) > 0) : foreach($themes as $theme) : ?>
                  <option value="<?= $theme->id ?>" <?= $_company->theme_id == $theme->id ? 'selected' : '' ?> data-css="<?= $theme->css ?>" data-url="<?= $this->Url->image('themes/'. $theme->image) ?>"><?= $theme->name ?></option>
                <?php endforeach; endif; ?>
              </select>
            </div>
            <div class="form-group">
              <label for="short">Nom de l'entreprise en <mark>3 lettres</mark> <span class="badge pull-right">max : 3</span></label>
              <input type="text" name="short_name" id="short" maxlength="3" value="<?= $_company->short_name ?>" class="form-control limited form-control-sm">
            </div>
            <div class="form-group">
              <label for="word">Votre activité en <mark>un mot</mark> <span class="badge pull-right">max : 25</span></label>
              <input type="text" name="one_word" id="word" maxlength="25" value="<?= $_company->one_word ?>" class="form-control limited form-control-sm">
            </div>

            <div class="form-group">
              <label for="slogan">Votre slogan <span class="badge pull-right">max : 255</span></label>
              <textarea name="slogan" id="slogan" maxlength="255" class="form-control form-control-sm limited" style="resize: none;" rows="10"><?= $_company->slogan ?></textarea>
            </div>

            <div class="form-group">
              <label for="description">Description complète de votre activité et entreprise</label>
              <textarea name="description" id="description" class="form-control form-control-sm" style="resize: none;" rows="10"><?= $_company->description ?></textarea>
            </div>

          </div>
          <div class="col-md-5">
            <div class="card shadow-sm">
              <div class="card-header">
                Prévisualisation avec logo
                <?php if($_company->logo !== null) { ?>
                  <a href="<?= $this->Url->build(['controller' => 'companies', 'action' => 'deletelogo', $_company->id]) ?>" class="fa-pull-right" title="Supprimer l'ancien logo">
                    <i class="fas fa-times-circle text-danger"></i>
                  </a>
                <?php } ?>
              </div>
              <div class="card-body text-center" id="prev_logo">
                <a class="sidebar-brand bg-white" href="#" style="min-height: 120px">
                    <img id="image_logo" src="<?= $this->Url->image($_company->logo) ?>" alt="image de logo" style="max-width: 87px">
                </a>
              </div>
            </div>
            <div class="card mt-3 shadow-sm">
              <div class="card-header">
                Prévisualisation sans logo
              </div>
              <div class="card-body text-center" id="prev_texte">
                <a class="sidebar-brand bg-white" href="#" style="min-height: 120px">
                    <b class="h2 bolder text-rose text-uppercase" id="text_short"><?= $this->Vue->getInfoSite($_user->company_id, 'short_name') ?></b> <br>
                    <small class="text-primary text-capitalize bolder" id="text_word"><?= $this->Vue->getInfoSite($_user->company_id, 'one_word') ?></small>
                </a>
              </div>
            </div>
            <div class="card mt-3 shadow-sm">
              <div class="card-header">
                Prévisualisation de votre thème
              </div>
              <div class="card-body text-center" id="prev_theme" style="padding: 0">
                <img id="theme_img" style="width: 100%; height: auto" src="<?= $_company->theme_id == null ? $this->Url->image('themes/theme-default.png') :  $this->Url->image('themes/'. $_company->theme->image) ?>" alt="Theme du site">
              </div>
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <button type="submit" class="btn btn-block btn-primary"><i class="fas fa-check mr-2"></i> Enregistrer</button>
            </div>
          </div>
        </form>
      </div>
    </div>
</div>

<?php $this->start('script_bottom'); ?>
    <script>
        CKEDITOR.replace('description', {
            // language: 'fr',
            //uiColor: '#2C80BE'
        });
        $(document).ready(function() {
            var description = $('#description').val();
            //Bind value description
            $('#description').change(function() {
                description = $(this).val();
            });

            $('#short').on('keyup',function() {
              $('#text_short').text($(this).val());
              if($(this).val().length == 0) $('#text_short').text('[---]');
            });

            $('#word').on('keyup',function() {
              $('#text_word').text($(this).val());
              if($(this).val().length == 0) $('#text_word').text('[--Un mot--]');
            });

            function readURL(input) {
              if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                  $('#image_logo').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
              }
            }

          $('#themes').change(function() {
            $('#theme_img').attr('src',$('#themes option:selected').data('url'));
          });

          $("#logo").change(function() {
            readURL(this);
          });

        });
    </script>
<?php $this->end(); ?>
