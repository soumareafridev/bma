<!-- Page Heading -->
<div class="col-md-12">
    <div class="d-sm-flex align-items-center justify-content-start mb-2 mt-2">
        <h1 class="h4 text-gray-800">
        <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-sm btn-primary text-white px-3 text-white mr-2"><i class="fas fa-arrow-left"></i></a>
            Visualisation - <small class="h6">enquête <span class="badge bg-primary text-white"> N° <?= $gestion->id ?></span></small>
        </h1>
    </div>
</div>

<div class="col-md-12">
    <!-- Collapsable Card Example -->
    <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapseModif" class="d-block card-header py-3 bg-primary" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseModif">
            <h6 class="m-0 font-weight-bold text-white">Informations générales</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse show" id="collapseModif">
            <div class="list-group list-pois m-2">
                <li class="list-group-item shadow-sm li-detail-item d-sm-flex align-items-center justify-content-between">
                    <span class="text-gray-600">Nom de l'enquête</span>
                    <span class="bolder"><?= h($survey->name) ?></span>
                    <a href="<?= $this->Url->Build(['controller' => 'Surveys','action' => 'edit',$survey->id]) ?>" class="btn btn-sm btn-primary text-white">
                        passer à l'édition
                    </a>
                </li>
                <li class="list-group-item shadow-sm li-detail-item d-sm-flex align-items-center justify-content-between">
                    <span class="text-gray-600">Equipe associée</span>
                    <span class="bolder"><?= h($team->name) ?></span>
                    <a href="<?= $this->Url->Build(['controller' => 'Teams','action' => 'edit',$team->id]) ?>" class="btn btn-sm btn-primary text-white">
                        passer à l'édition
                    </a>
                </li>
                <li class="list-group-item shadow-sm li-detail-item d-sm-flex align-items-center justify-content-between">
                    <span class="text-gray-600">Formulaire attribué</span>
                    <span class="bolder"><?= h($survey->form->name) ?></span>
                    <a href="<?= $this->Url->Build(['controller' => 'Forms','action' => 'edit',$survey->form->id]) ?>" class="btn btn-sm btn-primary text-white">
                        passer à l'édition
                    </a>
                </li>
                <li class="list-group-item shadow-sm li-detail-item d-sm-flex align-items-center justify-content-between">
                    <span class="text-gray-600">Type de POI à collecter</span>
                    <span class="bolder"><?= h($survey->type_pois->name) ?></span>
                </li>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <!-- Collapsable Card Example -->
    <div class="card shadow mb-4">
    <!-- Card Header - Accordion -->
    <a href="#collapseList" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseList">
        <h6 class="m-0 font-weight-bold text-primary">Les membres de l'équipe <sub class="badge bg-primary text-white"><?= h($team->name) ?></sub></h6>
    </a>
    <!-- Card Content - Collapse -->
    <div class="collapse show" id="collapseList">
        <div class="card-body">
            <?= $this->Element('Components/listMembers') ?>
        </div>
    </div>
    </div>
</div>

<div class="col-md-12">
    <!-- Collapsable Card Example -->
    <div class="card shadow mb-4">
    <!-- Card Header - Accordion -->
    <a href="#collapseForm" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseForm">
        <h6 class="m-0 font-weight-bold text-primary">Les champs du Formulaire <sub class="badge bg-primary text-white"><?= h($survey->form->name) ?></sub></h6>
    </a>
    <!-- Card Content - Collapse -->
    <div class="collapse show" id="collapseForm">
        <div class="card-body">
            <div class="list-group list-pois m-2">
                <?php foreach($fields as $field): ?>
                <li class="list-group-item shadow-sm li-detail-item d-sm-flex align-items-center justify-content-between">
                    <span class="text-gray-800"><sup>question </sup><?= h($field->name) ?></span>
                    <?= $field->choices !== null ? "<span class=\"text-gray-600\"><sup>choix </sup> $field->choices </span>": '' ?>
                    <span class="marqueur-blue"><sup>type </sup> <?= h($field->type_field->name) ?></span>
                </li>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    </div>
</div>

<?php $this->start('script_bottom'); ?>
    <?= $this->Element('Components/exportScript') ?>
<?php $this->end(); ?>
