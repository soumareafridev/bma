
<div class="col-lg-12 mt-1">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4 mt-2 customer__header bg-primary text-white">
        <h1 class="h4 text-gray-800">Gestion Enquêtes</h1>
    </div>
    <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapseList" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseList">
            <h6 class="m-0 font-weight-bold text-primary">Les Enquêtes démarrées</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse show" id="collapseList">
            <div class="card-body">
                   <!-- DataTales Example -->
                <div class="table-responsive shadow p-4">
                    <table class="table table-borderless table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Enquête</th>
                                <th>Equipe</th>
                                <th>Formulaire</th>
                                <th>Type POI</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($gestions as $gestion) : $survey = $gestion->survey;$team = $gestion->team;?>
                                <tr>
                                    <td><?= $gestion->id; ?></td>
                                    <td><?= $survey->name; ?></td>
                                    <td><?= $team->name; ?></td>
                                    <td><?= $survey->form->name; ?></td>
                                    <td><?= $survey->type_pois->name; ?></td>
                                    <td class="text-center">
                                        <a href="#"
                                            class="bg-white shadow-sm btn-sm mx-1 text-warning shareModal"
                                            data-toggle="modal"
                                            data-target="#shareModal"
                                            data-link="<?= $this->Url->Build(['prefix' => false, 'controller' => "Pois", "action" => "formulaire", "?" => [
                                                'hash' => substr(sha1($survey->id),0,5),
                                                "enquete" => $survey->id,
                                                "company_id" => $survey->company_id,
                                                'endhash' => substr(sha1($survey->id),0,5),
                                            ],
                                            '_full' => true]) ?>"
                                            data-label="<?= $survey->name ?>"
                                            >
                                            <i class="fas fa-share-alt"></i>
                                        </a>
                                        <a href="<?= $this->Url->Build(['controller' => 'Gestions', 'action' => 'consulter', $gestion->id]) ?>" class="bg-white shadow-sm btn-sm mx-1 text-primary">
                                                <i class="fas fa-eye"></i>
                                        </a>
                                        <?= $this->Form->postLink(__('<i class="fas fa-trash-alt"></i>'), ['controller' => 'Gestions', 'action' => 'delete', $gestion->id], ['escape' => false, 'class' => 'bg-white shadow-sm btn-sm mx-1 text-danger', 'confirm' => __('Voulez vous supprimer l\'élément :  ID: {0}?', $gestion->id)]) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <form class="card-footer" method="post" action="<?= $this->Url->Build(['controller' => 'Gestions','action' => "add"]) ?>">
                <table class="table table-borderless">
                    <tr>
                        <td>
                            <?= $this->Form->select('survey_id', $surveys,['required','class' => 'form-control','empty' => 'Choisir une enquête']); ?>
                        </td>
                        <td>
                        <?= $this->Form->select('team_id', $teams,['required','class' => 'form-control','empty' => 'Choisir une équipe']); ?>
                        </td>
                        <td>
                            <button type="submit" class="btn btn-block btn-primary"><i class="fas fa-plus mr-2"></i>Démarrer</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>

<?php for($i=0; $i<sizeof($cards);$i++): $card = $cards[$i]; ?>
    <div class="col-md-12">
        <!-- Collapsable Card Example -->
        <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapse<?= $i ?>ok" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapse<?= $i ?>ok">
            <h6 class="m-0 font-weight-bold text-primary"><?= $card['title'] ?></h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse show" id="collapse<?= $i ?>ok">
            <div class="card-body">
              <table class="table table-borderless table-hover table-striped" class="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                      <th>Intitulé</th>
                      <?php if($i == 0): ?>
                        <th>Type</th>
                        <th>Formulaire</th>
                      <?php endif; ?>
                      <?php if($i == 1): ?>
                        <th>Nombre de membres</th>
                      <?php endif; ?>
                      <?= $i !== 3 ? '<th>Actions</th>': '' ?>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach($card['listes'] as $item): ?>
                        <tr>
                            <td><?= $item->name; ?></td>
                            <?php if($i == 0): ?>
                              <td><?= $item->type_pois->name; ?></td>
                              <td><?= $item->form->name; ?></td>
                            <?php endif; ?>
                            <?php if($i == 1): ?>
                            <td><?= count($item->members); ?></td>
                          <?php endif; ?>
                            <?php if($i !== 3): ?>
                            <td>
                              <a href="<?= $this->Url->Build(['controller' => $card['action']['controller'],'action' => "edit", $item->id]) ?>" class="bg-white shadow-sm btn-sm">
                                <i class="fas fa-cogs text-primary"></i>
                              </a>
                              <a href="<?= $this->Url->Build(['controller' => $card['action']['controller'],'action' => "edit", $item->id]) ?>" data-titre="<?= $card['placeholder'] ?>" data-nom="<?= $item->name ?>" data-toggle="modal" data-target="#<?= $card['modal'] ?>" class="bg-white shadow-sm btn-sm mx-1 <?= $card['btnModal'] ?>">
                                <i class="fas fa-edit text-success"></i>
                              </a>
                                <?= $this->Form->postLink(__('<i class="fas fa-times text-danger"></i>'), ['controller' => $card['action']['controller'], 'action' => 'delete', $item->id], ['escape' => false,'class' => 'bg-white shadow-sm btn-sm mx-1', 'confirm' => __('Voulez vous supprimer :  {0}?', $item->name)]) ?>
                            </td>
                            <?php endif; ?>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
              </table>
            </div>
            <form class="card-footer text-right d-sm-flex align-items-center justify-content-around" action="<?= $this->Url->Build($card['action']) ?>" method="post">
                <input type="text" <?= $i ==3 ? 'disabled': "" ?> name="name" class="btn btn-sm bg-white shadow-sm" required placeholder="<?= $card['placeholder'] ?>">
                <?php if($i == 0):
                    echo $this->Form->select('type_poi_id', $card['type_pois'],['required', 'class' => 'btn btn-sm bg-white shadow-sm','empty' => 'Type de POIs']);
                    echo $this->Form->select('form_id', $card['forms'],['required', 'class' => 'btn btn-sm bg-white shadow-sm','empty' => 'Choix du formulaire']);
                endif; ?>
                <button type="submit" <?= $i ==3 ? 'disabled': "" ?> class="btn btn-sm btn-primary text-white text-white">Ajouter <i class="fas fa-plus-circle ml-3"></i></button>
            </form>
        </div>
    </div>
    </div>
<?php endfor; ?>

  <!-- Groupe and Statut Modal-->
  <div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <form method="post" action="" class="modal-content" id="staticForm">
        <div class="modal-header">
          <h5 class="modal-title" id="staticModalLabel">Modification</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="nameModal" id="labelStaticModal"></label>
                <input type="text" id="nameStaticModal" name="name" required class="form-control form-control-sm" >
            </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-sm btn-info" type="submit">Enregistrer</button>
        </div>
      </form>
    </div>
  </div>

  <!-- Share Modal -->
  <div class="modal fade" id="shareModal" tabindex="-1" role="dialog" aria-labelledby="shareModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="shareModalLabel">Lien Public</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <label for="" id="labelShare">Enquet</label>
            <div class="form-group text-center">
                <input type="text" class="form-control form-control-plaintext" disabled value="hheheheheheheh" id="myInput">  <hr>
                <button class="btn btn-lg btn-success" id="btnShare" onclick="myFunction()">Copier le texte</button>


            </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Ok, fermer</button>
          <!-- <button class="btn btn-sm btn-info" type="button">Fermer</button> -->
        </div>
      </form>
    </div>
  </div>

<?php $this->start('script_bottom'); ?>
    <?= $this->Element('Components/exportScript') ?>
    <script>
        $(".editStatic").click(function() {
            $("#nameStaticModal").val($(this).data("nom"));
            $("#staticForm").attr('action', $(this).attr("href"));
        });

        $(".shareModal").click(function() {
            $("#labelShare").text($(this).data("label"));
            $("#myInput").val($(this).data("link"));
        });

        function myFunction() {
            /* Get the text field */
            var copyText = document.getElementById("myInput");

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /* For mobile devices */

            /* Copy the text inside the text field */
            navigator.clipboard.writeText(copyText.value);

            /* Alert the copied text */
            $('#btnShare').text('Copié avec succès !');
            setTimeout(() => {
                $('#btnShare').text('Copier le texte');
            }, 3000);
        }
    </script>
<?php $this->end(); ?>
