<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PoiCustomer $poiCustomer
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Poi Customer'), ['action' => 'edit', $poiCustomer->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Poi Customer'), ['action' => 'delete', $poiCustomer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poiCustomer->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Poi Customers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Poi Customer'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Customer Types'), ['controller' => 'CustomerTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Customer Type'), ['controller' => 'CustomerTypes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pois'), ['controller' => 'Pois', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pois'), ['controller' => 'Pois', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="poiCustomers view large-9 medium-8 columns content">
    <h3><?= h($poiCustomer->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Fn') ?></th>
            <td><?= h($poiCustomer->fn) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ln') ?></th>
            <td><?= h($poiCustomer->ln) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Phone') ?></th>
            <td><?= h($poiCustomer->phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Customer Type') ?></th>
            <td><?= $poiCustomer->has('customer_type') ? $this->Html->link($poiCustomer->customer_type->name, ['controller' => 'CustomerTypes', 'action' => 'view', $poiCustomer->customer_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pois') ?></th>
            <td><?= $poiCustomer->has('pois') ? $this->Html->link($poiCustomer->pois->id, ['controller' => 'Pois', 'action' => 'view', $poiCustomer->pois->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($poiCustomer->id) ?></td>
        </tr>
    </table>
</div>
