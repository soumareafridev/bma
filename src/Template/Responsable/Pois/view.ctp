  <?php $this->layout = 'accueil'; ?>
  <div class="col-md-7 col-lg-8 pt-5" style="max-height: calc(100vh - (37px));min-height: calc(100vh - (37px));overflow-y:auto">

    <!-- Content Row -->
    <form class="row">
      <div class="list-group col-md-12 list-pois mb-4">
        <li class="list-group-item li-detail d-sm-flex align-items-center justify-content-between" id="elt" data-created="<?= $poi->created->nice('Africa/Dakar','fr-Fr') ?>" data-id="<?= $poi->id ?>" data-pin="<?= $this->Url->image('md-pin.png') ?>" data-dns="<?= $poi->dns ?>" data-lng="<?= $poi->lng ?>" data-lat="<?= $poi->lat ?>" data-img="<?= $poi->photo !== null ? $this->Url->image('pois/'.$poi->photo) : null ?>">
          <div class="info">
            <h3 class="h4 bold mb-0"><?= h($poi->dns) ?></h3>
            <small><i class="fas fa-phone-alt"></i> Téléphone: <?= h($poi->phone) ?></small>
          </div>
          <div class="info-sup">
            <span class="marqueur-blue"><?= h($poi->type_pois->name) ?></span> <br>
            <small>Enregistré le <?= $poi->created->nice('Africa/Dakar','fr-Fr') ?></small>
          </div>
          <a href="javascript:history.back()"><i class="fas fa-arrow-left"></i></a>
        </li>
      </div>
      <div class="col-md-8">
          <h3 class="h5 bold text-gray-500 text-uppercase">Informations POI</h3>
          <div class="list-group list-pois">
              <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                <span class="text-gray-500">Enquêteur</span>
                <span><?= h($poi->user->fn . ' '.$poi->user->ln ) ?></span>
              </li>
              <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                <span class="text-gray-500">Lattitude</span>
                <span class="marqueur-blue"><?= h($poi->lat) ?></span>
              </li>
              <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                <span class="text-gray-500">Longititude</span>
                <span class="marqueur-blue"><?= h($poi->lng) ?></span>
              </li>
              <li class="list-group-item input-info hidden li-detail-item d-sm-flex align-items-center justify-content-between">
                <span class="text-gray-500">Nom du POI</span>
                <input type="text" name="poi[dns]" value="<?= $poi->dns ?>" class="px-1 hidden input-info"/>
              </li>
              <li class="list-group-item input-info hidden li-detail-item d-sm-flex align-items-center justify-content-between">
                <span class="text-gray-500">Numéro de Téléphone</span>
                <input type="text" name="poi[phone]" value="<?= $poi->phone ?>" class="px-1 hidden input-info"/>
              </li>
              <li class="list-group-item input-info hidden li-detail-item d-sm-flex align-items-center justify-content-between">
                <span class="text-gray-500">Catégorie</span>
                <?= $this->Form->select('poi[type_poi_id]',$types,['class' => 'px-1 hidden input-info', 'default' => $poi->type_poi_id,'required']) ?>
              </li>
              <?php foreach($infos as $info): ?>
              <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                <small class="text-gray-500"><?=  h($info->field->name) ?></small>
                <span class="span-info"><?= $info->value !== null && $info->value !== "" && $info->value !== "null" ?  h($info->value) : "<i><small>(non renseigné)</small></i>" ?></span>
                <?php if($info->field->type_field_id !== 5 && $info->field->type_field_id !== 7 && $info->field->type_field_id !== 8): ?>
                  <input type="text" name="infos[<?= $info->id ?>]" value="<?= $info->value ?>" class="px-1 hidden input-info"/>
                <?php endif; ?>
                <?php if($info->field->type_field_id === 5): $choix = explode(";",$info->field->choices);?>
                  <select name="infos[<?= $info->id ?>]" class="px-1 hidden input-info">
                      <?php foreach($choix as $c): ?>
                        <option value default disabled>Selectionner une choix</option>
                        <option value="<?= $c ?>" <?= $c == $info->value ? "selected" : '' ?>><?= $c ?></option>
                      <?php endforeach; ?>
                  </select>
                <?php endif; ?>
                <?php if($info->field->type_field_id == 7 || $info->field->type_field_id == 8): ?>
                  <input type="date" name="infos[<?= $info->id ?>]" value="<?= $info->value ?>" class="px-1 hidden input-info"/>
                <?php endif; ?>
              <?php endforeach; ?>
          </div>
      </div>
      <div class="col-md-4">
          <h3 class="h5 bold text-gray-500 text-uppercase">Photo POI</h3>
          <?php if($poi->photo): ?>
            <a href="<?= $this->Url->image('pois/'.$poi->photo) ?>" data-fancybox="<?=h($poi->dns) ?>" data-caption="<?=h($poi->dns) ?>">
              <img style="max-width: 100%; cursor: zoom-in" src="<?= $this->Url->image("pois/$poi->photo") ?>" alt="Photo du POI">
            </a>
          <?php else: ?>
          <div class="jumbotron text-center">
              <h4 class="h5 text-gray-400">
                  Aucune photo enregistrée
              </h4>
          </div>
          <?php endif; ?>
      </div>
    </form>
  </div>
  <div class="col-md-5 col-lg-4 hidden-sm bg-primary text-white text-center" style="max-height: calc(100vh - (37px));min-height: calc(100vh - (37px));overflow:hidden" id="map"></div></div>

  <?php $this->start('script_bottom'); ?>
  <script>
    function initMap2() {
    $(function() {
      $("#edit").click(function() {
        $(".span-info, #edit").toggleClass('animated fadeOut hidden');
        $(".input-info, #cancel, #save").toggleClass('animated fadeIn hidden');
      });

      $("#cancel").click(function() {
        $(".span-info, #edit").toggleClass('animated fadeOut hidden');
        $(".input-info, #cancel, #save").toggleClass('animated fadeIn hidden');
      });



        var map;
        var marker;
        var lattitude = $('#elt').data('lat');
        var longitude = $('#elt').data('lng');

        var map = new google.maps.Map(
            document.getElementById('map'),
            {
                center: {lat : lattitude, lng : longitude},
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
        );

         marker = new google.maps.Marker({
            position: { lat: lattitude, lng: longitude },
            title: $('#elt').data('dns'),
            icon: $('#elt').data('pin')
        });

        var infowindow = new google.maps.InfoWindow({
                content: contentWindow($('#elt').data('id'),$('#elt').data('lat'),$('#elt').data('lng'),$('#elt').data('dns'),$('#elt').data('created'),$('#elt').data('img'))
              });

          marker.setMap(map);

          marker.addListener('click', function() {
                infowindow.open(map, marker);
          });

        infowindow.open(map, marker);


        function contentWindow(id,lat,lng,dns,date,img) {
            var content = '<div class="p-2 text-center" style="color:black">'+
            '<h1 class="h5 bolder text-primary">'+dns+'</h1>'+
            '<p><small class="text-gray-600">Enregistré le '+date+'</small></p>'+
            '<p>'+
                '<small>lat:'+lat+' lng:'+lng+' </small>'+
            '</p>';
            if(img) {
                content += '<p><a href="'+img+'" data-fancybox="'+dns+'-map" data-caption="'+dns+'-map" style="display:inline-block">'+
                    '<img src="'+img+'" class="ml-1" style="max-width: 100%;border-radius: 4px;box-shadow: 0 2px 3px rgba(0,0,0,.2);cursor:zoom-in" alt="Photo enregistrée de la POI">'
                '</a>'+
            '</p>';
            }

            return content;
        }
    });
  }
  </script>
  <?php $this->end(); ?>
