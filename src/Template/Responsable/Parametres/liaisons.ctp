<!-- Page Heading -->
<div class="col-md-12">
    <div class="d-sm-flex align-items-center justify-content-between mb-4 mt-2 customer__header bg-primary text-white">
        <h1 class="h3 text-gray-800">Liste des contacts</h1>
    </div>
</div>

<div class="col-md-12">
        <!-- Collapsable Card Example -->
        <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapseListe" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseListe">
            <h6 class="m-0 font-weight-bold text-primary">Les contacts</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse show" id="collapseListe">
            <div class="card-body">
                <table class="table table-borderless table-hover table-striped" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Type</th>
                        <th>Prénom & Nom</th>
                        <th>Téléphone</th>
                        <th>POIs</th>
                        <th>Type de POIs</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($customers as $customer): ?>
                            <tr>
                                <td><?= $customer->customer_type->name ?></td>
                                <td><?= $customer->fn . " " . $customer->ln ?></td>
                                <td><?= $customer->phone ?></td>
                                <td><?= $customer->pois->dns ?></td>
                                <td><?= $customer->pois->type_pois->name ?></td>
                                <td>
                                    <a href="<?= $this->Url->Build(['controller' => 'PoiCustomers','action' => "edit", $customer->id]) ?>" class="bg-white shadow-sm btn-sm mx-1" >
                                        <i class="fas fa-edit text-success"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                </div>
                <div class="card-footer text-right d-sm-flex align-items-center justify-content-end">
                    <a href="<?= $this->Url->Build(['controller' => "PoiCustomers",'action' => "add"]) ?>" class="btn btn-primary btn-sm"> Ajouter un contact <i class="fas fa-plus-circle ml-3"></i></a>
                </div>
            </div>
    </div>
    <!-- Collapsable Card Example -->
    <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapseActeur" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseActeur">
            <h6 class="m-0 font-weight-bold text-primary">Les types de contacts</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse show" id="collapseActeur">
            <div class="card-body">
                <?php if(count($type_customers) > 0):
                    foreach($type_customers as $item): ?>
                <label class="label-radio-gray" style="font-size: 12px">
                    <span>
                        <?= $item->name ?>
                            <!-- <a href="<?= $this->Url->Build(['controller' => "CustomerTypes",'action' => "edit", $item->id]) ?>">
                            <i class="fas fa-edit text-success ml-1 px-1"></i></a> -->
                            <?php echo $this->Form->postLink(__('<i class="fas fa-times text-danger ml-2"></i>'), ['controller' => "CustomerTypes", 'action' => 'delete', $item->id], ['escape' => false, 'confirm' => __('Voulez vous supprimer :  {0}?', $item->name)]) ?>
                    </span>
                </label>
                <?php endforeach; else: ?>
                <div class="text-center">Aucune données disponibles</div>
                <?php endif; ?>
            </div>
            <form class="card-footer text-right d-sm-flex align-items-center justify-content-around" action="<?= $this->Url->Build(['controller' => "CustomerTypes",'action' => "add"]) ?>" method="post">
                <input type="text" name="name" class="btn btn-sm bg-white shadow-sm" required placeholder="Client,Gérant,...">
                <button type="submit" class="btn btn-sm btn-primary text-white text-white">Ajouter <i class="fas fa-plus-circle ml-3"></i></button>
            </form>
        </div>
    </div>
</div>

<?php $this->start('script_bottom'); ?>
    <?= $this->Element('Components/exportScript') ?>
<?php $this->end(); ?>
