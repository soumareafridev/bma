<?= $this->Html->css('select2.min.css', ['block' => 'css']) ?>
<?= $this->Html->css('slick.css', ['block' => 'css2']) ?>
<div class="col-md-12 mt-3">
    <div class="d-sm-flex align-items-center justify-content-start mb-2 mt-2">
        <h1 class="h4 text-gray-800">
            <a href="javascript:history.back()" class="btn btn-sm btn-primary text-white px-3 text-white mr-2"><i class="fas fa-arrow-left"></i></a>
            Consulter distribution N° : <?= $distribution->id ?>
        </h1>
    </div>
</div>
<div class="col-md-12">
    <div class="row">
        <div class="col-md-12">
            <!-- Collapsable Card Example -->
            <div class="card shadow mb-4">
                <!-- Card Header - Accordion -->
                <a href="#collapseModif" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseModif">
                    <h4 class="m-0 font-weight-bold text-primary"><i class="fas fa-share-alt mr-4"></i> Information de la distribution et du client</h4>
                </a>
                <!-- Card Content - Collapse -->
                <div class="collapse show" id="collapseModif">
                    <div class="card-body row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="list-group card shadow-sm list-pois p-1 col-md-12">
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Type de compte</span>
                                        <span class="bolder spaninfo"><?= $distribution->poi_id !== null ? 'Personne morale' : 'Personne physique' ?></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Nom compte</span>
                                        <span class="bolder spaninfo"><?= $distribution->poi_id !== null ? $distribution->pois->dns : $distribution->poi_customer->fn . ' ' . $distribution->poi_customer->fn ?></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Montant HT</span>
                                        <span class="bolder spaninfo"><?= $this->Number->format($distribution->amount, ['locate' => 'fr_FR', 'after' => ' FCFA']); ?></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Statut</span>
                                        <span class="bolder spaninfo"><?= $distribution->distribution_statut->name; ?></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Attribué à</span>
                                        <span class="bolder spaninfo"><?= isset($distribution->user) ? $distribution->user->fn . ' ' . $distribution->user->ln : 'Non attribuée'; ?></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Lié à la commande</span>
                                        <span class="bolder spaninfo"><a href="<?= $this->Url->Build(['controller' => 'Orders', 'action' => 'view', $distribution->order_id]) ?>" class="btn-success shadow-sm btn-sm mx-1">
                                                <i class="fas fa-shopping-cart text-white"></i> N° <?= $distribution->order_id ?>
                                            </a></span>
                                    </li>
                                    <li class="list-group-item li-detail-item d-sm-flex align-items-center justify-content-between">
                                        <span class="text-gray-600">Date de création</span>
                                        <span class="bolder"><?= $distribution->created->nice('Africa/Dakar','fr-Fr') ?></span>
                                    </li>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <i class="fas fa-cogs"></i> Mise à jour de la distribution
                                </div>
                                <div class="card-body py-2">
                                    <div class="d-sm-flex align-items-center <?= $distribution->distribution_statut_id == 4 ? 'alert-danger' : 'bg-gray-300' ?> py-3 px-1 justify-content-between mb-4" method="POST" action="">
                                        <?php if ($distribution->distribution_statut_id == 1) : ?>
                                            <form method="POST" action="" class="text-right w-100 mb-4">
                                                <input type="hidden" name="distribution_statut_id" value="2">
                                                <select name="user_id" id="user_id" required class="form-control form-control-sm bg-light border-0 small bg-white">
                                                    <option selected disabled>Séléctionner le distributeur</option>
                                                    <?php if (count($roles) > 0) : foreach ($roles as $item) : ?>
                                                            <option value="<?= $item->user->id ?>"><?= $item->user->fn . ' ' . $item->user->ln ?></option>
                                                    <?php endforeach;
                                                        endif; ?>
                                                </select>
                                                <button type="submit" class="d-xs-inline-block mt-1 btn btn-sm btn-primary shadow-sm"><i class="fas fa-check"></i> Attribuer</button>
                                            </form>
                                        <?php elseif ($distribution->distribution_statut_id == 2) : ?>
                                            <form method="POST" action="">
                                                <input type="hidden" name="distribution_statut_id" value="4">
                                                <button type="submit" class="d-xs-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-times-circle"></i> Distribution annulée</button>
                                            </form>
                                            <form method="POST" action="">
                                                <input type="hidden" name="distribution_statut_id" value="1">
                                                <button type="submit" class="d-xs-inline-block btn btn-sm btn-warning shadow-sm"><i class="fas fa-chevron-circle-left"></i> Remettre en attente</button>
                                            </form>
                                            <form method="POST" action="">
                                                <input type="hidden" name="distribution_statut_id" value="3">
                                                <button type="submit" class="d-xs-inline-block btn btn-sm btn-success shadow-sm"><i class="fas fa-check"></i> Distribution livrée</button>
                                            </form>
                                        <?php elseif ($distribution->distribution_statut_id == 3) : ?>
                                            <form method="POST" action="">
                                                <input type="hidden" name="distribution_statut_id" value="4">
                                                <button type="submit" class="d-xs-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-times"></i> Distribution annulée</button>
                                            </form>
                                            <form method="POST" action="">
                                                <input type="hidden" name="distribution_statut_id" value="1">
                                                <button type="submit" class="d-xs-inline-block btn btn-sm btn-warning shadow-sm"><i class="fas fa-chevron-circle-left"></i> Remettre en attente</button>
                                            </form>
                                        <?php elseif ($distribution->distribution_statut_id == 4) : ?>
                                            <div class="text-center">
                                                Cette distribution a été annulée par l'administrateur
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
