<!-- Page Heading -->
<div class="col-md-12">
    <div class="d-sm-flex align-items-center justify-content-start mb-2 mt-2">
        <h1 class="h4 text-gray-800">
        <a href="javascript:history.back()" class="btn btn-sm btn-primary px-3 text-white mr-2"><i class="fas fa-arrow-left"></i></a>
            Configuration - <small class="h6"><?= $form->name ?></small>
        </h1>
    </div>
</div>

    <div class="col-md-12">
        <!-- Collapsable Card Example -->
        <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapseModif" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseModif">
            <h6 class="m-0 font-weight-bold text-primary">Modification du formulaire</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse" id="collapseModif">
            <form class="card-body" method="post" action="<?= $this->Url->Build(['controller' => "Forms",'action' => "edit", $form->id]) ?>">
              <div class="form-group">
                  <label for="name">Intitulé</label>
                  <input type="text" id="name" value="<?= $form->name ?>" class="form-control">
                </div>
              <div class="form-group">
                  <button type="submit" class="btn btn-primary"><i class="fas fa-edit mr-2"></i>Enregistrer</button>
                </div>
            </form>
        </div>
        </div>
    </div>

    <div class="col-md-12">
        <!-- Collapsable Card Example -->
        <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapseList" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseList">
            <h6 class="m-0 font-weight-bold text-primary">Champs liés au formulaire</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse show" id="collapseList">
            <div class="card-body">
                <?= $this->Element('Components/listFields') ?>
            </div>
            <form class="card-footer" method="post" action="<?= $this->Url->Build(['controller' => 'Fields','action' => "add"]) ?>">
                <table class="table table-borderless table-hover table-striped">
                    <tr>
                        <td>
                            <input type="text" name="name" class="form-control" required placeholder="intitulé">
                        </td>
                        <td>
                            <?= $this->Form->select('type_field_id', $types,['id' => 'type_field_id','class' => 'form-control','empty' => 'Type de champs','required']); ?>
                        </td>
                        <td>
                            <input type="text" name="choices" id="choices" placeholder="valeur1;valeur2;..." class="form-control" disabled>
                            <input type="hidden" name="form_id" value="<?= $form->id ?>">
                        </td>
                        <td>
                            <button type="submit" class="btn btn-primary"><i class="fas fa-edit mr-2"></i>Enregistrer</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        </div>
    </div>

    <!-- Groupe and Statut Modal-->
    <div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="post" action="" class="modal-content" id="staticForm">
            <div class="modal-header">
                <h5 class="modal-title" id="staticModalLabel">Modification</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <div class="form-group">
                    <label for="nameModal">Intitulé</label>
                    <input type="text" id="nameStaticModal" name="name" required class="form-control">
                </div>
                <div class="form-group">
                    <label for="typeModal">Type de champ</label>
                    <?= $this->Form->select('type_field_id', $types,['id' => 'typeModal','class' => 'form-control','empty' => 'Type de champs','required']); ?>
                </div>
                <div class="form-group">
                    <label for="choices">Valeurs <small>(séparer les valeurs par point virgule : ";")</small></label>
                    <input type="text" id="choicesModal" disabled name="choices" required class="form-control">
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <button class="btn btn-sm btn-info" type="submit">Enregistrer</button>
            </div>
            </form>
        </div>
    </div>
    <?php $this->start('script_bottom'); ?>
        <?= $this->Element('Components/exportScript') ?>
    <script>
        // $("#gammeForm").submit(function() {
        //     $("#gammeModal").modal('hide');
        // });
        // $("#staticForm").submit(function() {
        //     $("#staticModal").modal('hide');
        // });
        $("#type_field_id").change(function() {
                val = Number($(this).val());
                if([4,5,9].includes(val)) {
                    $("#choices").prop('disabled',false);
                } else {
                    $("#choices").prop('disabled',true);
                }
            });

        $(".editStatic").click(function() {
            $("#nameStaticModal").val($(this).data("nom"));
            if($(this).data("choices")) {
                $("#choicesModal").val($(this).data("choices"));
                $("#choicesModal").prop('disabled',false);
            } else $("#choicesModal").prop('disabled',true);
            $("#typeModal").val($(this).data("type"));
            $("#staticForm").attr('action', $(this).attr("href"));

            $("#typeModal").change(function() {
                val = Number($(this).val());
                if([4,5,9].includes(val)) {
                    $("#choicesModal").prop('disabled',false);
                } else {
                    $("#choicesModal").prop('disabled',true);
                }
            });
        });
    </script>
<?php $this->end(); ?>
