<?php

namespace App\Controller\Responsable;

use App\Controller\Responsable\AppController;
use Cake\I18n\Date;
use Cake\I18n\Time;

class DistributionsController extends AppController
{
    public function index()
    {
        $contain = [
            'contain' => ['Distributions' => [
                'conditions' => ['Distributions.company_id' => $this->Auth->user('company_id')]
            ]],
        ];
        $type_distributions = $this->Distributions->DistributionStatuts->find('all', $contain)->toArray();

        $this->set(compact('type_distributions'));
    }

    public function generation()
    {
        $type = $this->request->getQuery('i');

        if ($type) $this->set(compact('type'));
        $orders = [];

        $auto = $this->request->getQuery('auto');

        if (isset($auto) && $this->request->is('get')) {
            $type = $auto;
            $date = Date::now();


            if ($auto == 1) $createDistribution = false;
            if ($auto == 2) $createDistribution = true;

            $orderDistributions = $this->genererDistributions($createDistribution, $auto);
            $orders = $orderDistributions;

            if ($createDistribution && count($orderDistributions) > 0) {
                $nombreGeneration = count($orders);
                $msg = "<h3 class='my-0'><i class='fas fa-check-circle'></i> Une génération de distributions à été effective !</h3> <p>Vous venez de générer $nombreGeneration distribution(s).</p>";
                $this->Flash->success(__($msg), ['escape' => false]);

                return $this->redirect(['action' => 'visualiser', '?' => $this->hashUrl(1)]);
            }
            $this->set(compact('orderDistributions', 'date', 'type'));
        }


        if ($this->request->is('post')) {
            $temp = $this->request->getData();

            $dateString = $temp['date'];
            $type = $temp['type']; // 1 = manuelle -- 2 = Automatique - 0 = Tout les deux types
            $date = new Time($dateString);
            $createDistribution = isset($temp['generation'])  && $temp['generation'] == 1; // 1 = Générer --- 0 = Visualiser


            if ($type == 0) {
                $type = false;
                $createDistribution = false;
            }

            if ($type == 3) {
                $type = false;
                $createDistribution = true;
            }


            $orderDistributions = $this->genererDistributions($createDistribution, $type, $date);
            $orders = $orderDistributions;

            if ($createDistribution && count($orderDistributions) > 0) {
                $nombreGeneration = count($orders);
                $msg = "<h3 class='my-0'><i class='fas fa-check-circle'></i> Une génération de distributions à été effective !</h3> <p>Vous venez de générer $nombreGeneration distribution(s).</p>";
                $this->Flash->success(__($msg), ['escape' => false]);

                return $this->redirect(['action' => 'visualiser', '?' => $this->hashUrl(1)]);
            }

            $this->set(compact('orderDistributions', 'date', 'type'));
        }
        $this->set(compact('orders'));
    }

    public function visualiser()
    {
        $distribution_statut_id = $this->request->getQuery('i');
        $contain = [
            'contain' => ['Orders', 'Pois', 'PoiCustomers', 'DistributionStatuts'],
            'conditions' => ['Distributions.company_id' => $this->Auth->user('company_id'), 'Distributions.distribution_statut_id' => $distribution_statut_id]
        ];

        $statut_distribution =  $this->Distributions->DistributionStatuts->get($distribution_statut_id);
        $distributions = $this->Distributions->find('all', $contain)->toArray();

        $this->set(compact('statut_distribution', 'distributions'));
    }

    private function infoDate($dateEnChaine, $isString = false)
    { //Exemple '2019-01-23 2:00:00'
        if ($isString) $date = new Time($dateEnChaine);
        else $date = $dateEnChaine;

        return [
            'annee' => $date->format('Y'),
            'mois' => $date->format('m'),
            'jour' => $date->format('N'),
            'date' => $date->format('d')
        ];
    }

    private function diffDateEnJours($date1, $date2)
    { // Date2 - Date1
        return (int) round(strtotime($date2) - strtotime($date1)) / (60 * 60 * 24);
    }

    /*
        Fonction renvoie les distritubions de la date actuelle
        si elle ne reçoit pas de parametre
    */
    public function genererDistributions($createDistribution = true, $type = false, $dateOrToday = null, $dateFin = false)
    {
        if ($dateOrToday) $date = $dateOrToday;
        else $date = Date::now();
        $mois = $date->format('m');
        $jour = $date->format('N');

        $date = $date->i18nFormat('yyyy-MM-dd');

        $contain = [
            'contain' => [
                'OrderLines', 'Pois', 'PoiCustomers',
                'Distributions' => [
                    'DistributionLines', 'sort' => [
                        'Distributions.created' => 'DESC'
                    ]
                ],
            ],
            'conditions' => [
                'Orders.company_id' => $this->Auth->user('company_id'),
                'Orders.order_statut_id' => 2,
                'DATE(Orders.date_debut) <=' => $date
            ]
        ];

        if ($dateFin) {
            $dateFin = new Time($dateFin);
            $dateFin = $dateFin->i18nFormat('yyyy-MM-dd');
            $contain['conditions'][] = ['DATE(Orders.date_limited) <=' => $dateFin];
        }
        if ($type) $contain['conditions'][] = ['Orders.type' => $type];

        if ($type == 2) $contain['conditions'][] = ['DATE(Orders.date_limited) >' => $date];


        //  Premier filtre
        $orderEligbles = $this->Distributions->Orders->find('all', $contain)->toArray();

        $orders = [];

        if (count($orderEligbles) > 0) {
            foreach ($orderEligbles as $order) {

                //Recuperation de la derniere distribution si elle existe
                $last_distribution = null;
                if (count($order->distributions) > 0) $last_distribution = $order->distributions[0];
                if ($last_distribution) { //Si y a une distribution faite déja
                    $last_date_distribution = $this->infoDate($last_distribution->created);
                    $jourEpuises = $this->diffDateEnJours($last_distribution->created, $date);

                    //On sort de la boucle si la dernier distribution est en cours
                    if (in_array($last_distribution->distribution_statut_id, [1, 2])) continue;
                }

                if ($order->frequence_id == 1) { // commande journaliere
                    if (isset($order->frequences) && $order->frequences != null) {
                        if (in_array($jour, explode(';', $order->frequences))) {
                            $orders[] = $order;
                            if ($createDistribution) $this->creerDistribution($order);
                        }
                    }
                } elseif ($order->frequence_id > 1) {
                    if (isset($order->frequences) && $order->frequences != null) {

                        if ($order->frequence_id == 2) { // Commande Hebdomadaire

                            if ($last_distribution) { //Si y a une distribution faite déja
                                if ($jourEpuises > 6 && $jour == trim($order->frequences)) { //La semaine est complete et c'est bien le jour choisis
                                    $orders[] = $order;
                                    if ($createDistribution) $this->creerDistribution($order);
                                }
                            } else { //Si on ne trouve pas de distribution

                                if ($jour == trim($order->frequences)) { //Aucune distribution et c'est le jour pour creer une nouvelle distribution
                                    $orders[] = $order;
                                    if ($createDistribution) $this->creerDistribution($order);
                                }
                            }
                        } elseif ($order->frequence_id == 3) { //Commande Mensuelle

                            if ($last_distribution) { //Si y a une distribution faite déja
                                if ((($jourEpuises >= 28 && (int) $last_date_distribution['mois'] == 2) || $jourEpuises >= 30) && $jour == trim($order->frequences) && (int) $mois > (int) $last_date_distribution['mois']) { //La semaine est complete et c'est bien le jour choisis
                                    $orders[] = $order;
                                    if ($createDistribution) $this->creerDistribution($order);
                                }
                            } else { //Aucune distribution et c'est le jour pour creer une nouvelle distribution
                                if ($jour == trim($order->frequences)) {
                                    $orders[] = $order;
                                    if ($createDistribution) $this->creerDistribution($order);
                                }
                            }
                        } elseif ($order->frequence_id == 4) { //Commande Trimestrielle

                            if ($last_distribution) { //Si y a une distribution faite déja
                                $nombreMoisEpuises = (int) $mois - (int) $last_date_distribution['mois'];
                                if ($nombreMoisEpuises >= 3 && $jour == trim($order->frequences)) { //Si le nombre de mois est superieur ou égal à 3
                                    $orders[] = $order;
                                    if ($createDistribution) $this->creerDistribution($order);
                                }
                            } else {
                                //Aucune distribution et c'est le jour pour creer une nouvelle distribution
                                if ($jour == trim($order->frequences)) {
                                    $orders[] = $order;
                                    if ($createDistribution) $this->creerDistribution($order);
                                }
                            }
                        }
                    }
                }
            }
        }

        //$nombreGeneration = count($orders);
        // if($nombreGeneration > 0) $msg = "<h3 class='my-0'><i class='fas fa-check-circle'></i> Une génération de distributions à été effective !</h3> <p>Vous venez de générer $nombreGeneration distribution(s).</p>";
        // else $msg = "<h3 class='my-0'><i class='fas fa-info-circle'></i> Aucune génération !</h3> <p>Vous pouvez régénerer plutard peut-être.</p>";

        // $this->Flash->success(__($msg),['escape' => false]);

        return $orders;
    }

    public function creerDistribution($order)
    {
        //ENTITY DISTRIBUTION
        $distribution = $this->Distributions->newEntity();
        $distribution->order_id = $order->id;
        $distribution->poi_customer_id = $order->poi_customer_id;
        $distribution->poi_id = $order->poi_id;
        $distribution->distribution_statut_id = 1;
        $distribution->amount = 0;
        $distribution->company_id = $order->company_id;
        $distribution->user_id = null;

        $lignes = $order->order_lines;

        //ENTITIES DISTRIBUTION LINES
        if ($this->Distributions->save($distribution)) {
            foreach ($lignes as $ligne) {
                $reste = null;
                //La quantite restante
                if ($ligne->quantite_max !== null) {
                    $reste = $ligne->quantite_max - $ligne->quantite_livre;
                }
                if (($reste && $reste > 0) || !$reste) {
                    $distribution_line = $this->Distributions->DistributionLines->newEntity();
                    $distribution_line->distribution_id = $distribution->id;
                    $distribution_line->price_ht = $ligne->price_ht;
                    $distribution_line->order_line_id = $ligne->id;

                    //On s'arrete pour controler la quantité si l'on la limite
                    if ($reste) {
                        if ($reste > $ligne->quantite) $distribution_line->quantite = $ligne->quantite;
                        else $distribution_line->quantite = $reste;
                    } else $distribution_line->quantite = $ligne->quantite;

                    $distribution_line->product_id = $ligne->product_id;
                    $distribution_line->amount_ht = $distribution_line->quantite * $distribution_line->price_ht;

                    if ($this->Distributions->DistributionLines->save($distribution_line)) { //Si tout est ok on passe à l'enregistrement
                        $distribution->amount += $distribution_line->amount_ht;
                        //Mise à jour de la quantité
                        $this->Distributions->save($distribution);
                    }
                }
            }
        }
    }

    public function add()
    {
        $distribution = $this->Distributions->newEntity();
        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            $lignes = $temp['lignes'];
            $nb = count($lignes);

            $distribution = $this->Distributions->newEntity();
            $distribution->amount = 0;
            $distribution->order_id = $temp['order_id'];

            //Get Order
            $order = $this->Distributions->Orders->get($distribution->order_id);


            $distribution->company_id = $this->Auth->user('company_id');
            $distribution->poi_id = $order->poi_id;
            $distribution->poi_customer_id = $order->poi_customer_id;
            $distribution->distribution_statut_id = 2;
            $distribution->user_id = $temp['user_id'];

            //dd($distribution);

            if ($this->Distributions->save($distribution)) {
                if ($nb > 0) {
                    $nbsave = 0;
                    foreach ($lignes as $ligne) {
                        //$order_line = $this->Distributions->Orders->OrderLines->get($ligne);
                        $distribution_line = $this->Distributions->DistributionLines->newEntity();
                        $distribution_line->distribution_id = $distribution->id;
                        $distribution_line->order_line_id = $ligne;
                        $distribution_line->price_ht = $temp['price_ht' . $ligne];
                        $distribution_line->quantite = $temp['q' . $ligne];
                        $distribution_line->amount_ht = $distribution_line->price_ht * $distribution_line->quantite;
                        $distribution_line->product_id = $temp['product_id' . $ligne];
                        if ($this->Distributions->DistributionLines->save($distribution_line)) {
                            $distribution->amount += $distribution_line->amount_ht;

                        }
                    }
                    $this->Distributions->save($distribution);
                    $this->Flash->success(__('La distribution a été assignée avec succès !'));
                    return $this->redirect(['action' => 'visualiser', '?' => $this->hashUrl(2)]);
                }
            }
            return $this->redirect($this->referer());
            $this->Flash->error(__("La distribution de la commande n'a pas pu être enregistrée, merci de reessayer."));
        }

        $this->set(compact('distribution'));
    }

    public function edit($id = null)
    {
        $distribution = $this->Distributions->get($id, [
            'contain' => ['PoiCustomers', 'DistributionStatuts', 'Pois', 'Users', 'Companies', 'DistributionLines.Products.Gammes']
        ]);
        $this->loadModel('OrderLines');

        $etatInitialDistribution = clone $distribution;
        $distribution_lines = $distribution->distribution_lines;

        if ($this->request->is('post')) {
            $distribution = $this->Distributions->patchEntity($distribution, $this->request->getData());
            if ($this->Distributions->save($distribution)) {

                if($distribution->distribution_statut_id == 3 && count($distribution_lines) > 0) {
                    foreach ($distribution_lines as $dis_line) {
                        // Seulement si la distribuée est livrée
                        $order_line = $this->OrderLines->get($dis_line->order_line_id);
                        $order_line->quantite_livre += $dis_line->quantite;

                        $this->OrderLines->save($order_line);
                    }
                }

                if ($distribution->distribution_statut_id == 1) { $distribution->user_id = null; } //Lorsqu'on remet la distribution en cours

                //dd(count($distribution_lines) > 0 && ($distribution->distribution_statut_id == 4 || $distribution->distribution_statut_id == 2 || $distribution->distribution_statut_id == 1) && $etatInitialDistribution->distribution_statut_id == 3);
                //Lorsqu'on annule une commande déjà distribuée
                if (count($distribution_lines) > 0 && ($distribution->distribution_statut_id == 4 || $distribution->distribution_statut_id == 2 || $distribution->distribution_statut_id == 1) && $etatInitialDistribution->distribution_statut_id == 3) {
                    foreach ($distribution_lines as $dis_line) {
                        $order_line = $this->OrderLines->get($dis_line->order_line_id);
                        $order_line->quantite_livre = $order_line->quantite_livre - $dis_line->quantite;
                        $this->OrderLines->save($order_line);
                    }
                }

                if ($this->Distributions->save($distribution)) {
                    $this->Flash->success(__('La distribution a été enregistrée avec succès.'));
                    return $this->redirect(['action' => 'visualiser', '?' => $this->hashUrl($distribution->distribution_statut_id)]);
                }
            } else $this->Flash->error(__("La modification n'a pas pu être effectuée, merci de réessayer."));
        }

        $roles = $this->Users->Roles->find('all', [
            'contain' =>
            ['Users' =>
            ['conditions' => [
                'Users.company_id' => $this->Auth->user('company_id')
            ]]],
            'conditions' => [
                'Roles.profile_id' => 5
            ]
        ])->toArray();

        $this->set(compact('distribution','roles'));
    }

    public function view($id = null)
    {
        $distribution = $this->Distributions->get($id, [
            'contain' => ['PoiCustomers', 'DistributionStatuts', 'Pois', 'Users', 'Companies', 'DistributionLines.Products.Gammes']
        ]);

        $this->set('distribution', $distribution);
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $distribution = $this->Distributions->get($id);
        if ($this->Distributions->delete($distribution)) {
            $this->Flash->success(__('La distribution a été supprimée avec succès.'));
        } else {
            $this->Flash->error(__("La distribution n'a pas pu être supprimée, merci de réessayer."));
        }

        return $this->redirect($this->referer());
    }
}
