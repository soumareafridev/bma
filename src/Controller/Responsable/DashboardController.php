<?php
namespace App\Controller\Responsable;

use App\Controller\Responsable\AppController;
use Cake\I18n\Time;

class DashboardController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Distributions');
        $this->loadModel('Pois');
        $this->loadModel('Orders');
    }
    
    public function index()
    {
        $company_id = $this->Auth->user('company_id');

        $pois = $this->getPOI(1,5);
        $type_pois = $this->Pois->TypePois->find('all', ['contain' => ['Pois'],'conditions' => ['TypePois.company_id' => $this->Auth->user('company_id')]])->toArray();
        $distributions = $this->Distributions->find('all', ['conditions' => ['Distributions.company_id' => $company_id, 'Distributions.distribution_statut_id' => 3]])->toArray();
        $orders = $this->Orders->find('all', ['conditions' => ['Orders.company_id' => $company_id, 'Orders.order_statut_id' => 2]])->toArray();


        // DEBUT AREA CHART
        $mois = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];


        //NE PAS ENTRER N IMPORTE QUOI
        $regex = '/^2[0-9]{3}$/';


        $getAnnee = $this->request->getQuery('annee');

        $resultatRegex = \preg_match($regex, $getAnnee);

        $anneEnCours = Time::now()->format('Y');
        $dataAnnee = $anneEnCours;

        //Les tableaux a remplir
        $dataOrders = [];
        $dataDistributions = [];

        if ($getAnnee && $resultatRegex == 1) $dataAnnee = $getAnnee;
        else if ($getAnnee && ($resultatRegex == 0 || $resultatRegex == false)) {
            $this->Flash->error(__('Merci d\'entrer  une année valide'));
        }

        foreach ($mois as $period) {
            $itemOrder = 0;
            $itemDistribution = 0;
            foreach ($orders as $order) {
                if (($mois[(int) $order->modified->format('m') - 1] == $period) && $order->modified->format('Y') == $dataAnnee) $itemOrder += $order->amount;
            }
            foreach ($distributions as $distribution) {
                if (($mois[(int) $distribution->modified->format('m') - 1] == $period) && $distribution->modified->format('Y') == $dataAnnee) $itemDistribution += $distribution->amount;
            }
            $dataOrders[] = $itemOrder;
            $dataDistributions[] = $itemDistribution;
        }

        $dataOrders = \json_encode($dataOrders);
        $dataDistributions = \json_encode($dataDistributions);
         
         $this->set(compact('pois','type_pois','dataOrders','dataDistributions','anneEnCours', 'dataAnnee'));
    }
}
