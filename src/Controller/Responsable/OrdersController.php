<?php
namespace App\Controller\Responsable;

use App\Controller\Responsable\AppController;
use Cake\I18n\Time;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 *
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrdersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $contain = [
            'contain' => ['OrderStatuts', 'PoiCustomers', 'Pois'],
            'conditions' => [
                'Orders.company_id' => $this->Auth->user('company_id')
                ]
        ];
        $orders = $this->Orders->find('all',$contain)->toArray();

        $this->set(compact('orders'));
    }

    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => ['Users','OrderStatuts','Frequences', 'PoiCustomers.Pois.TypePois', 'Pois.TypePois', 'OrderLines.Products.Gammes',
                'Distributions' => ['PoiCustomers.Pois.TypePois', 'Pois.TypePois']]
        ]);
        $this->set('order', $order);
    }

    public function distribuer($id = null) {
        $roles = $this->Users->Roles->find('all', ['contain' =>
            ['Users' =>
                ['conditions' => [
                'Users.company_id' => $this->Auth->user('company_id')]]],
            'conditions' => [
                'Roles.profile_id' => 5
            ]])->toArray();
        $this->set(compact('roles'));
        $this->edit($id);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('Products');

        if ($this->request->is('post')) {
            $temp = $this->request->getData();

           // dd($temp);

            $lignes = json_decode($temp['lignes']);
            $nb = count($lignes);
            $ordersave = 0;
            if(isset($temp['poi_customer_id'])) {
                $ids = $temp['poi_customer_id'];
                $field = "poi_customer_id";
            }
            if(isset($temp['poi_id'])) {
                $ids = $temp['poi_id'];
                $field = "poi_id";
            }

            $date_limited = null;
            $date_debut = null;


            if(isset($temp['date_limited']) && $temp['date_limited'] ==! null) {
                $date_limited = $temp['date_limited'] . ' 00:00:00';
                if($temp['fin_heure'] ==! null) $date_limited = $temp['date_limited'] .' ' .$temp['fin_heure']. ':00:00';
                if($temp['fin_heure'] ==! null && $temp['fin_minute'] ==! null) $date_limited = $temp['date_limited'] .' ' .$temp['fin_heure']. ':'.$temp['fin_minute'].':00';
                $date_limited = new Time($date_limited);
            }

            if(isset($temp['date_debut']) && $temp['date_debut'] ==! null) {
                $date_debut = $temp['date_debut'] . ' 00:00:00';
                if($temp['debut_heure'] ==! null) $date_debut = $temp['date_debut'] .' ' .$temp['debut_heure']. ':00:00';
                if($temp['debut_heure'] ==! null && $temp['debut_minute'] ==! null) $date_debut = $temp['date_debut'] .' ' .$temp['debut_heure']. ':'.$temp['debut_minute'].':00';
                $date_debut = new Time($date_debut);
            }

            if($date_limited != null && $date_debut != null && $date_debut > $date_limited) {
                $this->Flash->error(__('La date de fin doit être une date ultérieure.'));
                return $this->redirect($this->referer());
            }

            $frequences = null;
            if(isset($temp['jours']) && $temp['jours'] ==! null) {
               $frequences = implode(';',$temp['jours']);
            }

            if(count($ids) > 0) {
                foreach($ids as $id) {
                    $order = $this->Orders->newEntity();

                    $order = $this->Orders->patchEntity($order,$temp['order']);

                    if($date_limited) $order->date_limited = $date_limited;
                    if($date_debut) $order->date_debut = $date_debut;
                    if($frequences) $order->frequences = $frequences;

                    $order->amount = 0;
                    if(isset($temp['user_id'])) $order->user_id = $temp['user_id'];
                    if($field == "poi_customer_id") $order->poi_customer_id = (int) $id;
                    if($field == "poi_id") $order->poi_id = (int) $id;

                    $order->order_statut_id = $temp['order_statut_id'];
                    $order->company_id = $this->Auth->user('company_id');

                    //dd($order);

                    if ($this->Orders->save($order)) {
                        if($nb > 0) {
                            $nbsave = 0;
                            foreach($lignes as $ligne) {
                                $orderline = $this->Orders->OrderLines->newEntity();
                                $orderline->order_id = $order->id;
                                $orderline->price_ht = $temp['price_ht'.$ligne];
                                $orderline->quantite = $temp['q'.$ligne];
                                $orderline->quantite_max = $temp['qmax'.$ligne];
                                $orderline->quantite_livre = 0;
                                $orderline->amount_ht = $temp['amount_ht'.$ligne];
                                $orderline->product_id = $temp['product_id'.$ligne];
                                if($this->Orders->OrderLines->save($orderline)) {
                                    $order->amount += $orderline->amount_ht;
                                    $nbsave++;
                                    $ordersave++;
                                }
                            }
                            if($nbsave == $nb) $this->Orders->save($order);

                            else $this->Orders->delete($order);

                        }
                    }
                }
            }
            if($ordersave >= $nb) {
                $this->Flash->success(__('La commande a été enregistrée avec succès !'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__("La commande n'a pas pu être enregistrée, merci de reessayer."));
        }
        $products = $this->Products->find('all', ['conditions' => ['Products.company_id' => $this->Auth->user('company_id'),'Products.status <' => 2], 'contain' => ['Gammes','Images']]);
        $frequences = $this->Orders->Frequences->find('all')->toArray();
        $product_format_json = json_decode(json_encode($products));
        $orderStatuts = $this->Orders->OrderStatuts->find('list', ['limit' => 2]);
        $poiCustomers = $this->Orders->PoiCustomers->find("all", ['conditions' => ['PoiCustomers.company_id' => $this->Auth->user('company_id')]])
        ->toArray();
        $pois = $this->Orders->Pois->find('all', ['conditions' => ['Pois.company_id' => $this->Auth->user('company_id')]])->select(['id','name' => 'dns'])
        ->toArray();
        $this->set(compact('orderStatuts', 'frequences','poiCustomers', 'pois','products','product_format_json'));
    }

    public function edit($id = null)
    {
        $this->loadModel('Products');
        $order = $this->Orders->get($id, [
            'contain' => ['OrderLines.Products','Pois','PoiCustomers','OrderStatuts']
        ]);
        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            if(isset($temp['poi_customer_id'])) $order->poi_customer_id = $temp['poi_customer_id'];
            if(isset($temp['poi_id'])) $order->poi_id = $temp['poi_id'];

            $order->order_statut_id = $temp['order_statut_id'];
            $lignes = json_decode($temp['lignes']);
            $nb = count($lignes);
            $nbsave = 0;
            if ($this->Orders->save($order)) {
                if($nb > 0) {
                    foreach($lignes as $ligne) {
                        $orderline = $this->Orders->OrderLines->newEntity();
                        $orderline->order_id = $order->id;
                        $orderline->price_ht = $temp['price_ht'.$ligne];
                        $orderline->quantite = $temp['q'.$ligne];
                        $orderline->amount_ht = $temp['amount_ht'.$ligne];
                        $orderline->product_id = $temp['product_id'.$ligne];
                        if($this->Orders->OrderLines->save($orderline)) {
                            $order->amount += $orderline->amount_ht;
                            $nbsave++;
                        }
                    }
                }
                if($nbsave == $nb) {
                    $this->Orders->save($order);
                    $this->Flash->success(__('La commande a été enregistrée avec succès !'));
                    return $this->redirect($this->referer());
                }
                else {
                    $this->Orders->delete($order);
                    $this->Flash->error(__("La commande n'a pas pu être enregistrée, merci de reessayer."));
                }

            }
            $this->Flash->error(__("La commande n'a pas pu être enregistrée, merci de reessayer."));
        }
        $products = $this->Products->find('all', ['conditions' => ['Products.company_id' => $this->Auth->user('company_id'),'Products.status <' => 2], 'contain' => ['Gammes','Images']]);
        $product_format_json = json_decode(json_encode($products));
        $orderStatuts = $this->Orders->OrderStatuts->find('list', ['conditions' => []]);
        $poiCustomers = $this->Orders->PoiCustomers->find("all", ['conditions' => ['PoiCustomers.company_id' => $this->Auth->user('company_id')]])
        ->toArray();
        $pois = $this->Orders->Pois->find('all', ['conditions' => ['Pois.company_id' => $this->Auth->user('company_id')]])->select(['id','name' => 'dns'])
        ->toArray();
        $this->set(compact('order', 'orderStatuts', 'poiCustomers', 'pois','products','product_format_json'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('La commande a été supprimée avec succès.'));
        } else {
            $this->Flash->error(__("La commande n'a pas pu être supprimée, merci de réessayer."));
        }

        return $this->redirect(['action' => 'index']);
    }
}
