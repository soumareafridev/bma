<?php
namespace App\Controller\Responsable;

use App\Controller\Responsable\AppController;
use App\Controller\TypePoisController;
use Cake\I18n\Time;

class StaticsController extends AppController
{
    public function sidebar() {
        $this->loadModel('TypePois');
        $this->loadModel('DistributionStatuts');

        $statut_distributions = $this->DistributionStatuts->find('all')->toArray();
        $typePois = new TypePoisController();
        $type_pois = $typePois->getList();

        $company = $this->Companies->get($this->Auth->user('company_id'));

        $access = [];

        if (isset($company->access) && $company->access != "") $access = explode(';', $company->access);


        $sidebar = [
            "principals" =>
            [
                [
                    "name" => "Dashboard",
                    "fa" => "fa-tachometer-alt",
                    "controller" => "Dashboard",
                    "action" => "index"
                ],
                [
                    "name" => "Cartographie",
                    "fa" => "fa-map-marked-alt",
                    "controller" => "Pois",
                    "action" => "cartographie"
                ],
            ],
            "secondaires" =>
            [
                [
                    "name" => "POIs",
                    "fa" => "fa-map-pin",
                    "header" => "Les types de POI",
                    "refs" => ['pois'],
                    "items" => []
                ]
            ]
        ];

        if(count($type_pois) > 0) {
            foreach($type_pois as $type) {
                $item = [
                    "name" => $type->name,
                    "controller" => "Pois",
                    "action" => "pois-enregistres",
                    "params" => $this->hashUrl($type->id)
                ];

                $sidebar["secondaires"][0]["items"][] = $item;
            }
        } else {
            $item = [
                "name" => "Aucun Type de POI n'est créé pour le moment",
                "controller" => "dashboard",
                "action" => "index"
            ];

            $sidebar["secondaires"][0]["items"][] = $item;
        }

        if (in_array(2, $access)) {
            $sidebar['secondaires'][] =
                [
                    "name" => "Commandes",
                    "fa" => "fa-shopping-cart",
                    "header" => "Gestion commandes",
                    "refs" => ['orders'],
                    "items" => [
                        [
                            "name" => "Liste commandes",
                            "controller" => "Orders",
                            "action" => "index"
                        ],
                        [
                            "name" => "Nouvelle commande",
                            "controller" => "Orders",
                            "action" => "add"
                        ]
                    ]
                ];
            $sidebar['secondaires'][] =
                [
                    "name" => "Produits",
                    "fa" => "fa-shopping-cart",
                    "header" => "Gestion produits",
                    "refs" => ['products'],
                    "items" => [
                        [
                            "name" => "Liste produits",
                            "controller" => "Products",
                            "action" => "index"
                        ],
                        [
                            "name" => "Nouveau produit",
                            "controller" => "Products",
                            "action" => "add"
                        ]
                    ]
                ];

            $sidebar['secondaires'][] = [
                "name" => "Distributions",
                "fa" => "fa-share-alt",
                "header" => "Gestion distributions",
                "refs" => ['distributions'],
                "items" => [
                    [
                        "name" => "Dashboard",
                        "controller" => "Distributions",
                        "action" => "index"
                    ]
                ]
            ];
            $sidebar['secondaires'][] =    [
                "name" => "Contacts",
                "fa" => "fa-user-friends",
                "controller" => "Parametres",
                "action" => "liaisons",
                "refs" => ['parametres']
            ];

            // array_merge($sidebar['secondaires'], $tableau_commande);

            if (count($statut_distributions) > 0) {
                foreach ($statut_distributions as $statut) {
                    $item = [
                        "name" => $statut->name,
                        "controller" => "Distributions",
                        "action" => "visualiser",
                        "params" => $this->hashUrl($statut->id)
                    ];

                    $sidebar["secondaires"][3]["items"][] = $item;
                }
            }
        }

        return $sidebar;
    }

}
