<?php
namespace App\Controller\Commercial;

use App\Controller\Commercial\AppController;

class PoisController extends AppController
{
    public function cartographie() {
        $pois = $this->Pois->find('all',[
            'conditions' => ['Pois.company_id' => $this->Auth->user('company_id')],
            'contain' => [
                'Surveys.Forms.Fields.TypeFields',
                'Infos.Fields.TypeFields',
                'Users',
                'Zones',
                'TypePois'
            ],
            'order' => ['Pois.id' => "desc"]
        ])->toArray();
        
        $datas = $pois;
        $zones = $this->Pois->Zones->find('all')->toArray();
        $groupes = $this->Pois->Groupes->find('all')->toArray();
        $this->set(compact('pois','zones','datas','groupes'));
    }

    public function clientsIndependants() {
        $pois = $this->getPOI(2);
        $titre = "Clients indépendants";
        $this->set(compact('pois','titre'));
    }
    public function pointsDeVente() {
        $pois = $this->getPOI(1);
        $titre = "Points de vente";
        $this->set(compact('pois','titre'));
    }

    public function poisEnregistres($id = null) {
        if(!isset($id)) $id = $this->request->getQuery('i');
        $type = $this->getPOIByType($id);
        $pois =  $type->pois;
        $this->set(compact('pois','type'));
    }


    
    public function view($id = null)
    {
        $poi = $this->Pois->get($id,[
            'contain' => [
                'Surveys.Forms.Fields.TypeFields',
                'Infos.Fields.TypeFields',
                'Users',
                'TypePois'
            ]
        ]);
        
        $infos = $poi->infos;
        $types = $this->Pois->TypePois->find('list', ['conditions' => ['TypePois.company_id' => $this->Auth->user('company_id')]])->toArray();

        $this->set(compact('poi','infos','types'));
    }

    public function edit($id = null)
    {
        $this->loadModel("Infos");
        $poi = $this->Pois->get($id,[
            'contain' => [
                'Surveys.Forms.Fields.TypeFields',
                'Infos.Fields.TypeFields',
                'Users'
            ]
        ]);
        
        if($this->request->is('post')) {
            $temp = $this->request->getData();
            $nb = sizeof($temp['infos']);
            $isItSave = 0;
            $poi = $this->Pois->patchEntity($poi, $temp['poi']);
            if($this->Pois->save($poi)) {       
                foreach($temp['infos'] as $key => $value) {
                    $info = $this->Infos->get($key);
                    $info->value = $value;
                    if($this->Infos->save($info)) $isItSave++;
                }
                if($nb == $isItSave) $this->Flash->success(__('Les informations du POI ont étés modifiées avec succès'));
                else $this->Flash->error(__('Toutes les informations du POI n\'ont pas pu être modifiées, reessayez'));
            } else $this->Flash->error(__('Toutes les informations du POI n\ont pas pu être modifiées, reessayez'));

            return $this->redirect($this->referer());
        }


        $this->set(compact('poi'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pois = $this->Pois->get($id);
        if ($this->Pois->delete($pois)) {
            $this->Flash->success(__('The pois has been deleted.'));
        } else {
            $this->Flash->error(__('The pois could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}