<?php
namespace App\Controller\Commercial;

use App\Controller\Commercial\AppController;
use Cake\Event\Event;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 *
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $paginate = [
            'conditions' => ['Products.company_id' => $this->Auth->user('company_id')],
            'contain' => ['Gammes']
        ];
        $products = $this->Products->find('all', $paginate);

        $this->set(compact('products'));
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => ['Gammes','Images']
        ]);

        $this->set('product', $product);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $product = $this->Products->newEntity();
        if ($this->request->is('post')) {
            $images = $this->request->getData('images');
            $errors = [];
            $product = $this->Products->patchEntity($product, $this->request->getData('product'));
            $product->company_id = $this->Auth->user('company_id');
            if ($this->Products->save($product)) { //error cannot convert string to integer

                foreach ($images as $key => $image) {
                    if($image["error"] <= 0) {
                        $isnotUploaded = $this->fileIsNotUploaded($image,$product,['jpg','png','jpeg','gif']);
                        if($isnotUploaded) $errors[] = $isnotUploaded;
                    }
                }

                if(count($errors) > 0) {
                    $message =  '<h5>'.count($errors).'image(s) Non-sauvegardée(s)  - Erreurs suivantes : </h5><ul class="list-group">';
                    foreach($errors as $error_imgs) {
                            foreach($error_imgs as $error) {
                                $message .= "<li class='list-group-item-danger'>$error</li>";
                            }
                        }
                        $message .='</ul>';
                        $this->Flash->error(__($message),['escape' => false]);
                }

                $this->Flash->success(__('Le produit a été ajouté avec succès.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__("Le produit n'a pu être sauvegardé, veuillez reessayer"));
        }
        $gammes =  $this->Products->Gammes->find('list', ['conditions' => ['Gammes.company_id' =>  $this->Auth->user('company_id')]]);
        $this->set(compact('product', 'gammes'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => ['Gammes','Images']
        ]);
        if ($this->request->is(['post','patch','put'])) {
            $images = $this->request->getData('images');
            $errors = [];
            $product = $this->Products->patchEntity($product, $this->request->getData('product'));
            // dd($product);
            if ($this->Products->save($product)) { //error cannot convert string to integer

                foreach ($images as $key => $image) {
                    if($image["error"] <= 0) {
                        $isnotUploaded = $this->fileIsNotUploaded($image,$product,['jpg','png','jpeg','gif']);
                        if($isnotUploaded) $errors[] = $isnotUploaded;
                    }
                }

                if(count($errors) > 0) {
                    $message =  '<h5>'.count($errors).'image(s) Non-sauvegardée(s)  - Erreurs suivantes : </h5><ul class="list-group">';
                    foreach($errors as $error_imgs) {
                            foreach($error_imgs as $error) {
                                $message .= "<li class='list-group-item-danger'>$error</li>";
                            }
                        }
                        $message .='</ul>';
                        $this->Flash->error(__($message),['escape' => false]);
                }

                $this->Flash->success(__('Le produit a été modifié avec succès.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__("Le produit n'a pu être sauvegardé, veuillez reessayer"));
        }
        $gammes =  $this->Products->Gammes->find('list', ['conditions' => ['Gammes.company_id' =>  $this->Auth->user('company_id')]]);
        $this->set(compact('product', 'gammes'));
    }


    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($id);

        $images = $this->Products->Images->find()->Where(['product_id' => $id])->toArray();
        if ($this->Products->delete($product)) {
        if(count($images) > 0){
            foreach($images as $image){
                if(unlink(WWW_ROOT . $image->url)) $this->Products->Images->delete($image);
            }
        }
        $this->Flash->success(__('Produit supprimé avec succès'));
        } else $this->Flash->error(__('Le produit n\'a pas pu etre supprimé'));

        return $this->redirect(['action' => 'index']);
    }
}
