<?php
namespace App\Controller\Commercial;

use App\Controller\Commercial\AppController;

class ParametresController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('CustomerTypes');
    }

    public function liaisons() {
        $type_customers = $this->CustomerTypes->find('all',['conditions' => ['CustomerTypes.company_id' => $this->Auth->user('company_id')] ])->toArray();
        $customers = $this->CustomerTypes->PoiCustomers->find('all',
            [
                'contain' => ['Pois.TypePois','CustomerTypes'],
                'conditions' => ['PoiCustomers.company_id' => $this->Auth->user('company_id')] 
            ]
        )->toArray();

        $this->set(compact("type_customers","customers"));
    }

}
