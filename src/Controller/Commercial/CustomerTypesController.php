<?php
namespace App\Controller\Commercial;

use App\Controller\Commercial\AppController;

class CustomerTypesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $paginate = ['conditions' => ['CustomerTypes.company_id' => $this->Auth->user('company_id')]];

        $customerTypes = $this->CustomerTypes->find('all',$paginate)->toArray();

        $this->set(compact('customerTypes'));
    }

    public function view($id = null)
    {
        $customerType = $this->CustomerTypes->get($id, [
            'contain' => ['PoiCustomers']
        ]);

        $this->set('customerType', $customerType);
    }

    public function add()
    {
        $customerType = $this->CustomerTypes->newEntity();
        if ($this->request->is('post')) {
            $customerType = $this->CustomerTypes->patchEntity($customerType, $this->request->getData());
            $customerType->company_id = $this->Auth->user('company_id');
            if ($this->CustomerTypes->save($customerType)) {
                $this->Flash->success(__('Le type d\'acteur a été enregistré avec succès.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Le type d\'acteur n\'a pas pu être enregistré, veuillez réessayez.'));
        }
        $this->set(compact('customerType'));
    }


    public function edit($id = null)
    {
        $customerType = $this->CustomerTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $customerType = $this->CustomerTypes->patchEntity($customerType, $this->request->getData());
            if ($this->CustomerTypes->save($customerType)) {
                $this->Flash->success(__('Le type d\'acteur a été enregistré avec succès.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Le type d\'acteur n\'a pas pu être enregistré, veuillez réessayez.'));
        }
        $this->set(compact('customerType'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Customer Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $customerType = $this->CustomerTypes->get($id);
        if ($this->CustomerTypes->delete($customerType)) {
            $this->Flash->success(__('Le type d\'acteur a été supprimé avec succès.'));
        } else {
            $this->Flash->error(__('Le type d\'acteur n\'a pas pu être supprimé, veuillez réessayez.'));
        }

        return $this->redirect($this->referer());
    }
}
