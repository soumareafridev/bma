<?php
namespace App\Controller\Commercial;

use App\Controller\Commercial\AppController;

/**
 * OrderLines Controller
 *
 * @property \App\Model\Table\OrderLinesTable $OrderLines
 *
 * @method \App\Model\Entity\OrderLine[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrderLinesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Orders']
        ];
        $orderLines = $this->paginate($this->OrderLines);

        $this->set(compact('orderLines'));
    }

    /**
     * View method
     *
     * @param string|null $id Order Line id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $orderLine = $this->OrderLines->get($id, [
            'contain' => ['Orders']
        ]);

        $this->set('orderLine', $orderLine);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('Orders');
        $orderLine = $this->OrderLines->newEntity();
        if ($this->request->is('post')) {
            $order_id = $this->request->getData('order_id');

            if(isset($order_id) && $order_id > 0)  $order = $this->Orders->get($order_id);
            else {
                $order = $this->Orders->newEntity();
                $order->amount = 0;
                if(!$this->Orders->save($order)) $order = null;
            }

            $orderLine = $this->OrderLines->patchEntity($orderLine, $this->request->getData());
            if($order) $orderLine->order_id = $order->id;
            if ($this->OrderLines->save($orderLine)) {
                $order->amount += $orderLine->amount_ht;
                $orderLine = $this->OrderLines->get($orderLine->id,['contain' => ['Products.Gammes','Orders']]);

                $rep = $orderLine;
            } else $rep = null;
        }
        echo json_encode($rep);
        die();
    }

    /**
     * Edit method
     *
     * @param string|null $id Order Line id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $orderLine = $this->OrderLines->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $orderLine = $this->OrderLines->patchEntity($orderLine, $this->request->getData());
            if ($this->OrderLines->save($orderLine)) {
                $this->Flash->success(__('The order line has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order line could not be saved. Please, try again.'));
        }
        $orders = $this->OrderLines->Orders->find('list', ['limit' => 200]);
        $this->set(compact('orderLine', 'orders'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Order Line id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $orderLine = $this->OrderLines->get($id,['contain' => ['Orders']]);
        $order = $orderLine->order;
        if ($this->OrderLines->delete($orderLine)) {
            $order->amount -= $orderLine->amount_ht;
            $this->OrderLines->Orders->save($order);
            $this->Flash->success(__('The order line has been deleted.'));
        } else {
            $this->Flash->error(__('The order line could not be deleted. Please, try again.'));
        }

        return $this->redirect($this->referer());
    }
}
