<?php
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');

        $this->loadModel('Companies');

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
                /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        // $this->loadComponent('Security');
        // $this->loadComponent('Csrf');
        $this->loadComponent('Auth', [
            'authorize' => ['Controller'], //pour les autorisations
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'phone',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'prefix' => false,
                'controller' => 'Users',
                'action' => 'login'
            ],
                // Si pas autorisé, on renvoit sur la page précédente
            'loginRedirect' => [
                'prefix' => false,
                'controller' => 'Users',
                'action' => 'logout'
            ],
            'unauthorizedRedirect' => [
                'prefix' => false,
                'controller' => 'Users',
                'action' => 'logout'
            ],
            'authError' => 'Vous devez d\'abord vous connecter !'
        ]);

        // if(!$this->Auth->user('id')) return $this->redirect(['controller' => 'Users', 'action' => 'login']);

        $this->Auth->allow(['login','logout']);
    }

    public function isAuthorized($user)
    {
        if($user){
            $auth = $this->getUser($user['id']);
            $company = $this->Companies->get($this->Auth->user('company_id'));

            $access = [];

            if (isset($company->access) && $company->access != "") $access = explode(';', $company->access);

        //Autorisation for enqueteur
            if (in_array(1,$auth->profile_ids) && $this->request->getParam('prefix') == 'enqueteur') return true;

        //Autorisation for commercial
            if (in_array(2,$auth->profile_ids) && $this->request->getParam('prefix') == 'commercial') {
                if(!in_array(2, $access) && ($this->request->getParam('controller') == "Orders" || $this->request->getParam('controller') == "distributions" || $this->request->getParam('controller') == "products")) return false;


                return true;
            }

        //Autorisation for responsable
            if (in_array(3,$auth->profile_ids) && $this->request->getParam('prefix') == 'responsable') {
                if(!in_array(2, $access) && ($this->request->getParam('controller') == "Orders" || $this->request->getParam('controller') == "distributions" || $this->request->getParam('controller') == "products")) return false;

                return true;
            }

        //Autorisation for admin
            if (in_array(4,$auth->profile_ids) && $this->request->getParam('prefix') == 'admin') {
                if(!in_array(2, $access) && ($this->request->getParam('controller') == "Orders" || $this->request->getParam('controller') == "distributions" || $this->request->getParam('controller') == "products")) return false;

                return true;
            }

        //Autorisation for super admin
            if (in_array(6,$auth->profile_ids) && $this->request->getParam('prefix') == 'super') return true;

        //Access deny
            return false;
        } else return false;

    }


    public function getUser($id)
    {
        $this->loadModel('Users');
        $user = $this->Users->get($id, ['contain' => ['Companies','Roles.Profiles']]);
        $user->profile_ids = [];

        if(count($user->roles) > 0) {
            foreach ($user->roles as $role) {
                $user->profile_ids[] = $role->profile_id;
            }
        }
        return $user;
    }

    public function beforeFilter(Event $event)
    {
        $_jours = $this::jours();

        if($this->Auth->user('id')) {
            $_user = $this->getUser($this->Auth->user('id'));
            $company = $this->Companies->get($this->Auth->user('company_id'));

            $_access = [];
            if(isset($company->access) && $company->access != "") $_access = explode(';', $company->access);

            $this->set(compact('_user', '_jours','_access'));
        }
    }

    protected function getPOIByType($type_poi_id) {

        $contain = [
            'contain' => [
                'pois' => [
                    'Surveys.Forms.Fields.TypeFields',
                    'Infos.Fields.TypeFields',
                    'Users',
                    'TypePois'
                ]
            ],
            'conditions' => ['TypePois.company_id' => $this->Auth->user('company_id')]
        ];
        $type = $this->Pois->TypePois->get($type_poi_id,$contain);
        return $type;
    }

    protected function getPOI($type_poi_id,$limit = null,$condition = null) {

        $contain = [
            'conditions' => ['Pois.type_poi_id' => $type_poi_id,'Pois.company_id' => $this->Auth->user('company_id')],
            'contain' => [
                'Surveys.Forms.Fields.TypeFields',
                'Infos.Fields.TypeFields',
                'Users',
                'TypePois'
            ],
            'order' => ['Pois.id' => "desc"]
        ];
        if($limit) $contain['limit'] = $limit;
        if($condition) $contain['conditions'][] = $condition ;
        $pois = $this->Pois->find('all',$contain)->toArray();
        return $pois;
    }

    public function hashUrl($id) {
        $params = [
            'sh' => $this->token(100),
            'i' => $id,
            'hs' => $this->token(50)
        ];
        return $params;
    }

    public function updloadLogo($fichier,$filename = "image",$target_dir = WWW_ROOT . DS . "img" . DS . "companies" . DS,$extensions = array("jpeg", "jpg", "gif", "png"))
    {
      $result = false;
      $errors = array();
      $bytes = 1024;
      $allowedKB = 1000000;
      $totalBytes = $allowedKB * $bytes;
      $uploadThisFile = true;

      $uploadThisFile = true;
      $file_name = $fichier['name'];
      $file_tmp = $fichier['tmp_name'];

        $ext = pathinfo($file_name, PATHINFO_EXTENSION);

        if (!in_array(strtolower($ext), $extensions)) {
            array_push($errors, "Le format du fichier est invalide " . $file_name);
            $uploadThisFile = false;
        }

        if ($fichier['size'] > $totalBytes) {
            array_push($errors, "La taille du fichier doit être inférieure ou égal 1MB. Fichier:- " . $file_name);
            $uploadThisFile = false;
        }


        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0777, true);
        }
        if ($uploadThisFile) {
            $newFileName = $filename .$this->token(5) .'.'. $ext;
            $desti_file = $target_dir . '/' . $newFileName;

            move_uploaded_file($file_tmp, $desti_file);
        }

        return ['errors' => $errors, 'filename' => $newFileName];
    }

    public function fileIsNotUploaded($fichier,$produit,$extensions = array("jpeg", "jpg", "gif", "png", "pdf", "docx"))
    {
      $result = false;
      $errors = array();
      $bytes = 1024;
      $allowedKB = 2000000;
      $totalBytes = $allowedKB * $bytes;
      $uploadThisFile = true;

      $uploadThisFile = true;
        //   $fichier = $this->request->getData($name);
      $file_name = $fichier['name'];
      $file_tmp = $fichier['tmp_name'];

        $ext = pathinfo($file_name, PATHINFO_EXTENSION);

        if (!in_array(strtolower($ext), $extensions)) {
            array_push($errors, "Le format du fichier est invalide " . $file_name);
            $uploadThisFile = false;
        }

        if ($fichier['size'] > $totalBytes) {
            array_push($errors, "La taille du fichier doit être inférieure ou égal 2MB. Fichier:- " . $file_name);
            $uploadThisFile = false;
        }

        $target_dir = WWW_ROOT . DS . "img" . DS . "produits" . DS;
        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0777, true);
        }
        if ($uploadThisFile) {
            $newFileName = $this->token(10). "-". $produit->id.'.' . $ext;
            $desti_file = $target_dir . '/' . $newFileName;

            move_uploaded_file($file_tmp, $desti_file);
            $this->loadModel('Images');

            $image = $this->Images->newEntity();
            $image->name = $newFileName;
            $image->url ="/img/produits/". $newFileName;
            $image->product_id = $produit->id;
            if($this->Images->save($image)) return false;
            else array_push($errors,"L'image n'a pas pu être enregistrée!");
        }

        return $errors;
    }

    static function jours() {
        return [
            "1" => "Lundi",
            "2" => "Mardi",
            "3" => "Mercredi",
            "4" => "Jeudi",
            "5" => "Vendredi",
            "6" => "Samedi",
            "7" => "Dimanche",
        ];
    }

    static function modules() {
        return [
            "1" => "Recensement",
            "2" => "Commande & Distribution",
        ];
    }

    public function returnErrors($arrays) {
        if(count($arrays > 0)) {
            $formErrors = $arrays;
            foreach($formErrors as $key => $valeurs) {
                if(count($valeurs) > 0){
                    foreach($valeurs as $v) {
                        $formErrors[$key] = $v;
                    }
                }
            }
        }

        if(count($formErrors) > 0) {
            return $formErrors;
        }
        return false;
    }

    public function msgErrors($errors) {
        $msg = '<h4 class="text-center">L\'opération a échoué</h4><p>Merci de revoir ces champs suivants : <ul class="list-group">';
        foreach($errors as $field => $error) {
            $msg .= '<li class="list-group-item list-group-item-danger"> <b>'.ucfirst($field).'</b> : '. $error.'</li>';
        }
        $msg .= '</ul></p>';
        return $msg;
    }

    protected function token($taille = 10) {
        $characters = '0123456789abdcdefghijklmnopkrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            return substr(str_shuffle($characters),0,$taille);
    }

    public function savePoiImg($Poi_id)
    {
        $image = $this->request->getData('image');
        $errors = array();
        $extensions = array("jpeg", "jpg", "gif", "png");
        $bytes = 1024;
        $allowedKB = 10000;
        $totalBytes = ($allowedKB * $bytes) * 5;
        $uploadThisFile = true;
        $file_name = $image['name'];
        $file_tmp = $image['tmp_name'];
        $ext = pathinfo($file_name, PATHINFO_EXTENSION);
        if (!in_array(strtolower($ext), $extensions)) {
            array_push($errors, "Le nom du fichier est invalide " . $file_name);
            $uploadThisFile = false;
        }

        if ($image['size'] > $totalBytes) {
            array_push($errors, "la taille du fichier ne doit pas depasser 100KB. Name:- " . $file_name);
            $uploadThisFile = false;
        }

        $target_dir = WWW_ROOT . DS . "img" . DS . "pois" . DS;
        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0777, true);
        }

        if ($uploadThisFile) {
            $Poi = $this->Pois->get($Poi_id);
            $newFileName = $this->genererCode() . 'id-' . $Poi_id . '.' . $ext;
            $Poi->photo = $newFileName;
            $desti_file = $target_dir . $newFileName;

            if (move_uploaded_file($file_tmp, $desti_file)) $this->Pois->save($Poi);
        }
    }

    public function genererCode() {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 4; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
