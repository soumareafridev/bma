<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\I18n\Time;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\UtilitiesController as utility;


/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Users');
        $this->loadModel('Forms');
        $this->loadModel('Pois');
        $this->loadModel('Infos');
        $this->loadModel('Companies');

        $this->Auth->allow();
    }

    public function display(...$path)
    {
        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
       $pois = $this->getPOI(1,5);
       $nb_grossistes = count($this->getPOI(2));
       $nb_pois = count($this->getPOI(1));
        
        $this->set(compact('page', 'subpage','pois','nb_grossistes','nb_pois'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    public function register() {
        // dd("okay");
        $company = $this->Companies->newEntity();
        $user = $this->Users->newEntity();
        $role = $this->Users->Roles->newEntity();

        if ($this->request->is('post')) {
            $user = $this->Companies->patchEntity($user, $this->request->getData('user'));
            $company = $this->Companies->patchEntity($company, $this->request->getData('company'));
            $company->companie_statut_id = 1; //En attente
            $user->name = "hello"; //Corriger le bug de field is required

            /*=======
            ENREGISTREMENT DE L'UTILISATEUR ET DE LA COMPAGNIE
            ============*/
            if ($this->Companies->save($company)) { //Enregistrement Company
                $user->company_id = $company->id;
                $user->token = \md5($user->company_id."".Time::now());

                if($this->Users->save($user)) {  //Enregistrement Utilisateur
                    $url = $this->request->host().'/users/activation/'.$user->token;

                    $role->profile_id = 4; //Administrateur
                    $role->user_id = $user->id;

                    $this->Users->Roles->save($role);

                    $message = "<p> $user->fn $user->ln, <br> Suite à l'ouverture de votre compte sur SCOD, merci de cliquer sur le lien ci-dessous pour l'activation de votre compte : <br>";
                    $message .= "Lien d'activation : <a href='".$url."'>Je souhaite activer mon compte maintenant</a></p>";
                    $sujet = "SCOD : Activation de votre compte.";

                    utility::sendEmail($user->email,$sujet,$message);

                    return $this->redirect(['controller' => 'Pages','action' => 'success']);
                }

                else { //Echec de l'Enregistrement Utilisateur
                    $this->Companies->delete($company);
                    $errors = $this->returnErrors($user->getErrors());

                    if(count($errors) > 0) $this->Flash->error(__($this->msgErrors($errors)),['escape' => false]);
                }
            }

            else { //Echec de l'Enregistrement company
                $errors = $this->returnErrors($company->getErrors());

                if(count($errors) > 0) $this->Flash->error(__($this->msgErrors($errors)),['escape' => false]);
            }
        }
    }

    public function success() {
        
    }

    public function add(){
        $this->loadModel('Users');
        $enqueteur = $this->Users->find('all')->toArray();
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
          $user = $this->Users->patchEntity($user, $this->request->getData());
        }
        $this->set(compact('user', 'enqueteur'));
    }
}
