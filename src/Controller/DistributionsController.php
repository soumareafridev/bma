<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

class DistributionsController extends AppController
{
    public function index()
    {
        $this->paginate = [
            'contain' => ['PoiCustomers', 'Pois', 'Users', 'Companies']
        ];
        $distributions = $this->paginate($this->Distributions);

        $this->set(compact('distributions'));
    }

    public function view($id = null)
    {
        $distribution = $this->Distributions->get($id, [
            'contain' => ['PoiCustomers', 'Pois', 'Users', 'Companies', 'DistributionLines']
        ]);

        $this->set('distribution', $distribution);
    }

    public function add()
    {
        $distribution = $this->Distributions->newEntity();
        if ($this->request->is('post')) {
            $distribution = $this->Distributions->patchEntity($distribution, $this->request->getData());
            if ($this->Distributions->save($distribution)) {
                $this->Flash->success(__('The distribution has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The distribution could not be saved. Please, try again.'));
        }
        $poiCustomers = $this->Distributions->PoiCustomers->find('list', ['limit' => 200]);
        $pois = $this->Distributions->Pois->find('list', ['limit' => 200]);
        $users = $this->Distributions->Users->find('list', ['limit' => 200]);
        $companies = $this->Distributions->Companies->find('list', ['limit' => 200]);
        $this->set(compact('distribution', 'poiCustomers', 'pois', 'users', 'companies'));
    }

    public function edit($id = null)
    {
        $distribution = $this->Distributions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $distribution = $this->Distributions->patchEntity($distribution, $this->request->getData());
            if ($this->Distributions->save($distribution)) {
                $this->Flash->success(__('The distribution has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The distribution could not be saved. Please, try again.'));
        }
        $poiCustomers = $this->Distributions->PoiCustomers->find('list', ['limit' => 200]);
        $pois = $this->Distributions->Pois->find('list', ['limit' => 200]);
        $users = $this->Distributions->Users->find('list', ['limit' => 200]);
        $companies = $this->Distributions->Companies->find('list', ['limit' => 200]);
        $this->set(compact('distribution', 'poiCustomers', 'pois', 'users', 'companies'));
    }


    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $distribution = $this->Distributions->get($id);
        if ($this->Distributions->delete($distribution)) {
            $this->Flash->success(__('The distribution has been deleted.'));
        } else {
            $this->Flash->error(__('The distribution could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
