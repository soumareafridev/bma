<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TypePois Controller
 *
 * @property \App\Model\Table\TypePoisTable $TypePois
 *
 * @method \App\Model\Entity\TypePois[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TypePoisController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $typePois = $this->paginate($this->TypePois);

        $this->set(compact('typePois'));
    }

    public function getList()
    {
        $typePois = $this->TypePois->find('all',['contain' => 'Pois', 'conditions' => ['TypePois.company_id' => $this->Auth->user('company_id')]])->toArray();

        return $typePois;
    }

    /**
     * View method
     *
     * @param string|null $id Type Pois id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $typePois = $this->TypePois->get($id, [
            'contain' => ['Pois']
        ]);

        $this->set('typePois', $typePois);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $typePois = $this->TypePois->newEntity();
        if ($this->request->is('post')) {
            $typePois = $this->TypePois->patchEntity($typePois, $this->request->getData());
            if ($this->TypePois->save($typePois)) {
                $this->Flash->success(__('Un nouveau type de POI a été enregistré.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__("Echec de l'enregistrement du type de POI."));
        }
        $this->set(compact('typePois'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Type Pois id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $typePois = $this->TypePois->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $typePois = $this->TypePois->patchEntity($typePois, $this->request->getData());
            if ($this->TypePois->save($typePois)) {
                $this->Flash->success(__('Vous avez supprimé un type de POI.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Echec de la suppression du type de POI.'));
        }
        $this->set(compact('typePois'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Type Pois id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $typePois = $this->TypePois->get($id);
        if ($this->TypePois->delete($typePois)) {
            $this->Flash->success(__('The type pois has been deleted.'));
        } else {
            $this->Flash->error(__('The type pois could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
