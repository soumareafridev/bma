<?php
namespace App\Controller\Auth;

use Cake\Controller\Controller;

class AppController extends Controller
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        $this->loadModel('Users');

         $this->loadComponent('Auth', [
             'authenticate' => [
                 'Form' => [
                     'fields' => ['username' => 'phone', 'password' => 'password']
                 ],
             ],
             'loginAction' => [
                 'prefix' => 'Auth',
                 'controller' => 'Users',
                 'action' => 'login',
                 '_ext' => 'json'
             ],
             'authError' => 'Veuillez d\'abord vous connecter pour y accéder !'
         ]);
         $this->Auth->allow();
        //$this->loadComponent('Security');
    }

    public function genererCode() {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 4; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
