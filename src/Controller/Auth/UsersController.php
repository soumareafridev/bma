<?php

namespace App\Controller\Auth;

use App\Controller\Auth\AppController;
use Cake\I18n\Time;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Users');
        $this->loadModel('Forms');
        $this->loadModel('Pois');
        $this->loadModel('Infos');
    }

    public function index($user_id)
    {
        $rep = $this->Pois->get($user_id, ['contain' => [
            'Surveys.Forms.Fields.TypeFields',
            'Infos.Fields.TypeFields'
        ]]);
        $this->set(compact('rep'));
        $this->set('_serialize', 'rep');
    }

    protected function infoDistributeurs($id)
    {
        $rep = $this->Users->get($id, [
            'contain' => [
                'Distributions' => [
                    'DistributionLines' => [
                        'OrderLines', 'Products'
                    ],
                'Orders' => ['Pois','PoiCustomers'],
                'DistributionStatuts'
                ]
            ]
        ]);
        return $rep;
    }

    protected function infoEnqueteurs($id)
    {
        $rep = $this->Users->get($id, [
            'contain' => [
                'Members' => [
                    'Teams' => [
                        'Gestions' => [
                            'Surveys.Forms' => [
                                'Fields.TypeFields'
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        return $rep;
    }
    protected function infoCommercials($id)
    {
        $rep = $this->Users->get($id, [
            'contain' => [
                'Orders' => [
                    'OrderStatuts',
                    'OrderLines' => ['Products' ]
                ]
            ]
        ]);

        return $rep;
    }

    public function login($profil_id = null)
    {
        if ($this->request->is('post')) {
            $auth = null;
            $temp = $this->request->getData();

            if (filter_var(trim($temp['phone']), FILTER_VALIDATE_EMAIL)) $getUserByInfo = $this->Users->find('All', ['contain' => ['Roles'],'conditions' => ['Users.email' => trim($temp['phone'])]])->first();
            else $getUserByInfo = $this->Users->find('All', ['contain' => ['Roles'],'conditions' => ['Users.phone' => trim($temp['phone'])]])->first();
            if ($getUserByInfo) {
                if (sha1($temp['password']) === $getUserByInfo->password) $auth = $getUserByInfo;
            }

            if ($auth) {
                $id = (int) $profil_id;
                if($profil_id == 1) $userInfos = $this->infoEnqueteurs($auth->id);
                if($profil_id == 2) $userInfos = $this->infoCommercials($auth->id);
                if($profil_id == 5) $userInfos = $this->infoDistributeurs($auth->id);

                if($profil_id) {
                    $profil_ids = [$id];
                    if($id == 2) $profil_ids = [2,3];
                    foreach($auth->roles as $role) {
                        if(in_array($role->profile_id,$profil_ids)) {
                            $rep = $userInfos;
                            break;
                        }
                        else $rep = "no_profile";
                    }
                } else $rep = $userInfos;

            } else $rep = null;
        } else $rep = null;


        $this->set(compact('rep'));
        $this->set('_serialize', 'rep');
    }

    public function loginContact() {
        $this->loadModel('PoiCustomers');
        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            $auth = $this->PoiCustomers->find('All', [
                'contain' => ['Companies' => ['Products.Images'], 'Pois'],
                'conditions' => [
                    'PoiCustomers.phone' => trim($temp['phone']),
                    'PoiCustomers.password' => trim($temp['password']),
                    ]
                ])->first();
        }
        $this->set(compact('auth'));
        $this->set('_serialize', 'auth');
    }

    protected function checkEmail($email)
    {
        $find1 = strpos($email, '@');
        $find2 = strpos($email, '.');
        return ($find1 !== false && $find2 !== false);
    }

    public function laodenqueteur($user_id = null)
    {
        $user = $this->infoEnqueteurs($user_id);

        if ($user) $rep = $user;
        else $rep = null;

        $this->set(compact('rep'));
        $this->set('_serialize', 'rep');
    }

    public function savepoi($form_id = null)
    {
        $form = $this->Forms->get($form_id, ['contain' => ['Fields.TypeFields']]);
        $company = $this->Forms->Companies->get($form->company_id);

        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            $poi = $this->Pois->newEntity();
            $poi = $this->Pois->patchEntity($poi, $temp);
            $poi->created = Time::now();
            $poi->company_id = $company->id;

            if($this->Pois->save($poi)) {
                if($form) {
                    if(count($form->fields) > 0) {
                        foreach($form->fields as $field) {
                            $info = $this->Infos->newEntity();
                            $info->poi_id = $poi->id;
                            $info->field_id = $field->id;
                            $info->value = $temp['info' . $field->id];
                            $this->Infos->save($info);
                        }
                    }
                }
                if (isset($temp['image']) && $temp['image'] !== null) {
                    $this->savePoiImg($poi->id);
                }
                $rep = $this->Pois->get($poi->id, ['contain' => [
                    'Surveys.Forms.Fields.TypeFields',
                    'Infos.Fields.TypeFields'
                ]]);
            } else $rep = null;
        }
        $this->set(compact('rep'));
        $this->set('_serialize', 'rep');
    }

    public function savePoiImg($Poi_id)
    {
        $image = $this->request->getData('image');
        $errors = array();
        $extensions = array("jpeg", "jpg", "gif", "png");
        $bytes = 1024;
        $allowedKB = 10000;
        $totalBytes = ($allowedKB * $bytes) * 5;
        $uploadThisFile = true;
        $file_name = $image['name'];
        $file_tmp = $image['tmp_name'];
        $ext = pathinfo($file_name, PATHINFO_EXTENSION);
        if (!in_array(strtolower($ext), $extensions)) {
            array_push($errors, "Le nom du fichier est invalide " . $file_name);
            $uploadThisFile = false;
        }

        if ($image['size'] > $totalBytes) {
            array_push($errors, "la taille du fichier ne doit pas depasser 100KB. Name:- " . $file_name);
            $uploadThisFile = false;
        }

        $target_dir = WWW_ROOT . DS . "img" . DS . "pois" . DS;
        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0777, true);
        }

        if ($uploadThisFile) {
            $Poi = $this->Pois->get($Poi_id);
            $newFileName = $this->genererCode() . 'id-' . $Poi_id . '.' . $ext;
            $Poi->photo = $newFileName;
            $desti_file = $target_dir . $newFileName;

            if (move_uploaded_file($file_tmp, $desti_file)) $this->Pois->save($Poi);
        }
    }

    public function listepoi($user_id = null)
    {
        $rep = null;

        $pois = $this->Pois->find('all', [
            'conditions' => ['Pois.user_id' => $user_id],
            'contain' => [
                'Surveys.Forms.Fields.TypeFields',
                'Infos.Fields.TypeFields'
            ]
        ]);

        if ($pois) $rep = $pois;

        $this->set(compact('rep'));
        $this->set('_serialize', 'rep');
    }
}
