<?php

namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\I18n\Time;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

class UpdatesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Users');
        $this->loadModel('Forms');
        $this->loadModel('DistributionLines');
        $this->loadModel('OrderLines');
        $this->loadModel('Distributions');
    }

    public function index($user_id)
    {
        $rep = $this->Pois->get($user_id, ['contain' => [
            'Surveys.Forms.Fields.TypeFields',
            'Infos.Fields.TypeFields'
        ]]);
        $this->set(compact('rep'));
        $this->set('_serialize', 'rep');
    }

    protected function checkEmail($email)
    {
        $find1 = strpos($email, '@');
        $find2 = strpos($email, '.');
        return ($find1 !== false && $find2 !== false);
    }

    public function updateline($line_id, $newq)
    {
        $line = $this->DistributionLines->get($line_id);

        $line->quantite = (int) $newq;
        $line->amount_ht = $line->quantite * $line->price_ht;

        if ($this->DistributionLines->save($line)) {
            $rep = null;
            $distribution = $this->Distributions->get($line->distribution_id,[
                'contain' => [
                    'DistributionLines' => [
                        'OrderLines', 'Products'
                    ],
                    'Orders' => ['Pois', 'PoiCustomers'],
                    'DistributionStatuts'
                ]
            ]);
            $lignes = $distribution->distribution_lines;
            if(count($lignes) > 0) {
                $distribution->amount = 0;
                foreach ($lignes as $ligne) {
                    $distribution->amount += $ligne->amount_ht;
                }
            }

            if($this->Distributions->save($distribution)) {
                $rep = $distribution;
            }
        }


        $this->set(compact('rep'));
        $this->set('_serialize', 'rep');
    }

    public function laodcommercial($user_id = null)
    {
        $user = $this->Users->get($user_id, [
            'contain' => [
                'Distributions' => [
                    'DistributionLines' => [
                        'OrderLines', 'Products'
                    ]
                ],
                'Orders' => ['Pois', 'PoiCustomers','OrderLines.Products','OrderStatuts']
            ]
        ]);

        if ($user) $rep = $user;
        else $rep = null;

        $this->set(compact('rep'));
        $this->set('_serialize', 'rep');
    }

    public function laodenqueteur($user_id = null)
    {
        $user = $this->Users->get($user_id, [
            'contain' => [
                'Distributions' => [
                    'DistributionLines' => [
                        'OrderLines', 'Products'
                    ],
                    'Orders' => ['Pois', 'PoiCustomers'],
                    'DistributionStatuts'
                ]
            ]
        ]);

        if ($user) $rep = $user;
        else $rep = null;

        $this->set(compact('rep'));
        $this->set('_serialize', 'rep');
    }

    public function editdistribution($distribution_id = null, $statut_id = null)
    {
        //statut_id : 3 distribuee --- 4 annulée
        $distribution = $this->Distributions->get($distribution_id, [
            'contain' => [
                'DistributionLines' => [
                    'OrderLines', 'Products'
                ],
                'Orders' => ['Pois', 'PoiCustomers'],
                'DistributionStatuts'
            ]
        ]);

        if ((int) $statut_id !== 0) {
            $distribution->distribution_statut_id = $statut_id;

            $distribution_lines = $distribution->distribution_lines;

            if ($this->Distributions->save($distribution)) {
                if ($distribution->distribution_statut_id == 3) {
                    foreach ($distribution_lines as $dis_line) {
                        $order_line = $this->OrderLines->get($dis_line->order_line_id);
                        $order_line->quantite_livre = $order_line->quantite_livre - $dis_line->quantite;
                        $this->OrderLines->save($order_line);
                    }
                }
                $rep = $distribution;
            } else $rep = null;
        } else {
            if ($distribution) $rep = $distribution;
            else $rep = null;
        }
        $this->set(compact('rep'));
        $this->set('_serialize', 'rep');
    }

    public function getProducts($company_id, $page = 1) {
        $this->loadModel('Products');

        $products = $this->Products->find('all')
                    ->contain(['Images'])
                    ->where(['Products.company_id' => $company_id])
                    ->limit(20)
                    ->page($page)
                    ->order(['Products.id DESC'])
                    ->toArray();

        $this->set(compact('products'));
        $this->set('_serialize', 'products');
    }

    public function company($company_id = null)
    {
        $this->loadModel('Companies');

        $rep = $this->Companies->get($company_id, [
            'contain' => [
                'Products.Images',
                'PoiCustomers',
                'Pois'
            ]
        ]);
        $this->set(compact('rep'));
        $this->set('_serialize', 'rep');
    }
}
