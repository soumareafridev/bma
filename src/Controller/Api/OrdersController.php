<?php

namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\I18n\Time;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');


class OrdersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function add()
    {
        $this->loadModel('Products');
        $rep = null;
        if ($this->request->is('post')) {
            $temp = $this->request->getData();

            $lignes = json_decode($temp['lignes']);
            $nb = count($lignes);

            $ordersave = 0;

            if (isset($temp['poi_customers']) && $temp['poi_customers'] !== null) {
                $ids = json_decode($temp['poi_customers']);
                $field = "poi_customer_id";
            }
            if (isset($temp['pois']) && $temp['pois'] !== null) {
                $ids = json_decode($temp['pois']);
                $field = "poi_id";
            }

            $date_limited = null;
            $date_debut = null;


            if (isset($temp['date_limited']) && $temp['date_limited'] !== null) {
                $date_limited = $temp['date_limited'] . ' 00:00:00';
                $date_limited = new Time($date_limited);
            }

            if (isset($temp['date_debut']) && $temp['date_debut'] == !null) {
                $date_debut = $temp['date_debut'] . ' 00:00:00';
                $date_debut = new Time($date_debut);
            }

            if ($date_limited != null && $date_debut != null && $date_debut > $date_limited) {
                $rep = 'date_error';
                $this->set(compact('rep'));
                $this->set('_serialize', 'rep');
                die();
            }

            $frequences = null;
            if (isset($temp['jours']) && $temp['jours'] == !null) {
                $frequences = implode(';', $temp['jours']);
            }

            if (count($ids) > 0) {
                foreach ($ids as $id) {
                    $order = $this->Orders->newEntity();

                    $order = $this->Orders->patchEntity($order, $temp);

                    if ($date_limited) $order->date_limited = $date_limited;
                    if ($date_debut) $order->date_debut = $date_debut;
                    if ($frequences) $order->frequences = $frequences;

                    $order->amount = 0;
                    if (isset($temp['user_id'])) $order->user_id = $temp['user_id'];
                    if ($field == "poi_customer_id") $order->poi_customer_id = (int) $id;
                    if ($field == "poi_id") $order->poi_id = (int) $id;

                    $order->order_statut_id = $temp['order_statut_id'];
                    $order->company_id = $temp['company_id'];

                    if ($this->Orders->save($order)) {
                        if ($nb > 0) {
                            $nbsave = 0;
                            foreach ($lignes as $ligne) {
                                $orderline = $this->Orders->OrderLines->newEntity();
                                $orderline->order_id = $order->id;
                                $orderline->price_ht = $ligne->price;
                                $orderline->quantite = $ligne->quantite;
                                $orderline->quantite_max = $ligne->limit;
                                $orderline->quantite_livre = 0;
                                $orderline->amount_ht = (int) $ligne->quantite * (int) $ligne->price;
                                $orderline->product_id = $ligne->product_id;
                                if ($this->Orders->OrderLines->save($orderline)) {
                                    $order->amount += $orderline->amount_ht;
                                    $nbsave++;
                                    $ordersave++;
                                }
                            }
                            if ($nbsave == $nb) {
                                $this->Orders->save($order);
                                $rep = $order;
                            } else {
                                $this->Orders->delete($order);
                                $rep = null;
                            }
                        }
                    }
                }
            }

            $this->set(compact('rep'));
            $this->set('_serialize', 'rep');
        }

        $this->set(compact('rep'));
        $this->set('_serialize', 'rep');
    }

    public function addOne()
    {
        $this->loadModel('Products');
        $rep = null;
        if ($this->request->is('post')) {
            $temp = $this->request->getData();

            $lignes = json_decode($temp['lignes']);
            $nb = count($lignes);

            $ordersave = 0;

            $date_limited = null;
            $date_debut = null;


            if (isset($temp['date_limited']) && $temp['date_limited'] != null) {
                $date_limited = $temp['date_limited'] . ' 00:00:00';
                $date_limited = new Time($date_limited);
            }

            if (isset($temp['date_debut']) && $temp['date_debut'] != null) {
                $date_debut = $temp['date_debut'] . ' 00:00:00';
                $date_debut = new Time($date_debut);
            }

            if ($date_limited != null && $date_debut != null && $date_debut > $date_limited) {
                $rep = 'date_error';
                $this->set(compact('rep'));
                $this->set('_serialize', 'rep');
                die();
            }

            $frequences = null;
            if (isset($temp['jours']) && $temp['jours'] != null) {
                $frequences = implode(';', $temp['jours']);
            }

            $order = $this->Orders->newEntity();

            $order = $this->Orders->patchEntity($order, $temp);

            if ($date_limited) $order->date_limited = $date_limited;
            if ($date_debut) $order->date_debut = $date_debut;
            if ($frequences) $order->frequences = $frequences;

            $order->amount = 0;

            $order->order_statut_id = $temp['order_statut_id'];
            $order->company_id = $temp['company_id'];

            if ($this->Orders->save($order)) {
                if ($nb > 0) {
                    $nbsave = 0;
                    foreach ($lignes as $ligne) {
                        $orderline = $this->Orders->OrderLines->newEntity();
                        $orderline->order_id = $order->id;
                        $orderline->price_ht = $ligne->price;
                        $orderline->quantite = $ligne->quantite;
                        $orderline->quantite_max = $ligne->limit;
                        $orderline->quantite_livre = 0;
                        $orderline->amount_ht = (int) $ligne->quantite * (int) $ligne->price;
                        $orderline->product_id = $ligne->product_id;
                        if ($this->Orders->OrderLines->save($orderline)) {
                            $order->amount += $orderline->amount_ht;
                            $nbsave++;
                        }
                    }
                    if ($nbsave == $nb) {
                        $this->Orders->save($order);
                        $rep = $order;
                    } else {
                        $this->Orders->delete($order);
                        $rep = null;
                    }
                }
            }

            $this->set(compact('rep'));
            $this->set('_serialize', 'rep');
        }

        $this->set(compact('rep'));
        $this->set('_serialize', 'rep');
    }

    public function getOrders($company_id, $page = 1)
    {
        $orders = $this->Orders->find('all', [
            'contain' => ['Pois', 'PoiCustomers', 'OrderLines.Products.Images', 'OrderStatuts'],
            'conditions' => ['Orders.company_id' => $company_id],
            'order' => ['Orders.id DESC']
        ])
            ->limit(20)
            ->page($page)
            ->toArray();

        $this->set(compact('orders'));
        $this->set('_serialize', 'orders');
    }

    public function edit($id = null)
    {
        $this->loadModel('Products');
        $order = $this->Orders->get($id, [
            'contain' => ['OrderLines.Products', 'Pois', 'PoiCustomers', 'OrderStatuts']
        ]);
        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            if (isset($temp['poi_customer_id'])) $order->poi_customer_id = $temp['poi_customer_id'];
            if (isset($temp['poi_id'])) $order->poi_id = $temp['poi_id'];

            $order->order_statut_id = $temp['order_statut_id'];
            $lignes = json_decode($temp['lignes']);
            $nb = count($lignes);
            $nbsave = 0;
            if ($this->Orders->save($order)) {
                if ($nb > 0) {
                    foreach ($lignes as $ligne) {
                        $orderline = $this->Orders->OrderLines->newEntity();
                        $orderline->order_id = $order->id;
                        $orderline->price_ht = $temp['price_ht' . $ligne];
                        $orderline->quantite = $temp['q' . $ligne];
                        $orderline->amount_ht = $temp['amount_ht' . $ligne];
                        $orderline->product_id = $temp['product_id' . $ligne];
                        if ($this->Orders->OrderLines->save($orderline)) {
                            $order->amount += $orderline->amount_ht;
                            $nbsave++;
                        }
                    }
                }
                if ($nbsave == $nb) {
                    $this->Orders->save($order);
                    $this->Flash->success(__('La commande a été enregistrée avec succès !'));
                    return $this->redirect($this->referer());
                } else {
                    $this->Orders->delete($order);
                    $this->Flash->error(__("La commande n'a pas pu être enregistrée, merci de reessayer."));
                }
            }
            $this->Flash->error(__("La commande n'a pas pu être enregistrée, merci de reessayer."));
        }
        $products = $this->Products->find('all', ['conditions' => ['Products.company_id' => $temp['company_id'], 'Products.status <' => 2], 'contain' => ['Gammes', 'Images']]);
        $product_format_json = json_decode(json_encode($products));
        $orderStatuts = $this->Orders->OrderStatuts->find('list', ['conditions' => []]);
        $poiCustomers = $this->Orders->PoiCustomers->find("all", ['conditions' => ['PoiCustomers.company_id' => $temp['company_id']]])
            ->toArray();
        $pois = $this->Orders->Pois->find('all', ['conditions' => ['Pois.company_id' => $temp['company_id']]])->select(['id', 'name' => 'dns'])
            ->toArray();
        $this->set(compact('order', 'orderStatuts', 'poiCustomers', 'pois', 'products', 'product_format_json'));
    }

    public function editorder($order_id = null, $statut_id = null)
    {
        $order = $this->Orders->get($order_id, [
            'contain' => ['OrderLines.Products', 'Pois', 'PoiCustomers', 'OrderStatuts']
        ]);

        $order->order_statut_id = $statut_id;
        if ($this->Orders->save($order)) $rep = $order;
        else $rep = null;

        $this->set(compact('rep'));
        $this->set('_serialize', 'rep');
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('La commande a été supprimée avec succès.'));
        } else {
            $this->Flash->error(__("La commande n'a pas pu être supprimée, merci de réessayer."));
        }

        return $this->redirect(['action' => 'index']);
    }
}
