<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

class PoisController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->Auth->allow(['formulaire']);
    }
    public function formulaire()
    {
        $this->loadModel("Surveys");
        $survey = null;

        $survey_id = $this->request->getQuery('enquete');
        $company_id = $this->request->getQuery('company_id');

        if(isset($survey_id) && $survey_id != "" && isset($company_id)) {
            $survey = $this->Surveys->get($survey_id, ['contain' => ['Forms.Fields.TypeFields'], 'conditions' => ['Surveys.company_id' => $company_id]]);
        }

        if ($survey && $this->request->is('post')) {
            $temp = $this->request->getData();
            $poi = $this->Pois->newEntity();
            $poi = $this->Pois->patchEntity($poi, $temp);
            $poi->created = Time::now();
            $poi->status = 1;
            $poi->groupe_id = $survey->groupe_id;
            $poi->company_id = $company_id;
            $poi->survey_id = $survey->id;
            $poi->user_id = null;

            $form = $survey->form;

            if($this->Pois->save($poi)) {
                $this->loadModel('Infos');
                if($form) {
                    if(isset($form) && count($form->fields) > 0) {
                        foreach($form->fields as $field) {
                            $info = $this->Infos->newEntity();
                            $info->poi_id = $poi->id;
                            $info->field_id = $field->id;
                            $info->value = $temp['info' . $field->id];
                            $this->Infos->save($info);
                        }
                    }
                }
                if (isset($temp['image']) && $temp['image'] !== null) {
                    $this->savePoiImg($poi->id);
                }

                $this->Flash->success(__('<div>
                    <h1>Félicitations !</h1>
                    <h6>
                        Votre questionnaire a été bien rempli et soumis. <br>
                        Nous vous remercions d\'avoir pris le temps pour répondre au questionnaire, à très bientôt.
                    </h6>
                </div>'), ['escape' => false]);
                return $this->redirect($this->referer());

            } else $this->Flash->error($this->msgErrors($this->returnErrors($poi->getErrors())));
        }

        $this->set(compact('survey', 'survey_id'));
    }
}
