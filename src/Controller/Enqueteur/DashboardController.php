<?php
namespace App\Controller\Enqueteur;

use App\Controller\Enqueteur\AppController;

class DashboardController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Users');
        $this->loadModel('Forms');
        $this->loadModel('Pois');
        $this->loadModel('Infos');
    }
    public function index()
    {

       $pois = $this->getPOI(1,5,['Pois.user_id' => $this->Auth->user('id')]);
       $type_pois = $this->Pois->TypePois->find('all', [
           'contain' => ['Pois' => ['conditions' => ['Pois.user_id' => $this->Auth->user('id')]]],
            'conditions' => ['TypePois.company_id' => $this->Auth->user('company_id')]])->toArray();
        
        $this->set(compact('pois','type_pois'));
    }

}
