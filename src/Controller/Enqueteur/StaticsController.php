<?php
namespace App\Controller\Enqueteur;

use App\Controller\Enqueteur\AppController;
use App\Controller\TypePoisController;
use Cake\I18n\Time;

class StaticsController extends AppController
{
    public function sidebar() {
        $this->loadModel('TypePois');

        $typePois = new TypePoisController();
        $type_pois = $typePois->getList();

        $sidebar = [
            "principals" =>
            [
                [
                    "name" => "Dashboard",
                    "fa" => "fa-tachometer-alt",
                    "controller" => "Dashboard",
                    "action" => "index"
                ],
                [
                    "name" => "Cartographie",
                    "fa" => "fa-map-marked-alt",
                    "controller" => "Pois",
                    "action" => "cartographie"
                ],
            ],
            "secondaires" =>
            [
                [
                    "name" => "POIs",
                    "fa" => "fa-map-pin",
                    "header" => "Les types de POI",
                    "refs" => ['pois'],
                    "items" => []
                ]
            ]
        ];

        if(count($type_pois) > 0) {
            foreach($type_pois as $type) {
                $item = [
                    "name" => $type->name,
                    "controller" => "Pois",
                    "action" => "pois-enregistres",
                    "params" => $this->hashUrl($type->id)
                ];

                $sidebar["secondaires"][0]["items"][] = $item;
            }
            $sidebar["secondaires"][0]["items"][] = [
                "name" => "Ajoutez un POI",
                "controller" => "Pois",
                "action" => "add"
            ];

        } else {
            $item = [
                "name" => "Aucun Type de POI n'est créé pour le moment",
                "controller" => "dashboard",
                "action" => "index"
            ];

            $sidebar["secondaires"][0]["items"][] = $item;
        }
        return $sidebar;
    }

    public static $sidebar = [
        "principals" =>
        [
            [
                "name" => "Dashboard",
                "fa" => "fa-tachometer-alt",
                "controller" => "Dashboard",
                "action" => "index"
            ],
            [
                "name" => "Cartographie",
                "fa" => "fa-map-marked-alt",
                "controller" => "Pois",
                "action" => "cartographie"
            ],
        ],
        "secondaires" =>
        [
            [
                "name" => "POIs",
                "fa" => "fa-map-pin",
                "header" => "Les types de POI",
                "refs" => ['pois'],
                "items" => [
                    [
                        "name" => "Points de vente",
                        "controller" => "Pois",
                        "action" => "points-de-vente"
                    ],
                    [
                        "name" => "Les Clients indépendants",
                        "controller" => "Pois",
                        "action" => "clients-independants"
                    ]
                ]
            ]
        ]
    ];
}
