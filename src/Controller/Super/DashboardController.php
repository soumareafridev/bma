<?php

namespace App\Controller\Super;

use App\Controller\Super\AppController;
use Cake\I18n\Time;

class DashboardController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Distributions');
        $this->loadModel('Forms');
        $this->loadModel('Pois');
        $this->loadModel('Orders');
    }

    public function index()
    {
        $company_id = $this->Auth->user('company_id');

        $distributions = $this->Distributions->find('all', ['conditions' => ['Distributions.distribution_statut_id' => 3]])->toArray();
        $orders = $this->Orders->find('all', ['conditions' => ['Orders.order_statut_id' => 2]])->toArray();

        $companies = $this->Pois->Companies->find('all', ['conditions' => ['Companies.id <>' => $company_id]])->toArray();


        // DEBUT AREA CHART
        $mois = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];


        //NE PAS ENTRER N IMPORTE QUOI
        $regex = '/^2[0-9]{3}$/';


        $getAnnee = $this->request->getQuery('annee');

        $resultatRegex = \preg_match($regex, $getAnnee);

        $anneEnCours = Time::now()->format('Y');
        $dataAnnee = $anneEnCours;

        //Les tableaux a remplir
        $dataOrders = [];
        $dataDistributions = [];

        if ($getAnnee && $resultatRegex == 1) $dataAnnee = $getAnnee;
        else if ($getAnnee && ($resultatRegex == 0 || $resultatRegex == false)) {
            $this->Flash->error(__('Merci d\'entrer  une année valide'));
        }

        foreach ($mois as $period) {
            $itemOrder = 0;
            $itemDistribution = 0;
            $itemCompanie = 0;
            foreach ($orders as $order) {
                if (($mois[(int) $order->modified->format('m') - 1] == $period) && $order->modified->format('Y') == $dataAnnee) $itemOrder += $order->amount;
            }
            foreach ($distributions as $distribution) {
                if (($mois[(int) $distribution->modified->format('m') - 1] == $period) && $distribution->modified->format('Y') == $dataAnnee) $itemDistribution += $distribution->amount;
            }

            foreach ($companies as $company) {
                if (($mois[(int) $company->created->format('m') - 1] == $period) && $company->created->format('Y') == $dataAnnee) $itemCompanie ++;
            }

            $dataOrders[] = $itemOrder;
            $dataDistributions[] = $itemDistribution;
            $dataCompanies[] = $itemCompanie;
        }

        $dataOrders = \json_encode($dataOrders);
        $dataDistributions = \json_encode($dataDistributions);
        $dataCompanies = \json_encode($dataCompanies);
        // FIN AREA CHART

        $this->set(compact('dataDistributions', 'dataAnnee', 'dataOrders','dataCompanies'));
    }


    public function bienvenue()
    {
        $this->loadModel('Themes');
        $this->loadModel('Companies');
        $themes = $this->Themes->find('all')->toArray();
        $company = $this->Companies->get($this->Auth->user('company_id'));
        if ($this->request->is(['patch', 'post', 'put'])) {
            $company->theme_id = $this->request->getData('theme_id');
            $this->Companies->save($company);
            return $this->redirect(['action' => 'index']);
        }
        $this->set(compact('themes'));
    }
}
