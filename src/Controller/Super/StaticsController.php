<?php
namespace App\Controller\Super;

use App\Controller\Super\AppController;
use App\Controller\TypePoisController;
use Cake\I18n\Time;

class StaticsController extends AppController
{
    public function sidebar() {
        $sidebar = [
            "principals" =>
            [
                [
                    "name" => "Dashboard",
                    "fa" => "fa-tachometer-alt",
                    "controller" => "Dashboard",
                    "action" => "index"
                ]
            ],
            "secondaires" =>
            [
                [
                    "name" => "Les entreprises",
                    "fa" => "fa-building",
                    "controller" => "Companies",
                    "action" => "index",
                    "refs" => ['companies']
                ],
                [
                    "name" => "Statistiques",
                    "fa" => "fa-chart-line",
                    "controller" => "Statistiques",
                    "action" => "index",
                    "refs" => ['statistiques']
                ],
                [
                    "name" => "Revenus",
                    "fa" => "fa-dollar-sign",
                    "controller" => "Statistiques",
                    "action" => "pricing",
                    "refs" => ['statistiques']
                ],
            ]
        ];

        return $sidebar;
    }
}
