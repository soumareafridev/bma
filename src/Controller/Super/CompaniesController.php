<?php
namespace App\Controller\Super;

use App\Controller\Super\AppController;

/**
 * Companies Controller
 *
 * @property \App\Model\Table\CompaniesTable $Companies
 *
 * @method \App\Model\Entity\Company[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CompaniesController extends AppController
{

    public function index()
    {
        $companies = $this->Companies->find('all', [
            'conditions' => ['Companies.id <>' => $this->Auth->user('company_id')],
            'contain' => ['Users', 'CompanieStatuts'],
            'order' => ['Companies.id' => 'DESC']
            ])->toArray();

        $statuts = $this->Companies->CompanieStatuts->find('all')->toArray();
        $this->set(compact('companies','statuts'));
    }


    public function statut()
    {
        if($this->request->is('post')) {
            $company_id = $this->request->getQuery('company_id');
            $statut_id = $this->request->getQuery('statut_id');

            $company = $this->Companies->get($company_id);
            
            if(in_array($statut_id, [1,2,3])) {
                $company->companie_statut_id = $statut_id;
                if($this->Companies->save($company)) $this->Flash->success(__('Le statut de l\'entreprise a été modifié avec succès !'));
                else $this->Flash->error(__($this->msgErrors($this->returnErrors($company->getErrors()))), ['escape' => false]);
            }
            else $this->Flash->error(__('Le statut est invalide !'));
        }

        return $this->redirect($this->referer());
    }

    public function get($id = null)
    {
        $company = $this->Companies->get($id, [
            'contain' => ['Users']
        ]);
        if(!$company) return false;
        return $company;
    }

    public function access()
    {
        $id = $this->request->getQuery('i');
        $modules = $this::modules();

        $company = $this->Companies->get($id, [
            'contain' => []
        ]);

        $acces = [];

        if(isset($company->access) && $company->access != "") {
            $acces = explode(';', $company->access);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $temp_access = $this->request->getData('access');

            if($temp_access) $acces = implode(";", $this->request->getData('access'));
            else $acces = "";

             
            $company->access = $acces;
            if ($this->Companies->save($company)) {
                $this->Flash->success(__('Les accès de l\'entreprise ont été mises à jour !'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Echec du traitement de la requête, réessayez !'));
        }
        $this->set(compact('company', 'modules', 'acces'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $company = $this->Companies->get($id);
        if ($this->Companies->delete($company)) {
            $this->Flash->success(__('The company has been deleted.'));
        } else {
            $this->Flash->error(__('The company could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
