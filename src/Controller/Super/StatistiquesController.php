<?php

namespace App\Controller\Super;

use App\Controller\Super\AppController;
use Cake\I18n\Time;

class StatistiquesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Distributions');
        $this->loadModel('Forms');
        $this->loadModel('Pois');
        $this->loadModel('Orders');
        $this->loadModel('Users');
    }

    public function index()
    {
        // reception variable
        $getAnnee = $this->request->getQuery('annee');
        $getLegende = $this->request->getQuery('legende');
        $getEntrepriseId = $this->request->getQuery('entreprise');

        $company_id = $this->Auth->user('company_id');

        $option_distributions = ['conditions' => ['Distributions.distribution_statut_id' => 3]];
        $option_commandes = ['conditions' => ['Orders.order_statut_id' => 2]];
        $option_users = ['contain' => ['Roles'],'conditions' => ['Users.company_id <>' => $company_id]];
        $option_pois = ['conditions' => ['Pois.company_id <>' => $company_id]];

        if(isset($getEntrepriseId) && $getEntrepriseId != "" && (int) $getEntrepriseId > 0) {
            $company = $this->Companies->get($getEntrepriseId);
            $option_distributions['conditions'][] = ['Distributions.company_id' => $getEntrepriseId];
            $option_commandes['conditions'][] = ['Orders.company_id' => $getEntrepriseId];
            $option_users['conditions'][] = ['Users.company_id' => $getEntrepriseId];
            $option_pois['conditions'][] = ['Pois.company_id' => $getEntrepriseId];

            $this->set(compact('company'));
        }

        $distributions = $this->Distributions->find('all', $option_distributions)->toArray();
        $orders = $this->Orders->find('all', $option_commandes)->toArray();
        $users = $this->Users->find('all', $option_users)->toArray();
        $pois = $this->Pois->find('all', $option_pois)->toArray();

        $list_companies = $this->Pois->Companies->find('list', ['conditions' => ['Companies.id <>' => $company_id]]);


        // DEBUT AREA CHART
        $mois = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];


        //NE PAS ENTRER N IMPORTE QUOI
        $regex = '/^2[0-9]{3}$/';


        $resultatRegex = \preg_match($regex, $getAnnee);

        $anneEnCours = Time::now()->format('Y');
        $dataAnnee = $anneEnCours;

        //Les tableaux a remplir
        $dataOrders = [];
        $dataDistributions = [];
        $dataPois = [];
        $dataUser = [];
        $dataEnq = [];
        $dataDist = [];
        $dataComm = [];
        $dataAdmin = [];
        

        if ($getAnnee && $resultatRegex == 1) $dataAnnee = $getAnnee;
        else if ($getAnnee && ($resultatRegex == 0 || $resultatRegex == false)) {
            $this->Flash->error(__('Merci d\'entrer  une année valide'));
        }

        foreach ($mois as $period) {
            $itemOrder = 0;
            $itemDistribution = 0;
            $itemPois = 0;
            $itemUser = 0;
            $itemEnq = 0;
            $itemDist = 0;
            $itemComm = 0;
            $itemAdmin = 0;
            foreach ($orders as $order) {
                if (($mois[(int) $order->modified->format('m') - 1] == $period) && $order->modified->format('Y') == $dataAnnee) $itemOrder += $order->amount;
            }
            foreach ($distributions as $distribution) {
                if (($mois[(int) $distribution->modified->format('m') - 1] == $period) && $distribution->modified->format('Y') == $dataAnnee) $itemDistribution += $distribution->amount;
            }

            foreach ($pois as $poi) {
                if (($mois[(int) $poi->created->format('m') - 1] == $period) && $poi->created->format('Y') == $dataAnnee) $itemPois++;
            }

            foreach ($users as $user) {
                if (($mois[(int) $user->created->format('m') - 1] == $period) && $user->created->format('Y') == $dataAnnee) {
                    $itemUser ++;
                    foreach($user->roles as $role) {
                        switch($role->profile_id) {
                            case 1: 
                                $itemEnq++;
                            break;
                            case 2: 
                            case 3 :
                                $itemComm++;
                            break;
                            case 4: 
                                $itemAdmin++;
                            break;
                            case 5: 
                                $itemDist++;
                            break;
                        }
                    }
                } 
            }

            $dataOrders[] = $itemOrder;
            $dataDistributions[] = $itemDistribution;
            $dataPois[] = $itemPois;

            $dataUser[] = $itemUser;
            $dataEnq[] = $itemEnq;
            $dataDist[] = $itemDist;
            $dataComm[] = $itemComm;
            $dataAdmin[] = $itemAdmin;
        }

        $dataOrders = \json_encode($dataOrders);
        $dataDistributions = \json_encode($dataDistributions);
        $dataPois = $dataPois;
        $dataUsers = [
            'users' => \json_encode($dataUser),
            'enqueteurs'=> \json_encode($dataEnq),
            'distributeurs' => \json_encode($dataDist),
            'commerciaux' => \json_encode($dataComm),
            'adminitrateurs'=> \json_encode($dataAdmin),
        ];

        // FIN AREA CHART

        $this->set(compact('dataDistributions', 'dataOrders', 'list_companies', 'dataUsers', 'dataAnnee', 'dataPois'));
    }


    public function pricing()
    {
        // reception variable
        $getAnnee = $this->request->getQuery('annee');
        $getEntrepriseId = $this->request->getQuery('entreprise');

        $prix_commande = $this->request->getQuery('prix_commande');
        $prix_distribution = $this->request->getQuery('prix_distribution');
        $prix_pois = $this->request->getQuery('prix_pois');

        if(!isset($prix_commande) || $prix_commande == "") $prix_commande = 75;
        if(!isset($prix_distribution) || $prix_distribution == "") $prix_distribution = 50;

        $company_id = $this->Auth->user('company_id');

        $option_distributions = ['conditions' => ['Distributions.distribution_statut_id' => 3]];
        $option_commandes = ['conditions' => ['Orders.order_statut_id' => 2]];
        // $option_users = ['contain' => ['Roles'],'conditions' => ['Users.company_id <>' => $company_id]];
        $option_pois = ['conditions' => ['Pois.company_id <>' => $company_id]];

        if(isset($getEntrepriseId) && $getEntrepriseId != "" && (int) $getEntrepriseId > 0) {
            $company = $this->Companies->get($getEntrepriseId);
            $option_distributions['conditions'][] = ['Distributions.company_id' => $getEntrepriseId];
            $option_commandes['conditions'][] = ['Orders.company_id' => $getEntrepriseId];
            // $option_users['conditions'][] = ['Users.company_id' => $getEntrepriseId];
            $option_pois['conditions'][] = ['Pois.company_id' => $getEntrepriseId];

            $this->set(compact('company'));
        }

        $distributions = $this->Distributions->find('all', $option_distributions)->toArray();
        $orders = $this->Orders->find('all', $option_commandes)->toArray();
        // $users = $this->Users->find('all', $option_users)->toArray();
        $pois = $this->Pois->find('all', $option_pois)->toArray();

        $list_companies = $this->Pois->Companies->find('list', ['conditions' => ['Companies.id <>' => $company_id]]);


        // DEBUT AREA CHART
        $mois = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];


        //NE PAS ENTRER N IMPORTE QUOI
        $regex = '/^2[0-9]{3}$/';


        $resultatRegex = \preg_match($regex, $getAnnee);

        $anneEnCours = Time::now()->format('Y');
        $dataAnnee = $anneEnCours;

        //Les tableaux a remplir
        $dataOrders = [];
        $dataDistributions = [];
        $dataPois = [];
        $dataUser = [];
        $dataEnq = [];
        $dataDist = [];
        $dataComm = [];
        $dataAdmin = [];
        

        if ($getAnnee && $resultatRegex == 1) $dataAnnee = $getAnnee;
        else if ($getAnnee && ($resultatRegex == 0 || $resultatRegex == false)) {
            $this->Flash->error(__('Merci d\'entrer  une année valide'));
        }

        foreach ($mois as $period) {
            $itemOrder = 0;
            $itemDistribution = 0;
            $itemPois = 0;
            // $itemUser = 0;
            // $itemEnq = 0;
            // $itemDist = 0;
            // $itemComm = 0;
            // $itemAdmin = 0;
            foreach ($orders as $order) {
                if (($mois[(int) $order->modified->format('m') - 1] == $period) && $order->modified->format('Y') == $dataAnnee) $itemOrder += (int) $prix_commande;
            }
            foreach ($distributions as $distribution) {
                if (($mois[(int) $distribution->modified->format('m') - 1] == $period) && $distribution->modified->format('Y') == $dataAnnee) $itemDistribution += (int) $prix_distribution;
            }

            foreach ($pois as $poi) {
                if (($mois[(int) $poi->created->format('m') - 1] == $period) && $poi->created->format('Y') == $dataAnnee) $itemPois += (int) $prix_pois;
            }

            // foreach ($users as $user) {
            //     if (($mois[(int) $user->created->format('m') - 1] == $period) && $user->created->format('Y') == $dataAnnee) {
            //         $itemUser ++;
            //         foreach($user->roles as $role) {
            //             switch($role->profile_id) {
            //                 case 1: 
            //                     $itemEnq++;
            //                 break;
            //                 case 2: 
            //                 case 3 :
            //                     $itemComm++;
            //                 break;
            //                 case 4: 
            //                     $itemAdmin++;
            //                 break;
            //                 case 5: 
            //                     $itemDist++;
            //                 break;
            //             }
            //         }
            //     } 
            // }

            $dataOrders[] = $itemOrder;
            $dataDistributions[] = $itemDistribution;
            $dataPois[] = $itemPois;

            // $dataUser[] = $itemUser;
            // $dataEnq[] = $itemEnq;
            // $dataDist[] = $itemDist;
            // $dataComm[] = $itemComm;
            // $dataAdmin[] = $itemAdmin;
        }

        $dataOrders = $dataOrders;
        $dataDistributions = $dataDistributions;
        $dataPois = $dataPois;

        // $dataUsers = [
        //     'users' => \json_encode($dataUser),
        //     'enqueteurs'=> \json_encode($dataEnq),
        //     'distributeurs' => \json_encode($dataDist),
        //     'commerciaux' => \json_encode($dataComm),
        //     'adminitrateurs'=> \json_encode($dataAdmin),
        // ];

        // FIN AREA CHART

        $this->set(compact('mois','prix_distribution', 'prix_commande', 'prix_pois','dataDistributions', 'dataOrders', 'list_companies', 'dataAnnee', 'dataPois'));
    }


    public function bienvenue()
    {
        $this->loadModel('Themes');
        $this->loadModel('Companies');
        $themes = $this->Themes->find('all')->toArray();
        $company = $this->Companies->get($this->Auth->user('company_id'));
        if ($this->request->is(['patch', 'post', 'put'])) {
            $company->theme_id = $this->request->getData('theme_id');
            $this->Companies->save($company);
            return $this->redirect(['action' => 'index']);
        }
        $this->set(compact('themes'));
    }
}
