<?php
namespace App\Controller;

use App\Controller\AppController;
use \Mailjet\Resources;

class UtilitiesController extends AppController {
        //envoi de mail
        static function sendEmail($user_email,$objet,$msgHTML)
        {
            $apiKey = "3b795f369165a7a108363397cb73a61c";
            $secretKey = "00abdc94e2f7abdee5ff8ef6f65abcf8";
            $mj = new \Mailjet\Client($apiKey, $secretKey);
            $mj->setTimeout(60);
            $body = [
                'FromEmail' => "contact@afridev-group.com",
                'FromName' => "SCOD - Système de cartographie et de distribution",
                'Subject' => $objet,
                'Html-part' => $msgHTML,
                'Recipients' => [
                    [
                        'Email' => $user_email
                    ]
                ]
            ];
            try{
                $response = $mj->post(Resources::$Email, ['body' => $body]);
                $response->success();     
    
            } catch(Exception $e) {
                //$e->getMessage();
            }
        }
}