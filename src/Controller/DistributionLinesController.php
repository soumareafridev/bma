<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DistributionLines Controller
 *
 * @property \App\Model\Table\DistributionLinesTable $DistributionLines
 *
 * @method \App\Model\Entity\DistributionLine[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DistributionLinesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Distributions', 'Products']
        ];
        $distributionLines = $this->paginate($this->DistributionLines);

        $this->set(compact('distributionLines'));
    }

    /**
     * View method
     *
     * @param string|null $id Distribution Line id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $distributionLine = $this->DistributionLines->get($id, [
            'contain' => ['Distributions', 'Products']
        ]);

        $this->set('distributionLine', $distributionLine);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $distributionLine = $this->DistributionLines->newEntity();
        if ($this->request->is('post')) {
            $distributionLine = $this->DistributionLines->patchEntity($distributionLine, $this->request->getData());
            if ($this->DistributionLines->save($distributionLine)) {
                $this->Flash->success(__('The distribution line has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The distribution line could not be saved. Please, try again.'));
        }
        $distributions = $this->DistributionLines->Distributions->find('list', ['limit' => 200]);
        $products = $this->DistributionLines->Products->find('list', ['limit' => 200]);
        $this->set(compact('distributionLine', 'distributions', 'products'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Distribution Line id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $distributionLine = $this->DistributionLines->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $distributionLine = $this->DistributionLines->patchEntity($distributionLine, $this->request->getData());
            if ($this->DistributionLines->save($distributionLine)) {
                $this->Flash->success(__('The distribution line has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The distribution line could not be saved. Please, try again.'));
        }
        $distributions = $this->DistributionLines->Distributions->find('list', ['limit' => 200]);
        $products = $this->DistributionLines->Products->find('list', ['limit' => 200]);
        $this->set(compact('distributionLine', 'distributions', 'products'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Distribution Line id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $distributionLine = $this->DistributionLines->get($id);
        if ($this->DistributionLines->delete($distributionLine)) {
            $this->Flash->success(__('The distribution line has been deleted.'));
        } else {
            $this->Flash->error(__('The distribution line could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
