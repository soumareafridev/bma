<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Teams Controller
 *
 * @property \App\Model\Table\TeamsTable $Teams
 *
 * @method \App\Model\Entity\Team[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TeamsController extends AppController
{

    public function index()
    {
        $paginate = [
            'conditions' => ['Teams.company_id' => $this->Auth->user('company_id')]
        ];
        $teams =$this->Teams->find('all', $paginate)->toArray();

        $this->set(compact('teams'));
    }

    public function view($id = null)
    {
        $team = $this->Teams->get($id, [
            'contain' => ['Gestions', 'Members']
        ]);

        $this->set('team', $team);
    }

    public function add()
    {
        $team = $this->Teams->newEntity();
        if ($this->request->is('post')) {
            $team = $this->Teams->patchEntity($team, $this->request->getData());
            $team->company_id = $this->Auth->user('company_id');
            if ($this->Teams->save($team)) {
                $this->Flash->success(__("L'équipe a été ajoutée avec succès !"));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__("L'équipe n'a pas pu être ajoutée, reessayez!"));
        }
        $this->set(compact('team'));
    }

    public function edit($id = null)
    {
        $team = $this->Teams->get($id, [
            'contain' => ['Members.Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $team = $this->Teams->patchEntity($team, $this->request->getData());
            if ($this->Teams->save($team)) {
                $this->Flash->success(__("L'équipe a été modifiée avec succès !"));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__("L'équipe n'a pas pu être modifiée, reessayez!"));
        }

        $member_ids = $this->Teams->Members->find('all',['conditions' => ['Members.team_id' => $team->id]])->extract('user_id')->toArray();

        if(count($member_ids) > 0) $no_members = $this->Teams->Members->Users->find('all',['conditions' => ['Users.id NOT IN' => $member_ids, 'Users.company_id' => $this->Auth->user('company_id')]])->toArray();
        else $no_members = $this->Teams->Members->Users->find('all',['conditions' => ['Users.company_id' => $this->Auth->user('company_id')]])->toArray();

        $members = $team->members;

        $this->set(compact('team','members','no_members'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $team = $this->Teams->get($id);
        if ($this->Teams->delete($team)) {
            $this->Flash->success(__("L'équipe a été supprimée avec succès !"));
        } else {
            $this->Flash->error(__("L'équipe n'a pas pu être supprimée, reessayez!"));
        }

        return $this->redirect($this->referer());
    }
}
