<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\I18n\Time;

class DashboardController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Distributions');
        $this->loadModel('Forms');
        $this->loadModel('Pois');
        $this->loadModel('Orders');
    }
    public $colors = [
        '#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6',
        '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
        '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A',
        '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
        '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC',
        '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
        '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
        '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
        '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
        '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'
    ];

    public function index()
    {
        $company_id = $this->Auth->user('company_id');

        $pois = $this->getPOI(1, 5);
        $type_pois = $this->Pois->TypePois->find('all', ['contain' => ['Pois'], 'conditions' => ['TypePois.company_id' => $company_id]])->toArray();

        $distributions = $this->Distributions->find('all', ['conditions' => ['Distributions.company_id' => $company_id, 'Distributions.distribution_statut_id' => 3]])->toArray();
        $orders = $this->Orders->find('all', ['conditions' => ['Orders.company_id' => $company_id, 'Orders.order_statut_id' => 2]])->toArray();


        // DEBUT AREA CHART
        $mois = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];


        //NE PAS ENTRER N IMPORTE QUOI
        $regex = '/^2[0-9]{3}$/';


        $getAnnee = $this->request->getQuery('annee');

        $resultatRegex = \preg_match($regex, $getAnnee);

        $anneEnCours = Time::now()->format('Y');
        $dataAnnee = $anneEnCours;

        //Les tableaux a remplir
        $dataOrders = [];
        $dataDistributions = [];

        if ($getAnnee && $resultatRegex == 1) $dataAnnee = $getAnnee;
        else if ($getAnnee && ($resultatRegex == 0 || $resultatRegex == false)) {
            $this->Flash->error(__('Merci d\'entrer  une année valide'));
        }

        foreach ($mois as $period) {
            $itemOrder = 0;
            $itemDistribution = 0;
            foreach ($orders as $order) {
                if (($mois[(int) $order->modified->format('m') - 1] == $period) && $order->modified->format('Y') == $dataAnnee) $itemOrder += $order->amount;
            }
            foreach ($distributions as $distribution) {
                if (($mois[(int) $distribution->modified->format('m') - 1] == $period) && $distribution->modified->format('Y') == $dataAnnee) $itemDistribution += $distribution->amount;
            }
            $dataOrders[] = $itemOrder;
            $dataDistributions[] = $itemDistribution;
        }

        $dataOrders = \json_encode($dataOrders);
        $dataDistributions = \json_encode($dataDistributions);
        // FIN AREA CHART

        /* 
            DEBUT  PIE CHART
        */
        /* $pieData = [
            "labels" => [],
            "data" => [],
            "bgcolors" => [],
        ];
        foreach($type_pois as $type) {
            $ind = rand(0,count($this->colors) - 1);
            if(count($type->pois) > 0) {
                $pieData["labels"][] = $type->name;
                $pieData["data"][] = count($type->pois);
                $pieData["bgcolors"][] = $this->colors[$ind];
            }
        }

        $pieData = \json_encode($pieData); */

        $this->set(compact('pois', 'type_pois', 'dataAnnee', 'dataDistributions', 'dataOrders','anneEnCours'));
    }


    public function bienvenue()
    {
        $this->loadModel('Themes');
        $this->loadModel('Companies');
        $themes = $this->Themes->find('all')->toArray();
        $company = $this->Companies->get($this->Auth->user('company_id'));
        if ($this->request->is(['patch', 'post', 'put'])) {
            $company->theme_id = $this->request->getData('theme_id');
            $this->Companies->save($company);
            return $this->redirect(['action' => 'index']);
        }
        $this->set(compact('themes'));
    }
}
