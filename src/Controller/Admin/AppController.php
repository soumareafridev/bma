<?php
namespace App\Controller\Admin;

use App\Controller\AppController as app;
use App\Controller\Admin\StaticsController;
use Cake\Event\Event;
use App\Controller\TypePoisController;

class AppController extends app
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Companies');
    }

    public function getUser($id)
    {
        $this->loadModel('Users');  
        $user = $this->Users->get($id, ['contain' => ['Roles.Profiles']]);
        $user->profile_ids = [];

        if(count($user->roles) > 0) {
            foreach ($user->roles as $role) {
                $user->profile_ids[] = $role->profile_id;
            }
        }
        return $user;
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        
        
        // dd($sidebar);
        
        if($this->Auth->user('id')) {
            $type_pois = new TypePoisController();
            $_typePois = $type_pois->getList();
    
            $staticManager = new StaticsController();
            $sidebar = json_decode(json_encode($staticManager->sidebar()));
            
            $_company = $this->Companies->get($this->Auth->user('company_id'),['contain' => ['Themes']]);
            $_user = $this->getUser($this->Auth->user('id'));
            $_user->profile_id = 4;
            $this->set(compact('_user','sidebar','_typePois', '_company'));
        }
    }
}
