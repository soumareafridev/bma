<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * PoiCustomers Controller
 *
 * @property \App\Model\Table\PoiCustomersTable $PoiCustomers
 *
 * @method \App\Model\Entity\PoiCustomer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PoiCustomersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $paginate = [
            'contain' => ['CustomerTypes'],
            'conditions' => ['PoiCustomers.company_id' => $this->Auth->user('company_id')]
        ];
        $poiCustomers = $this->PoiCustomers->find('all',$paginate)->toArray();

        $this->set(compact('poiCustomers'));
    }

    /**
     * View method
     *
     * @param string|null $id Poi Customer id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $poiCustomer = $this->PoiCustomers->get($id, [
            'contain' => ['CustomerTypes']
        ]);

        $this->set('poiCustomer', $poiCustomer);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $poiCustomer = $this->PoiCustomers->newEntity();
        if ($this->request->is('post')) {
            $poiCustomer = $this->PoiCustomers->patchEntity($poiCustomer, $this->request->getData());
            $poiCustomer->company_id = $this->Auth->user('company_id');
            if ($this->PoiCustomers->save($poiCustomer)) {
                $this->Flash->success(__('Un nouveau acteur a été ajouté avec succès.'));

                return $this->redirect(['controller' => 'parametres','action' => 'liaisons']);
            }
            $this->Flash->error(__('L\'acteur n\'a pas pu être ajouté, veuillez réessayez.'));
        }
        $list_types  = $this->PoiCustomers->CustomerTypes->find('list', ['conditions' => ['CustomerTypes.company_id' => $this->Auth->user('company_id')]]);

        $list_pois = $this->PoiCustomers->Pois->find()
        ->where(['Pois.company_id' => $this->Auth->user('company_id')])
        ->select(['id','name' => 'dns'])
        ->toArray();

        $this->set(compact('poiCustomer', 'list_types','list_pois'));
    }


    public function edit($id = null)
    {
        $poiCustomer = $this->PoiCustomers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $poiCustomer = $this->PoiCustomers->patchEntity($poiCustomer, $this->request->getData());
            if ($this->PoiCustomers->save($poiCustomer)) {
                $this->Flash->success(__("L'acteur a été modifié avec succès !"));

                return $this->redirect(['controller' => 'parametres','action' => 'liaisons']);
            }
            $this->Flash->error(__('L\'acteur n\'a pas pu être modifié, veuillez réessayez.'));
        }
        $list_types  = $this->PoiCustomers->CustomerTypes->find('list', ['conditions' => ['CustomerTypes.company_id' => $this->Auth->user('company_id')],'limit' => 200]);

        $list_pois = $this->PoiCustomers->Pois->find()
        ->where(['Pois.company_id' => $this->Auth->user('company_id')])
        ->select(['id','name' => 'dns'])
        ->toArray();

        $this->set(compact('poiCustomer', 'list_types','list_pois'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Poi Customer id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $poiCustomer = $this->PoiCustomers->get($id);
        if ($this->PoiCustomers->delete($poiCustomer)) {
            $this->Flash->success(__('The poi customer has been deleted.'));
        } else {
            $this->Flash->error(__('The poi customer could not be deleted. Please, try again.'));
        }

        return $this->redirect($this->referer());
    }
}