<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Members Controller
 *
 * @property \App\Model\Table\MembersTable $Members
 *
 * @method \App\Model\Entity\Member[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MembersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Teams'],
            'conditions' => ['Members.company_id' => $this->Auth->user('company_id')]
        ];
        $members = $this->paginate($this->Members);

        $this->set(compact('members'));
    }

    /**
     * View method
     *
     * @param string|null $id Member id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $member = $this->Members->get($id, [
            'contain' => ['Users', 'Teams']
        ]);

        $this->set('member', $member);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $member = $this->Members->newEntity();
        if ($this->request->is('post')) {
            $member = $this->Members->patchEntity($member, $this->request->getData());

            $getMember = $this->Members->find('all',['conditions' => ['Members.user_id' => $member->user_id, 'Members.team_id' => $member->team_id]])->first();

            if($getMember) {
                $this->Flash->error(__('Cet utilisateur est déjà présent dans cette équipe.'));
                return $this->redirect($this->referer());
            }
            $member->company_id = $this->Auth->user('company_id');
            if ($this->Members->save($member)) {
                $this->Flash->success(__('Un nouveau membre a été enregistré.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Le membre n\'a pas pu être enregistré, réessayez.'));
        }
        return $this->redirect($this->referer());
        $users = $this->Members->Users->find('list', ['limit' => 200]);
        $teams = $this->Members->Teams->find('list', ['limit' => 200]);
        $this->set(compact('member', 'users', 'teams'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Member id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $member = $this->Members->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $member = $this->Members->patchEntity($member, $this->request->getData());
            if ($this->Members->save($member)) {
                $this->Flash->success(__('The member has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The member could not be saved. Please, try again.'));
        }
        $users = $this->Members->Users->find('list', ['limit' => 200]);
        $teams = $this->Members->Teams->find('list', ['limit' => 200]);
        $this->set(compact('member', 'users', 'teams'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Member id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $member = $this->Members->get($id);
        if ($this->Members->delete($member)) {
            $this->Flash->success(__('The member has been deleted.'));
        } else {
            $this->Flash->error(__('The member could not be deleted. Please, try again.'));
        }

        return $this->redirect($this->referer());
    }
}
