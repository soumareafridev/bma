<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Frequences Controller
 *
 * @property \App\Model\Table\FrequencesTable $Frequences
 *
 * @method \App\Model\Entity\Frequence[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FrequencesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $frequences = $this->paginate($this->Frequences);

        $this->set(compact('frequences'));
    }

    /**
     * View method
     *
     * @param string|null $id Frequence id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $frequence = $this->Frequences->get($id, [
            'contain' => ['Orders']
        ]);

        $this->set('frequence', $frequence);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $frequence = $this->Frequences->newEntity();
        if ($this->request->is('post')) {
            $frequence = $this->Frequences->patchEntity($frequence, $this->request->getData());
            $frequence->company_id = $this->Auth->user('company_id');
            if ($this->Frequences->save($frequence)) {
                $this->Flash->success(__('La fréquence a été ajoutée avec succès'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('La fréquence n\'a pas pu être ajoutée, reessayez.'));
        }
        $this->set(compact('frequence'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Frequence id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $frequence = $this->Frequences->get($id, [
            'contain' => []
        ]);
        $frequence->company_id = $this->Auth->user('company_id');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $frequence = $this->Frequences->patchEntity($frequence, $this->request->getData());
            if ($this->Frequences->save($frequence)) {
                $this->Flash->success(__('La fréquence a été modifiée avec succès'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('La fréquence n\'a pas pu être modifiée, reessayez.'));
        }
        $this->set(compact('frequence'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Frequence id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $frequence = $this->Frequences->get($id);
        if ($this->Frequences->delete($frequence)) {
            $this->Flash->success(__('La fréquence a été supprimée avec succès.'));
        } else {
            $this->Flash->error(__('La fréquence n\'a pas pu être supprimée, reessayez'));
        }

        return $this->redirect($this->referer());
    }
}
