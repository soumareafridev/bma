<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * TypePois Controller
 *
 * @property \App\Model\Table\TypePoisTable $TypePois
 *
 * @method \App\Model\Entity\TypePois[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TypePoisController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $typePois = $this->paginate($this->TypePois);

        $this->set(compact('typePois'));
    }

    /**
     * View method
     *
     * @param string|null $id Type Pois id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $typePois = $this->TypePois->get($id, [
            'contain' => []
        ]);

        $this->set('typePois', $typePois);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $typePois = $this->TypePois->newEntity();
        if ($this->request->is('post')) {
            $typePois = $this->TypePois->patchEntity($typePois, $this->request->getData());
            $typePois->company_id = $this->Auth->user('company_id');
            if ($this->TypePois->save($typePois)) {
                $this->Flash->success(__('La type de POI a été ajouté avec succès.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('La type de POI n\'a pas pu être ajouté, reessayez.'));
        }
        $this->set(compact('typePois'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Type Pois id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $typePois = $this->TypePois->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $typePois = $this->TypePois->patchEntity($typePois, $this->request->getData());
            if ($this->TypePois->save($typePois)) {
                $this->Flash->success(__('The type pois has been saved.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The type pois could not be saved. Please, try again.'));
        }
        $this->set(compact('typePois'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Type Pois id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $typePois = $this->TypePois->get($id);
        if ($this->TypePois->delete($typePois)) {
            $this->Flash->success(__('The type pois has been deleted.'));
        } else {
            $this->Flash->error(__('La type de POI n\'a pas pu être suprimé, reessayez.'));
        }

        return $this->redirect($this->referer());
    }
}
