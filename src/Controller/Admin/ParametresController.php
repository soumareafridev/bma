<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

class ParametresController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Gammes');
        $this->loadModel('Groupes');
        $this->loadModel('Pois');
        $this->loadModel('OrderStatuts');
        $this->loadModel('Zones');
        $this->loadModel('Frequences');
        $this->loadModel('CustomerTypes');
    }

    public function index() {
        $this->loadModel('Themes');
        $themes = $this->Themes->find('all')->toArray();
        $this->set(compact('themes'));
    }

    public function bases() {
        $cards = [
            [
                'title' => "Les types de points d'intérêts (POIs)",
                'entity' => "POI",
                'btnModal' => "editStatic",
                'modal' => "staticModal",
                'placeholder' => "Nom type de POI",
                'action' => ['controller' => 'TypePois','action' => "add"],
                'listes' => $this->Pois->TypePois->find('all',['conditions' => ['TypePois.company_id' => $this->Auth->user('company_id')]])->toArray()
            ],
            [
                'title' => "Les gammes",
                'entity' => "Gamme",
                'btnModal' => "editGamme",
                'modal' => "gammeModal",
                'placeholder' => "Nom de la gamme",
                'action' => ['controller' => 'Gammes','action' => "add"],
                'listes' => $this->Gammes->find('all',['conditions' => ['Gammes.company_id' => $this->Auth->user('company_id')]])->toArray()
            ],
            // [
            //     'title' => "Les statuts des commandes",
            //     'entity' => "Statut",
            //     'btnModal' => "editStatic",
            //     'modal' => "staticModal",
            //     'placeholder' => "Nom du statut",
            //     'action' => ['controller' => 'OrderStatuts','action' => "add"],
            //     'listes' => $this->OrderStatuts->find('all')->toArray()
            // ],
            // [
            //     'title' => "Les Groupes de POIs",
            //     'entity' => "Groupe",
            //     'btnModal' => "editStatic",
            //     'modal' => "staticModal",
            //     'action' => ['controller' => 'Groupes','action' => "add"],
            //     'placeholder' => "Nom du groupe",
            //     'listes' => $this->Groupes->find('all',['conditions' => ['Groupes.company_id' => $this->Auth->user('company_id')]])->toArray()
            // ],
            [
                'title' => "Les fréquences des commandes",
                'entity' => 'Fréquence',
                'btnModal' => "editGamme",
                'modal' => "gammeModal",
                'action' => ['controller' => 'Frequences','action' => "add"],
                'placeholder' => "Nom de la fréquence",
                'listes' => $this->Frequences->find('all')->toArray()
            ],
        ];

        // $cards = json_decode(json_encode($cards));
        $this->set(compact('cards'));
    }

    public function liaisons() {
        $type_customers = $this->CustomerTypes->find('all',['conditions' => ['CustomerTypes.company_id' => $this->Auth->user('company_id')] ])->toArray();
        $customers = $this->CustomerTypes->PoiCustomers->find('all',
            [
                'contain' => ['Pois.TypePois','CustomerTypes'],
                'conditions' => ['PoiCustomers.company_id' => $this->Auth->user('company_id')]
            ]
        )->toArray();

        $this->set(compact("type_customers","customers"));
    }

    public function enquete() {
        $this->loadModel("Surveys");
        $this->loadModel("Teams");

        $cards = [
            [
                'title' => "Les Equipes",
                'entity' => "Equipe",
                'btnModal' => "editStatic",
                'modal' => "staticModal",
                'placeholder' => "Nom de l'equipe",
                'action' => ['controller' => 'Teams','action' => "add"],
                'listes' => $this->Teams->find('all',['contain' => ['Members'], 'conditions' => ['Teams.company_id' => $this->Auth->user('company_id')]])->toArray()
            ],
            [
                'title' => "Les Formulaires",
                'entity' => "Formulaire",
                'btnModal' => "editStatic",
                'modal' => "staticModal",
                'placeholder' => "Nom du formulaire",
                'action' => ['controller' => 'Forms','action' => "add"],
                'listes' => $this->Surveys->Forms->find('all',['conditions' => ['Forms.company_id' => $this->Auth->user('company_id')]])->toArray()
            ],
            // [
            //     'title' => "Les types de POIs",
            //     'action' => ['controller' => 'TypePois','action' => "add"],
            //     'placeholder' => "Nom du type de POIs",
            //     'listes' => $this->Surveys->TypePois->find('all',['conditions' => ['TypePois.company_id' => $this->Auth->user('company_id')]])->toArray()
            // ]
        ];

        // $cards = json_decode(json_encode($cards));
        $this->set(compact('cards'));
    }
}
