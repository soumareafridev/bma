<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Users');
    }

    public function login()
    {
        $query = $this->request->getQuery();

        if(isset($query['redirect'])) {
            $redirect = $query['redirect'];
            $this->set(compact('redirect'));
        }

        if ($this->request->is('post')) {
            $user = null;
            $authorize = false;
            $redirect = $this->request->getData('redirect');
            $temp = $this->request->getData();

            // if($this->checkEmail(trim($temp['phone']))) $getUserByInfo = $this->Users->find('All',['conditions' => ['Users.email' => trim($temp['phone'])],'contain' => ['Roles']])->first();
            if(filter_var(trim($temp['phone']),FILTER_VALIDATE_EMAIL)) $getUserByInfo = $this->Users->find('All',['conditions' => ['Users.email' => trim($temp['phone'])],'contain' => ['Roles']])->first();
            else $getUserByInfo = $this->Users->find('All',['conditions' => ['Users.phone' => trim($temp['phone'])], 'contain' => ['Roles']])->first();
            if($getUserByInfo) {
                if(sha1($temp['password']) === $getUserByInfo->password) $user= $getUserByInfo;
            }

            if ($user) {
                if(sizeof($user->roles) > 0) {
                    foreach($user->roles as $role) {
                        if($role->profile_id == 3) $authorize = true;
                    }
                } else $authorize = false;

                if($authorize) {
                    $this->Auth->setUser($user);
                    if(isset($redirect)) return $this->redirect($redirect);
                    return $this->redirect(['controller' => "pages",'action' => 'display']);
                } else $this->Flash->error(__('Vous devez posseder un compte administrateur pour accèder à cet espace !'));
            } else $this->Flash->error(__('Mot de passe ou email incorrect, réessayez !'));
        }
    }

    public function add(){
        $this->loadModel('Profiles');
        $user = $this->Users->newEntity();
        //$member = $this->Users->Members->newEntity();
        $role = $this->Users->Roles->newEntity();

        $user->company_id = $this->Auth->user('company_id');
        //$member->company_id = $this->Auth->user('company_id');

        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->active = 1;
            $email = $this->request->getData('email');
            if($email == "") $user->email = null;
            $profile_id = $this->request->getData('profile');
            //$member->team_id = 1;
            if($this->Users->save($user)){
                //$member->user_id = $user->id;
                $role->user_id = $user->id;
                $role->profile_id = $profile_id;
                $this->Users->Roles->save($role);
                //$this->Users->Members->save($member);
                $this->Flash->success(__('Utilisateur ajouté avec succès !'));
                if($profile_id == 1) return $this->redirect(['controller' => "Users",'action' => 'enqueteurs']);
                if($profile_id == 2) return $this->redirect(['controller' => "Users",'action' => 'commercials']);
                if($profile_id == 5) return $this->redirect(['controller' => "Users",'action' => 'distributeurs']);
                if($profile_id == 3) return $this->redirect(['controller' => "Users",'action' => 'responsableCommercials']);
                if($profile_id == 4) return $this->redirect(['controller' => "Users",'action' => 'administrateurs']);
            } else $this->Flash->error(__('La création d\'un nouveau utilisateur a échouée !'));
        }
        $profiles = $this->Profiles->find('list', ['conditions' => ['Profiles.id <>' => 6]])->toArray();
        $this->set(compact('user','profiles'));
    }

    public function edit($id=null){
        $this->loadModel('Profiles');
        $user = $this->Users->get($id,['contain' => ['Roles.Profiles']]);
        $userprofiles = [];
        if(count($user->roles) > 0) {
            foreach ($user->roles as $value) {
                $userprofiles[] = $value->profile_id;
            }
        }
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $email = $this->request->getData('email');
            if($email == "") $user->email = null;

            if($this->Users->save($user)){
                $this->Flash->success(__('Utilisateur modifié avec succès !'));
                if($userprofiles[0] == 1) return $this->redirect(['controller' => "Users",'action' => 'enqueteurs']);
                if($userprofiles[0] == 2) return $this->redirect(['controller' => "Users",'action' => 'commercials']);
                if($userprofiles[0] == 5) return $this->redirect(['controller' => "Users",'action' => 'distributeurs']);
                if($userprofiles[0] == 3) return $this->redirect(['controller' => "Users",'action' => 'responsableCommercials']);
                if($userprofiles[0] == 4) return $this->redirect(['controller' => "Users",'action' => 'administrateurs']);
                return $this->redirect(['controller' => "Users",'action' => 'administrateurs']);
            } else $this->Flash->error(__('La modification d\'un nouveau utilisateur a échouée !'));
        }
        if(count($userprofiles) > 0) $profiles = $this->Profiles->find('list',['conditions' => ['Profiles.id NOT IN' => $userprofiles,'Profiles.id <>' => 6]])->toArray();
        else $profiles = $this->Profiles->find('list', ['conditions' => ['Profiles.id <>' => 6]])->toArray();
        $this->set(compact('user','profiles'));
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    // public function index () {
    //     $users = $this->Users->find('all',['contain' => ['Roles.Profiles']])->toArray();
    //     $this->set(compact('users'));
    // }

    public function consulter ($id = null) {
        $user = $this->Users->get($id,['contain' => ['Roles.Profiles']]);
        if($user && $id !== null) {
            $pois = $this->Users->Pois->find('all',[
                'conditions' => ['Pois.user_id' => $user->id],
                'contain' => [
                    'Surveys.Forms.Fields.TypeFields',
                    'Infos.Fields.TypeFields',
                    'Users',
                    'Zones',
                    'TypePois'
                ],
                'order' => ['Pois.id' => "desc"]
            ])->toArray();
        } else return $this->redirect($this->referer());
        $this->set(compact('user','pois'));
    }

    protected function getUsers($profile_id) {
        $roles = $this->Users->Roles->find('all',[
            'conditions' =>
                ['Roles.profile_id' => $profile_id],
                'contain' => ['Profiles','Users' => [
                    'conditions' => ['Users.company_id' => $this->Auth->user('company_id')]]
                    ]])->toArray();
        return $roles;
    }


    public function enqueteurs() {
        $roles = $this->getUsers(1);
        $this->set(compact('roles'));
    }

    public function commercials() {
        $roles = $this->getUsers(2);
        $this->set(compact('roles'));
    }

    public function distributeurs() {
        $roles = $this->getUsers(5);
        $this->set(compact('roles'));
    }

    public function responsableCommercials() {
        $roles = $this->getUsers(3);
        $this->set(compact('roles'));
    }

    public function administrateurs() {
        $roles = $this->getUsers(4);
        $this->set(compact('roles'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__("L'utilisateur a été supprimé avec succès !"));
        } else {
            $this->Flash->error(__("L'utilisateur n'a pas pu être supprimé, merci de reéssayer !"));
        }

        return $this->redirect(['action' => 'index']);
    }
}
