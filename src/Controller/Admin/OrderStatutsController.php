<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * OrderStatuts Controller
 *
 * @property \App\Model\Table\OrderStatutsTable $OrderStatuts
 *
 * @method \App\Model\Entity\OrderStatut[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrderStatutsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $orderStatuts = $this->paginate($this->OrderStatuts);

        $this->set(compact('orderStatuts'));
    }

    /**
     * View method
     *
     * @param string|null $id Order Statut id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $orderStatut = $this->OrderStatuts->get($id, [
            'contain' => ['Orders']
        ]);

        $this->set('orderStatut', $orderStatut);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $orderStatut = $this->OrderStatuts->newEntity();
        if ($this->request->is('post')) {
            $orderStatut = $this->OrderStatuts->patchEntity($orderStatut, $this->request->getData());
            //$orderStatut->company_id = $this->Auth->user('company_id');
            if ($this->OrderStatuts->save($orderStatut)) {
                $this->Flash->success(__('Le statut a été ajouté avec succès !.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Le statut n\'a pas pu être ajouté, reessayez !'));
        }
        $this->set(compact('orderStatut'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Order Statut id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $orderStatut = $this->OrderStatuts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $orderStatut = $this->OrderStatuts->patchEntity($orderStatut, $this->request->getData());
            if ($this->OrderStatuts->save($orderStatut)) {
                $this->Flash->success(__('Le statut a été modifié avec succès !'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Le statut n\'a pas pu être modifié, reessayez !'));
        }
        $this->set(compact('orderStatut'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Order Statut id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $orderStatut = $this->OrderStatuts->get($id);
        if ($this->OrderStatuts->delete($orderStatut)) {
            $this->Flash->success(__('Le statut a été supprimé avec succès !'));
        } else {
            $this->Flash->error(__('Le statut n\'a pas pu être supprimé, reessayez !'));
        }

        return $this->redirect($this->referer());
    }
}
