<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Companies Controller
 *
 * @property \App\Model\Table\CompaniesTable $Companies
 *
 * @method \App\Model\Entity\Company[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CompaniesController extends AppController
{
    /**
     * View method
     *
     * @param string|null $id Company id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $company = $this->Companies->get($id, [
            'contain' => ['Users']
        ]);
        
        $this->set('company', $company);
    }

    public function get($id = null)
    {
        $company = $this->Companies->get($id, [
            'contain' => ['Users']
        ]);
        if(!$company) return false;
        return $company;
    }

    public function edit($id = null)
    {
        $company = $this->Companies->get($id, [
            'contain' => []
        ]);


        if ($this->request->is(['patch', 'post', 'put'])) {
            $company = $this->Companies->patchEntity($company, $this->request->getData());
            if ($this->Companies->save($company)) {
                $fichier = $this->request->getData('image');
                if($fichier !== null && $fichier['error'] <= 0 && $fichier['size'] > 0) {
                    $name = "logo$company->id";
                    $infos = $this->updloadLogo($fichier,$name);

                    if(count($infos['errors']) > 0) {
                        $this->Flash->error(__($this->returnErrorMessage($infos['errors'])),['escape' => false]);
                    } 
                    else {
                        if($company->logo) {
                            $this->removeOldLogo($company);
                        }
                        $company->logo = '/img/companies/'.$infos['filename'];
                        $this->Companies->save($company);
                    }
                }
                $this->Flash->success(__('Les informations ont été enregistrées avec succès.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Les informations n\'ont pas pu être enregistrées.'));
        }
        return $this->redirect($this->referer());
    }

    private function getLogoPath($url) {
        $paths = \explode('/',$url);
        $filenmane = $paths[count($paths) - 1];

        return WWW_ROOT . DS . "img" . DS . "companies" . DS. $filenmane;
    }

    private function removeOldLogo($company = null) {
        if($company->logo) {
            $path = $this->getLogoPath($company->logo);
            if(file_exists($path) && \unlink($path)) {
                $company->logo = null;
                return $this->Companies->save($company);
            }
        }
        return false;
    }

    public function deletelogo($id = null) {
        $company = $this->Companies->get($id);
       if($this->removeOldLogo($company)) {
           $this->Flash->success(__('Logo supprimé avec succès'));
       } else $this->Flash->error(__('Logo n\'a pas pu être supprimé.'));

       return $this->redirect($this->referer());
    }

    private function returnErrorMessage($errors = []) {
        $message =  '<h5>image de logo Non-sauvegardée  - Erreurs suivantes : </h5><ul class="list-group">';
        foreach($errors as $error) {
                $message .= "<li class='list-group-item-danger'>$error</li>";
            }
            $message .='</ul>';

            return $message;
    }

    /**
     * Delete method
     *
     * @param string|null $id Company id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $company = $this->Companies->get($id);
        if ($this->Companies->delete($company)) {
            $this->Flash->success(__('The company has been deleted.'));
        } else {
            $this->Flash->error(__('The company could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
