<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Fields Controller
 *
 * @property \App\Model\Table\FieldsTable $Fields
 *
 * @method \App\Model\Entity\Field[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FieldsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['TypeFields', 'Forms', 'Values']
        ];
        $fields = $this->paginate($this->Fields);

        $this->set(compact('fields'));
    }

    /**
     * View method
     *
     * @param string|null $id Field id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $field = $this->Fields->get($id, [
            'contain' => ['TypeFields', 'Forms', 'Values', 'Infos']
        ]);

        $this->set('field', $field);
    }


    public function add()
    {
        $field = $this->Fields->newEntity();
        if ($this->request->is('post')) {
            $field = $this->Fields->patchEntity($field, $this->request->getData());
            
            if ($this->Fields->save($field)) {
                $this->Flash->success(__('Le champ a été enregistré avec succès !'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Le champ n\'a pas pu etre ajouté, reesayez.'));
        }
        return $this->redirect($this->referer());
        $typeFields = $this->Fields->TypeFields->find('list', ['limit' => 200]);
        $forms = $this->Fields->Forms->find('list', ['conditions' => ['Forms.company_id' => $this->Auth->user('company_id')],'limit' => 200]);
        $values = $this->Fields->Values->find('list', ['limit' => 200]);
        $this->set(compact('field', 'typeFields', 'forms', 'values'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Field id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $field = $this->Fields->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $field = $this->Fields->patchEntity($field, $this->request->getData());
            if ($this->Fields->save($field)) {
                $this->Flash->success(__('Le champ a été enregistré avec succès !'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Le champ n\'a pas pu etre modifié, reesayez.'));
        }
        $typeFields = $this->Fields->TypeFields->find('list', ['limit' => 200]);
        $forms = $this->Fields->Forms->find('list', ['conditions' => ['Forms.company_id' => $this->Auth->user('company_id')],'limit' => 200]);
        $values = $this->Fields->Values->find('list', ['limit' => 200]);
        $this->set(compact('field', 'typeFields', 'forms', 'values'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Field id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $field = $this->Fields->get($id);
        if ($this->Fields->delete($field)) {
            $this->Flash->success(__('Le champ a été supprimé avec succès .'));
        } else {
            $this->Flash->error(__('Le champ n\'a pas pu etre modifié, reesayez.'));
        }

        return $this->redirect($this->referer());
    }
}
