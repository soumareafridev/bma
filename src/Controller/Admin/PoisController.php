<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\I18n\Time;

class PoisController extends AppController
{
    public function cartographie()
    {
        $pois = $this->Pois->find('all', [
            'conditions' => ['Pois.company_id' => $this->Auth->user('company_id')],
            'contain' => [
                'Surveys.Forms.Fields.TypeFields',
                'Infos.Fields.TypeFields',
                'Users',
                'Zones',
                'TypePois'
            ],
            'order' => ['Pois.id' => "desc"]
        ])->toArray();

        $datas = $pois;
        $zones = $this->Pois->Zones->find('all')->toArray();
        $groupes = $this->Pois->Groupes->find('all')->toArray();
        $this->set(compact('pois', 'zones', 'datas', 'groupes'));
    }

    public function clientsIndependants()
    {
        $pois = $this->getPOI(2);
        $titre = "Clients indépendants";
        $this->set(compact('pois', 'titre'));
    }
    public function pointsDeVente()
    {
        $pois = $this->getPOI(1);
        $titre = "Points de vente";
        $this->set(compact('pois', 'titre'));
    }

    public function poisEnregistres($id = null)
    {
        if (!isset($id)) $id = $this->request->getQuery('i');

        // dd($id);

        $type = $this->getPOIByType($id);
        $pois =  $type->pois;
        $this->set(compact('pois', 'type'));
    }



    public function view($id = null)
    {
        $poi = $this->Pois->get($id, [
            'contain' => [
                'Surveys.Forms.Fields.TypeFields',
                'Infos.Fields.TypeFields',
                'Users',
                'TypePois'
            ]
        ]);

        $infos = $poi->infos;
        $types = $this->Pois->TypePois->find('list', ['conditions' => ['TypePois.company_id' => $this->Auth->user('company_id')]])->toArray();

        $this->set(compact('poi', 'infos', 'types'));
    }

    public function edit($id = null)
    {
        $this->loadModel("Infos");
        $poi = $this->Pois->get($id, [
            'contain' => [
                'Surveys.Forms.Fields.TypeFields',
                'Infos.Fields.TypeFields',
                'Users'
            ]
        ]);

        if ($this->request->is('post')) {
            $temp = $this->request->getData();
            $nb = sizeof($temp['infos']);
            $isItSave = 0;
            $poi = $this->Pois->patchEntity($poi, $temp['poi']);
            if ($this->Pois->save($poi)) {
                foreach ($temp['infos'] as $key => $value) {
                    $info = $this->Infos->get($key);
                    $info->value = $value;
                    if ($this->Infos->save($info)) $isItSave++;
                }
                if ($nb == $isItSave) $this->Flash->success(__('Les informations du POI ont étés modifiées avec succès'));
                else $this->Flash->error(__('Toutes les informations du POI n\'ont pas pu être modifiées, reessayez'));
            } else $this->Flash->error(__('Toutes les informations du POI n\ont pas pu être modifiées, reessayez'));

            return $this->redirect($this->referer());
        }


        $this->set(compact('poi'));
    }

    public function add()
    {
        $this->loadModel('Gestions');
        $survey = null;

        $survey_id = $this->request->getQuery('enquete');

        $gestions = $this->Gestions->find('all', [
            'conditions' => [
                'Gestions.company_id' => $this->Auth->user('company_id')
            ],
            'contain' => [
                'Teams',
                'Surveys.Forms' => [
                    'Fields.TypeFields'
                ]
            ]
        ])->toArray();

        if(isset($survey_id) && $survey_id != "") {
            $survey = $this->Gestions->Surveys->get($survey_id, ['contain' => ['Forms.Fields.TypeFields'], 'conditions' => ['Surveys.company_id' => $this->Auth->user('company_id')]]);
        }



        if ($survey && $this->request->is('post')) {
            $temp = $this->request->getData();
            $poi = $this->Pois->newEntity();
            $poi = $this->Pois->patchEntity($poi, $temp);
            $poi->created = Time::now();
            $poi->status = 1;
            $poi->groupe_id = $survey->groupe_id;
            $poi->company_id = $this->Auth->user('company_id');
            $poi->survey_id = $survey->id;
            $poi->user_id = $this->Auth->user('id');

            $form = $survey->form;

            if($this->Pois->save($poi)) {
                $this->loadModel('Infos');
                if($form) {
                    if(isset($form) && count($form->fields) > 0) {
                        foreach($form->fields as $field) {
                            $info = $this->Infos->newEntity();
                            $info->poi_id = $poi->id;
                            $info->field_id = $field->id;
                            $info->value = $temp['info' . $field->id];
                            $this->Infos->save($info);
                        }
                    }
                }
                if (isset($temp['image']) && $temp['image'] !== null) {
                    $this->savePoiImg($poi->id);
                }

                $this->Flash->success(__('Votre Point d\'intérêt  a été enregistré avec succès !'));
                return $this->redirect(['action' => 'pois-enregistres', '?' => $this->hashUrl($poi->type_poi_id)]);

            } else $this->Flash->error($this->msgErrors($this->returnErrors($poi->getErrors())));
        }

        $this->set(compact('gestions', 'survey', 'survey_id'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pois = $this->Pois->get($id);
        if ($this->Pois->delete($pois)) {
            $this->Flash->success(__('The pois has been deleted.'));
        } else {
            $this->Flash->error(__('The pois could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
