<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Roles Controller
 *
 * @property \App\Model\Table\RolesTable $Roles
 *
 * @method \App\Model\Entity\Role[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RolesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Profiles']
        ];
        $roles = $this->paginate($this->Roles);

        $this->set(compact('roles'));
    }

    /**
     * View method
     *
     * @param string|null $id Role id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $role = $this->Roles->get($id, [
            'contain' => ['Users', 'Profiles']
        ]);

        $this->set('role', $role);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $role = $this->Roles->newEntity();
        if ($this->request->is('post')) {
            $role = $this->Roles->patchEntity($role, $this->request->getData());

            if ($this->Roles->save($role)) {
                $this->Flash->success(__('Le role a été sauvegardé avec succès.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Le role n\'a pas pu être enregistré, reesayez.'));
        }
        $users = $this->Roles->Users->find('list', ['limit' => 200]);
        $profiles = $this->Roles->Profiles->find('list', ['limit' => 200]);
        $this->set(compact('role', 'users', 'profiles'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Role id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $role = $this->Roles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $role = $this->Roles->patchEntity($role, $this->request->getData());
            if ($this->Roles->save($role)) {
                $this->Flash->success(__('The role has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The role could not be saved. Please, try again.'));
        }
        $users = $this->Roles->Users->find('list', ['limit' => 200]);
        $profiles = $this->Roles->Profiles->find('list', ['limit' => 200]);
        $this->set(compact('role', 'users', 'profiles'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Role id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $role = $this->Roles->get($id);
        if($role->profile_id == 4) {
            $this->Flash->error(__('Vous ne pouvez pas retiré le role d\'administrateur.'));
            return $this->redirect($this->referer());
        }
        if ($this->Roles->delete($role)) {
            $this->Flash->success(__('Le role a été retiré avec succès.'));
        } else {
            $this->Flash->error(__("Le role n'a pas pu être retiré, reessayez."));
        }

        return $this->redirect($this->referer());
    }
}
