<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Forms Controller
 *
 * @property \App\Model\Table\FormsTable $Forms
 *
 * @method \App\Model\Entity\Form[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FormsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $paginate = ['conditions' => ['Forms.company_id' => $this->Auth->user('company_id')]];

        $forms = $this->Forms->find('all',$paginate)->toArray();

        $this->set(compact('forms'));
    }

    /**
     * View method
     *
     * @param string|null $id Form id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $form = $this->Forms->get($id, [
            'contain' => ['Fields', 'Surveys']
        ]);

        $this->set('form', $form);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $form = $this->Forms->newEntity();
        if ($this->request->is('post')) {
            $form = $this->Forms->patchEntity($form, $this->request->getData());
            $form->status = 1;
            $form->company_id = $this->Auth->user('company_id');
            if ($this->Forms->save($form)) {
                $this->Flash->success(__("Le formulaire a été ajouté avec succès !"));

            return $this->redirect($this->referer());
            }
            $this->Flash->error(__("Le formulaire n'a pas pu être ajouté, reessayez !"));
        }
        $this->set(compact('form'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Form id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $form = $this->Forms->get($id, [
            'contain' => ['Fields' => function($q) {
                return $q->contain(['TypeFields','Forms'])->order(['Fields.rang' => 'ASC']);
            }]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $form = $this->Forms->patchEntity($form, $this->request->getData());
            if ($this->Forms->save($form)) {
                $this->Flash->success(__('Le formulaire a été modifié avec succès.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Le formulaire n\'a pas pu etre modifié, reessayez.'));
        }
        $fields = $form->fields;
        $types = $this->Forms->Fields->TypeFields->find('list')->toArray();
        $this->set(compact('form','fields','types'));
    }

    public function ranger($id = null) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $fields = $this->Forms->Fields->find("all", [
                'conditions' => ['Fields.form_id' => $id]
            ])->toArray();
            $temp = $this->request->getData();

            if(count($fields) > 0) {
                foreach($fields as $champ) {
                    $rang = (int) $temp['champs'.$champ->id];

                    $champ->rang = $rang;
                    $this->Forms->Fields->save($champ);
                }
            $this->Flash->success(__('Les champs ont été réorganisés avec succès.'));
            }
        }

        return $this->redirect($this->referer());
    }

    /**
     * Delete method
     *
     * @param string|null $id Form id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $form = $this->Forms->get($id);
        if ($this->Forms->delete($form)) {
            $this->Flash->success(__("Le formulaire a été supprimé avec succès !"));
        } else {
            $this->Flash->error(__("Le formulaire n'a pas pu être supprimé, reessayez !"));
        }

        return $this->redirect($this->referer());
    }
}
