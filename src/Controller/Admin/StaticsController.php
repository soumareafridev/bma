<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use App\Controller\TypePoisController;

class StaticsController extends AppController
{
    public function sidebar()
    {
        $this->loadModel('TypePois');
        $this->loadModel('DistributionStatuts');
        $this->loadModel('Companies');

        $statut_distributions = $this->DistributionStatuts->find('all')->toArray();
        $typePois = new TypePoisController();
        $type_pois = $typePois->getList();

        $company = $this->Companies->get($this->Auth->user('company_id'));

        $access = [];

        if (isset($company->access) && $company->access != "") $access = explode(';', $company->access);

        $sidebar = [
            "principals" =>
            [
                [
                    "name" => "Dashboard",
                    "fa" => "fa-tachometer-alt",
                    "controller" => "Dashboard",
                    "action" => "index"
                ],
                [
                    "name" => "Cartographie",
                    "fa" => "fa-map-marked-alt",
                    "controller" => "Pois",
                    "action" => "cartographie"
                ],
            ],
            "secondaires" =>
            [
                [
                    "name" => "POIs",
                    "fa" => "fa-map-pin",
                    "header" => "Les types de POI",
                    "refs" => ['pois'],
                    "items" => []
                ]
            ]
        ];

        if (count($type_pois) > 0) {
            $item = [
                "name" => "Ajoutez un POI",
                "controller" => "Pois",
                "action" => "add"
            ];

            $sidebar["secondaires"][0]["items"][] = $item;

            foreach ($type_pois as $type) {
                $item = [
                    "name" => $type->name,
                    "controller" => "Pois",
                    "action" => "pois-enregistres",
                    "params" => $this->hashUrl($type->id)
                ];

                $sidebar["secondaires"][0]["items"][] = $item;
            }
        } else {
            $item = [
                "name" => "Créer un type de POI",
                "controller" => "Parametres",
                "action" => "bases"
            ];

            $sidebar["secondaires"][0]["items"][] = $item;
        }

        if (in_array(2, $access)) {
            $sidebar['secondaires'][] =
                [
                    "name" => "Commandes",
                    "fa" => "fa-shopping-cart",
                    "header" => "Gestion commandes",
                    "refs" => ['orders'],
                    "items" => [
                        [
                            "name" => "Liste commandes",
                            "controller" => "Orders",
                            "action" => "index"
                        ],
                        [
                            "name" => "Nouvelle commande",
                            "controller" => "Orders",
                            "action" => "add"
                        ]
                    ]
                ];
            $sidebar['secondaires'][] =
                [
                    "name" => "Produits",
                    "fa" => "fa-shopping-cart",
                    "header" => "Gestion produits",
                    "refs" => ['products'],
                    "items" => [
                        [
                            "name" => "Liste produits",
                            "controller" => "Products",
                            "action" => "index"
                        ],
                        [
                            "name" => "Nouveau produit",
                            "controller" => "Products",
                            "action" => "add"
                        ]
                    ]
                ];

            $sidebar['secondaires'][] = [
                "name" => "Distributions",
                "fa" => "fa-share-alt",
                "header" => "Gestion distributions",
                "refs" => ['distributions'],
                "items" => [
                    [
                        "name" => "Dashboard",
                        "controller" => "Distributions",
                        "action" => "index"
                    ]
                ]
            ];
            $sidebar['secondaires'][] =    [
                "name" => "Contacts",
                "fa" => "fa-user-friends",
                "controller" => "Parametres",
                "action" => "liaisons",
                "refs" => ['parametres']
            ];

            // array_merge($sidebar['secondaires'], $tableau_commande);

            if (count($statut_distributions) > 0) {
                foreach ($statut_distributions as $statut) {
                    $item = [
                        "name" => $statut->name,
                        "controller" => "Distributions",
                        "action" => "visualiser",
                        "params" => $this->hashUrl($statut->id)
                    ];

                    $sidebar["secondaires"][3]["items"][] = $item;
                }
            }
        }

        if(in_array(1, $access) || in_array(2, $access) ) {
            $sidebar['secondaires'][] =    [
                "name" => "Enquêtes",
                "fa" => "fa-file-archive",
                "controller" => "Gestions",
                "action" => "index",
                "refs" => ['gestions']
            ];
        }

        $sidebar['secondaires'][] =
            [
                "name" => "Utilisateurs",
                "fa" => "fa-users",
                "header" => "Les types d'utilisateurs",
                "refs" => ['users'],
                "items" => [
                    [
                        "name" => "Les Enquêteurs",
                        "controller" => "Users",
                        "action" => "enqueteurs"
                    ],
                    [
                        "name" => "Les Commerciaux",
                        "controller" => "Users",
                        "action" => "commercials"
                    ],
                    [
                        "name" => "Les Responsables Commerciaux",
                        "controller" => "Users",
                        "action" => "responsable-commercials"
                    ],
                    [
                        "name" => "Les Administrateurs",
                        "controller" => "Users",
                        "action" => "administrateurs"
                    ],
                    [
                        "name" => "Les Distributeurs",
                        "controller" => "Users",
                        "action" => "distributeurs"
                    ]
                ]
            ];
        $sidebar['secondaires'][] = [
            "name" => "Paramètres",
            "fa" => "fa-cogs",
            "header" => "Les options",
            "refs" => ['parametres', 'forms', 'surveys', 'teams'],
            "items" => [
                [
                    "name" => "Générales",
                    "controller" => "Parametres",
                    "action" => "index"
                ],
                [
                    "name" => "Bases",
                    "controller" => "Parametres",
                    "action" => "bases"
                ],
                [
                    "name" => "Enquêtes",
                    "controller" => "Parametres",
                    "action" => "enquete"
                ]
            ]
        ];
        $sidebar['secondaires'][] = [
            "name" => "Exports",
            "fa" => "fa-download",
            "header" => "Listes",
            "refs" => ['exports'],
            "items" => [
                [
                    "name" => "Les POIs",
                    "controller" => "exports",
                    "action" => "pois"
                ],
                [
                    "name" => "Les Utilisateurs",
                    "controller" => "exports",
                    "action" => "users"
                ]
            ]
        ];

        // array_merge($sidebar['secondaires'], $tableau_parametres);

        return $sidebar;
    }

    public function hashUrl($id)
    {
        $params = [
            'sh' => $this->token(100),
            'i' => $id,
            'hs' => $this->token(50)
        ];

        return $params;
    }
}
