<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Statuts Controller
 *
 * @property \App\Model\Table\StatutsTable $Statuts
 *
 * @method \App\Model\Entity\Statut[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StatutsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $statuts = $this->paginate($this->Statuts);

        $this->set(compact('statuts'));
    }

    /**
     * View method
     *
     * @param string|null $id Statut id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $statut = $this->Statuts->get($id, [
            'contain' => []
        ]);

        $this->set('statut', $statut);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $statut = $this->Statuts->newEntity();
        if ($this->request->is('post')) {
            $statut = $this->Statuts->patchEntity($statut, $this->request->getData());
            $statut->company_id = $this->Auth->user('company_id');
            if ($this->Statuts->save($statut)) {
                $this->Flash->success(__('Le statut a été ajouté avec succès !.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Le statut n\'a pas pu être ajouté, reessayez !'));
        }
        $this->set(compact('statut'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Statut id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $statut = $this->Statuts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $statut = $this->Statuts->patchEntity($statut, $this->request->getData());
            if ($this->Statuts->save($statut)) {
                $this->Flash->success(__('Le statut a été modifié avec succès !'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Le statut n\'a pas pu être modifié, reessayez !'));
        }
        $this->set(compact('statut'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Statut id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $statut = $this->Statuts->get($id);
        if ($this->Statuts->delete($statut)) {
            $this->Flash->success(__('Le statut a été supprimé avec succès !'));
        } else {
            $this->Flash->error(__('Le statut n\'a pas pu être supprimé, reessayez !'));
        }

        return $this->redirect($this->referer());
    }
}
