<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Gestions Controller
 *
 * @property \App\Model\Table\GestionsTable $Gestions
 *
 * @method \App\Model\Entity\Gestion[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GestionsController extends AppController
{
    public function index()
    {
        $gestions = $this->Gestions->find('all',[
            'contain' => ['Surveys' => ['TypePois','Forms'], 'Teams'],
            'conditions' => ['Gestions.company_id' => $this->Auth->user('company_id')]
        ])->toArray();

        $surveys = $this->Gestions->Surveys->find('list',['conditions' => ['Surveys.company_id' => $this->Auth->user('company_id')]])->toArray();
        $teams = $this->Gestions->Teams->find('list',['conditions' => ['Teams.company_id' => $this->Auth->user('company_id')]])->toArray();

        $this->loadModel('TypePois');
        $type_pois = $this->TypePois->find('list',['conditions' => ['TypePois.company_id' => $this->Auth->user('company_id')]])->toArray();

        $cards = [
            [
                'title' => "Les enquêtes créées",
                'entity' => "Enquête",
                'btnModal' => "editStatic",
                'modal' => "staticModal",
                'placeholder' => "Nom de l'enquête",
                'type_pois' => $type_pois,
                'forms' => $this->Gestions->Surveys->Forms->find('list',['conditions' => ['Forms.company_id' => $this->Auth->user('company_id')]])->toArray(),
                'action' => ['controller' => 'Surveys','action' => "add"],
                'listes' => $this->Gestions->Surveys->find('all',['conditions' => ['Surveys.company_id' => $this->Auth->user('company_id')],'contain' => ['Forms','TypePois']])->toArray()
            ]
        ];

        $this->set(compact('gestions','surveys','teams','cards'));
    }


    public function consulter($id = null)
    {
        $gestion = $this->Gestions->get($id, [
            'contain' => ['Surveys' => ['TypePois','Forms.Fields' => ['TypeFields','Forms']], 'Teams.Members.Users']
        ]);

        $team = $gestion->team;
        $survey = $gestion->survey;
        $members = $team->members;
        $fields = $survey->form->fields;
        $this->set(compact('gestion','team','survey','members','fields'));
    }

    public function add()
    {
        $gestion = $this->Gestions->newEntity();
        if ($this->request->is('post')) {
            $gestion = $this->Gestions->patchEntity($gestion, $this->request->getData());
            $gestion->company_id = $this->Auth->user('company_id');
            $alreadyExist = $this->Gestions->find('all', [
                'conditions' => ['and' =>['Gestions.survey_id' => $gestion->survey_id,'Gestions.team_id' => $gestion->team_id]]
            ])->first();

            if($alreadyExist) {
                $this->Flash->error(__('Cette liaison existe déjà, merci de choisir une équipe ou enquête différente.'));
                return $this->redirect($this->referer());
            }

            if ($this->Gestions->save($gestion)) {
                $this->Flash->success(__('L\'enquête a été démarrée avec succès !'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('L\'enquête n\'a pas pu être démarrée, reessayez !'));
        }
        return $this->redirect($this->referer());
        $surveys = $this->Gestions->Surveys->find('list', ['limit' => 200]);
        $teams = $this->Gestions->Teams->find('list', ['limit' => 200]);
        $this->set(compact('gestion', 'surveys', 'teams'));
    }


    public function edit($id = null)
    {
        $gestion = $this->Gestions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gestion = $this->Gestions->patchEntity($gestion, $this->request->getData());
            if ($this->Gestions->save($gestion)) {
                $this->Flash->success(__('The gestion has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gestion could not be saved. Please, try again.'));
        }
        $surveys = $this->Gestions->Surveys->find('list', ['limit' => 200]);
        $teams = $this->Gestions->Teams->find('list', ['limit' => 200]);
        $this->set(compact('gestion', 'surveys', 'teams'));
    }


    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $gestion = $this->Gestions->get($id);
        if ($this->Gestions->delete($gestion)) {
            $this->Flash->success(__('Supression effectuée avec succès !'));
        } else {
            $this->Flash->error(__('La supression n\a pas pu aboutir, reesayez !'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
