<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;


class ExportsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Pois');
    }

    public function pois()
    {
        $pois = $this->Pois->find('all',[
            'conditions' => ['Pois.company_id' => $this->Auth->user('company_id')],
            'contain' => [
                'Surveys.Forms.Fields.TypeFields',
                'Infos.Fields.TypeFields',
                'Users',
                'Zones',
                'TypePois'
            ],
            'order' => ['Pois.id' => "desc"]
        ])->toArray();

        $this->set(compact('pois'));
    }


    public function users($id = null)
    {
        $users = $this->Pois->Users->find('all',['conditions' => ['Users.company_id' => $this->Auth->user('company_id')],'contain' => ['Roles.Profiles']])->toArray();
        $this->set(compact('users'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $form = $this->Forms->newEntity();
        if ($this->request->is('post')) {
            $form = $this->Forms->patchEntity($form, $this->request->getData());
            $form->status = 1;
            $form->company_id = $this->Auth->user('company_id');
            if ($this->Forms->save($form)) {
                $this->Flash->success(__("Le formulaire a été ajouté avec succès !"));

            return $this->redirect($this->referer());
            }
            $this->Flash->error(__("Le formulaire n'a pas pu être ajouté, reessayez !"));
        }
        $this->set(compact('form'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Form id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $form = $this->Forms->get($id, [
            'contain' => ['Fields' => ['TypeFields','Forms']]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $form = $this->Forms->patchEntity($form, $this->request->getData());
            if ($this->Forms->save($form)) {
                $this->Flash->success(__('Le formulaire a été modifié avec succès.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Le formulaire n\'a pas pu etre modifié, reessayez.'));
        }
        $fields = $form->fields;
        $types = $this->Forms->Fields->TypeFields->find('list')->toArray();
        $this->set(compact('form','fields','types'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Form id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $form = $this->Forms->get($id);
        if ($this->Forms->delete($form)) {
            $this->Flash->success(__("Le formulaire a été supprimé avec succès !"));
        } else {
            $this->Flash->error(__("Le formulaire n'a pas pu être supprimé, reessayez !"));
        }

        return $this->redirect($this->referer());
    }
}
