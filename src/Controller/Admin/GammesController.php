<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Gammes Controller
 *
 * @property \App\Model\Table\GammesTable $Gammes
 *
 * @method \App\Model\Entity\Gamme[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GammesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $gammes = $this->paginate($this->Gammes);

        $this->set(compact('gammes'));
    }

    /**
     * View method
     *
     * @param string|null $id Gamme id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $gamme = $this->Gammes->get($id, [
            'contain' => ['Produits'],
            'conditions' => ['Gammes.company_id' => $this->Auth->user('company_id')],
        ]);

        $this->set('gamme', $gamme);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $gamme = $this->Gammes->newEntity();
        if ($this->request->is('post')) {
            $gamme = $this->Gammes->patchEntity($gamme, $this->request->getData());
            $gamme->company_id = $this->Auth->user('company_id');
            if ($this->Gammes->save($gamme)) {
                $this->Flash->success(__('La gamme a été ajoutée avec succès'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('La gamme n\'a pas pu être ajoutée, reessayez.'));
        }
        $this->set(compact('gamme'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Gamme id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $gamme = $this->Gammes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gamme = $this->Gammes->patchEntity($gamme, $this->request->getData());
            if ($this->Gammes->save($gamme)) {
                $this->Flash->success(__('La gamme a été modifiée avec succès'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('La gamme n\'a pas pu être modifiée, reessayez.'));
        }
        $this->set(compact('gamme'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Gamme id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $gamme = $this->Gammes->get($id);
        if ($this->Gammes->delete($gamme)) {
            $this->Flash->success(__('La gamme a été supprimée avec succès.'));
        } else {
            $this->Flash->error(__('La gamme n\'a pas pu être supprimée, reessayez'));
        }

        return $this->redirect($this->referer());
    }
}
