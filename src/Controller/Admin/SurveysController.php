<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Surveys Controller
 *
 * @property \App\Model\Table\SurveysTable $Surveys
 *
 * @method \App\Model\Entity\Survey[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SurveysController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $paginate = [
            'contain' => ['TypePois', 'Forms'],
            'conditions' => ['Surveys.company_id' => $this->Auth->user('company_id')]
        ];
        $forms = $this->Surveys->Forms->find('list',['conditions' => ['Forms.company_id' => $this->Auth->user('company_id')]])->toArray();
        $type_pois = $this->Surveys->TypePois->find('list',['conditions' => ['TypePois.company_id' => $this->Auth->user('company_id')]])->toArray();
        $surveys = $$this->Surveys->find('all',$paginate)->toArray();

        $this->set(compact('surveys','type_pois','forms'));
    }

    /**
     * View method
     *
     * @param string|null $id Survey id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $survey = $this->Surveys->get($id, [
            'contain' => ['TypePois', 'Forms', 'Gestions', 'Pois']
        ]);

        $this->set('survey', $survey);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $survey = $this->Surveys->newEntity();
        if ($this->request->is('post')) {
            $survey = $this->Surveys->patchEntity($survey, $this->request->getData());
            $survey->company_id = $this->Auth->user('company_id');
            if ($this->Surveys->save($survey)) {
                $this->Flash->success(__("L'enquête a été ajoutée avec succès."));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__("L'enquête n'a pas pu être ajoutée, reessayez."));
        }
        $typePois = $this->Surveys->TypePois->find('list', ['conditions' => ['TypePois.company_id' => $this->Auth->user('company_id')],'limit' => 200]);
        $forms = $this->Surveys->Forms->find('list', ['conditions' => ['Forms.company_id' => $this->Auth->user('company_id')],'limit' => 200]);
        $this->set(compact('survey', 'typePois', 'forms'));
    }

    public function edit($id = null)
    {
        $survey = $this->Surveys->get($id, [
            'contain' => ['Gestions' => ['Teams.Members']]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $survey = $this->Surveys->patchEntity($survey, $this->request->getData());
            if ($this->Surveys->save($survey)) {
                $this->Flash->success(__("L'enquête a été modifiée avec succès."));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__("L'enquête n'a pas pu être modifiée, reessayez."));
        }
        $gestions = $survey->gestions;
        $typePois = $this->Surveys->TypePois->find('list', ['conditions' => ['TypePois.company_id' => $this->Auth->user('company_id')],'limit' => 200]);
        $forms = $this->Surveys->Forms->find('list', ['conditions' => ['Forms.company_id' => $this->Auth->user('company_id')],'limit' => 200]);
        $this->set(compact('survey', 'typePois', 'forms','gestions'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $survey = $this->Surveys->get($id);
        if ($this->Surveys->delete($survey)) {
            $this->Flash->success(__("L'enquête a été supprimée avec succès."));
        } else {
            $this->Flash->error(__("L'enquête n'a pas pu être modifiée, reessayez."));
        }

        return $this->redirect($this->referer());
    }
}
