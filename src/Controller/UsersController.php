<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use App\Controller\UtilitiesController as utility;

class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Users');
        $this->Auth->allow();
    }

    public function login()
    {
        $query = $this->request->getQuery();

        if(isset($query['redirect'])) {
            $redirect = $query['redirect'];
            $this->set(compact('redirect'));
        }

        if ($this->request->is('post')) {
            $user = null;
            $authorize = false;
            $redirect = $this->request->getData('redirect');
            $temp = $this->request->getData();

            // if($this->checkEmail(trim($temp['phone']))) $getUserByInfo = $this->Users->find('All',['conditions' => ['Users.email' => trim($temp['phone'])],'contain' => ['Roles']])->first();
            if(filter_var(trim($temp['phone']),FILTER_VALIDATE_EMAIL)) $getUserByInfo = $this->Users->find('All',['conditions' => ['Users.email' => trim($temp['phone'])],'contain' => ['Roles', 'Companies']])->first();
            else $getUserByInfo = $this->Users->find('All',['conditions' => ['Users.phone' => trim($temp['phone'])], 'contain' => ['Roles', 'Companies']])->first();
            if($getUserByInfo) {
                if(sha1($temp['password']) === $getUserByInfo->password) $user = $getUserByInfo;
            }

            if ($user) {
                if($user->active == 1) {
                    if(!in_array($user->company->companie_statut_id, [1,3])) {
                        if(sizeof($user->roles) > 0) {
                            foreach($user->roles as $role) {
                                if(in_array($role->profile_id,[4,3,2,6])) $authorize = true;
                            }
                        } else $authorize = false;
    
                        if($authorize) {
                            $this->Auth->setUser($user);
                            if(isset($redirect)) return $this->redirect($redirect);
                            return $this->redirectUser($user);
                        } else $this->Flash->error(__('Vous n\'avez pas les droits recquis pour accéder à cet espace !'));
                    } else $this->Flash->error(__('Votre compte n\'est pas encore actif, veuillez contacter l\'administrateur !'));
                } else $this->Flash->error(__('Veuillez activer votre compte avant de vous connecter, réessayez !'));
            } else $this->Flash->error(__('Mot de passe ou email incorrect, réessayez !'));
        }
    }

    public function activation($cle = null)
    {
        $user = $this->Users->find('All', ['contain' => ['Companies'],'field' => 'id','conditions' => ['token' => $cle]])->first();
        if($user){
            $clebd = $user->token;
            $actif = $user->active;
            $company = $user->company;
            $company->companie_statut_id = 2; //Actif
            if ($actif == 1) {
                $alert = 'Votre compte a été déjà activé';
                $success = true;
            } else {
                if ($cle == $clebd) {
                    $this->Users->updateAll(['active' => 1], ['token' => $cle]);
                    $this->Users->Companies->save($company);

                    $alert = 'Votre compte est activé avec succès.';
                    $success = true;
                } else { $alert = 'votre compte ne peut pas être activé, clé d\'activation invalide.'; $success = false; }
            }
        } else { $alert = 'votre compte est introuvable.'; $success = false;  }

        $this->set(compact('success','alert'));
    }

    public function forgotpassword() {

        if($this->request->is('post')) {
            $email = $this->request->getData('email');

            $user = $this->Users->find('all', ['conditions' => ['Users.email' => $email]])->first();

            if($user) {
                $user->token = \md5($user->company_id."".Time::now());
                if($this->Users->save($user)) {
                    $url = $this->request->host().'/users/changepassword/'.$user->token;
                    $message = "<p> $user->fn $user->ln, <br> mot de passe oublié ? merci de cliquer sur le lien ci-dessous pour la modification de votre mot de passe : <br>";
                    $message .= "<a href='".$url."'>Modifier mon mot de passe</a></p>";
                    $sujet = "SCOD : Réinitialisation de mot de passe.";

                    utility::sendEmail($user->email,$sujet,$message);

                    $this->Flash->success('Vérifiez votre messagerie pour modifier votre mot de passe.');
                }
            }
            else $this->Flash->error('Vous n\'avez pas de compte dans notre site.');

            return $this->redirect(['action' => 'login']);
        }
    }

    public function changepassword($cle = null) {
            $user = $this->Users->find('All', ['conditions' => ['Users.token' => $cle]])->first();
            if($user){
                if($this->request->is('post')) {
                    $user->password = $this->request->getData('password');

                    if($this->Users->save($user)) {
                        $this->Flash->success('Votre mot de passe a été modifié avec succès.');
                        return $this->redirect(['action' => 'login']);
                    } else $this->Flash->error($this->msgErrors($this->returnErrors($user->getErrors())), ['escape' => false]);
                }
            }
            else $this->Flash->error('Vous n\'avez pas de compte dans notre site.');

        $this->set(compact('user','cle'));
    }

    protected function redirectUser($user) {
        $auth = $this->getUser($user->id);
        //Autorisation for super amdin
            if (in_array(6,$auth->profile_ids)) {
                $company = $this->Users->Companies->get($user->company_id);
                if(!$company->theme_id) return $this->redirect(['prefix' => 'super','controller' => 'dashboard','action' => 'bienvenue']);
                return $this->redirect(['prefix' => 'super','controller' => 'dashboard','action' => 'index']);
            }
        //Autorisation for amdin
            if (in_array(4,$auth->profile_ids)) {
                $company = $this->Users->Companies->get($user->company_id);
                if(!$company->theme_id) return $this->redirect(['prefix' => 'admin','controller' => 'dashboard','action' => 'bienvenue']);
                return $this->redirect(['prefix' => 'admin','controller' => 'dashboard','action' => 'index']);
            }
        //Autorisation for responsable
            if (in_array(3,$auth->profile_ids)) return $this->redirect(['prefix' => 'responsable','controller' => 'dashboard','action' => 'index']);
        //Autorisation for commercial
            if (in_array(2,$auth->profile_ids)) return $this->redirect(['prefix' => 'commercial','controller' => 'dashboard','action' => 'index']);
        //Autorisation for enqueteur
            if (in_array(1,$auth->profile_ids)) return $this->redirect(['prefix' => 'enqueteur','controller' => 'dashboard','action' => 'index']);
        //Autorisation for distributeur
            if (in_array(5,$auth->profile_ids)) return $this->redirect(['prefix' => 'enqueteur','controller' => 'dashboard','action' => 'index']);
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
}
