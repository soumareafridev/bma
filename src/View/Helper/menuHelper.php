<?php
namespace App\View\Helper;

use Cake\View\Helper;

class menuHelper extends Helper
{
    public function bread() {
        $crumbs = explode("/",$_SERVER["REQUEST_URI"]);
        foreach($crumbs as $crumb){
            echo ucfirst(str_replace(array(".php","_"),array(""," "),$crumb) . ' ');
        }
    }
}