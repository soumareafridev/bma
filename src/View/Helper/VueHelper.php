<?php
namespace App\View\Helper;
 
use Cake\View\Helper;
use App\Controller\CompaniesController;
 
class VueHelper extends Helper {   

    public function getInfoSite($company_id,$champ){
        $compagnies = new CompaniesController();
        $company = $compagnies->get($company_id);
        if($company->$champ) return $company->$champ;
        else {
            if($champ == 'short_name') return '[---]';
            if($champ == 'one_word') return '[--Un mot--]';

        }
        return "[--- $champ ---]";
    }

    public function statutCompany() {
        return [
            '1' => '<span class="badge badge-info shadow-sm"><i class="fas fa-spinner mr-1"></i> En attente</span>',
            '2' => '<span class="badge badge-success shadow-sm"><i class="fas fa-check-circle mr-1"></i> Actif</span>',
            '3' => '<span class="badge badge-danger shadow-sm"><i class="fas fa-check-circle mr-1"></i> Désactivé</span>',
        ];
    }

    public function hashUrl($id) {
        $params = [
            'sh' => $this->token(100),
            'i' => $id,
            'hs' => $this->token(50)
        ];

        return $params;
    }

    protected function token($taille)
    {
        $alphabet = "0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN";
        $code = substr(str_shuffle(str_repeat($alphabet, 50)), 0, $taille);
        return $code;
    }
}