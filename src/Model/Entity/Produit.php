<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Produit Entity
 *
 * @property int $id
 * @property string $designation
 * @property float $prix_unitaire_ht
 * @property float $prix_ht
 * @property float $prix_ttc
 * @property int|null $etat
 * @property int|null $gamme_id
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\Gamme $gamme
 */
class Produit extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'designation' => true,
        'prix_unitaire_ht' => true,
        'prix_ht' => true,
        'prix_ttc' => true,
        'etat' => true,
        'gamme_id' => true,
        'created' => true,
        'gamme' => true
    ];
}
