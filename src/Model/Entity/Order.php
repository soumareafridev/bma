<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity
 *
 * @property int $id
 * @property float $amount
 * @property int|null $order_statut_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int|null $poi_customer_id
 * @property int|null $poi_id
 * @property int|null $user_id
 * @property int $company_id
 * @property bool $type
 * @property bool $limited
 * @property \Cake\I18n\FrozenTime|null $date_limited
 * @property \Cake\I18n\FrozenTime|null $date_debut
 * @property string|null $frequences
 * @property int|null $frequence_id
 *
 * @property \App\Model\Entity\OrderStatut $order_statut
 * @property \App\Model\Entity\PoiCustomer $poi_customer
 * @property \App\Model\Entity\Pois $pois
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Company $company
 * @property \App\Model\Entity\Frequence $frequence
 * @property \App\Model\Entity\Distribution[] $distributions
 * @property \App\Model\Entity\OrderLine[] $order_lines
 */
class Order extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'amount' => true,
        'order_statut_id' => true,
        'created' => true,
        'modified' => true,
        'poi_customer_id' => true,
        'poi_id' => true,
        'user_id' => true,
        'company_id' => true,
        'type' => true,
        'limited' => true,
        'date_limited' => true,
        'date_debut' => true,
        'frequences' => true,
        'frequence_id' => true,
        'order_statut' => true,
        'poi_customer' => true,
        'pois' => true,
        'user' => true,
        'company' => true,
        'frequence' => true,
        'distributions' => true,
        'order_lines' => true
    ];
}
