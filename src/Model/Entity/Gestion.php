<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Gestion Entity
 *
 * @property int $id
 * @property int $survey_id
 * @property int $team_id
 * @property int|null $company_id
 *
 * @property \App\Model\Entity\Survey $survey
 * @property \App\Model\Entity\Team $team
 * @property \App\Model\Entity\Company $company
 */
class Gestion extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'survey_id' => true,
        'team_id' => true,
        'company_id' => true,
        'survey' => true,
        'team' => true,
        'company' => true
    ];
}
