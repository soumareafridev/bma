<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $pack
 * @property float $poids_net
 * @property float $poids_brut
 * @property string|null $description
 * @property float|null $price_ht
 * @property int|null $status
 * @property int|null $stock
 * @property int|null $gamme_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int $company_id
 *
 * @property \App\Model\Entity\Gamme $gamme
 * @property \App\Model\Entity\Company $company
 * @property \App\Model\Entity\Image[] $images
 * @property \App\Model\Entity\OrderLine[] $order_lines
 */
class Product extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'code' => true,
        'name' => true,
        'pack' => true,
        'poids_net' => true,
        'poids_brut' => true,
        'description' => true,
        'price_ht' => true,
        'status' => true,
        'stock' => true,
        'gamme_id' => true,
        'created' => true,
        'modified' => true,
        'company_id' => true,
        'gamme' => true,
        'company' => true,
        'images' => true,
        'order_lines' => true
    ];
}
