<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PoiCustomer Entity
 *
 * @property int $id
 * @property string $fn
 * @property string $ln
 * @property string|null $phone
 * @property string|null $password
 * @property bool $is_admin
 * @property int|null $customer_type_id
 * @property int $poi_id
 * @property int|null $company_id
 *
 * @property \App\Model\Entity\CustomerType $customer_type
 * @property \App\Model\Entity\Pois $pois
 * @property \App\Model\Entity\Company $company
 * @property \App\Model\Entity\Order[] $orders
 */
class PoiCustomer extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fn' => true,
        'ln' => true,
        'phone' => true,
        'password' => true,
        'is_admin' => true,
        'customer_type_id' => true,
        'poi_id' => true,
        'company_id' => true,
        'customer_type' => true,
        'pois' => true,
        'company' => true,
        'orders' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
