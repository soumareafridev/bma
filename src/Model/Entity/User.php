<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $fn
 * @property string $ln
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property string|null $token
 * @property bool $active
 * @property int $company_id
 *
 * @property \App\Model\Entity\Company $company
 * @property \App\Model\Entity\Member[] $members
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\Pois[] $pois
 * @property \App\Model\Entity\Role[] $roles
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fn' => true,
        'ln' => true,
        'email' => true,
        'password' => true,
        'phone' => true,
        'token' => true,
        'active' => true,
        'company_id' => true,
        'company' => true,
        'members' => true,
        'orders' => true,
        'created' => true,
        'modified' => true,
        'distributions' => true,
        'pois' => true,
        'roles' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'token'
    ];

    protected function _setPassword($value)
    {
        // $hasher = new DefaultPasswordHasher();
        return sha1($value);
    }
}
