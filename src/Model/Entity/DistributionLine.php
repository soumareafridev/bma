<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DistributionLine Entity
 *
 * @property int $id
 * @property float|null $taxe
 * @property int $quantite
 * @property float $price_ht
 * @property float|null $amount
 * @property float|null $amount_ht
 * @property int|null $distribution_id
 * @property int|null $product_id
 * @property int|null $order_line_id
 *
 * @property \App\Model\Entity\Distribution $distribution
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\OrderLine $order_line
 */
class DistributionLine extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'taxe' => true,
        'quantite' => true,
        'price_ht' => true,
        'amount' => true,
        'amount_ht' => true,
        'distribution_id' => true,
        'product_id' => true,
        'order_line_id' => true,
        'distribution' => true,
        'product' => true,
        'order_line' => true
    ];
}
