<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Field Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $choices
 * @property int $type_field_id
 * @property int $form_id
 * @property int|null $value_id
 *
 * @property \App\Model\Entity\TypeField $type_field
 * @property \App\Model\Entity\Form $form
 * @property \App\Model\Entity\Value $value
 * @property \App\Model\Entity\Info[] $infos
 */
class Field extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'choices' => true,
        'type_field_id' => true,
        'form_id' => true,
        'value_id' => true,
        'type_field' => true,
        'form' => true,
        'value' => true,
        'infos' => true
    ];
}
