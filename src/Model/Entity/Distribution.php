<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Distribution Entity
 *
 * @property int $id
 * @property float $amount
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int|null $poi_customer_id
 * @property int|null $poi_id
 * @property int|null $user_id
 * @property int $company_id
 * @property int|null $order_id
 * @property int|null $distribution_statut_id
 *
 * @property \App\Model\Entity\PoiCustomer $poi_customer
 * @property \App\Model\Entity\Pois $pois
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Company $company
 * @property \App\Model\Entity\Order $order
 * @property \App\Model\Entity\DistributionStatut $distribution_statut
 * @property \App\Model\Entity\DistributionLine[] $distribution_lines
 */
class Distribution extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'amount' => true,
        'created' => true,
        'modified' => true,
        'poi_customer_id' => true,
        'poi_id' => true,
        'user_id' => true,
        'company_id' => true,
        'order_id' => true,
        'distribution_statut_id' => true,
        'poi_customer' => true,
        'pois' => true,
        'user' => true,
        'company' => true,
        'order' => true,
        'distribution_statut' => true,
        'distribution_lines' => true
    ];
}
