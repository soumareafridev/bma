<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Pois Entity
 *
 * @property int $id
 * @property float $lng
 * @property float $lat
 * @property string|null $dns
 * @property string|null $phone
 * @property string|null $photo
 * @property int|null $status
 * @property int|null $type_poi_id
 * @property int|null $survey_id
 * @property int|null $user_id
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\TypePois $type_pois
 * @property \App\Model\Entity\Survey $survey
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Info[] $infos
 */
class Pois extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'lng' => true,
        'lat' => true,
        'dns' => true,
        'phone' => true,
        'photo' => true,
        'status' => true,
        'type_poi_id' => true,
        'survey_id' => true,
        'user_id' => true,
        'company_id' => true,
        'created' => true,
        'type_pois' => true,
        'survey' => true,
        'user' => true,
        'infos' => true
    ];
}
