<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Company Entity
 *
 * @property int $id
 * @property string|null $slogan
 * @property string|null $description
 * @property string|null $logo
 * @property string|null $short_name
 * @property string|null $one_word
 * @property string $name
 * @property int|null $theme_id
 * @property int|null $companie_statut_id
 * @property \Cake\I18n\FrozenTime|null $created
 *
 * @property \App\Model\Entity\Theme $theme
 * @property \App\Model\Entity\CompanieStatut $companie_statut
 * @property \App\Model\Entity\CustomerType[] $customer_types
 * @property \App\Model\Entity\Distribution[] $distributions
 * @property \App\Model\Entity\Form[] $forms
 * @property \App\Model\Entity\Gamme[] $gammes
 * @property \App\Model\Entity\Gestion[] $gestions
 * @property \App\Model\Entity\Groupe[] $groupes
 * @property \App\Model\Entity\Member[] $members
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\PoiCustomer[] $poi_customers
 * @property \App\Model\Entity\Pois[] $pois
 * @property \App\Model\Entity\Product[] $products
 * @property \App\Model\Entity\Statut[] $statuts
 * @property \App\Model\Entity\Survey[] $surveys
 * @property \App\Model\Entity\Team[] $teams
 * @property \App\Model\Entity\TypePois[] $type_pois
 * @property \App\Model\Entity\User[] $users
 */
class Company extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'slogan' => true,
        'description' => true,
        'logo' => true,
        'short_name' => true,
        'one_word' => true,
        'name' => true,
        'theme_id' => true,
        'companie_statut_id' => true,
        'created' => true,
        'theme' => true,
        'companie_statut' => true,
        'customer_types' => true,
        'distributions' => true,
        'forms' => true,
        'gammes' => true,
        'gestions' => true,
        'groupes' => true,
        'members' => true,
        'access' => true,
        'orders' => true,
        'poi_customers' => true,
        'pois' => true,
        'products' => true,
        'statuts' => true,
        'surveys' => true,
        'teams' => true,
        'type_pois' => true,
        'users' => true
    ];
}
