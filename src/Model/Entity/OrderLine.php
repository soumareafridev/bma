<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OrderLine Entity
 *
 * @property int $id
 * @property float|null $taxe
 * @property int $quantite
 * @property int|null $quantite_max
 * @property int|null $quantite_livre
 * @property float $price_ht
 * @property float|null $amount
 * @property float|null $amount_ht
 * @property int|null $order_id
 * @property int|null $product_id
 *
 * @property \App\Model\Entity\Order $order
 * @property \App\Model\Entity\Product $product
 */
class OrderLine extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'taxe' => true,
        'quantite' => true,
        'quantite_max' => true,
        'quantite_livre' => true,
        'price_ht' => true,
        'amount' => true,
        'amount_ht' => true,
        'order_id' => true,
        'product_id' => true,
        'order' => true,
        'product' => true
    ];
}
