<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Survey Entity
 *
 * @property int $id
 * @property string $name
 * @property int $type_poi_id
 * @property int $form_id
 * @property int|null $company_id
 *
 * @property \App\Model\Entity\TypePois $type_pois
 * @property \App\Model\Entity\Form $form
 * @property \App\Model\Entity\Company $company
 * @property \App\Model\Entity\Gestion[] $gestions
 * @property \App\Model\Entity\Pois[] $pois
 */
class Survey extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'type_poi_id' => true,
        'form_id' => true,
        'company_id' => true,
        'type_pois' => true,
        'form' => true,
        'company' => true,
        'gestions' => true,
        'pois' => true
    ];
}
