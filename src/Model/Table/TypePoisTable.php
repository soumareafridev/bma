<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TypePois Model
 *
 * @method \App\Model\Entity\TypePois get($primaryKey, $options = [])
 * @method \App\Model\Entity\TypePois newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TypePois[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TypePois|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypePois saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypePois patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TypePois[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TypePois findOrCreate($search, callable $callback = null, $options = [])
 */
class TypePoisTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('type_pois');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Pois', [
            'foreignKey' => 'type_poi_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }
}
