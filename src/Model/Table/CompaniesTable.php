<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Companies Model
 *
 * @property \App\Model\Table\ThemesTable&\Cake\ORM\Association\BelongsTo $Themes
 * @property \App\Model\Table\CompanieStatutsTable&\Cake\ORM\Association\BelongsTo $CompanieStatuts
 * @property \App\Model\Table\CustomerTypesTable&\Cake\ORM\Association\HasMany $CustomerTypes
 * @property \App\Model\Table\DistributionsTable&\Cake\ORM\Association\HasMany $Distributions
 * @property \App\Model\Table\FormsTable&\Cake\ORM\Association\HasMany $Forms
 * @property \App\Model\Table\GammesTable&\Cake\ORM\Association\HasMany $Gammes
 * @property \App\Model\Table\GestionsTable&\Cake\ORM\Association\HasMany $Gestions
 * @property \App\Model\Table\GroupesTable&\Cake\ORM\Association\HasMany $Groupes
 * @property \App\Model\Table\MembersTable&\Cake\ORM\Association\HasMany $Members
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 * @property \App\Model\Table\PoiCustomersTable&\Cake\ORM\Association\HasMany $PoiCustomers
 * @property \App\Model\Table\PoisTable&\Cake\ORM\Association\HasMany $Pois
 * @property \App\Model\Table\ProductsTable&\Cake\ORM\Association\HasMany $Products
 * @property \App\Model\Table\StatutsTable&\Cake\ORM\Association\HasMany $Statuts
 * @property \App\Model\Table\SurveysTable&\Cake\ORM\Association\HasMany $Surveys
 * @property \App\Model\Table\TeamsTable&\Cake\ORM\Association\HasMany $Teams
 * @property \App\Model\Table\TypePoisTable&\Cake\ORM\Association\HasMany $TypePois
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\HasMany $Users
 *
 * @method \App\Model\Entity\Company get($primaryKey, $options = [])
 * @method \App\Model\Entity\Company newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Company[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Company|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Company saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Company patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Company[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Company findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CompaniesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('companies');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Themes', [
            'foreignKey' => 'theme_id'
        ]);
        $this->belongsTo('CompanieStatuts', [
            'foreignKey' => 'companie_statut_id'
        ]);
        $this->hasMany('CustomerTypes', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('Distributions', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('Forms', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('Gammes', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('Gestions', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('Groupes', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('Members', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('PoiCustomers', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('Pois', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('Products', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('Statuts', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('Surveys', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('Teams', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('TypePois', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'company_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('slogan')
            ->maxLength('slogan', 255)
            ->allowEmptyString('slogan');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->scalar('logo')
            ->maxLength('logo', 255)
            ->allowEmptyString('logo');

        $validator
            ->scalar('access')
            ->maxLength('access', 20)
            ->allowEmptyString('access');

        $validator
            ->scalar('short_name')
            ->maxLength('short_name', 3)
            ->allowEmptyString('short_name');

        $validator
            ->scalar('one_word')
            ->maxLength('one_word', 40)
            ->allowEmptyString('one_word');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['theme_id'], 'Themes'));
        $rules->add($rules->existsIn(['companie_statut_id'], 'CompanieStatuts'));

        return $rules;
    }
}
