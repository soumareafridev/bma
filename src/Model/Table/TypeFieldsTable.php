<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TypeFields Model
 *
 * @property \App\Model\Table\FieldsTable|\Cake\ORM\Association\HasMany $Fields
 *
 * @method \App\Model\Entity\TypeField get($primaryKey, $options = [])
 * @method \App\Model\Entity\TypeField newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TypeField[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TypeField|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypeField saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypeField patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TypeField[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TypeField findOrCreate($search, callable $callback = null, $options = [])
 */
class TypeFieldsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('type_fields');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Fields', [
            'foreignKey' => 'type_field_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('type')
            ->maxLength('type', 100)
            ->allowEmptyString('type');

        return $validator;
    }
}
