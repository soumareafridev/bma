<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DistributionStatuts Model
 *
 * @property \App\Model\Table\DistributionsTable&\Cake\ORM\Association\HasMany $Distributions
 *
 * @method \App\Model\Entity\DistributionStatut get($primaryKey, $options = [])
 * @method \App\Model\Entity\DistributionStatut newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DistributionStatut[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DistributionStatut|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DistributionStatut saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DistributionStatut patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DistributionStatut[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DistributionStatut findOrCreate($search, callable $callback = null, $options = [])
 */
class DistributionStatutsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('distribution_statuts');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Distributions', [
            'foreignKey' => 'distribution_statut_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }
}
