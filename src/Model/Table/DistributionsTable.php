<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Distributions Model
 *
 * @property \App\Model\Table\PoiCustomersTable&\Cake\ORM\Association\BelongsTo $PoiCustomers
 * @property \App\Model\Table\PoisTable&\Cake\ORM\Association\BelongsTo $Pois
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\CompaniesTable&\Cake\ORM\Association\BelongsTo $Companies
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\BelongsTo $Orders
 * @property \App\Model\Table\DistributionStatutsTable&\Cake\ORM\Association\BelongsTo $DistributionStatuts
 * @property \App\Model\Table\DistributionLinesTable&\Cake\ORM\Association\HasMany $DistributionLines
 *
 * @method \App\Model\Entity\Distribution get($primaryKey, $options = [])
 * @method \App\Model\Entity\Distribution newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Distribution[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Distribution|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Distribution saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Distribution patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Distribution[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Distribution findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DistributionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('distributions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('PoiCustomers', [
            'foreignKey' => 'poi_customer_id'
        ]);
        $this->belongsTo('Pois', [
            'foreignKey' => 'poi_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id'
        ]);
        $this->belongsTo('DistributionStatuts', [
            'foreignKey' => 'distribution_statut_id'
        ]);
        $this->hasMany('DistributionLines', [
            'foreignKey' => 'distribution_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('amount')
            ->requirePresence('amount', 'create')
            ->notEmptyString('amount');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['poi_customer_id'], 'PoiCustomers'));
        $rules->add($rules->existsIn(['poi_id'], 'Pois'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['company_id'], 'Companies'));
        $rules->add($rules->existsIn(['order_id'], 'Orders'));
        $rules->add($rules->existsIn(['distribution_statut_id'], 'DistributionStatuts'));

        return $rules;
    }
}
