<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PoiCustomers Model
 *
 * @property \App\Model\Table\CustomerTypesTable&\Cake\ORM\Association\BelongsTo $CustomerTypes
 * @property \App\Model\Table\PoisTable&\Cake\ORM\Association\BelongsTo $Pois
 * @property \App\Model\Table\CompaniesTable&\Cake\ORM\Association\BelongsTo $Companies
 * @property &\Cake\ORM\Association\HasMany $Distributions
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 *
 * @method \App\Model\Entity\PoiCustomer get($primaryKey, $options = [])
 * @method \App\Model\Entity\PoiCustomer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PoiCustomer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PoiCustomer|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PoiCustomer saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PoiCustomer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PoiCustomer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PoiCustomer findOrCreate($search, callable $callback = null, $options = [])
 */
class PoiCustomersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('poi_customers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('CustomerTypes', [
            'foreignKey' => 'customer_type_id'
        ]);
        $this->belongsTo('Pois', [
            'foreignKey' => 'poi_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id'
        ]);
        $this->hasMany('Distributions', [
            'foreignKey' => 'poi_customer_id'
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'poi_customer_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('fn')
            ->maxLength('fn', 255)
            ->requirePresence('fn', 'create')
            ->notEmptyString('fn');

        $validator
            ->scalar('ln')
            ->maxLength('ln', 255)
            ->requirePresence('ln', 'create')
            ->notEmptyString('ln');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 24)
            ->allowEmptyString('phone');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->allowEmptyString('password');

        $validator
            ->boolean('is_admin')
            ->notEmptyString('is_admin');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['customer_type_id'], 'CustomerTypes'));
        $rules->add($rules->existsIn(['poi_id'], 'Pois'));
        $rules->add($rules->existsIn(['company_id'], 'Companies'));

        return $rules;
    }
}
