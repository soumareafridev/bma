<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DistributionLines Model
 *
 * @property \App\Model\Table\DistributionsTable&\Cake\ORM\Association\BelongsTo $Distributions
 * @property \App\Model\Table\ProductsTable&\Cake\ORM\Association\BelongsTo $Products
 * @property \App\Model\Table\OrderLinesTable&\Cake\ORM\Association\BelongsTo $OrderLines
 *
 * @method \App\Model\Entity\DistributionLine get($primaryKey, $options = [])
 * @method \App\Model\Entity\DistributionLine newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DistributionLine[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DistributionLine|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DistributionLine saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DistributionLine patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DistributionLine[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DistributionLine findOrCreate($search, callable $callback = null, $options = [])
 */
class DistributionLinesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('distribution_lines');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Distributions', [
            'foreignKey' => 'distribution_id'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id'
        ]);
        $this->belongsTo('OrderLines', [
            'foreignKey' => 'order_line_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('taxe')
            ->allowEmptyString('taxe');

        $validator
            ->integer('quantite')
            ->requirePresence('quantite', 'create')
            ->notEmptyString('quantite');

        $validator
            ->numeric('price_ht')
            ->requirePresence('price_ht', 'create')
            ->notEmptyString('price_ht');

        $validator
            ->numeric('amount')
            ->allowEmptyString('amount');

        $validator
            ->numeric('amount_ht')
            ->allowEmptyString('amount_ht');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['distribution_id'], 'Distributions'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        $rules->add($rules->existsIn(['order_line_id'], 'OrderLines'));

        return $rules;
    }
}
