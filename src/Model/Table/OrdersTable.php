<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Orders Model
 *
 * @property \App\Model\Table\OrderStatutsTable&\Cake\ORM\Association\BelongsTo $OrderStatuts
 * @property \App\Model\Table\PoiCustomersTable&\Cake\ORM\Association\BelongsTo $PoiCustomers
 * @property \App\Model\Table\PoisTable&\Cake\ORM\Association\BelongsTo $Pois
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\CompaniesTable&\Cake\ORM\Association\BelongsTo $Companies
 * @property \App\Model\Table\FrequencesTable&\Cake\ORM\Association\BelongsTo $Frequences
 * @property \App\Model\Table\DistributionsTable&\Cake\ORM\Association\HasMany $Distributions
 * @property \App\Model\Table\OrderLinesTable&\Cake\ORM\Association\HasMany $OrderLines
 *
 * @method \App\Model\Entity\Order get($primaryKey, $options = [])
 * @method \App\Model\Entity\Order newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Order[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Order|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Order[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Order findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrdersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('orders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('OrderStatuts', [
            'foreignKey' => 'order_statut_id'
        ]);
        $this->belongsTo('PoiCustomers', [
            'foreignKey' => 'poi_customer_id'
        ]);
        $this->belongsTo('Pois', [
            'foreignKey' => 'poi_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Frequences', [
            'foreignKey' => 'frequence_id'
        ]);
        $this->hasMany('Distributions', [
            'foreignKey' => 'order_id'
        ]);
        $this->hasMany('OrderLines', [
            'foreignKey' => 'order_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('amount')
            ->requirePresence('amount', 'create')
            ->notEmptyString('amount');

        $validator
            ->integer('type')
            ->notEmptyString('type');

        $validator
            ->boolean('limited')
            ->notEmptyString('limited');

        $validator
            ->dateTime('date_limited')
            ->allowEmptyDateTime('date_limited');

        $validator
            ->dateTime('date_debut')
            ->allowEmptyDateTime('date_debut');

        $validator
            ->scalar('frequences')
            ->allowEmptyString('frequences');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['order_statut_id'], 'OrderStatuts'));
        $rules->add($rules->existsIn(['poi_customer_id'], 'PoiCustomers'));
        $rules->add($rules->existsIn(['poi_id'], 'Pois'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['company_id'], 'Companies'));
        $rules->add($rules->existsIn(['frequence_id'], 'Frequences'));

        return $rules;
    }
}
