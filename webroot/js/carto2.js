    /*Afridev Project Cartographie stations js
    **
    *Function afficherSations retourne les elements recherhés dans le filtre
    *Reçoit les parametres de la fonction filtrage
    **/

    //DEBUT Variable qui va contenir tous les objets station avec leur informations
    var stations = {};
    var stationComplete = [];

    //FIN Variable qui va contenir tous les objets station avec leur informations



    var selectionPetroliers = [];
    var selectionZones = [];
    // Initialize and add the map
    function initMap2()
    {
        // Create an array of alphabetical characters used to label the markers.
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        //Initilisation du tableau

        var map;
        var markers = [];

        //Centrer à dakar au depart
        function initialize()
        {
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 7,
                center: {
                    lat: 14.4974,
                    lng: -14.4524
                }
            });
        }

        initialize();

        // function groupePetro(index) {
        //     var bounds = new google.maps.LatLngBounds();
        //     $.each(petroliers[index], function(i,v){
        //         $("#station"+v.id).prop('checked', true).parent().css('display','block').addClass('animated fadeIn');
        //         var marker;
        //         var myLatLng = new google.maps.LatLng(v.lat, v.lng);
        //         marker = new google.maps.Marker({
        //         position: {lat: v.lat,lng: v.lng},
        //         map: map,
        //         title: v.name,
        //         label: v.name.charAt(0)
        //         });
        //         bounds.extend(myLatLng);
        //         markers.push(marker);
        //     });
        //     map.fitBounds(bounds);
        // }
        // // function groupeZones(index) {
        // //     var bounds = new google.maps.LatLngBounds();
        // //     $.each(petroliers[index], function(i,v){
        // //         $("#station"+v.id).prop('checked', true).parent().css('display','block').addClass('animated fadeIn');
        // //         var marker;
        // //         var myLatLng = new google.maps.LatLng(v.lat, v.lng);
        // //         marker = new google.maps.Marker({
        // //         position: {lat: v.lat,lng: v.lng},
        // //         map: map,
        // //         title: v.name,
        // //         label: v.name.charAt(0)
        // //         });
        // //         bounds.extend(myLatLng);
        // //         markers.push(marker);
        // //     });
        // //     map.fitBounds(bounds);
        // // }
        // function newPetro() {
        //     $.each(petroliers, function (i, val) {
        //         if ($("#petrolier" + i).prop('checked') === true) {
        //             if ($.inArray($("#petrolier" + i).val(), selectionPetroliers) === -1) {
        //                 //si l'element est déjà coché on va l'ajouter à l'index
        //                 selectionPetroliers.push($("#petrolier" + i).val());
        //             }
                    // $.each(val[i], function(index, value){
                    //     $("#station"+value.id).prop('checked', true).parent().css('display','block');
                    // });
        //         } else {
        //             if ($.inArray($("#petrolier" + i).val(), selectionPetroliers) !== -1) {
        //                 //On enleve si l'elleent n'est pas coché
        //                 selectionPetroliers = $.grep(selectionPetroliers, function (value) {
        //                     return value != $("#petrolier" + i).val();
        //                 });
        //             }
        //             $.each(val[i], function(index, value){
        //                 $("#station"+value.id).prop('checked', false).parent().css('display','none').removeClass('animated fadeIn');
        //             });
        //         }
        //     });
        // }

        // newPetro();

        // function parcourePetro() {
        //     newPetro();
        //     $.each(selectionPetroliers, function (index, val) {
        //         groupePetro(val);
        //     });
        // }

        // if (selectionPetroliers.length > 0) {
        //     parcourePetro();
        // }

        // $("input[name=petroliers]").on('change', function (event) {
        //     if ($(this).prop('checked') === true) {
        //         parcourePetro();
        //     }
        //     if ($(this).prop('checked') === false) {
        //         map = new google.maps.Map(
        //             document.getElementById('map'), {
        //                 zoom: 8,
        //                 center: {
        //                     lat: 14.4974,
        //                     lng: -14.4524
        //                 }
        //             });
        //         $.each(petroliers[$(this).val()], function(index, value){
        //             $("#station"+value.id).prop('checked', false).parent().css('display','none').removeClass('animated fadeIn');
        //         });
        //         parcourePetro();
        //         if($('#allPetro').prop('checked') === true){ $('#allPetro').prop('checked',false);  }
        //     }
        // });

        //FIN Les différents tableaux de nos criteres de recherche
        var filtres = [];
        function recentrer()
        {
            if(filtres.length <= 0){
                hideAll();
            }
        }
        function nbrPetroCoche()
        {
            var nbr = 0;
            $.each(petroliers, function(index, value){
                if ($("#petrolier"+index).prop('checked') === true) {
                    nbr++;
                }
            });
            return nbr;
        }
        function nbrZoneCoche()
        {
            var nbr = 0;
            $.each(zones, function(index, value){
                if ($("#zone"+index).prop('checked') === true) {
                    nbr++;
                }
            });
            return nbr;
        }
        function checkPetrolier()
        {
            $("input[name=petroliers]").on('change', function (event) {
                if ($(this).prop('checked') === true) {
                    $.each(petroliers[$(this).val()], function(index, value){
                        $("#station"+value.id).prop('checked', true);
                        $("#zone"+value.zone_id).prop('checked', true);
                    });
                    filtrage();
                    creationMarkeurs();
                }
                else
                {
                    map = new google.maps.Map(
                        document.getElementById('map'), {
                        zoom: 8,
                        center: {
                        lat: 14.4974,
                        lng: -14.4524
                        }
                    });
                    if(nbrPetroCoche() <= 0)
                    {
                        $.each(petroliers[$(this).val()], function(index, value){
                            $("#station"+value.id).prop('checked', false);
                            $("#zone"+value.zone_id).prop('checked', false);
                        });
                    }

                    if($('#allPetro').prop('checked') === true) { $('#allPetro').prop('checked',false) };
                    filtrage();
                    creationMarkeurs();
                    recentrer();                    
                }
            });
        }

        function checkZone()
        {
            $("input[name=zones]").on('change', function (event) {
                if(nbrPetroCoche() <= 0)
                {
                    if ($(this).prop('checked') === true) {
                        $.each(zones[$(this).val()], function(index, value){
                            $("#station"+value.id).prop('checked', true);
                            $("#petrolier"+value.petrolier_id).prop('checked', true);
                        });
                        afficherStations();
                    }                    
                }
                else
                {
                    if ($(this).prop('checked') === true) {
                        afficherStations();
                    }
                    else
                    { 
                        map = new google.maps.Map(
                            document.getElementById('map'), {
                            zoom: 8,
                            center: {
                            lat: 14.4974,
                            lng: -14.4524
                            }
                        });
                        if(nbrZoneCoche() <= 0)
                        {
                            $.each(zones[$(this).val()], function(index, value){
                                $("#station"+value.id).prop('checked', false);
                                $("#petrolier"+value.petrolier_id).prop('checked', false);
                            });
                        }                   
                        afficherStations();
                        recentrer(); 
                    }
                }
            });
        }
        function checkStation()
        {
            $("input[name=stations]").on('change', function (event) {
                var idPetrolier = stations[$(this).val()].petrolier_id;
                var idZone = stations[$(this).val()].zone_id;
                if ($(this).prop('checked') === true) {
                    if($("#petrolier"+idPetrolier).prop('checked') === false)
                    {
                        $("#petrolier"+idPetrolier).prop('checked',true);
                    }
                    if($("#zone"+idZone).prop('checked') === false)
                    {
                        $("#zone"+idZone).prop('checked',true);
                    }
                    afficherStations();
                }
                else
                {
                    map = new google.maps.Map(
                        document.getElementById('map'), {
                        zoom: 8,
                        center: {
                        lat: 14.4974,
                        lng: -14.4524
                        }
                    });
                    afficherStations();
                    recentrer();                    
                }
            });
        }

        function creationMarkeurs()
        {
            var bounds = new google.maps.LatLngBounds();
            $.each(filtres, function(i,v){
                $("#station"+v.id).prop('checked', true).parent().css('display','block').addClass('animated fadeIn');
                var marker;
                // var image;
                var urlImage;
                var petroId = stations[v.id].petrolier_id;
                switch(petroId)
                {
                    case 1:
                    urlImage = "<?= $this->Url->image("tn_marker-total.png") ?>";
                    break;
                    case 2:
                    urlImage = "<?= $this->Url->image("tn_marker-oil.png") ?>";
                    break;
                    case 3:
                    urlImage = "<?= $this->Url->image("tn_marker-elton.png") ?>";
                    break;
                    case 4:
                    urlImage = "<?= $this->Url->image("tn_marker-shell.png") ?>";
                    break;
                    default:
                    urlImage = "";
                    break;
                }
                var myLatLng = new google.maps.LatLng(v.lat, v.lng);
                marker = new google.maps.Marker({
                position: {lat: v.lat,lng: v.lng},
                map: map,
                title: v.name,
                icon: urlImage
                // label: v.name.charAt(0)
                });
                bounds.extend(myLatLng);
                // markers.push(marker);
            });
            map.fitBounds(bounds);
        }

        function filtrage()
        {
            $.each(stations, function(index, value){
                if($("#station" + value.id).prop('checked') === true && $("#petrolier" + value.petrolier_id).prop('checked') === true && $("#zone" + value.zone_id).prop('checked') === true)
                {
                    if ($.inArray(value, filtres) === -1) {
                        filtres.push(value);
                        $("#station"+index).prop('checked', true);
                    }
                }
                else
                {
                    if ($.inArray(value, filtres) !== -1) {
                        //On enleve si l'eleMent n'est pas coché
                        $("#station"+index).prop('checked', false);
                        filtres = $.grep(filtres, function (val) {
                            return val != value;
                        });
                    }
                }
            });
        }
        //Fonction pour cocher tout
        function openAll()
        {
            $.each(stations, function (i, val) {
                $('#petrolier'+val.petrolier_id).prop('checked', true);
                $('#zone'+val.zone_id).prop('checked', true);
                $("#station"+i).prop('checked', true);
            });
            filtrage();
            creationMarkeurs();
        }
        //fonction qui va cacher toutes les stations
        function hideAll()
        {
            $.each(stations, function(index, val){
                $('#petrolier'+val.petrolier_id).prop('checked', false);
                $('#zone'+val.zone_id).prop('checked', false);
                $("#station"+index).prop('checked', false);
            });
            filtrage();
            initialize();
        }

        function open(index) {}

        function afficherStations()
        {
            filtrage();
            creationMarkeurs();
        }

        filtrage();
        
        recentrer();

        if (filtres.length > 0) {
            creationMarkeurs();
        }
        
        checkStation();

        checkZone();

        checkPetrolier();


        $("#allPetro").change(function(){
            if ($(this).prop('checked') === true) 
            {
                openAll();
            } 
            else
            {
                hideAll();
                recentrer();
            }
        });

        if($('#allPetro').prop('checked') === true)
        {
            openAll();
        }
    }