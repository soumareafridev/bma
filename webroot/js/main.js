function initMap2() {
    $(function() {
        var map;
        var markers = [];
        var lattitude = 14.7645;
        var longitude = -17.366;
        var _infowindow = null;
        var _i = null;

         map = new google.maps.Map(
            document.getElementById('map'),
            {
                center: {lat : lattitude, lng : longitude},
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
        );

        function url() {
            return window.location.href.split('dashboard')[0];
        }

        $.each($('.marker'),function(i,item) {
            var marker = new google.maps.Marker({
                position: { lat: $(item).data('lat'), lng: $(item).data('lng') },
                title: $(item).data('dns'),
                icon: $(item).data('pin'),
                index: i
            });

            markers.push(marker);

            var infowindow = new google.maps.InfoWindow({
                content: contentWindow($(item).data('id'),$(item).data('lat'),$(item).data('lng'),$(item).data('dns'),$(item).data('created'),$(item).data('img'))
              });

              infowindow.addListener('closeclick', function() {
                if($('#marker'+i+" p.eye i").hasClass('icon-circle-rose'))  $('#marker'+i+" p.eye i").removeClass('icon-circle-rose').addClass('icon-circle-blue');
              });

            marker.setMap(map);

            marker.addListener('click', function() {
                if(_infowindow) _infowindow.close();
                if(_i && $('#marker'+_i+" p.eye i").hasClass('icon-circle-rose'))  $('#marker'+_i+" p.eye i").removeClass('icon-circle-rose').addClass('icon-circle-blue');
                if(!$('#marker'+i+" p.eye i").hasClass('icon-circle-rose'))  $('#marker'+i+" p.eye i").removeClass('icon-circle-blue').addClass('icon-circle-rose');
                infowindow.open(map, marker);
                _infowindow = infowindow;
                _i = marker.index;
            });

            $('#marker'+i+" p.eye i").click(function() {
                if($(this).hasClass('icon-circle-blue')) {
                    if(_infowindow) _infowindow.close();
                    if(_i && $('#marker'+_i+" p.eye i").hasClass('icon-circle-rose'))  $('#marker'+_i+" p.eye i").removeClass('icon-circle-rose').addClass('icon-circle-blue');
                    $(this).removeClass('icon-circle-blue').addClass('icon-circle-rose');
                    infowindow.open(map, marker);
                    _infowindow = infowindow;
                    _i = marker.index;
                }
                else {
                    $(this).removeClass('icon-circle-rose').addClass('icon-circle-blue');
                    infowindow.close();
                }
            });
        });

        if(markers.length > 0) {
            // Add a marker clusterer to manage the markers.
                var markerCluster = new MarkerClusterer(map, markers,
                    {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
        }


        function contentWindow(id,lat,lng,dns,date,img) {
            var content = '<div class="p-2 text-center" style="color:black">'+
            '<h1 class="h5 bolder text-primary">'+dns+'</h1>'+
            '<p><small class="text-gray-600">Enregistré le '+date+'</small></p>'+
            '<p>'+
                '<small>lat:'+lat+' lng:'+lng+' </small>'+
            '</p>';
            if(img) {
                content += '<p><a href="'+img+'" data-fancybox="'+dns+'-map" data-caption="'+dns+'-map" style="display:inline-block">'+
                    '<img src="'+img+'" class="ml-1" style="max-width: 100%;border-radius: 4px;box-shadow: 0 2px 3px rgba(0,0,0,.2);cursor:zoom-in" alt="Photo enregistrée de la POI">'
                '</a>'+
            '</p>';
            }
            content += '<p><a href="'+url()+'pois/view/'+id+'" class="btn text-white btn-primary btn-sm">Point de vente</a></p>'+
            '</div>';
            return content;
        }
    });
}