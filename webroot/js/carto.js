function initMap2() {
    $(function () {
        var markers = [];
        var filtres = [];
        var _infowindow = null;
        var map;

        for(var i in pois) {
            let poi = pois[i];
            poi.checked = true;
            filtres.push(poi);
        }


        function initialize() {
            var lattitude = 14.7645;
            var longitude = -17.366;
    
            map = new google.maps.Map(
                document.getElementById('map'),
                {
                    center: {lat : lattitude, lng : longitude},
                    zoom: 8
                    // mapTypeId: google.maps.MapTypeId.ROADMAP
                }
            );
        }

        initialize();

        creationMarkeurs();

      function creationMarkeurs()
      {
          if(filtres.length > 0) {
              var bounds = new google.maps.LatLngBounds();
              for(var i in filtres){
                  var poi = filtres[i];
                  var marker;
                  var myLatLng = new google.maps.LatLng(poi.lat, poi.lng);
                  marker = new google.maps.Marker({
                    position: {lat: poi.lat,lng: poi.lng},
                    title: poi.dns,
                    icon: $('#icon').val()
                  });

                  marker.setMap(map);

                google.maps.event.addListener(
                    marker,
                    'click',
                    (function(marker,content,infowindow){
                    {
                        return function() {
                        if(_infowindow) _infowindow.close();
                        infowindow.setContent(content);
                        infowindow.open(map,marker);
                        _infowindow = infowindow;
                    };
                    }})(marker,contentWindow(poi),new google.maps.InfoWindow())
                );
    
                markers.push(marker);
                bounds.extend(myLatLng);
              }
    
                // if(markers.length > 0) {
                //     // Add a marker clusterer to manage the markers.
                //     var markerCluster = new MarkerClusterer(map, markers,
                //         {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
                // }
              map.fitBounds(bounds);
          }
          else{
              initialize();
          }
      }


        function ajoutPoi(poi) {
            filtres.push(poi);
            $('#filtres').text(filtres.length);
        }

        function removePoi(poi) {
            var index = filtres.indexOf(poi);
            if (index > -1) {
                var sup = filtres.splice(index, 1);
            }
            $('#filtres').text(filtres.length);
        }

        filterByTypePoi();
        filterByZone();

        function filterByTypePoi() {
            $.each($('input[name="type_poi"]'), function(i,item) {
                $(item).change(function() {
                    var id = $(this).data('id');
                    var changes = [];
                    if($(this).prop('checked') == true) {
                        for(let i in pois) {
                            let poi = pois[i];
                            if(poi.type_poi_id == id && !poi.checked) {
                                poi.checked = true;
                                changes.push(poi);
                            }
                        }
                    }
                    else {
                        for(let i in pois) {
                            let poi = pois[i];
                            if(poi.type_poi_id == id && poi.checked) {
                                poi.checked = false;
                                changes.push(poi);
                            }
                        }  
                    }
                    if(changes.length > 0) {
                        filtrage();
                        creationMarkeurs();
                    }
                });
            });
        }

        function filterByZone() {
            $.each($('input[name="zone_id"]'), function(i,item) {
                $(item).change(function() {
                    var id = $(this).data('id');
                    var changes = [];
                    if($(this).prop('checked') == true) {
                        for(let i in pois) {
                            let poi = pois[i];
                            if(poi.zone_id == id && !poi.checked) {
                                poi.checked = true;
                                changes.push(poi);
                            }
                        }
                    }
                    else {
                        for(let i in pois) {
                          let poi = pois[i];
                            if(poi.zone_id == id && poi.checked) {
                                poi.checked = false;
                                changes.push(poi);
                            }
                        }  
                    }
                    if(changes.length > 0) {
                        filtrage();
                        creationMarkeurs();
                    }
                });
            });
        }

        function filtrage() {
            filtres = [];
            for(let i in pois) {
                let poi = pois[i];
                if(poi.checked == true) {
                    ajoutPoi(poi);
                }
                else {
                    removePoi(poi);
                }
            }
        }



        $(".i-open").click(function() {
            $(this).addClass('hidden');
            $("#panel").removeClass('panel-filtre-close').addClass('panel-filtre-open');
        });
        $(".i-close").click(function() {
            $(".i-open").removeClass('hidden');
            $("#panel").removeClass('panel-filtre-open').addClass('panel-filtre-close');
        });

        function url() {
            return window.location.href.substr(0,(window.location.href.length - ('/pois').length))+'/img/pois/';
        }
        function contentWindow(poi) {
            var content = '<div class="p-2 text-center" style="color:black">'+
            '<h1 class="h5 bolder text-primary">'+poi.dns+'</h1>'+
            '<p><small class="text-gray-600">Enregistré le '+new Date(poi.created).toLocaleDateString()+'</small></p>'+
            '<p  class="text-gray-700">'+
                'Distributeur: '+poi.infos[0].value+
            '</p>'+
            '<p  class="text-gray-700">'+
                'Téléphone: '+poi.phone+
            '</p>';
            if(poi.photo) {
                content += '<p class="text-center"><a href="'+url()+poi.photo+'" data-fancybox="'+poi.dns+'-map" data-caption="'+poi.dns+'-map" style="display:inline-block">'+
                    '<img src="'+url()+poi.photo+'" class="ml-1" style="max-width: 100%;max-height: 150px;border-radius: 4px;box-shadow: 0 2px 3px rgba(0,0,0,.2);cursor:zoom-in" alt="Photo enregistrée de la POI">'
                '</a>'+
            '</p>';
            }
            content+='<p>'+
                '<small class="text-black-50">lat:'+poi.lat+' | lng:'+poi.lng+' </small>'+
            '</p>'+
            '<p><a href="view/'+poi.id+'" class="btn btn-primary text-white btn-sm">'+poi.type_pois.name+'</a></p>'+
            '</div>';
            return content;
        }
    });
}